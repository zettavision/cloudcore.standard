﻿using System.Collections.Generic;

namespace ZettaVision.CloudCore.Infrastructure.Images
{
    public static class ImageContentTypes
    {
        public static string Jpeg = "image/jpeg";
        public static string Png = "image/png";
        public static string Bmp = "image/bmp";
        public static string Gif = "image/gif";
        public static string Tiff = "image/tiff";
        public static string Icon = "image/x-icon";

        public static IEnumerable<string> All = new[] {Jpeg, Png, Bmp, Gif, Tiff, Icon};
    }
}
