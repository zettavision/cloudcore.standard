﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace ZettaVision.CloudCore.Infrastructure.Images
{
    public static class ImageExtenstions
    {
        public static Bitmap Resize(this Image sourceImage, int width, int height)
        {
            var bitmap = new Bitmap(width, height);

            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawImage(sourceImage, 0, 0, width, height);
            }

            return bitmap;
        }

        public static MemoryStream SaveToMemoryStream(this Image image, string contentType)
        {
            ImageFormat imageFormat = GetImageFormatFromContentType(contentType);

            var stream = new MemoryStream();
            image.Save(stream, imageFormat);
            stream.Position = 0;
            return stream;
        }

        private static ImageFormat GetImageFormatFromContentType(string contentType)
        {
            if(contentType == ImageContentTypes.Bmp)
                return ImageFormat.Bmp;

            if (contentType == ImageContentTypes.Gif)
                return ImageFormat.Gif;

            if (contentType == ImageContentTypes.Icon)
                return ImageFormat.Icon;

            if (contentType == ImageContentTypes.Jpeg)
                return ImageFormat.Jpeg;

            if (contentType == ImageContentTypes.Png)
                return ImageFormat.Png;

            if (contentType == ImageContentTypes.Tiff)
                return ImageFormat.Tiff;

            throw new Exception($"Unable to find a Image format match for content type {contentType}");
        }

        public static Size ResizeToFitInBoxSize(this Size sourceSize, Size boxSize)
        {
            return sourceSize.ResizeToFitInBoxSize(boxSize, true);
        }

        public static Size ResizeToFitInBoxSize(this Size sourceSize, Size boxSize, bool allowGrow)
        {
            if(sourceSize.Width < 1)
                throw new Exception("sourceSize.Width must be at least 1");

            if (sourceSize.Height < 1)
                throw new Exception("sourceSize.Height must be at least 1");

            if (boxSize.Width < 1)
                throw new Exception("boxSize.Width must be at least 1");

            if (boxSize.Height < 1)
                throw new Exception("boxSize.Height must be at least 1");

            var widthRatio = (double) boxSize.Width/(double) sourceSize.Width;
            var heightRatio = (double)boxSize.Height / (double)sourceSize.Height;

            var ratio = Math.Min(widthRatio, heightRatio);

            if (allowGrow == false && ratio > 1)
                ratio = 1;

            var doubleWidth = Math.Floor(ratio * sourceSize.Width);
            var doubleHeight = Math.Floor(ratio * sourceSize.Height);

            if (doubleWidth < 1)
                doubleWidth = 1;

            if (doubleHeight < 1)
                doubleHeight = 1;

            if (doubleWidth > boxSize.Width)
                doubleWidth = boxSize.Width;

            if (doubleHeight > boxSize.Height)
                doubleHeight = boxSize.Height;

            return new Size(Convert.ToInt32(doubleWidth), Convert.ToInt32(doubleHeight));
        }




    }
}