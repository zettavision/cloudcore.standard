﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using MimeTypes;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.Images
{
    public class ImageStreamModel : FileStreamModel
    {
        public ImageStreamModel()
        {
        }

        public ImageStreamModel(FileStreamModel fileStream)
        {

            ContentType = GetContentType(fileStream.ContentType, fileStream.FileName);
            FileName = fileStream.FileName;

            Stream = fileStream.Stream;
            Stream.Position = 0;
            Length = fileStream.Length;

            Size imageSize;
            using (var sourceBitmap = new Bitmap(fileStream.Stream))
            {
                if(sourceBitmap.Width < 1)
                    throw new Exception("Image width must be at least 1");

                if (sourceBitmap.Height < 1)
                    throw new Exception("Image height must be at least 1");

                var exifOrientationId = 0x112;
                if (sourceBitmap.PropertyIdList.Contains(exifOrientationId))
                {
                    var prop = sourceBitmap.GetPropertyItem(exifOrientationId);
                    var orient = BitConverter.ToInt16(prop.Value, 0);
                    var rotateFlipType = GetRotateFlipTypeFromExifOrientation(orient);
                    if (rotateFlipType != RotateFlipType.RotateNoneFlipNone)
                    {
                        // Force value to 1
                        prop.Value = BitConverter.GetBytes((short)1);
                        sourceBitmap.SetPropertyItem(prop);
                        sourceBitmap.RotateFlip(rotateFlipType);
                

                        Stream = sourceBitmap.SaveToMemoryStream(ContentType);
                        Stream.Position = 0;
                        Length = Stream.Length;
                    }

                }

                imageSize = new Size(sourceBitmap.Width, sourceBitmap.Height);
            }

            Width = imageSize.Width;
            Height = imageSize.Height;
        }

        private static RotateFlipType GetRotateFlipTypeFromExifOrientation(short orient)
        {
            switch (orient)
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                case 4:
                    return RotateFlipType.Rotate180FlipX;
                case 5:
                    return RotateFlipType.Rotate90FlipX;
                case 6:
                    return RotateFlipType.Rotate90FlipNone;
                case 7:
                    return RotateFlipType.Rotate270FlipX;
                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        public ImageStreamModel ChangeContentType(string contentType)
        {
            MemoryStream newStream;
            using (var sourceBitmap = new Bitmap(Stream))
            {
                newStream = sourceBitmap.SaveToMemoryStream(contentType);
            }

            Stream.Position = 0;

            var newImageStreamModel = new ImageStreamModel()
            {
                Stream = newStream,
                ContentType = contentType,
                FileName = FileName,
                Length = newStream.Length,
                Height = Height,
                Width = Width
            };

            return newImageStreamModel;
        }

        public ImageStreamModel FixOrientation()
        {
            return FixOrientation(ContentType);
        }

        public ImageStreamModel FixOrientation(string contentType)
        {
            MemoryStream newStream;
            Size imageSize;
            using (var sourceBitmap = new Bitmap(Stream))
            {
                var exifOrientationId = 0x112;

                if (!sourceBitmap.PropertyIdList.Contains(exifOrientationId))
                    return this;

                var prop = sourceBitmap.GetPropertyItem(exifOrientationId);
                var orient = BitConverter.ToInt16(prop.Value, 0);
                var rotateFlipType = GetRotateFlipTypeFromExifOrientation(orient);
                if (rotateFlipType == RotateFlipType.RotateNoneFlipNone)
                    return this;

                // Force value to 1
                prop.Value = BitConverter.GetBytes((short)1);
                sourceBitmap.SetPropertyItem(prop);
                sourceBitmap.RotateFlip(rotateFlipType);
                

                newStream = sourceBitmap.SaveToMemoryStream(contentType);
                imageSize = new Size(sourceBitmap.Width, sourceBitmap.Height);
            }

            Stream.Position = 0;

            var newImageStreamModel = new ImageStreamModel()
            {
                Stream = newStream,
                ContentType = contentType,
                FileName = FileName,
                Length = newStream.Length,
                Height = imageSize.Height,
                Width = imageSize.Width
            };

            return newImageStreamModel;
        }

        public ImageStreamModel Rotate(RotateFlipType rotateFlipType)
        {
            MemoryStream newStream;
            Size imageSize;
            using (var sourceBitmap = new Bitmap(Stream))
            {
                sourceBitmap.RotateFlip(rotateFlipType);

                newStream = sourceBitmap.SaveToMemoryStream(ContentType);
                imageSize = new Size(sourceBitmap.Width, sourceBitmap.Height);
            }

            Stream.Position = 0;

            var newImageStreamModel = new ImageStreamModel()
            {
                Stream = newStream,
                ContentType = ContentType,
                FileName = FileName,
                Length = newStream.Length,
                Height = imageSize.Height,
                Width = imageSize.Width
            };

            return newImageStreamModel;
        }



        public ImageStreamModel ResizeToFitInBoxSizeAndChangeContentType(int boxWidth, int boxHeight,
            string contentType)
        {
            return ResizeToFitInBoxSizeAndChangeContentType( boxWidth,  boxHeight,  contentType, true);
        }

        public ImageStreamModel ResizeToFitInBoxSizeAndChangeContentType(int boxWidth, int boxHeight, string contentType, bool allowGrow)
        {
            var boxSize = new Size(boxWidth, boxHeight);
            var currentSize = new Size(Width, Height);
            var newSize = currentSize.ResizeToFitInBoxSize(boxSize, allowGrow);

            return ResizeAndChangeContentType(newSize.Width, newSize.Height, contentType);
        }


        public ImageStreamModel ResizeToFitInBoxSize(int boxWidth, int boxHeight)
        {
            return ResizeToFitInBoxSize(boxWidth, boxHeight, true);
        }

        public ImageStreamModel ResizeToFitInBoxSize(int boxWidth, int boxHeight, bool allowGrow)
        {
            return ResizeToFitInBoxSizeAndChangeContentType(boxWidth, boxHeight, ContentType, allowGrow);
        }


        public ImageStreamModel Resize(int width, int height)
        {
            return ResizeAndChangeContentType(width, height, ContentType);
        }

        public ImageStreamModel ResizeAndChangeContentType(int width, int height, string contentType)
        {
            MemoryStream newStream;
            using (var sourceBitmap = new Bitmap(Stream))
            using(var newBitmap = sourceBitmap.Resize(width, height))
            {
                newStream = newBitmap.SaveToMemoryStream(contentType);
                
            }

            Stream.Position = 0;

            var newImageStreamModel = new ImageStreamModel()
            {
                Stream = newStream,
                ContentType = contentType,
                FileName = FileName,
                Length = newStream.Length,
                Height = height,
                Width = width
            };

            return newImageStreamModel;

        }

        private static string GetContentType(string contentType, string fileName)
        {
            var allowedTypes = new HashSet<string>(ImageContentTypes.All);

            if (allowedTypes.Contains(contentType))
                return contentType;

            var fileExtension = System.IO.Path.GetExtension(fileName).ToLowerInvariant();
            var newContentType = MimeTypeMap.GetMimeType(fileExtension);
          
            if(!allowedTypes.Contains(newContentType))
                throw new UnsupportedMediaTypeException($"Unable to find a valid image content type form provided content type {contentType} and file extension {fileExtension}");

            return newContentType;
        }

        public int Height { get; set; }
        public int Width { get; set; }

        public ImageStreamModel Crop(int top, int left, int width, int height)
        {
            return CropAndChangeContentType(top, left, width, height, ContentType);
        }

        public ImageStreamModel CropAndChangeContentType(int top, int left, int width, int height, string contentType)
        {
            //verify face is in image
            if(left >= Width)
                throw new InvalidOperationException("Crop rectange is not in the image");

            if(left + width > Width)
                throw new InvalidOperationException("Crop rectange is not in the image");

            if(top >= Height)
                throw new InvalidOperationException("Crop rectange is not in the image");

            if(top + height > Height)
                throw new InvalidOperationException("Crop rectange is not in the image");


            var cropRect = new Rectangle(left, top, width, height);

            var targetBitmap = new Bitmap(cropRect.Width, cropRect.Height);

            MemoryStream newStream;
            using (var sourceBitmap = new Bitmap(Stream))
            using(var g = Graphics.FromImage(targetBitmap))
            {
                g.DrawImage(sourceBitmap, new Rectangle(0, 0, targetBitmap.Width, targetBitmap.Height), 
                    cropRect,                        
                    GraphicsUnit.Pixel);

                newStream = targetBitmap.SaveToMemoryStream(contentType);
            }

            Stream.Position = 0;

            var newImageStreamModel = new ImageStreamModel()
            {
                Stream = newStream,
                ContentType = contentType,
                FileName = FileName,
                Length = newStream.Length,
                Height = height,
                Width = width
            };

            return newImageStreamModel;

        }
    }
}