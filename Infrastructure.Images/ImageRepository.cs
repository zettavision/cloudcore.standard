﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MimeTypes;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.Images
{
    public class ImageRepository 
    {
        private readonly INondestructiveBlobRepository nondestructiveBlobRepository;
        private readonly HashSet<string> validContentTypes;

        public ImageRepository(INondestructiveBlobRepository nondestructiveBlobRepository, IEnumerable<string> validContentTypes)
        {
            this.nondestructiveBlobRepository = nondestructiveBlobRepository;
            this.validContentTypes = new HashSet<string>(validContentTypes);
        }

        public async Task<ImageReferenceModel> InsertAsync(string blobNameWithoutFileExtension, ImageStreamModel imageStream,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!validContentTypes.Contains(imageStream.ContentType))
                throw new InvalidOperationException($"Invalid content type {imageStream.ContentType}.  Valid content types include {string.Join(",", validContentTypes)}");

            var fileExtention = MimeTypeMap.GetExtension(imageStream.ContentType);

            var blobName = $"{blobNameWithoutFileExtension}{fileExtention}".ToLowerInvariant();

            var url = await nondestructiveBlobRepository
                .InsertAsync(blobName, imageStream.Stream, null, cancellationToken).ConfigureAwait(false);

            imageStream.Stream.Position = 0;
            var length = imageStream.Stream.Length;
            imageStream.Stream.Position = 0;
            var md5Hash = GetMd5Hash(imageStream.Stream);

            return new ImageReferenceModel()
            {
                ContentType = imageStream.ContentType,
                Length = length,
                Md5Hash =  md5Hash,
                Url = url,
                Width = imageStream.Width,
                Height = imageStream.Height
            };
        }

        private static string GetMd5Hash(Stream stream)
        {
            
            using (var md5 = MD5.Create())
            {
                var hashBytes = md5.ComputeHash(stream);
                return Convert.ToBase64String(hashBytes);
            }

        }

        public async Task<ImageReferenceModel> InsertWithRandomGuidNameAsync(string blobNamePrefix, ImageStreamModel imageStream,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var blobNameWithoutFileExtension = string.IsNullOrEmpty(blobNamePrefix)  
                ? $"{GuidId.GetNewId()}" 
                : $"{blobNamePrefix}/{GuidId.GetNewId()}";

            return await InsertAsync(blobNameWithoutFileExtension, imageStream, cancellationToken).ConfigureAwait(false);
        }


    }
}
