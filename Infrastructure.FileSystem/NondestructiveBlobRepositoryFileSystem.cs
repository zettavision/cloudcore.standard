﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;

namespace Infrastructure.FileSystem
{
    public class NondestructiveBlobRepositoryFileSystem : INondestructiveBlobRepository
    {
        private readonly string cdnPrefix;
        private readonly string rootDirectory;

        public NondestructiveBlobRepositoryFileSystem(string cdnPrefix, string rootDirectory)
        {
            if (cdnPrefix != null && cdnPrefix.EndsWith("/"))
                cdnPrefix = this.cdnPrefix.Substring(0, cdnPrefix.Length - 1);

            this.cdnPrefix = cdnPrefix;
            this.rootDirectory = rootDirectory;

            if (!System.IO.Directory.Exists(rootDirectory))
                Directory.CreateDirectory(rootDirectory);
        }

        private string GetUrl(string blobName)
        {
             return $"{cdnPrefix}/{blobName}";
        }

        private string GetLocalPath(string blobName)
        {
            return Path.Combine(rootDirectory, blobName.Replace("/", "\\"));
        }

        public async Task<string> InsertAsync(string blobName, Stream stream, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
   
            stream.Position = 0;
            var bytes = ReadFully(stream);

            return await InsertAsync(blobName, bytes, contentType, cancellationToken).ConfigureAwait(false);

        }

        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }



        public  Task<string> InsertAsync(string blobName, byte[] bytes, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var localPath = GetLocalPath(blobName);
            var localDirectory = Path.GetDirectoryName(localPath);
            Directory.CreateDirectory(localDirectory);

            if (File.Exists(localPath))
                throw new ObjectAlreadyExistsException();

            File.WriteAllBytes(localPath, bytes);

            var url = GetUrl(blobName);

            cancellationToken.ThrowIfCancellationRequested();
            return Task.FromResult(url);

        }
    }
}