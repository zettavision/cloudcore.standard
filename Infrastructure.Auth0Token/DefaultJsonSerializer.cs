﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JWT;
using Newtonsoft.Json;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token
{
    public class DefaultJsonSerializer : IJsonSerializer
    {
        public DefaultJsonSerializer()
        {
            
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
