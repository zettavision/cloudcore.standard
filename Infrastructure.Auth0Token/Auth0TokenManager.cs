﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.ValueObjects;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token
{
    public class Auth0TokenManager : IAuth0TokenManager
    {
        private readonly string domain;
        private readonly string clientId;
        private readonly string clientSecret;
        private readonly bool isClientSecretBase64;
        private readonly string name;

        public Auth0TokenManager(string name, string domain, string clientId, string clientSecret, bool isClientSecretBase64 = true)
        {
            this.domain = domain;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.isClientSecretBase64 = isClientSecretBase64;
            this.name = name;


        }

        public void ValidateTokenIssuerSigningKey(string token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            if (token.ToLowerInvariant().StartsWith("bearer "))
                token = token.Substring(7);

            TokenValidationParameters validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(GetSecretKeyAsBytes()),

                ValidateAudience = false,
                ValidateActor = false,
                ValidateIssuer = false,
                ValidateLifetime = false,
                ValidateTokenReplay = false
            };

            // Now validate the token. If the token is not valid for any reason, an exception will be thrown by the method
            SecurityToken validatedToken;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            try
            {
                handler.ValidateToken(token, validationParameters, out validatedToken);
            }
            catch (Exception ex)
            { 
                throw new UnauthorizedAccessException(ex.Message, ex);
            }
        }

        public string BuildToken(IEnumerable<string> roles, string userId = null, DateTime? expirationDate = null)
        {
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            var payload = GetPayload(roles, userId, expirationDate);
            var secret = GetSecretKeyAsBytes();

            var token = encoder.Encode(payload, secret);
            return token;
        }

        private byte[] GetSecretKeyAsBytes()
        {
            byte[] secretKeyAsBytes;

            if (isClientSecretBase64)
                secretKeyAsBytes = Convert.FromBase64String(CleanClientSecret());
            else
                secretKeyAsBytes = Encoding.ASCII.GetBytes(clientSecret);

            return secretKeyAsBytes;
        }

        private string CleanClientSecret()
        {
            return clientSecret.Replace('-', '+').Replace('_', '/');
        }

        private object GetPayload(IEnumerable<string> roles, string userId, DateTime? expirationDate)
        {
            DateTimeOffset? expirationDateTimeOffset = null;
            if (expirationDate != null)
                expirationDateTimeOffset = new DateTimeOffset(expirationDate.Value);

            var payloadName = name;

            var payload = new
            {
                _id = userId,
                name = payloadName,
                roles = roles.Select(x => x.ToString()),
                user_id = userId,
                iss = $"https://{domain}/",
                sub = userId,
                aud = clientId,
                exp = expirationDateTimeOffset == null ? 253402300799 : expirationDateTimeOffset.Value.ToUnixTimeSeconds(),
                iat = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
            };

            return payload;
        }

        public Auth0TokenPayloadValueObject GetPayload(string token)
        {
            return JwtPayloadReader.GetPayload(token);
        }

    }
}