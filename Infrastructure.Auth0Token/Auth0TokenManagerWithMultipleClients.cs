﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JWT;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token
{
    public class Auth0TokenManagerWithMultipleClients : IAuth0TokenManager
    {
        private readonly IAuth0TokenManager mainTokenManager;
        private readonly IEnumerable<IAuth0TokenManager> additionalTokenManagers;

        public Auth0TokenManagerWithMultipleClients(IAuth0TokenManager mainTokenManager, 
            IEnumerable<IAuth0TokenManager> additionalTokenManagers)
        {
            this.mainTokenManager = mainTokenManager ?? throw new ArgumentNullException(nameof(mainTokenManager));
            this.additionalTokenManagers = additionalTokenManagers;
        }

        public string BuildToken(IEnumerable<string> roles, string userId = null, DateTime? expirationDate = null)
        {
            return mainTokenManager.BuildToken(roles, userId, expirationDate);
        }

        public Auth0TokenPayloadValueObject GetPayload(string token)
        {
            if (token == null)
                return null;

            var tokenManagers = new List<IAuth0TokenManager>();
            tokenManagers.Add(mainTokenManager);

            if(additionalTokenManagers != null && additionalTokenManagers.Any())
                tokenManagers.AddRange(additionalTokenManagers);

            SignatureVerificationException firstSignatureVerificationException = null;

            foreach (var tokenManger in tokenManagers)
            {
                try
                {
                    return tokenManger.GetPayload(token);
                }
                catch (SignatureVerificationException ex)
                {
                    if (firstSignatureVerificationException == null)
                        firstSignatureVerificationException = ex;
                }
            }

            throw firstSignatureVerificationException;
        }

        public void ValidateTokenIssuerSigningKey(string token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            var tokenManagers = new List<IAuth0TokenManager>();
            tokenManagers.Add(mainTokenManager);

            if (additionalTokenManagers != null && additionalTokenManagers.Any())
                tokenManagers.AddRange(additionalTokenManagers);

            UnauthorizedAccessException firstUnauthorizedAccessException = null;

            foreach (var tokenManger in tokenManagers)
            {
                try
                {
                    tokenManger.ValidateTokenIssuerSigningKey(token);
                    return;
                }
                catch (UnauthorizedAccessException ex)
                {
                    if (firstUnauthorizedAccessException == null)
                        firstUnauthorizedAccessException = ex;
                }
            }

            throw firstUnauthorizedAccessException;
        }
    }
}