using System.Linq;
using JWT;
using JWT.Builder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token
{
    public class Auth0TokenDecoder : IAuth0TokenDecoder
    {
        public string Decode(string token)
        {
            if (token == null)
                return null;

            if (token.ToLowerInvariant().StartsWith("bearer "))
                token = token.Substring(7);

            var json = new JwtBuilder()
                .Decode(token); 

 
            return json;
        }

        public Auth0TokenPayloadValueObject GetPayload(string token)
        {
            return JwtPayloadReader.GetPayload(token);
        }
    }
}