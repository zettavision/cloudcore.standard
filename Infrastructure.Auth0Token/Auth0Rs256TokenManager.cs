﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token
{
    public class Auth0Rs256TokenManager : IAuth0TokenManager
    {
        private readonly string clientSigningCertificate;

        public Auth0Rs256TokenManager(string clientSigningCertificate)
        {
            this.clientSigningCertificate = clientSigningCertificate;
        }

        public string BuildToken(IEnumerable<string> roles, string userId = null, DateTime? expirationDate = null)
        {
            throw new NotImplementedException();
        }

        public Auth0TokenPayloadValueObject GetPayload(string token)
        {
            return JwtPayloadReader.GetPayload(token);
        }

        public void ValidateTokenIssuerSigningKey(string token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            if (token.ToLowerInvariant().StartsWith("bearer "))
                token = token.Substring(7);

            var certificate = new X509Certificate2(Convert.FromBase64String(clientSigningCertificate));

            var validationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                ValidateAudience = false,
                ValidateActor = false,
                ValidateIssuer = false,
                ValidateLifetime = false,
                ValidateTokenReplay = false,
                IssuerSigningKey = new X509SecurityKey(certificate)
            };

            var handler = new JwtSecurityTokenHandler();

            try
            {
                handler.ValidateToken(token, validationParameters, out var validatedToken);
            }
            catch(Exception ex)
            {
                throw new UnauthorizedAccessException(ex.Message, ex);
            }
        }

    }
}