﻿using System;
using System.Collections.Generic;
using System.Linq;
using JWT.Builder;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token
{
    public static class JwtPayloadReader
    {
        public static Auth0TokenPayloadValueObject GetPayload(string token)
        {
            if (token == null)
                return null;

            if (token.ToLowerInvariant().StartsWith("bearer "))
                token = token.Substring(7);

            var json = new JwtBuilder()
                .Decode(token);

            var payload = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(json);

            var userId = GetUserId(payload);
            var roles = GetRoles(payload);
            var expirationDate = GetExpirationDate(payload);
            return new Auth0TokenPayloadValueObject(userId, roles, expirationDate, json);
        }

        public static IEnumerable<string> GetRoles(JObject payload)
        {
            if (!payload.HasValues)
                return null;

            IEnumerable<string> roles = null;

            var rolesProperty = payload.Properties().SingleOrDefault(p => p.Name == "roles");
            if (rolesProperty != null)
                roles = ((JArray)rolesProperty.Value)?.Select(jo => ((JValue)jo).Value as string).ToList();

            if (roles == null || !roles.Any())
            {
                var zettaVisionRolesProperty = payload.Properties()
                    .SingleOrDefault(p => p.Name == "https://www.zettavision.com/roles");

                if (zettaVisionRolesProperty != null)
                {
                    roles = ((JArray)zettaVisionRolesProperty.Value)?.Select(jo => ((JValue)jo).Value as string).ToList();
                }
            }
            return roles;
        }

        public static string GetUserId(JObject payload)
        {
            if (!payload.HasValues)
                return null;

            string userId = null;
            var userIdProperty = payload.Properties().SingleOrDefault(p => p.Name == "user_id");
            if (userIdProperty != null)
                userId = ((JValue)userIdProperty.Value)?.Value as string;

            if (string.IsNullOrEmpty(userId))
            {
                var subProperty = payload.Properties().SingleOrDefault(p => p.Name == "sub");
                if (subProperty != null)
                    userId = ((JValue)subProperty.Value)?.Value as string;
            }
            return userId;
        }

        public static DateTime? GetExpirationDate(JObject payload)
        {
            if (!payload.HasValues)
                return null;

            long? unixDateTimeSeconds = null;
            var expProperty = payload.Properties().SingleOrDefault(p => p.Name == "exp");
            if (expProperty != null && expProperty.Value.Type != JTokenType.Null)
                unixDateTimeSeconds = (JValue)expProperty.Value != null ? (long)((JValue)expProperty.Value) : 0;

            if (unixDateTimeSeconds == null)
                return null;
            else
            {
                var expiration = DateTimeOffset.FromUnixTimeSeconds(unixDateTimeSeconds.Value).DateTime;
                return expiration;
            }
        }
    }
}