﻿using System;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ZettaVision.CloudCore.WebApi.ApplicationInsights
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AiErrorHandleAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext?.HttpContext != null && filterContext.Exception != null)
            {
#pragma warning disable CS0618 // Type or member is obsolete
                var ai = new TelemetryClient();
#pragma warning restore CS0618 // Type or member is obsolete
                ai.TrackException(filterContext.Exception);
            }
            base.OnException(filterContext);
        }
    }
}
