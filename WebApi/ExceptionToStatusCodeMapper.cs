﻿using System;
using System.Net;
using System.Security.Authentication;
using ZettaVision.CloudCore.Domain.Exceptions;

namespace ZettaVision.CloudCore.WebApi
{
    public static class ExceptionToStatusCodeMapper
    {
        public static HttpStatusCode GetStatusCodeForException(Exception exception)
        {
            if (exception is ArgumentException)
                return HttpStatusCode.BadRequest;
            if(exception is FormatException)
                return HttpStatusCode.BadRequest;
            if (exception is ObjectAlreadyExistsException)
                return HttpStatusCode.Conflict;
            if (exception is OptimisticConcurrencyException)
                return HttpStatusCode.PreconditionFailed;
            if (exception is ObjectNotFoundException)
                return HttpStatusCode.NotFound;
            if (exception is UnauthorizedAccessException)
                return HttpStatusCode.Forbidden;
            if (exception is InvalidCredentialException)
                return HttpStatusCode.Unauthorized;
            if (exception is InvalidOperationException)
                return (HttpStatusCode)422;//Unprocessable Entity
            if (exception is UnsupportedMediaTypeException)
                return HttpStatusCode.UnsupportedMediaType;
            if (exception is PaymentRequiredException)
                return HttpStatusCode.PaymentRequired;
            return HttpStatusCode.InternalServerError;
        }
    }
}
