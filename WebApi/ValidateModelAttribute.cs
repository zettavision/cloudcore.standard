﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ZettaVision.CloudCore.WebApi
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid == false)
            {

                var errors = context.ModelState.Values
                    .SelectMany(v => v.Errors);

                var jsonErrors = new List<object>();

                foreach (var error in errors)
                {
                    dynamic jsonError = new ExpandoObject();
                    jsonError.Message = error.ErrorMessage;

                    if (error.Exception != null)
                    {
                        dynamic jsonException = new ExpandoObject();

                        jsonException.Message = error.Exception.Message;
                        jsonException.Type = error.Exception.GetType().Name;

                        jsonError.Exception = jsonException;
                    }
                    

                    jsonErrors.Add(jsonError);
                }




                context.Result = new JsonResult(jsonErrors)
                {
                    StatusCode = (int) HttpStatusCode.BadRequest
                };
            }
            else
            {
                base.OnActionExecuting(context);
            }
        }
    }
}