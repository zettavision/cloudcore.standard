﻿using Microsoft.AspNetCore.Http;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.WebApi
{
    public static class HttpRequestExtenstions
    {
        public static string GetUserId(this HttpRequest request, IAuth0TokenManager auth0TokenManager)
        {
            if (!request.Headers.ContainsKey("Authorization"))
                return null;
            var authorizationHeader = request.Headers["Authorization"];

            return null;
        }

        public static string GetETag(this HttpRequest request)
        {
            return request.GetSigularHeader("If-Match");
        }

        public static string GetSigularHeader(this HttpRequest request, string headerName)
        {
            if (!request.Headers.ContainsKey(headerName))
                return null;
            var value = request.Headers[headerName];

            if (value.Count != 1)
                return null;

            if (string.IsNullOrEmpty(value[0]))
                return null;

            return value[0];
        }

        public static PageParametersModel GetPageParameters(this HttpRequest request)
        {
            var pageParametersModel = new PageParametersModel();

            var continuationToken = request.GetSigularHeader("continuation-token");
            var maxItemCountString = request.GetSigularHeader("max-item-count");

            pageParametersModel.ContinuationToken = continuationToken;
            pageParametersModel.MaxItemCount = maxItemCountString == null ? (int?)null : int.Parse(maxItemCountString);

            return pageParametersModel;
        }


    }
}