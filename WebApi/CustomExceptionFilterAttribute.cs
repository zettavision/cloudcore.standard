﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ZettaVision.CloudCore.WebApi
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly bool isDevelopment;

        public CustomExceptionFilterAttribute(bool isDevelopment)
        {
            this.isDevelopment = isDevelopment;
        }

        private IDictionary<string, object> GetData(IDictionary exceptionData)
        {
            if (exceptionData == null || exceptionData.Count == 0)
                return null;

            var dictionary = new Dictionary<string, object>();
            foreach (var key in exceptionData.Keys)
            {
                // ReSharper disable once UseNullPropagation
                if(key == null)
                    continue;

                if(!(key is string))
                    continue;

                dictionary.Add((string)key, exceptionData[key]);
            }

            if (dictionary.Count == 0)
                return null;

            return dictionary;

        }

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            dynamic body = new ExpandoObject();
            
            body.Message = exception.Message;
            body.Data = GetData(exception.Data);
            body.Type = exception.GetType().FullName;

            if (isDevelopment)
            {
                body.StackTrace = exception.StackTrace;
                if (exception.InnerException != null)
                    body.InnerException = exception.InnerException.ToString();
            }

            var statusCode = (int) ExceptionToStatusCodeMapper.GetStatusCodeForException(exception);

            context.Result = new JsonResult(body)
            {
                StatusCode = statusCode
            };
        }
    

    }
}
