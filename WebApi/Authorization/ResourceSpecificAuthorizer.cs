﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.WebApi.Authorization
{
    public abstract class ResourceSpecificAuthorizer
    {
        private readonly IAuth0TokenDecoder auth0TokenDecoder;
        private readonly IAuth0TokenManager auth0TokenManager;

        protected ResourceSpecificAuthorizer(IAuth0TokenDecoder auth0TokenDecoder)
        {
            this.auth0TokenDecoder = auth0TokenDecoder;
        }

        protected ResourceSpecificAuthorizer(IAuth0TokenManager auth0TokenManager)
        {
            this.auth0TokenManager = auth0TokenManager;
        }

        


        public string UserId { get; protected set; }

        public IEnumerable<string> Roles { get; protected set; }

        public bool IsAuthorized { get; protected set; }
        public DateTime? ExpirationDate { get; protected set; }

        //wrap this method in the derived classes
        public void SetToken(string token)
        {
            if(string.IsNullOrEmpty(token))
                throw new UnauthorizedAccessException();

            IsAuthorized = false;

            if (token.ToLowerInvariant().StartsWith("Bearer "))
                token = token.Substring(7);

            Auth0TokenPayloadValueObject payload = null;

            if (auth0TokenDecoder != null)
            {
                payload = auth0TokenDecoder.GetPayload(token);
            }
            else if (auth0TokenManager != null)
            {
                payload = auth0TokenManager.GetPayload(token);
                auth0TokenManager.ValidateTokenIssuerSigningKey(token);

                if (payload.ExpirationDate != null
                    && payload.ExpirationDate.Value < DateTime.UtcNow)
                {
                    throw new UnauthorizedAccessException($"Token has exipred. Expiration Date ({payload.ExpirationDate.Value})");
                }
            }


            if (payload == null)
                throw new Exception("Payload cannot be null");

            UserId = payload.UserId;
        
            Roles = payload.Roles;

            ExpirationDate = payload.ExpirationDate;

            if (ExpirationDate.HasValue)
            {
                if (ExpirationDate.Value < DateTime.UtcNow)
                {
                    throw new UnauthorizedAccessException("Token has expired");
                }
            }
        }

        //wrap this method in the derived classes
        protected void HasGlobalRoles(IEnumerable<string> globalRoles)
        {
            if (Roles != null && globalRoles != null && Roles.Intersect(globalRoles).Any())
                IsAuthorized = true;
        }

        public void Authorize()
        {
            if (IsAuthorized == false)
            {
                throw new UnauthorizedAccessException();
            }
        }
    }
}
