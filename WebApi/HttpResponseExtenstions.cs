﻿using Microsoft.AspNetCore.Http;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.WebApi
{
    public static class HttpResponseExtenstions
    {
        public static void AddContinuationTokenHeader(this HttpResponse response, string continuationToken)
        {
            //if(continuationToken != null)
                response.Headers.Add("continuation-token", continuationToken);
        }

        public static void AddETagHeader(this HttpResponse response, IAggregateModel aggregateModel)
        {
             response.Headers.Add("ETag", aggregateModel.ETag);
        }
    }
}
