﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregatePurgerAzureBlob<T> : IAggregatePurger
                where T : AggregateIdentifier
    {
        private readonly AggregateAzureBlobHelper<T> aggregateAzureBlobHelper;

        public AggregatePurgerAzureBlob(string connectionString, string containerName = null,
            Func<string, string> idPropertyFormatter = null, Func<string, string> partitionPathFormatterFunc = null)
        {
            aggregateAzureBlobHelper = new AggregateAzureBlobHelper<T>(connectionString, containerName,
                idPropertyFormatter, partitionPathFormatterFunc);
        }

        public AggregatePurgerAzureBlob(BlobServiceClient blobServiceClient, string containerName = null,
            Func<string, string> idPropertyFormatter = null, Func<string, string> partitionPathFormatterFunc = null)
        {
            aggregateAzureBlobHelper = new AggregateAzureBlobHelper<T>(blobServiceClient, containerName,
                idPropertyFormatter, partitionPathFormatterFunc);
        }

        public async Task<long> PurgeAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {

            long deletedCount = 0;
            string blobContinuationToken = null;
            int maxItemCount = 100;

            var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                Azure.Storage.Blobs.Models.BlobStates.None,
                null, cancellationToken)
                .AsPages(blobContinuationToken, maxItemCount);

            await foreach (var blobPage in resultSegment)
            {
                foreach (BlobItem blobItem in blobPage.Values)
                {
                    var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(blobItem.Name);
                    await blobClient.DeleteAsync(DeleteSnapshotsOption.None, null, cancellationToken).ConfigureAwait(false);
                    deletedCount++;
                }
                blobContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
            }

            return deletedCount;
        }        
    }
}
