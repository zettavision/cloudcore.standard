﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregateReaderAzureBlob<T, T2> : IAggregateGetByIdReader<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        private readonly AggregateAzureBlobHelper<T> aggregateAzureBlobHelper;

        private readonly DocumentToAggregateModelMapper<T2> documentToAggregateModelMapper;

        public AggregateReaderAzureBlob(string connectionString, string containerName = null,
            Func<string, string> idPropertyFormatter = null,
            Func<string, string> partitionPathFormatterFunc = null,
            IEnumerable<JsonConverter> jsonConverters = null,
            Func<string, string> reverseIdPropertyFormatter = null,
            Func<string, string> reversePartitionPathFormatterFunc = null)
        {
            aggregateAzureBlobHelper = new AggregateAzureBlobHelper<T>(
                connectionString,
                containerName,
                idPropertyFormatter,
                partitionPathFormatterFunc,
                reverseIdPropertyFormatter,
               reversePartitionPathFormatterFunc);

            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T2>(jsonConverters);
        }

        public AggregateReaderAzureBlob(BlobServiceClient blobServiceClient, string containerName = null,
            Func<string, string> idPropertyFormatter = null,
            Func<string, string> partitionPathFormatterFunc = null,
            IEnumerable<JsonConverter> jsonConverters = null,
            Func<string, string> reverseIdPropertyFormatter = null,
            Func<string, string> reversePartitionPathFormatterFunc = null)
        {
            aggregateAzureBlobHelper = new AggregateAzureBlobHelper<T>(
                blobServiceClient,
                containerName,
                idPropertyFormatter,
                partitionPathFormatterFunc,
                reverseIdPropertyFormatter,
               reversePartitionPathFormatterFunc);

            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T2>(jsonConverters);
        }

        public async Task<bool> ExistsAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            var path = aggregateAzureBlobHelper.GetPath(aggregateIdentifier);
            var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(path);

            return await blobClient.ExistsAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<T2> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            var path = aggregateAzureBlobHelper.GetPath(aggregateIdentifier);
            var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(path);

            return await GetWithBlobClientAsync(blobClient, FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier), cancellationToken).ConfigureAwait(false);
        }

        public async Task<T2> GetWithBlobClientAsync(BlobClient blobClient, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await GetWithBlobClientAsync(blobClient, null, cancellationToken).ConfigureAwait(false);
        }

        private async Task<T2> GetWithBlobClientAsync(BlobClient blobClient, string fullyQualifiedIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var blobDownloadInfo = await blobClient.DownloadAsync(cancellationToken).ConfigureAwait(false);
                string json;

                using (var stream = new MemoryStream())
                {
                    await blobDownloadInfo.Value.Content.CopyToAsync(stream);
                    stream.Close();
                    json = Encoding.UTF8.GetString(stream.ToArray());
                }
                return documentToAggregateModelMapper.Map(json, blobClient.GetProperties().Value.ETag.ToString());

            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;

                if (statusCode != (int)HttpStatusCode.NotFound) throw;

                var errorMessage = string.IsNullOrEmpty(fullyQualifiedIdentifier) ? $"Object Not Found ({blobClient.Name})" : $"Object Not Found ({fullyQualifiedIdentifier})";
                throw new ObjectNotFoundException(errorMessage, ex);

            }
        }

        public async Task<PagedResult<T2>> GetAllWithBlobLastModifiedAsync(
            PageParametersModel pageParameters = null,
            DateTime? blobModifiedOnOrAfter = null,
            DateTime? blobModifiedBefore = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var maxItemCount = pageParameters?.MaxItemCount;
            string blobContinuationToken = null;
            if (!string.IsNullOrEmpty(pageParameters?.ContinuationToken))
                blobContinuationToken = pageParameters.ContinuationToken;

            var items = new List<T2>();
            var pagedResult = new PagedResult<T2>();

            try
            {
                var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                    Azure.Storage.Blobs.Models.BlobStates.None,
                    null, cancellationToken)
                    .AsPages(blobContinuationToken, maxItemCount);

                await foreach (var blobPage in resultSegment)
                {
                    foreach (BlobItem blobItem in blobPage.Values)
                    {
                        if (blobModifiedOnOrAfter != null && blobItem.Properties.LastModified != null)
                        {
                            if (blobItem.Properties.LastModified.Value.DateTime < blobModifiedOnOrAfter.Value)
                                continue;
                        }
                        else if (blobModifiedBefore != null && blobItem.Properties.LastModified != null)
                        {
                            if (blobItem.Properties.LastModified.Value.DateTime >= blobModifiedBefore.Value)
                                continue;
                        }

                        var model = await GetWithBlobClientAsync(aggregateAzureBlobHelper.Container.GetBlobClient(blobItem.Name), cancellationToken).ConfigureAwait(false);
                        items.Add(model);
                    }

                    pagedResult.ContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
                    break;
                }
            }
            catch (RequestFailedException exception)
            {
                if (exception.Status == 400)
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            pagedResult.Items = items;

            return pagedResult;
        }

        public async Task<PagedResult<T2>> GetAllByPartitionWithBlobLastModifiedAsync(
            string partitionPrefixPath,
            PageParametersModel pageParameters = null,
            DateTime? blobModifiedOnOrAfter = null,
            DateTime? blobModifiedBefore = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var maxItemCount = pageParameters?.MaxItemCount;
            string blobContinuationToken = null;
            if (!string.IsNullOrEmpty(pageParameters?.ContinuationToken))
                blobContinuationToken = pageParameters.ContinuationToken;

            var partitionPrefixPathWithEndingSlash = partitionPrefixPath == null
                ? null
                : $"{partitionPrefixPath.Trim('/')}/";


            var items = new List<T2>();
            var pagedResult = new PagedResult<T2>();

            try
            {
                var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                    Azure.Storage.Blobs.Models.BlobStates.None,
                    partitionPrefixPathWithEndingSlash, cancellationToken)
                    .AsPages(blobContinuationToken, maxItemCount);

                await foreach (var blobPage in resultSegment)
                {
                    foreach (BlobItem blobItem in blobPage.Values)
                    {
                        if (blobModifiedOnOrAfter != null && blobItem.Properties.LastModified != null)
                        {
                            if (blobItem.Properties.LastModified.Value.DateTime < blobModifiedOnOrAfter.Value)
                                continue;
                        }
                        else if (blobModifiedBefore != null && blobItem.Properties.LastModified != null)
                        {
                            if (blobItem.Properties.LastModified.Value.DateTime >= blobModifiedBefore.Value)
                                continue;
                        }

                        var model = await GetWithBlobClientAsync(aggregateAzureBlobHelper.Container.GetBlobClient(blobItem.Name), cancellationToken).ConfigureAwait(false);
                        items.Add(model);
                    }

                    pagedResult.ContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
                    break;
                }
            }
            catch (RequestFailedException exception)
            {
                if (exception.Status == 400)
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            pagedResult.Items = items;

            return pagedResult;
        }

        public async Task<PagedResult<T2>> GetAllAsync(PageParametersModel pageParameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var maxItemCount = pageParameters?.MaxItemCount;
            var blobContinuationToken = pageParameters?.ContinuationToken;

            var items = new List<T2>();
            var pagedResult = new PagedResult<T2>();

            try
            {
                var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                    Azure.Storage.Blobs.Models.BlobStates.None,
                    null, cancellationToken)
                    .AsPages(blobContinuationToken, maxItemCount);

                await foreach (var blobPage in resultSegment)
                {
                    foreach (BlobItem blobItem in blobPage.Values)
                    {
                        var model = await GetWithBlobClientAsync(aggregateAzureBlobHelper.Container.GetBlobClient(blobItem.Name), cancellationToken).ConfigureAwait(false);
                        items.Add(model);
                    }
                    pagedResult.ContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
                    break;
                }
            }
            catch (RequestFailedException exception)
            {
                if (exception.Status == 400)
                    throw new ArgumentException(exception.Message, exception);
                
                throw;
            }

            pagedResult.Items = items;

            return pagedResult;
        }


        public string GetPartitionPrefixPath(T aggregateIdentifier)
        {
            return aggregateAzureBlobHelper.GetPartitionPath(aggregateIdentifier);
        }

        public string GetPartitionPrefixPathFromParentId(string parentId)
        {
            return aggregateAzureBlobHelper.GetPartitionPathFromParentId(parentId);
        }



        public async Task<PagedResult<T2>> GetAllByPartitionAsync(string partitionPrefixPath,
            PageParametersModel pageParameters, CancellationToken cancellationToken = default(CancellationToken))
        {
            var maxItemCount = pageParameters?.MaxItemCount;
            var blobContinuationToken = pageParameters?.ContinuationToken;

            var partitionPrefixPathWithEndingSlash = partitionPrefixPath == null
                ? null
                : $"{partitionPrefixPath.Trim('/')}/";

            var items = new List<T2>();
            var pagedResult = new PagedResult<T2>();

            try
            {
                var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                    Azure.Storage.Blobs.Models.BlobStates.None,
                    partitionPrefixPathWithEndingSlash, cancellationToken)
                    .AsPages(blobContinuationToken, maxItemCount);

                await foreach (var blobPage in resultSegment)
                {
                    foreach (BlobItem blobItem in blobPage.Values)
                    {
                        var model = await GetWithBlobClientAsync(aggregateAzureBlobHelper.Container.GetBlobClient(blobItem.Name), cancellationToken).ConfigureAwait(false);
                        items.Add(model);
                    }
                    pagedResult.ContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
                    break;
                }
            }
            catch (RequestFailedException exception)
            {
                if (exception.Status == 400)
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            pagedResult.Items = items;

            return pagedResult;
        }

        public async Task<PagedResult<AggregateBlobSummary<T>>> GetAllSummariesAsync(PageParametersModel pageParameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var maxItemCount = pageParameters?.MaxItemCount;
            var blobContinuationToken = pageParameters?.ContinuationToken;

            var items = new List<AggregateBlobSummary<T>>();
            var pagedResult = new PagedResult<AggregateBlobSummary<T>>();

            try
            {
                var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                    Azure.Storage.Blobs.Models.BlobStates.None,
                    null, cancellationToken)
                    .AsPages(blobContinuationToken, maxItemCount);

                await foreach (var blobPage in resultSegment)
                {
                    foreach (BlobItem blobItem in blobPage.Values)
                    {
                        var model = GetAggregateBlobSummary(blobItem);
                        items.Add(model);
                    }
                    pagedResult.ContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
                    break;
                }
            }
            catch (RequestFailedException exception)
            {
                if (exception.Status == 400)
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            pagedResult.Items = items;

            return pagedResult;

        }

        public async Task<PagedResult<AggregateBlobSummary<T>>> GetAllSummariesByPartitionAsync(
            string partitionPrefixPath, PageParametersModel pageParameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var maxItemCount = pageParameters?.MaxItemCount;
            var blobContinuationToken = pageParameters?.ContinuationToken;

            var partitionPrefixPathWithEndingSlash = partitionPrefixPath == null
                ? null
                : $"{partitionPrefixPath.Trim('/')}/";

            var items = new List<AggregateBlobSummary<T>>();
            var pagedResult = new PagedResult<AggregateBlobSummary<T>>();

            try
            {
                var resultSegment = aggregateAzureBlobHelper.Container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                    Azure.Storage.Blobs.Models.BlobStates.None,
                    partitionPrefixPathWithEndingSlash, cancellationToken)
                    .AsPages(blobContinuationToken, maxItemCount);

                //var resultSegment = await aggregateAzureBlobHelper.Container.ListBlobsSegmentedAsync(partitionPrefixPathWithEndingSlash, false,
                //    BlobListingDetails.None, maxItemCount, blobContinuationToken, null, null, cancellationToken).ConfigureAwait(false);



                await foreach (var blobPage in resultSegment)
                {
                    foreach (BlobItem blobItem in blobPage.Values)
                    {
                        var model = GetAggregateBlobSummary(blobItem);
                        items.Add(model);
                    }
                    pagedResult.ContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
                    break;
                }
            }
            catch (RequestFailedException exception)
            {
                if (exception.Status == 400)
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            pagedResult.Items = items;

            return pagedResult;
        }

        private AggregateBlobSummary<T> GetAggregateBlobSummary(BlobItem blob)
        {
            var summary = new AggregateBlobSummary<T>();
            summary.Identifier = aggregateAzureBlobHelper.GetIdentifierFromPath(blob.Name);
            summary.Identifier.ETag = blob.Properties.ETag.ToString();
            summary.BlobProperties = GetBlobProperties(blob.Properties);

            return summary;
        }

        private AggregateBlobSummaryProperties GetBlobProperties(BlobItemProperties blobItemProperties)
        {
            var summaryProperties = new AggregateBlobSummaryProperties();
            summaryProperties.Length = blobItemProperties.ContentLength.Value;
            summaryProperties.ContentMd5 = Convert.ToBase64String(blobItemProperties.ContentHash);
            summaryProperties.ContentType = blobItemProperties.ContentType;
            summaryProperties.LastModified = blobItemProperties.LastModified?.DateTime;

            return summaryProperties;
        }
    }
}