﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class DocumentToAggregateModelMapper<T> where T : IAggregateModel
    {
        private readonly JsonSerializer serializer;

        public DocumentToAggregateModelMapper( IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            serializer = JsonSerializer.Create(jsonSerializerSettings);
        }

        public T Map(string documentJson, string eTag)
        {
            var documentJObject = JObject.Parse(documentJson);
            var aggregateInfoJson = documentJObject["AggregateInfo"].ToString();
            var aggregateJson = documentJObject["Aggregate"].ToString();

;
            var aggregateJObject = JObject.Parse(aggregateJson);
            var aggregateModel = aggregateJObject.ToObject<T>(serializer);


            var aggregateInfoJObject = JObject.Parse(aggregateInfoJson);
            var aggregateInfoModel = aggregateInfoJObject.ToObject<AggregateInfoModel>(serializer);

            aggregateModel.ETag = eTag;
            aggregateModel.AggregateInfo = aggregateInfoModel;

            return aggregateModel;
        }
    }
}
