﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;
using ZettaVision.CloudCore.Infrastructure.AzureBlob;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregateRepositoryAzureBlob<T, T2, T3> : IAggregateRepository<T, T2, T3>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {
        private readonly AggreateToAggregateIdentifierMapper<T, T2> aggreateToAggregateIdentifierMapper;
        private readonly AggregateAzureBlobHelper<T> aggregateAzureBlobHelper;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly DocumentToAggregateModelMapper<T3> documentToAggregateModelMapper;
        private readonly string aggregatePluralName;

        public AggregateRepositoryAzureBlob(string connectionString, string containerName = null,
            Func<string, string> idPropertyFormatter = null, Func<string, string> partitionPathFormatterFunc = null, IEnumerable<JsonConverter> jsonConverters = null)
        {
            aggregateAzureBlobHelper = new AggregateAzureBlobHelper<T>(connectionString, containerName,
                idPropertyFormatter, partitionPathFormatterFunc);
            aggreateToAggregateIdentifierMapper = new AggreateToAggregateIdentifierMapper<T, T2>();

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T3>(jsonConverters);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }

        public AggregateRepositoryAzureBlob(BlobServiceClient blobServiceClient, string containerName = null,
            Func<string, string> idPropertyFormatter = null, Func<string, string> partitionPathFormatterFunc = null, IEnumerable<JsonConverter> jsonConverters = null)
        {
            aggregateAzureBlobHelper = new AggregateAzureBlobHelper<T>(blobServiceClient, containerName,
                idPropertyFormatter, partitionPathFormatterFunc);
            aggreateToAggregateIdentifierMapper = new AggreateToAggregateIdentifierMapper<T, T2>();

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T3>(jsonConverters);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }

        public async Task<T3> InsertAsync(T2 aggregate, string userId, DateTime? createDate = null, string lastModifiedByUserId = null, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregate);
                var path = aggregateAzureBlobHelper.GetPath(aggregateIdentifier);

                var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(path);

                if (createDate == null)
                    createDate = DateTime.UtcNow;

                var aggregateInfo = new AggregateInfoValueObject(userId, createDate.Value);

                if (lastModifiedDate != null)
                    aggregateInfo = aggregateInfo.MarkModified(lastModifiedByUserId, lastModifiedDate.Value);

                var aggregateWithAggregateInfo = new AggregateWithAggregateInfoAzureBlob<T2>
                {
                    Aggregate = aggregate,
                    AggregateInfo = aggregateInfo
                };

                var json = JsonConvert.SerializeObject(aggregateWithAggregateInfo, jsonSerializerSettings);

                var blobUploadOptions = new BlobUploadOptions()
                {
                    Conditions = new BlobRequestConditions() { IfNoneMatch = ETag.All }
                };

                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    await blobClient.UploadAsync(ms, blobUploadOptions, cancellationToken).ConfigureAwait(false);
                }

                return documentToAggregateModelMapper.Map(json, blobClient.GetProperties().Value.ETag.ToString());
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;

                if (statusCode == (int)HttpStatusCode.Conflict) 
                    throw new ObjectAlreadyExistsException($"Object Already Exists ({FullyQualifiedIdentifier.GetIdentifier(aggregate)})", ex)
                    .AddToData("Error", "RepositoryObjectAlreadyExists")
                    .AddToData("AggregatePluralName", aggregatePluralName);

                if (statusCode == (int)HttpStatusCode.BadRequest)
                    throw new ArgumentException(ex.Message, ex)
                    .AddToData("Error", "RepositoryArgument")
                    .AddToData("AggregatePluralName", aggregatePluralName);

                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(FormatException))
                    throw new ArgumentException(ex.Message, ex)
                    .AddToData("Error", "RepositoryArgument")
                    .AddToData("AggregatePluralName", aggregatePluralName);


                throw;
            }
        }

        public async Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var path = aggregateAzureBlobHelper.GetPath(aggregateIdentifier);
                var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(path);

                BlobRequestConditions blobRequestConditions = null;
                if (!string.IsNullOrEmpty(aggregateIdentifier.ETag))
                {
                    blobRequestConditions = new BlobRequestConditions() { IfMatch = new ETag(aggregateIdentifier.ETag) };
                }

                var blobDownloadInfo = await blobClient.DownloadAsync(default, blobRequestConditions, false, cancellationToken).ConfigureAwait(false);

                string json;

                using (var stream = new MemoryStream())
                {
                    await blobDownloadInfo.Value.Content.CopyToAsync(stream);
                    stream.Close();
                    json = Encoding.UTF8.GetString(stream.ToArray());
                }

                var aggreateWithMetadata = JsonConvert.DeserializeObject<AggregateWithMetadata<T2>>(json,
                    jsonSerializerSettings);
                aggreateWithMetadata.ETag = blobClient.GetProperties().Value.ETag.ToString();

                return aggreateWithMetadata;
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.PreconditionFailed)
                    throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})", ex)
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName);

                if (statusCode == (int)HttpStatusCode.NotFound)
                    throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})", ex)
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);

                if (statusCode == (int)HttpStatusCode.BadRequest)
                    throw new ArgumentException(ex.Message, ex)
                    .AddToData("Error", "RepositoryArgument")
                    .AddToData("AggregatePluralName", aggregatePluralName);

                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(FormatException))
                    throw new ArgumentException(ex.Message, ex)
                    .AddToData("Error", "RepositoryArgument")
                    .AddToData("AggregatePluralName", aggregatePluralName);

                throw;
            }
        }

        public async Task<T3> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId,
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregateWithMetadata.Aggregate);
            var path = aggregateAzureBlobHelper.GetPath(aggregateIdentifier);
            var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(path);

            try
            {
                if (lastModifiedDate == null)
                    lastModifiedDate = DateTime.UtcNow;

                var aggregateInfo = aggregateWithMetadata.AggregateInfo.MarkModified(userId, lastModifiedDate.Value);

                var aggregateWithAggregateInfo = new AggregateWithAggregateInfoAzureBlob<T2>
                {
                    Aggregate = aggregateWithMetadata.Aggregate,
                    AggregateInfo = aggregateInfo
                };

                var json = JsonConvert.SerializeObject(aggregateWithAggregateInfo, jsonSerializerSettings);

                var blobUploadOptions = new BlobUploadOptions()
                {
                    Conditions = new BlobRequestConditions() { IfMatch = new ETag(aggregateWithMetadata.ETag) }
                };

                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    await blobClient.UploadAsync(ms, blobUploadOptions, cancellationToken).ConfigureAwait(false);
                }

                return documentToAggregateModelMapper.Map(json, blobClient.GetProperties().Value.ETag.ToString());
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.PreconditionFailed)
                {
                    if (await blobClient.ExistsAsync(cancellationToken).ConfigureAwait(false) == false)
                    {
                        throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})", ex)
                            .AddToData("Error", "RepositoryObjectNotFound")
                            .AddToData("AggregatePluralName", aggregatePluralName); 
                    }
                    throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})", ex)
                        .AddToData("Error", "RepositoryOptimisticConcurrency")
                        .AddToData("AggregatePluralName", aggregatePluralName);
                }

                if (statusCode == (int)HttpStatusCode.BadRequest)
                    throw new ArgumentException(ex.Message, ex)
                        .AddToData("Error", "RepositoryArgument")
                        .AddToData("AggregatePluralName", aggregatePluralName); 

                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(FormatException))
                    throw new ArgumentException(ex.Message, ex)
                        .AddToData("Error", "RepositoryArgument")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                throw;
            }
        }

        public async Task DeleteAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var path = aggregateAzureBlobHelper.GetPath(aggregateIdentifier);
                var blobClient = aggregateAzureBlobHelper.Container.GetBlobClient(path);

                BlobRequestConditions blobRequestConditions = new BlobRequestConditions() { IfMatch = new ETag(aggregateIdentifier.ETag) };

                await
                    blobClient.DeleteAsync(DeleteSnapshotsOption.None, blobRequestConditions, cancellationToken).ConfigureAwait(false);
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.PreconditionFailed)
                    throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})", ex)
                        .AddToData("Error", "RepositoryOptimisticConcurrency")
                        .AddToData("AggregatePluralName", aggregatePluralName); 

                if (statusCode == (int)HttpStatusCode.NotFound)
                    throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})", ex)
                        .AddToData("Error", "RepositoryObjectNotFound")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                if (statusCode == (int)HttpStatusCode.BadRequest)
                    throw new ArgumentException(ex.Message, ex)
                        .AddToData("Error", "RepositoryArgument")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(FormatException))
                    throw new ArgumentException(ex.Message, ex)
                        .AddToData("Error", "RepositoryArgument")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                throw;
            }
        }
    }
}