﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Azure.Storage.Blobs;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregateAzureBlobHelper<T>
        where T : AggregateIdentifier
    {
        private readonly Func<string, string> idPropertyFormatter;
        private readonly Func<string, string> partitionPathFormatterFunc;

        private readonly Func<string, string> reverseIdPropertyFormatter;
        private readonly Func<string, string> reversePartitionPathFormatterFunc;

        public AggregateAzureBlobHelper(
            string connectionString, 
            string containerName = null,
            Func<string, string> idPropertyFormatter = null, 
            Func<string, string> partitionPathFormatterFunc = null,
            Func<string, string> reverseIdPropertyFormatter = null,
            Func<string, string> reversePartitionPathFormatterFunc = null)
        {

            if (reverseIdPropertyFormatter != null && idPropertyFormatter == null)
                throw new InvalidOperationException("If reverseIdPropertyFormatter is used idPropertyFormatter cannot be null");

            if (reversePartitionPathFormatterFunc != null && partitionPathFormatterFunc == null)
                throw new InvalidOperationException("If reversePartitionPathFormatterFunc is used partitionPathFormatterFunc cannot be null");


            this.idPropertyFormatter = idPropertyFormatter;
            this.partitionPathFormatterFunc = partitionPathFormatterFunc;

            this.reverseIdPropertyFormatter = reverseIdPropertyFormatter;
            this.reversePartitionPathFormatterFunc = reversePartitionPathFormatterFunc;

            containerName = containerName ?? GetContainerName();
            Container = new BlobContainerClient(connectionString, containerName);

        }

        public AggregateAzureBlobHelper(
            BlobServiceClient blobServiceClient,
            string containerName = null,
            Func<string, string> idPropertyFormatter = null,
            Func<string, string> partitionPathFormatterFunc = null,
            Func<string, string> reverseIdPropertyFormatter = null,
            Func<string, string> reversePartitionPathFormatterFunc = null)
        {

            if (reverseIdPropertyFormatter != null && idPropertyFormatter == null)
                throw new InvalidOperationException("If reverseIdPropertyFormatter is used idPropertyFormatter cannot be null");

            if (reversePartitionPathFormatterFunc != null && partitionPathFormatterFunc == null)
                throw new InvalidOperationException("If reversePartitionPathFormatterFunc is used partitionPathFormatterFunc cannot be null");


            this.idPropertyFormatter = idPropertyFormatter;
            this.partitionPathFormatterFunc = partitionPathFormatterFunc;

            this.reverseIdPropertyFormatter = reverseIdPropertyFormatter;
            this.reversePartitionPathFormatterFunc = reversePartitionPathFormatterFunc;

            containerName = containerName ?? GetContainerName();
            Container = blobServiceClient.GetBlobContainerClient(containerName);

        }

        private static string GetContainerName()
        {
            var idIdentifierPluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
            var containerName = idIdentifierPluralName.ToLowerInvariant();

            if (containerName.Length > 63)
                containerName = containerName.Substring(0, 63);

            if (containerName.Length < 3)
                containerName = containerName.PadRight(3, 'x');

            return containerName.ToLowerInvariant();
        }

        public BlobContainerClient Container { get; private set; }


        public string GetPath(T aggregateIdentifier)
        {
            var identifierList = new List<string>();

            var idProperty = aggregateIdentifier.GetType().GetProperties()
                .First(p => p.Name == "Id");

            var value = idProperty.GetValue(aggregateIdentifier) as string;
            var formatedIdValue = idPropertyFormatter == null ? value : idPropertyFormatter(value);
            var partitionPath = GetPartitionPath(aggregateIdentifier);

            if (string.IsNullOrEmpty(partitionPath))
                return formatedIdValue;
            else
                return $"{partitionPath}/{formatedIdValue}";
        }

        public T GetIdentifierFromPath(string path)
        {
            var pathParts = path.Split('/');

            if(pathParts.Length < 1)
                throw new InvalidOperationException("Path must have at least one value");

            var partitionPath = pathParts.Length == 1
                ? null 
                :string.Join("/",pathParts.Take(pathParts.Length -1).ToArray());

            var id = pathParts.Last();


            var propertyNames = new List<string>();

            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    if(identifierAttribute == null)
                        continue;

                    if (property.Name == "Id")
                        continue;

                    propertyNames.Add(property.Name);
                }
            }

            T identifier = (T) Activator.CreateInstance(typeof(T));

            id = reverseIdPropertyFormatter != null 
                ? reverseIdPropertyFormatter(id) 
                : pathParts.Last();

            var idPropertyInfo = typeof(T).GetProperty("Id");
            idPropertyInfo.SetValue(identifier, id, null);

            if (partitionPath == null)
                return identifier;

            if (reversePartitionPathFormatterFunc != null)
            {
                partitionPath = reversePartitionPathFormatterFunc(partitionPath);
            }

            var partitionParts = partitionPath.Split('/');

            if (partitionParts.Length != propertyNames.Count)
                throw new InvalidOperationException("The number of parts in the path don't match the number of identifiers in the aggregate");


            var index = 0;
            foreach (var propertyName in propertyNames)
            {
                var propertyInfo = typeof(T).GetProperty(propertyName);
                propertyInfo.SetValue(identifier, partitionParts[index], null);

                index++;
            }

            return identifier;

        }

        public string GetPartitionPath(T aggregateIdentifier)
        {
            var identifierList = new List<string>();

            var properties = aggregateIdentifier.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    if (property.Name == "Id")
                        continue;

                    var value = property.GetValue(aggregateIdentifier) as string;

                    identifierList.Add($"{value}");
                }
            }

            var simplePartitionPath = !identifierList.Any() ? string.Empty : string.Join("/", identifierList);

            if (partitionPathFormatterFunc != null)
                return partitionPathFormatterFunc(simplePartitionPath);
            else
                return simplePartitionPath;
        }

        public string GetPartitionPathFromParentId(string parentId)
        {
            if (string.IsNullOrEmpty(parentId))
                parentId = string.Empty;

            return parentId.Replace(".", "/");
        }

    }
}