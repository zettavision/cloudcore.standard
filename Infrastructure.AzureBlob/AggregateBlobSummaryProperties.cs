﻿using System;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregateBlobSummaryProperties
    {
        public long Length { get; internal set; }
        public string ContentMd5 { get; set; }
        public string ContentType { get; set; }
        public DateTime? LastModified { get;  set; }

    }
}