﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregateSnapshotRepositoryAzureBlob<T,T2> : IAggregateSnapshotRepository<T,T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {

        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly BlobContainerClient container;

        public AggregateSnapshotRepositoryAzureBlob(string connectionString, string containerName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            container = new BlobContainerClient(connectionString, containerName);

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }
        }

        public AggregateSnapshotRepositoryAzureBlob(BlobServiceClient blobServiceClient, string containerName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            container = blobServiceClient.GetBlobContainerClient(containerName);

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }
        }

        private static string GetPath(T aggregateIdentifier, int version)
        {
            var identifierList = new List<string>();

            var properties = aggregateIdentifier.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    var value = property.GetValue(aggregateIdentifier) as string;

                    identifierList.Add($"{value}");
                }
            }

            identifierList.Add(version.ToString());

            return string.Join("/", identifierList);

        }

        public async Task InsertAsync(T aggregateIdentifier, T2 aggregateModel, CancellationToken cancellationToken = default(CancellationToken))
        {
            var path = GetPath(aggregateIdentifier, aggregateModel.AggregateInfo.Version);
            var blobClient = container.GetBlobClient(path);

            var json = JsonConvert.SerializeObject(aggregateModel, jsonSerializerSettings);

            var blobUploadOptions = new BlobUploadOptions()
            {
                Conditions = new BlobRequestConditions() { IfNoneMatch = ETag.All }
            };

            try
            {
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    await blobClient.UploadAsync(ms, blobUploadOptions, cancellationToken).ConfigureAwait(false);
                }
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.Conflict)
                    throw new ObjectAlreadyExistsException(null, ex);

                throw;
            }
            
        }

        public async Task UpdateAsync(T aggregateIdentifier, T2 aggregateModel, CancellationToken cancellationToken = default(CancellationToken))
        {
            var path = GetPath(aggregateIdentifier, aggregateModel.AggregateInfo.Version);
            var blobClient = container.GetBlobClient(path);

            var exist = await blobClient.ExistsAsync(cancellationToken).ConfigureAwait(false);

            if(exist == false)
                throw new ObjectNotFoundException();


            var json = JsonConvert.SerializeObject(aggregateModel, jsonSerializerSettings);

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                await blobClient.UploadAsync(ms, cancellationToken).ConfigureAwait(false);
            }
        }

        public async Task<T2> LoadAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken))
        {
            var path = GetPath(aggregateIdentifier, version);
            var blobClient = container.GetBlobClient(path);

            string json;
            try
            {
                var blobDownloadInfo = await blobClient.DownloadAsync(default, null, false, cancellationToken).ConfigureAwait(false);

                using (var stream = new MemoryStream())
                {
                    await blobDownloadInfo.Value.Content.CopyToAsync(stream);
                    stream.Close();
                    json = Encoding.UTF8.GetString(stream.ToArray());
                }
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.NotFound)
                    throw new ObjectNotFoundException(null, ex);

                throw;
            }
            

            var model = JsonConvert.DeserializeObject<T2>(json,
                jsonSerializerSettings);

            return model;

        }
    }
}
