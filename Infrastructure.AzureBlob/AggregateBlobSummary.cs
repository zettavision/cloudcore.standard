﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class AggregateBlobSummary<T> where T : AggregateIdentifier
    {
        public T Identifier { get; set; }
        public AggregateBlobSummaryProperties BlobProperties { get; set; }

    }
}
