﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Infrastructure.AggregateEvent;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.AggregateEvent
{
    public class AggregateEventRepositoryAzureBlob : IAggregateEventRepository
    {
        private readonly AggregateRepositoryAzureBlob<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel> aggregateRepository;
        private readonly AggregateEventModelGenericToAggregateWithMetadataMapper aggregateEventModelGenericToAggregateWithMetadataMapper;
        private readonly AggregateReaderAzureBlob<AggregateEventIdentifier, AggregateEventModel> aggregateReader;

        public AggregateEventRepositoryAzureBlob(string connectionString, string containerName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateRepository = new AggregateRepositoryAzureBlob<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>
                (connectionString, containerName, KeyFormatter.PadForInt32, null, jsonConverters);


            aggregateReader = new AggregateReaderAzureBlob<AggregateEventIdentifier, AggregateEventModel>(connectionString, containerName,
                KeyFormatter.PadForInt32, null, jsonConverters);

            aggregateEventModelGenericToAggregateWithMetadataMapper = new AggregateEventModelGenericToAggregateWithMetadataMapper();
        }

        public AggregateEventRepositoryAzureBlob(BlobServiceClient blobServiceClient, string containerName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateRepository = new AggregateRepositoryAzureBlob<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>
                (blobServiceClient, containerName, KeyFormatter.PadForInt32, null, jsonConverters);


            aggregateReader = new AggregateReaderAzureBlob<AggregateEventIdentifier, AggregateEventModel>(blobServiceClient, containerName,
                KeyFormatter.PadForInt32, null, jsonConverters);

            aggregateEventModelGenericToAggregateWithMetadataMapper = new AggregateEventModelGenericToAggregateWithMetadataMapper();
        }

        public async Task<AggregateEventModel> InsertAsync(AggregateEventAggregate aggregate, string userId, DateTime? createDate = null,
            string lastModifiedByUserId = null, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.InsertAsync(aggregate, userId, createDate, lastModifiedByUserId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        public async Task<AggregateWithMetadata<AggregateEventAggregate>> LoadAsync(AggregateEventIdentifier aggregateIdentifier,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var model = await aggregateReader.GetByIdAsync(aggregateIdentifier).ConfigureAwait(false);

            if (aggregateIdentifier.ETag != null && aggregateIdentifier.ETag != model.ETag)
                throw new OptimisticConcurrencyException($"Unable to load '{model.AggregateId}.{model.Id}' etag does not match");

            return aggregateEventModelGenericToAggregateWithMetadataMapper.Map(model);
        }

        public async Task<AggregateEventModel> UpdateAsync(AggregateWithMetadata<AggregateEventAggregate> aggregateWithMetadata, 
            string userId, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.UpdateAsync(aggregateWithMetadata, userId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        public async Task DeleteAsync(AggregateEventIdentifier aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            await aggregateRepository.DeleteAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }
    }
}
