﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Infrastructure.AggregateEvent;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.AggregateEvent
{
    public class AggregateEventReaderAzureBlob : IAggregateEventReader
    {
        private readonly AggregateReaderAzureBlob<AggregateEventIdentifier, AggregateEventModel> aggregateReader;

        public AggregateEventReaderAzureBlob(string connectionString, string containerName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateReader = new AggregateReaderAzureBlob<AggregateEventIdentifier, AggregateEventModel>(connectionString, containerName,
                KeyFormatter.PadForInt32, null, jsonConverters);
        }

        public AggregateEventReaderAzureBlob(BlobServiceClient blobServiceClient, string containerName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateReader = new AggregateReaderAzureBlob<AggregateEventIdentifier, AggregateEventModel>(blobServiceClient, containerName,
                KeyFormatter.PadForInt32, null, jsonConverters);
        }

        public async Task<PagedResult<AggregateEventModel>> HandleAsync(AggregateEventGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {

            var partitionPrefixPath = aggregateReader.GetPartitionPrefixPathFromParentId(query.Identifier);

            var result = await
                aggregateReader.GetAllByPartitionAsync(partitionPrefixPath, query.PageParameters, cancellationToken)
                    .ConfigureAwait(false);

            if (result?.Items?.Any() == true)
            {
                foreach (var item in result.Items)
                {
                    AggregateEventExtendedPropertyCalculator.Calculate(item);
                }
            }

            return result;
        }

        public async Task<AggregateEventModel> HandleAsync(AggregateEventGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await
                aggregateReader.GetByIdAsync(new AggregateEventIdentifier(query.AggregateId, query.Id), cancellationToken)
                    .ConfigureAwait(false);

            AggregateEventExtendedPropertyCalculator.Calculate(result);

            return result;
        }
    }
}
