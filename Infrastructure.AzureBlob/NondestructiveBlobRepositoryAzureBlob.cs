﻿using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class NondestructiveBlobRepositoryAzureBlob : INondestructiveBlobRepository
    {
        private readonly string cdnPrefix;
        private readonly BlobContainerClient container;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="containerName"></param>
        /// <param name="cdnPrefix">Must contain the containerName if used in the cdn path</param>
        public NondestructiveBlobRepositoryAzureBlob(string connectionString, string containerName, string cdnPrefix = null)
        {
            if (cdnPrefix != null && cdnPrefix.EndsWith("/"))
                this.cdnPrefix = cdnPrefix.Substring(0, cdnPrefix.Length - 1);
            else
                this.cdnPrefix = cdnPrefix;

            container = new BlobContainerClient(connectionString, containerName);
        }

        public NondestructiveBlobRepositoryAzureBlob(BlobServiceClient blobServiceClient, string containerName, string cdnPrefix = null)
        {
            if (cdnPrefix != null && cdnPrefix.EndsWith("/"))
                this.cdnPrefix = cdnPrefix.Substring(0, cdnPrefix.Length - 1);
            else
                this.cdnPrefix = cdnPrefix;

            container = blobServiceClient.GetBlobContainerClient(containerName);
        }

        private string GetUrl(BlobClient blobClient)
        {
            if (cdnPrefix == null)
                return blobClient.Uri.AbsoluteUri.Replace("https://", "http://");
            else
                return $"{cdnPrefix}/{blobClient.Name}";
        }

        public async Task<string> InsertAsync(string blobName, Stream stream, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                stream.Position = 0;

                var blobClient = container.GetBlobClient(blobName);

                var blobUploadOptions = new BlobUploadOptions()
                {
                    Conditions = new BlobRequestConditions() { IfNoneMatch = ETag.All }
                };

                if (contentType != null) 
                {
                    blobUploadOptions.HttpHeaders = new BlobHttpHeaders() { ContentType = contentType };
                }

                await blobClient.UploadAsync(stream, blobUploadOptions, cancellationToken)
                    .ConfigureAwait(false);

                return GetUrl(blobClient);
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.Conflict)
                    throw new ObjectAlreadyExistsException(null, ex);

                throw;
            }

        }

        

        public async Task<string> InsertAsync(string blobName, byte[] bytes, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var blobClient = container.GetBlobClient(blobName);
                var blobUploadOptions = new BlobUploadOptions()
                {
                    Conditions = new BlobRequestConditions() { IfNoneMatch = ETag.All }
                };

                if (contentType != null)
                {
                    blobUploadOptions.HttpHeaders = new BlobHttpHeaders() { ContentType = contentType };
                }

                MemoryStream stream = new MemoryStream(bytes);
                await blobClient.UploadAsync(stream, blobUploadOptions, cancellationToken)
                    .ConfigureAwait(false);

                return GetUrl(blobClient);
            }
            catch (RequestFailedException ex)
            {
                var statusCode = ex.Status;
                if (statusCode == (int)HttpStatusCode.Conflict)
                    throw new ObjectAlreadyExistsException(null, ex);

                throw;
            }

        }
    }
}