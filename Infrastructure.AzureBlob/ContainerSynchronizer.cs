﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob
{
    public class ContainerSynchronizer
    {
        private readonly BlobContainerClient sourceContainer;
        private readonly BlobContainerClient destinationContainer;

        public ContainerSynchronizer(string sourceConnectionString, string sourceContainerName,
            string destinationConnectionString, string destinationContainerName)
        {
            sourceContainer = new BlobContainerClient(sourceConnectionString, sourceContainerName);
            destinationContainer = new BlobContainerClient(destinationConnectionString, destinationContainerName);
        }

        public ContainerSynchronizer(BlobServiceClient sourceBlobServiceClient, string sourceContainerName,
            BlobServiceClient destinationBlobServiceClient, string destinationContainerName)
        {
            sourceContainer = sourceBlobServiceClient.GetBlobContainerClient(sourceContainerName);
            destinationContainer = sourceBlobServiceClient.GetBlobContainerClient(destinationContainerName);
        }

        /// <summary>
        /// This will copy all blobs from one container to another.  It will overwrite existing blobs but will not delete blobs
        /// </summary>
        /// <returns></returns>
        public async Task RunAsync()
        {
            await destinationContainer.CreateIfNotExistsAsync().ConfigureAwait(false);

            var sourceBlobs = await ListAllBlobsAsync(sourceContainer).ConfigureAwait(false);
            var destinationBlobs = await ListAllBlobsAsync(destinationContainer).ConfigureAwait(false);
            var destinationBlobDictionary = destinationBlobs.ToDictionary(b => b.Name, b => b);

            var blobsToMove = new List<BlobItem>();
            foreach (var sourceBlob in sourceBlobs)
            {
                if (!destinationBlobDictionary.ContainsKey(sourceBlob.Name))
                    blobsToMove.Add(sourceBlob);
                else
                {
                    var destinationBlob = destinationBlobDictionary[sourceBlob.Name];
                    if (!sourceBlob.Properties.ContentHash.SequenceEqual(destinationBlob.Properties.ContentHash))
                    {
                        blobsToMove.Add(sourceBlob);
                    }
                }
            }

            foreach (var blobToMove in blobsToMove)
            {
                BlobClient destinationBlob = destinationContainer.GetBlobClient(blobToMove.Name);
                await destinationBlob.StartCopyFromUriAsync(sourceContainer.GetBlobClient(blobToMove.Name).Uri).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// This will copy all blobs from one container to another and delete any blobs that don't exist in the source
        /// </summary>
        /// <returns></returns>
        public async Task MirrorAsync()
        {
            await destinationContainer.CreateIfNotExistsAsync().ConfigureAwait(false);

            var sourceBlobs = await ListAllBlobsAsync(sourceContainer).ConfigureAwait(false);
            var sourceBlobDictionary = sourceBlobs.ToDictionary(b => b.Name, b => b);

            var destinationBlobs = await ListAllBlobsAsync(destinationContainer).ConfigureAwait(false);
            var destinationBlobDictionary = destinationBlobs.ToDictionary(b => b.Name, b => b);
            

            var blobsToMove = new List<BlobItem>();
            foreach (var sourceBlob in sourceBlobs)
            {
                if (!destinationBlobDictionary.ContainsKey(sourceBlob.Name))
                    blobsToMove.Add(sourceBlob);
                else
                {
                    var destinationBlob = destinationBlobDictionary[sourceBlob.Name];
                    if (!sourceBlob.Properties.ContentHash.SequenceEqual(destinationBlob.Properties.ContentHash))
                    {
                        blobsToMove.Add(sourceBlob);
                    }
                }
            }

            var blobsToDelete = new List<BlobItem>();
            foreach (var destinationBlob in destinationBlobs)
            {
                if (!sourceBlobDictionary.ContainsKey(destinationBlob.Name))
                    blobsToDelete.Add(destinationBlob);
            }

            foreach (var blobToMove in blobsToMove)
            {
                BlobClient destinationBlob = destinationContainer.GetBlobClient(blobToMove.Name);
                await destinationBlob.StartCopyFromUriAsync(sourceContainer.GetBlobClient(blobToMove.Name).Uri).ConfigureAwait(false);
            }

            foreach (var blobToDelete in blobsToDelete)
            {
                await destinationContainer.GetBlobClient(blobToDelete.Name).DeleteAsync().ConfigureAwait(false);
            }
        }



        private async Task<IEnumerable<BlobItem>> ListAllBlobsAsync(BlobContainerClient container)
        {
            var maxItemCount = 100;
            string blobContinuationToken = null;

            var resultSegment = container.GetBlobsAsync(Azure.Storage.Blobs.Models.BlobTraits.None,
                Azure.Storage.Blobs.Models.BlobStates.None,
                null, default)
                .AsPages(blobContinuationToken, maxItemCount);

            var items = new List<BlobItem>();

            await foreach (var blobPage in resultSegment)
            {
                foreach (BlobItem blobItem in blobPage.Values)
                {
                    items.Add(blobItem);
                }
                blobContinuationToken = string.IsNullOrEmpty(blobPage.ContinuationToken) ? null : blobPage.ContinuationToken;
            }
            return items;
        }
    }
}
