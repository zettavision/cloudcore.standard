using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ZettaVision.CloudCore.WebApi.Swashbuckle
{
    public class PageHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var controllerActionDescriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;

            var name = controllerActionDescriptor.ActionName;

            if (name.StartsWith("GetAll"))
            {
                if (operation.Parameters == null)
                    operation.Parameters = new List<OpenApiParameter>();

                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "max-item-count",
                    In = ParameterLocation.Header,
                    Description = "max number of items per page",
                    Required = false,
                    //Type = "integer"
                    Schema = new OpenApiSchema()
                    {
                        Type = "integer"
                    }
                });

                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "continuation-token",
                    In = ParameterLocation.Header,
                    Description = "used to get the next page",
                    Required = false,
                    //Type = "string"
                    Schema = new OpenApiSchema()
                    {
                        Type = "string"
                    }
                });
            }
        }
    }
}