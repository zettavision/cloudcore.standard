using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ZettaVision.CloudCore.WebApi.Swashbuckle
{
    public class ReadableOperationIdsOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var controllerActionDescriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;

            var controllerName = controllerActionDescriptor.ControllerName;

            if (controllerName.EndsWith("Controller"))
                controllerName = controllerName.Substring(0, controllerName.Length - "Controller".Length);

            var name = controllerActionDescriptor.ActionName;

            if (name.EndsWith("Async"))
                name = name.Substring(0, name.Length - "Async".Length);

            operation.OperationId = controllerName + "_" + name;
        }
    }
}