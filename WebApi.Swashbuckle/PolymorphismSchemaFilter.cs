﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;

namespace ZettaVision.CloudCore.WebApi.Swashbuckle
{
    public class PolymorphismSchemaFilter<T> : ISchemaFilter
    {
        private HashSet<Type> derivedTypes;

        private static HashSet<Type> GetDerivedTypes()
        {
            var abstractType = typeof(T);
            var dTypes = abstractType.Assembly
                                     .GetTypes()
                                     .Where(x => abstractType != x && abstractType.IsAssignableFrom(x));

            var result = new HashSet<Type>();

            foreach (var item in dTypes)
                result.Add(item);

            return result;
        }

        public void Apply(Schema schema, SchemaFilterContext context)
        {
            if (derivedTypes == null)
                derivedTypes = GetDerivedTypes();

            var t = context.SystemType;


            if (!derivedTypes.Contains(t)) return;

            var clonedSchema = new Schema
            {
                Properties = schema.Properties,
                Type = schema.Type,
                Required = schema.Required
            };

            //schemaRegistry.Definitions[typeof(T).Name]; does not work correctly in SwashBuckle
            var parentSchema = new Schema { Ref = "#/definitions/" + typeof(T).Name };

            schema.AllOf = new List<Schema> { parentSchema, clonedSchema };

            //reset properties for they are included in allOf, should be null but code does not handle it
            schema.Properties = new Dictionary<string, Schema>();
        }
    }
}
