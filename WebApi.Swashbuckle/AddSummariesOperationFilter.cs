using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ZettaVision.CloudCore.WebApi.Swashbuckle
{
    public class AddSummariesOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var controllerActionDescriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;

            var name = controllerActionDescriptor.ActionName;

            if (name.EndsWith("Async"))
                name = name.Substring(0, name.Length - "Async".Length);

            operation.Summary = name;
        }
    }
}