using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZettaVision.CloudCore.Infrastructure.SnowMaker;

namespace Infrastructure.SnowMaker.Tests
{
    [TestClass]
    public class UniqueIdGeneratorTests
    {
        private readonly IConfigurationRoot config;
        private readonly UniqueIdGenerator uniqueIdGenerator;


        public UniqueIdGeneratorTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            var blobServiceClient = new BlobServiceClient(config["StorageConnectionString"]);
            uniqueIdGenerator = new UniqueIdGenerator(new BlobOptimisticDataStore(blobServiceClient, "testidgenerator"));
            uniqueIdGenerator.BatchSize = 10;
        }

        [TestMethod]
        public void NextId()
        {
            var idAsLong = uniqueIdGenerator.NextId("testids");
            var id = idAsLong.ToString();

        }

        [TestMethod]
        public void NextIdMultipleNewScopes()
        {
            for(int i=1; i<=10; i++)
            {
                var idAsLong = uniqueIdGenerator.NextId($"testids-{i}-{Guid.NewGuid():N}");
                var id = idAsLong.ToString();
            }



        }
    }
}
