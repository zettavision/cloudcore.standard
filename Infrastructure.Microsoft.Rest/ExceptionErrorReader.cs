﻿using System;
using System.Text;
using Microsoft.Rest;
using ZettaVision.CloudCore.Domain.Run.Infrastructure;

namespace ZettaVision.CloudCore.Infrastructure.Microsoft.Rest
{
    public class ExceptionErrorReader : IExceptionErrorReader
    {
        public string GetErrorMessage(Exception ex)
        {
            if (ex == null)
                return null;

            // ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
            if (ex is HttpOperationException)
            {
                var httpOperationException = (HttpOperationException) ex;
                var stringBuilder = new StringBuilder();

                stringBuilder.AppendLine(ex.ToString());

                if (httpOperationException.Request?.RequestUri != null)
                    stringBuilder.AppendLine($"Request.RequestUri: {httpOperationException.Request?.Method} {httpOperationException.Request?.RequestUri}");

                if(httpOperationException.Response?.ReasonPhrase != null)
                    stringBuilder.AppendLine($"Response.ReasonPhrase: {httpOperationException.Response.ReasonPhrase}");

                if (httpOperationException.Response?.StatusCode != null)
                    stringBuilder.AppendLine($"Response.StatusCode: {httpOperationException.Response.StatusCode}");

                if (httpOperationException.Response?.Content != null)
                    stringBuilder.AppendLine($"Response.Content: {httpOperationException.Response.Content}");

                return stringBuilder.ToString();
            }
            else
            {
                return ex.ToString();
            }
        }
    }
}
