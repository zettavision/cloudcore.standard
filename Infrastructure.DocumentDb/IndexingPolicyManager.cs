﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class IndexingPolicyManager
    {
        private readonly IDocumentClient documentClient;
        private readonly Uri documentCollectionUri;
        protected List<IncludedPath> IncludedPaths;

        public IndexingPolicyManager(string endpointUri, string authKey, string databaseName,
            string collectionName,
            IEnumerable<IAggregateIndexingPolicy> entityIndexingPolicies)
        {
            documentClient = new DocumentClient(new Uri(endpointUri), authKey);
            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);

            Setup(entityIndexingPolicies);
        }

        public IndexingPolicyManager(IDocumentClient documentClient, string databaseName,
            string collectionName,
            IEnumerable<IAggregateIndexingPolicy> entityIndexingPolicies)
        {
            this.documentClient = documentClient;
            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);

            Setup(entityIndexingPolicies);
        }

        private void Setup(IEnumerable<IAggregateIndexingPolicy> entityIndexingPolicies)
        {
            IncludedPaths = new List<IncludedPath>();

            IncludedPaths.Add(IncludedPathFactory.BuildHash("/Type/?"));

            IncludedPaths.Add(IncludedPathFactory.BuildRange(
                $"/ExtendedProperties/AggregateInfo_{nameof(AggregateInfoValueObject.CreatedDate)}_Ticks/?"));

            IncludedPaths.Add(IncludedPathFactory.BuildRange(
                $"/ExtendedProperties/AggregateInfo_{nameof(AggregateInfoValueObject.LastModifiedDate)}_Ticks/?"));

            IncludedPaths.Add(IncludedPathFactory.BuildHash("/AggregateInfo/CreatedByUserId/?"));
            IncludedPaths.Add(IncludedPathFactory.BuildHash("/AggregateInfo/LastModifiedByUserId/?"));

            IncludedPaths.Add(IncludedPathFactory.BuildRange("/AggregateInfo/Version/?"));

            foreach (var entityIndexingPolicy in entityIndexingPolicies)
                IncludedPaths.AddRange(entityIndexingPolicy.IncludedPaths);
        }

        public async Task SetIndexingPolicyAsync(IProgress<double> progress = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var indexingPolicy = IndexingPolicyFactory.Build(IncludedPaths);
            await SetIndexingPolicyAsync(indexingPolicy, progress, cancellationToken).ConfigureAwait(false);
        }

        private async Task SetIndexingPolicyAsync(IndexingPolicy indexingPolicy, IProgress<double> progress = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var collection =
                (await documentClient.ReadDocumentCollectionAsync(documentCollectionUri).ConfigureAwait(false)).Resource;

           
            if (collection.IndexingPolicy == null &&  indexingPolicy == null)
                return;

            var currentIndexingPolicyJson = JsonConvert.SerializeObject(collection.IndexingPolicy);
            var indexingPolicyJson = JsonConvert.SerializeObject(indexingPolicy);
            if (currentIndexingPolicyJson == indexingPolicyJson)
                return;

            collection.IndexingPolicy = indexingPolicy;

            cancellationToken.ThrowIfCancellationRequested();
            await documentClient.ReplaceDocumentCollectionAsync(collection).ConfigureAwait(false);

            await WaitForIndexTransformationToCompleteAsync(collection, progress, cancellationToken).ConfigureAwait(false);
        }

        private async Task WaitForIndexTransformationToCompleteAsync(DocumentCollection collection,
            IProgress<double> progress, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var smallWaitTimeMilliseconds = TimeSpan.FromSeconds(1);

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();
                var collectionReadResponse =
                    await documentClient.ReadDocumentCollectionAsync(collection.SelfLink).ConfigureAwait(false);
                var indexTransformationProgress = collectionReadResponse.IndexTransformationProgress;

                if (indexTransformationProgress >= 100 || indexTransformationProgress == -1)
                    break;

                progress?.Report(indexTransformationProgress/(double) 100);

                await Task.Delay(smallWaitTimeMilliseconds, cancellationToken).ConfigureAwait(false);
            }
        }
    }
}