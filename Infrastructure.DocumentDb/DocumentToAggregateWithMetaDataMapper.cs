﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class DocumentToAggregateWithMetadataMapper<T> where T : IAggregate
    {
        private readonly JsonSerializer serializer;
        private readonly string typeName;

        public DocumentToAggregateWithMetadataMapper(string typeName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            serializer = JsonSerializer.Create(jsonSerializerSettings);

            this.typeName = typeName;
        }

        private string GetTypeName()
        {
            var identifierList = new List<string>();

            var properties = typeof(T).GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    if (!string.IsNullOrEmpty(identifierAttribute.PluralName))
                        identifierList.Add(identifierAttribute.PluralName.ToLowerInvariant());
                }
            }

            return string.Join(string.Empty, identifierList);
        }

        public AggregateWithMetadata<T> Map(Document resource)
        {
            var documentJObject = JObject.FromObject(resource);

            var json = documentJObject[typeName].ToString();
            var jObject = JObject.Parse(json);
            var aggregate = jObject.ToObject<T>(serializer);

            var aggregateInfoJson = documentJObject["AggregateInfo"].ToString();
            var aggregateInfoJObject = JObject.Parse(aggregateInfoJson);
            var aggregateInfoValueObject = aggregateInfoJObject.ToObject<AggregateInfoValueObject>(serializer);

            var aggregateWithMetadata = new AggregateWithMetadata<T>();
            aggregateWithMetadata.ETag = resource.ETag;
            aggregateWithMetadata.AggregateInfo = aggregateInfoValueObject;
            aggregateWithMetadata.Aggregate = aggregate;

            return aggregateWithMetadata;
        }

        public AggregateWithMetadata<T> MapWithDocumentJson(string documentJson)
        {
            var documentJObject = JObject.Parse(documentJson);
            var json = documentJObject[typeName].ToString();
            var eTag = documentJObject["_etag"].Value<string>();

            var jObject = JObject.Parse(json);
            var aggregate = jObject.ToObject<T>(serializer);

            var aggregateInfoJson = documentJObject["AggregateInfo"].ToString();
            var aggregateInfoJObject = JObject.Parse(aggregateInfoJson);
            var aggregateInfoValueObject = aggregateInfoJObject.ToObject<AggregateInfoValueObject>(serializer);

            var aggregateWithMetadata = new AggregateWithMetadata<T>();
            aggregateWithMetadata.ETag = eTag;
            aggregateWithMetadata.AggregateInfo = aggregateInfoValueObject;
            aggregateWithMetadata.Aggregate = aggregate;

            return aggregateWithMetadata;
        }
    }
}