﻿using Newtonsoft.Json;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class DocumentIdentifier
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        public string PartitionKey { get; set; }
    }
}