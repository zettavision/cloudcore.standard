﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents.Client;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public static class DocumentClientFactory
    {
        public static DocumentClient GetDocumentClient(string endpointUri, string authKey,
            int? maxRetryAttemptsOnThrottledRequests = null, int? maxRetryWaitTimeInSeconds = null)
        {
            if (maxRetryAttemptsOnThrottledRequests == null && maxRetryWaitTimeInSeconds == null)
                return new DocumentClient(new Uri(endpointUri), authKey);
            else
            {
                return new DocumentClient(new Uri(endpointUri), authKey, new ConnectionPolicy
                {
                    RetryOptions = new RetryOptions
                    {
                        MaxRetryAttemptsOnThrottledRequests = maxRetryAttemptsOnThrottledRequests ?? 9,
                        MaxRetryWaitTimeInSeconds = maxRetryWaitTimeInSeconds ?? 30
                    }
                });
            }
        }
    }
}
