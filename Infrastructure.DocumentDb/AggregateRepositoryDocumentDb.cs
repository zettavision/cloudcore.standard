﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class AggregateRepositoryDocumentDb<T, T2, T3> : IAggregateRepository<T, T2, T3>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {
        private readonly AggregateToDocumentMapper<T2> aggregateToDocumentMapper;
        private readonly string collectionName;
        private readonly string databaseName;
        private readonly IDocumentClient documentClient;
        private readonly Uri documentCollectionUri;
        private readonly DocumentToAggregateModelMapper<T3> documentToAggregateModelMapper;
        private readonly DocumentToAggregateWithMetadataMapper<T2> documentToAggregateWithMetadataMapper;
        private readonly string aggregatePluralName;
        private readonly bool isPartitionCollection;
        private readonly string partitionKeyPrefix;
        private readonly string idPrefix;
        private bool isPartitionKeyFromId = false;

        public bool IsPartitionKeyFromId { 
            get => isPartitionKeyFromId; 
            set 
            { 
                isPartitionKeyFromId = value;

                if (aggregateToDocumentMapper != null)
                {
                    aggregateToDocumentMapper.IsPartitionKeyFromId = value;
                }
            } 
        }
        public AggregateRepositoryDocumentDb(string endpointUri, string authKey, string databaseName,
            string collectionName, IAggregateExtendedProperties<T2> aggregateExtendedProperties = null,
            IEnumerable<JsonConverter> jsonConverters = null, 
            bool isPartitionCollection = false, 
            string partitionKeyPrefix = null,
            string typeNamePrefix = null,
            string idPrefix = null)
        {
            documentClient = new DocumentClient(new Uri(endpointUri), authKey);

            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;
            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);



            var typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);

            aggregateToDocumentMapper = new AggregateToDocumentMapper<T2>(typeName, aggregateExtendedProperties?.ExtendedProperties, null, partitionKeyPrefix, idPrefix);
            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T3>(typeName, jsonConverters);
            documentToAggregateWithMetadataMapper = new DocumentToAggregateWithMetadataMapper<T2>(typeName, jsonConverters);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }



        public AggregateRepositoryDocumentDb(IDocumentClient documentClient, string databaseName,
            string collectionName, IAggregateExtendedProperties<T2> aggregateExtendedProperties = null,
            IEnumerable<JsonConverter> jsonConverters = null, bool isPartitionCollection = false, 
            string partitionKeyPrefix = null,
            string typeNamePrefix = null,
            string idPrefix = null)
        {
            this.documentClient = documentClient;

            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;
            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);

            var typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);

            aggregateToDocumentMapper = new AggregateToDocumentMapper<T2>(typeName, aggregateExtendedProperties?.ExtendedProperties, null, partitionKeyPrefix, idPrefix);
            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T3>(typeName, jsonConverters);
            documentToAggregateWithMetadataMapper = new DocumentToAggregateWithMetadataMapper<T2>(typeName, jsonConverters);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }




        public async Task<T3> InsertAsync(T2 aggregate, string userId, DateTime? createDate = null,
            string lastModifiedByUserId = null, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                if (createDate == null)
                    createDate = DateTime.UtcNow;

                var aggregateInfo = new AggregateInfoValueObject(userId, createDate.Value);

                if (lastModifiedDate != null)
                    aggregateInfo = aggregateInfo.MarkModified(lastModifiedByUserId, lastModifiedDate.Value);

                var document = aggregateToDocumentMapper.Map(aggregate, aggregateInfo);
                var response =
                    await
                        documentClient.CreateDocumentAsync(documentCollectionUri, document, null, true, cancellationToken)
                            .ConfigureAwait(false);

                return documentToAggregateModelMapper.Map(response.Resource);
            }
            catch (DocumentClientException ex)
            {
                ThrowIfRepositoryException(ex, FullyQualifiedIdentifier.GetIdentifier(aggregate, idPrefix));
                throw;
            }
        }

        public async Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {


            try
            {
                var documentId = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, idPrefix);

                var documentUri = UriFactory.CreateDocumentUri(databaseName, collectionName, documentId);

                ResourceResponse<Document> response;

                if (isPartitionCollection == false)
                    response = await documentClient.ReadDocumentAsync(documentUri, null, cancellationToken).ConfigureAwait(false);
                else
                {
                    response =
                        await
                            documentClient.ReadDocumentAsync(documentUri,
                                new RequestOptions { PartitionKey = new PartitionKey(aggregateIdentifier.GetPartitionKey(partitionKeyPrefix, IsPartitionKeyFromId)) }, cancellationToken).ConfigureAwait(false);
                }



                var aggregateWithMetadata = documentToAggregateWithMetadataMapper.Map(response.Resource);

                if ((aggregateIdentifier.ETag != null) && (aggregateWithMetadata.ETag != aggregateIdentifier.ETag))
                    throw new OptimisticConcurrencyException($"Unable to load '{documentId}' etag does not match")
                        .AddToData("Error", "RepositoryOptimisticConcurrency")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                return aggregateWithMetadata;
            }
            catch (DocumentClientException ex)
            {
                ThrowIfRepositoryException(ex, FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, idPrefix));
                throw;
            }
        }

        public async Task<T3> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId,
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                if (lastModifiedDate == null)
                    lastModifiedDate = DateTime.UtcNow;

                var aggregateInfo = aggregateWithMetadata.AggregateInfo.MarkModified(userId, lastModifiedDate.Value);

                if (aggregateWithMetadata.ETag == null)
                    throw new Exception("Etag cannot be null");

                var documentId = FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate, idPrefix);

                var document = aggregateToDocumentMapper.Map(aggregateWithMetadata.Aggregate, aggregateInfo);

                var documentUri = UriFactory.CreateDocumentUri(databaseName, collectionName, documentId);

                var response =
                    await
                        documentClient.ReplaceDocumentAsync(documentUri, document,
                            GetRequestOptionsWithEtag(aggregateWithMetadata.ETag), cancellationToken).ConfigureAwait(false);

                return documentToAggregateModelMapper.Map(response.Resource);
            }
            catch (DocumentClientException ex)
            {
                ThrowIfRepositoryException(ex, FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate, idPrefix));
                throw;
            }
        }

        public async Task DeleteAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            var documentId = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, idPrefix);
            var documentUri = UriFactory.CreateDocumentUri(databaseName, collectionName, documentId);

            try
            {
                if (isPartitionCollection == false)
                    await documentClient.DeleteDocumentAsync(documentUri, GetRequestOptionsWithEtag(aggregateIdentifier.ETag), cancellationToken).ConfigureAwait(false);
                else
                {
                    var requestOptions = GetRequestOptionsWithEtag(aggregateIdentifier.ETag) ?? new RequestOptions();

                    requestOptions.PartitionKey = new PartitionKey(aggregateIdentifier.GetPartitionKey(partitionKeyPrefix, IsPartitionKeyFromId));

                    await documentClient.DeleteDocumentAsync(documentUri, requestOptions, cancellationToken).ConfigureAwait(false);
                }
            }
            catch (DocumentClientException ex)
            {
                ThrowIfRepositoryException(ex, FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, idPrefix));
                throw;
            }
        }

        private void ThrowIfRepositoryException(DocumentClientException ex, string fullyQualifiedIdentifier)
        {
            if (ex.Error.Code == "PreconditionFailed")
                throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({fullyQualifiedIdentifier})", ex)
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName); 
            else if (ex.Error.Code == "NotFound")
                throw new ObjectNotFoundException($"Object Not Found ({fullyQualifiedIdentifier})", ex)
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            else if (ex.Error.Code == "Conflict")
                throw new ObjectAlreadyExistsException($"Object Already Exists ({fullyQualifiedIdentifier})", ex)
                    .AddToData("Error", "RepositoryObjectAlreadyExists")
                    .AddToData("AggregatePluralName", aggregatePluralName);
        }

        private static RequestOptions GetRequestOptionsWithEtag(string etag)
        {
            if (etag == null)
                return null;

            return new RequestOptions
            {
                AccessCondition = new AccessCondition
                {
                    Condition = etag,
                    Type = AccessConditionType.IfMatch
                }
            };
        }
    }
}