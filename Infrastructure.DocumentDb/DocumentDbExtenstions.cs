﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public static class DocumentDbExtenstions
    {
        public static string GetIdentifierTypeName(this Type type, string typeNamePrefix = null)
        {
            var typeName = IdentifierAttributeReader.GetIdIdentifierPluralName(type).ToLowerInvariant();

            if(string.IsNullOrEmpty(typeNamePrefix))
                return typeName;

            return $"{typeNamePrefix}{typeName}";
        }

        public static string GetPartitionKey(this object aggregate, string partitionKeyPrefix)
        {
            return aggregate.GetPartitionKey(partitionKeyPrefix, false);
        }

        public static string GetPartitionKey(this object aggregate, string partitionKeyPrefix, bool fromId)
        {
            var identifierList = new List<string>();

            if(!string.IsNullOrEmpty(partitionKeyPrefix))
                identifierList.Add(partitionKeyPrefix);

            var properties = aggregate.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes == null) || !attributes.Any())
                    continue;

                var identifierAttribute =
                    attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                if (string.IsNullOrEmpty(identifierAttribute?.PluralName))
                    continue;

                if (fromId)
                {
                    if (property.Name != "Id")
                        continue;
                }
                else
                {
                    if (property.Name == "Id")
                        continue;
                }

                identifierList.Add(property.GetValue(aggregate) as string);
            }

            var partitionKey = !identifierList.Any() ? string.Empty : string.Join(".", identifierList);

            if (fromId)
            {
                if (string.IsNullOrEmpty(partitionKey))
                    return string.Empty;

                if(!partitionKey.Contains("."))
                    return string.Empty;

                var lastPeriod = partitionKey.LastIndexOf('.');
                if (lastPeriod == 0)
                    return string.Empty;

                partitionKey = partitionKey.Substring(0, lastPeriod);
            }

            return partitionKey;
        }

        public static string GetIdentifierTypeName(this object obj, string typeNamePrefix)
        {
            return obj.GetType().GetIdentifierTypeName(typeNamePrefix);
        }

        public static string GetDocumentId(this object aggregateIdentifier, string idPrefix = null)
        {
            return FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, idPrefix);
        }
    }
}