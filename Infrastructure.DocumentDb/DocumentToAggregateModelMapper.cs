﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Microsoft.Azure.Documents;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class DocumentToAggregateModelMapper<T> where T : IAggregateModel
    {
        private readonly JsonSerializer serializer;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly string typeName;

        public DocumentToAggregateModelMapper(string typeName, IEnumerable<JsonConverter> jsonConverters = null)
        {

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }


            serializer = JsonSerializer.Create(jsonSerializerSettings);

            this.typeName = typeName;

        }

        public T Map(Document resource)
        {
            var documentJObject = JObject.FromObject(resource);
            return Map(documentJObject, resource.ETag);
        }

        public T Map(string documentJson)
        {
            var documentJObject = JObject.Parse(documentJson);
            var eTag = documentJObject["_etag"].Value<string>();
            

            return Map(documentJObject,eTag);
        }

        private T Map(JObject documentJObject, string eTag)
        {
            var json = documentJObject[typeName].ToString();

            var aggregateModel = JsonConvert.DeserializeObject<T>(json, jsonSerializerSettings);

            var aggregateInfoJson = documentJObject["AggregateInfo"].ToString();

            var aggregateInfoModel = JsonConvert.DeserializeObject<AggregateInfoModel>(aggregateInfoJson, jsonSerializerSettings);


            aggregateModel.ETag = eTag;
            aggregateModel.AggregateInfo = aggregateInfoModel;

            return aggregateModel;
        }

        private T MapWithDynamic(DynamicObject dynamicObjectDocument)
        {
            var dynamicDocument = dynamicObjectDocument as dynamic;

            var binder = Binder.GetMember(CSharpBinderFlags.None, typeName, dynamicDocument.GetType(),
                new[] {CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null)});

            object obj;
            if (dynamicObjectDocument.TryGetMember(binder, out obj) == false)
                throw new Exception($"document does not contain property {typeName}");

            var jObject = (JObject) obj;
            var aggregateModel = JsonConvert.DeserializeObject<T>(jObject.ToString(), jsonSerializerSettings);


            var aggregateInfoBinder = Binder.GetMember(CSharpBinderFlags.None, "AggregateInfo",
                dynamicDocument.GetType(), new[] {CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null)});

            object aggregateInfoObj;
            if (dynamicObjectDocument.TryGetMember(aggregateInfoBinder, out aggregateInfoObj) == false)
                throw new Exception("document does not contain property AggregateInfo");

            var aggregateInfoJObject = (JObject)aggregateInfoObj;
        
            var aggregateInfoModel = JsonConvert.DeserializeObject<AggregateInfoModel>(aggregateInfoJObject.ToString(), jsonSerializerSettings);


            aggregateModel.ETag = dynamicDocument._etag;
            aggregateModel.AggregateInfo = aggregateInfoModel;

            return aggregateModel;
        }
    }
}