﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Azure.Documents;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    internal static class IndexingPolicyFactory
    {
        internal static IndexingPolicy Build(IEnumerable<IncludedPath> includedPaths)
        {
            var indexingPolicy = new IndexingPolicy();

            indexingPolicy.IncludedPaths = new Collection<IncludedPath>();
            foreach (var includedPath in includedPaths)
                indexingPolicy.IncludedPaths.Add(includedPath);


            indexingPolicy.ExcludedPaths = new Collection<ExcludedPath>();
            indexingPolicy.ExcludedPaths.Add(new ExcludedPath
            {
                Path = "/*"
            });

            indexingPolicy.IndexingMode = IndexingMode.Consistent;

            return indexingPolicy;
        }
    }
}