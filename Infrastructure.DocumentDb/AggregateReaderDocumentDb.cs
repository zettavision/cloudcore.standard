﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class AggregateReaderDocumentDb<T, T2> : IAggregateGetByIdReader<T, T2>
        where T : AggregateIdentifier
        where T2 : class, IAggregateModel
    {
        private readonly string collectionName;
        private readonly string databaseName;
        private readonly IDocumentClient documentClient;
        private readonly Uri documentCollectionUri;
        private readonly DocumentToAggregateModelMapper<T2> documentToAggregateModelMapper;
        private readonly bool isPartitionCollection;
        private readonly string partitionKeyPrefix;
        private readonly string idPrefix;

        public bool IsPartitionKeyFromId { get; set; } = false;

        public AggregateReaderDocumentDb(string endpointUri, string authKey, string databaseName,
            string collectionName, IEnumerable<JsonConverter> jsonConverters = null, bool isPartitionCollection = false,
            string partitionKeyPrefix = null, 
            int? maxRetryAttemptsOnThrottledRequests = null, 
            int? maxRetryWaitTimeInSeconds = null,
            string typeNamePrefix = null,
            string idPrefix = null)
        {
            if (maxRetryAttemptsOnThrottledRequests == null && maxRetryWaitTimeInSeconds == null)
                documentClient = new DocumentClient(new Uri(endpointUri), authKey);
            else
            {
                documentClient = new DocumentClient(new Uri(endpointUri), authKey, new ConnectionPolicy
                {
                    RetryOptions = new RetryOptions
                    {
                        MaxRetryAttemptsOnThrottledRequests = maxRetryAttemptsOnThrottledRequests ?? 9,
                        MaxRetryWaitTimeInSeconds = maxRetryWaitTimeInSeconds ?? 30
                    }
                });
            }

            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;
            TypeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);
            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T2>(TypeName, jsonConverters);
            
        }

        public AggregateReaderDocumentDb(IDocumentClient documentClient, string databaseName,
            string collectionName, IEnumerable<JsonConverter> jsonConverters = null, bool isPartitionCollection = false,
            string partitionKeyPrefix = null,
            string typeNamePrefix = null,
            string idPrefix = null)
        {
            this.documentClient = documentClient;
            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;

            TypeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);
            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T2>(TypeName, jsonConverters);
            
        }

        public string TypeName { get; }

        public async Task<T2> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var documentId = aggregateIdentifier.GetDocumentId(idPrefix);
                var documentUri = UriFactory.CreateDocumentUri(databaseName, collectionName, documentId);

                ResourceResponse<Document> response;

                if (isPartitionCollection == false)
                    response = await documentClient.ReadDocumentAsync(documentUri, null, cancellationToken).ConfigureAwait(false);
                else
                {
                    response =
                        await
                            documentClient.ReadDocumentAsync(documentUri,
                                new RequestOptions { PartitionKey = new PartitionKey(aggregateIdentifier.GetPartitionKey(partitionKeyPrefix, IsPartitionKeyFromId)) },
                                cancellationToken).ConfigureAwait(false);
                }


                return documentToAggregateModelMapper.Map(response.Resource);
            }
            catch (DocumentClientException ex)
            {
                if (ex.Error.Code == "NotFound")
                {
                    throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, idPrefix)})", ex);
                }

                throw;
            }
        }

        public string GetPartitionKey(object partitionIdentifier)
        {
            return partitionIdentifier.GetPartitionKey(partitionKeyPrefix, IsPartitionKeyFromId);
        }

        public async Task<PagedResult<T3>> QueryAsync<T3>(SqlQuerySpec query, Func<string, T3> mapFunc,
            PageParametersModel pageParameters = null, string partitionKey = null,
            CancellationToken cancellationToken = default(CancellationToken)) where T3 : class
        {
            if (query == null)
                throw new Exception("query cannot be null");

            var doucmentDbfeedOptions =
                pageParameters == null
                    ? null
                    : new FeedOptions
                    {
                        MaxItemCount = pageParameters.MaxItemCount,
                        RequestContinuation = pageParameters.ContinuationToken
                    };

            if (isPartitionCollection)
            {
                if (doucmentDbfeedOptions == null)
                {
                    doucmentDbfeedOptions = new FeedOptions();
                }

                if (partitionKey == null)
                    partitionKey = string.Empty;

                doucmentDbfeedOptions.PartitionKey = new PartitionKey(partitionKey);
                doucmentDbfeedOptions.EnableCrossPartitionQuery = false;
            }

            var documentQuery = documentClient.CreateDocumentQuery<dynamic>(documentCollectionUri, query,
                doucmentDbfeedOptions);

            var docQuery = documentQuery.AsDocumentQuery();

            FeedResponse<dynamic> batch;
            try
            {
                batch = await docQuery.ExecuteNextAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (DocumentClientException exception)
            {
                if (exception.Error?.Code == "BadRequest")
                    throw new ArgumentException(exception.Message, exception);
                
                throw;
            }

            var results = new List<T3>();
            foreach (var dynamicDoc in batch)
            {
                var model = mapFunc(dynamicDoc.ToString());
                results.Add(model);
            }

            return new PagedResult<T3> { ContinuationToken = batch.ResponseContinuation, Items = results };
        }

        public async Task<AggregateFunctionResult> QueryAggregateFunctionAsync(string partitionKey, 
            SqlQuerySpec query, string continuationToken = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query == null)
                throw new Exception("query cannot be null");

            var doucmentDbfeedOptions = new FeedOptions
                    {
                        MaxItemCount = 1,
                        RequestContinuation = continuationToken
                    };

            if (isPartitionCollection)
            {
                if (partitionKey == null)
                    partitionKey = string.Empty;

                doucmentDbfeedOptions.PartitionKey = new PartitionKey(partitionKey);
                doucmentDbfeedOptions.EnableCrossPartitionQuery = false;
            }

            IQueryable<long> documentQuery = documentClient.CreateDocumentQuery<long>(documentCollectionUri, query, doucmentDbfeedOptions);

            var docQuery = documentQuery.AsDocumentQuery();

            FeedResponse<dynamic> batch;
            try
            {
                batch = await docQuery.ExecuteNextAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (DocumentClientException exception)
            {
                if (exception.Error?.Code == "BadRequest")
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            var resultAsString =  batch.AsEnumerable().FirstOrDefault().ToString();
            var result = double.Parse(resultAsString);

            return new AggregateFunctionResult { ContinuationToken = batch.ResponseContinuation, Result = result };
        }

        public async Task<PagedResult<T2>> GetAllByPartitionAsync(string partitionKey, 
            SqlQuerySpec query, PageParametersModel pageParameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query == null)
                query = GetDefaultQuery();

            return
    await
     QueryAsync<T2>(query, doc => documentToAggregateModelMapper.Map(doc), pageParameters, partitionKey, cancellationToken)
         .ConfigureAwait(false);

        }

        public async Task<PagedResult<T2>> GetAllAsync(SqlQuerySpec query, 
            PageParametersModel pageParameters = null, CancellationToken cancellationToken = default(CancellationToken))
        {

            if (query == null)
                query = GetDefaultQuery();


            return
    await
     QueryAsync<T2>(query, doc => documentToAggregateModelMapper.Map(doc), pageParameters, null, cancellationToken)
         .ConfigureAwait(false);


        }

        public async Task<PagedResult<T2>> GetAllAsync(PageParametersModel pageParameters, CancellationToken cancellationToken = default(CancellationToken))
        {
            var query = GetDefaultQuery();

            return await GetAllAsync(query, pageParameters, cancellationToken).ConfigureAwait(false);
        }

        private SqlQuerySpec GetDefaultQuery()
        {
            return new SqlQuerySpec(
    $"SELECT * FROM root WHERE root.Type = @type",
    new SqlParameterCollection(new[] { new SqlParameter { Name = "@type", Value = TypeName } }));
        }
    }
}