﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class AggregatePurgerDocumentDb<T> : IAggregatePurger
        where T : AggregateIdentifier
    {
        private readonly string databaseName;
        private readonly string collectionName;

        private readonly IDocumentClient documentClient;
        private readonly Uri documentCollectionUri;
        private readonly string typeName;
        private readonly bool isPartitionCollection;

        public AggregatePurgerDocumentDb(string endpointUri, string authKey, string databaseName,
            string collectionName, bool isPartitionCollection = false, 
            string partitionKeyPrefix = null,
            string typeNamePrefix = null)
        {
            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            documentClient = new DocumentClient(new Uri(endpointUri), authKey);

            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);
            
        }

        public AggregatePurgerDocumentDb(IDocumentClient documentClient, string databaseName,
            string collectionName, bool isPartitionCollection = false, 
            string partitionKeyPrefix = null, 
            string typeNamePrefix = null)
        {
            
            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.documentClient = documentClient;

            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);
        }

        public async Task<long> PurgeAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            long deletedCount = 0;
            var result = await GetAllAsync(new PageParametersModel() { MaxItemCount = 1000}, cancellationToken).ConfigureAwait(false);
            await DeleteDocumentsAysnc(result?.Items, cancellationToken).ConfigureAwait(false);
            if (result?.Items != null)
                deletedCount = deletedCount + result.Items.Count();

            while (result.ContinuationToken != null)
            {
                result = await GetAllAsync(
                    new PageParametersModel() {ContinuationToken = result.ContinuationToken, MaxItemCount = 1000},
                    cancellationToken).ConfigureAwait(false);
                await DeleteDocumentsAysnc(result?.Items, cancellationToken).ConfigureAwait(false);
                if (result?.Items != null)
                    deletedCount = deletedCount + result.Items.Count();
            }

            return deletedCount;
        }

        private async Task DeleteDocumentsAysnc(IEnumerable<DocumentIdentifier> documentIds,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (documentIds == null || !documentIds.Any())
                return;

            foreach (var documentId in documentIds)
            {
                var documentUri = UriFactory.CreateDocumentUri(databaseName, collectionName, documentId.Id);
                if (isPartitionCollection)
                    await documentClient.DeleteDocumentAsync(documentUri,
                        new RequestOptions {PartitionKey = new PartitionKey(documentId.PartitionKey)},
                        cancellationToken).ConfigureAwait(false);
                else
                    await documentClient.DeleteDocumentAsync(documentUri, null, cancellationToken).ConfigureAwait(false);
            }
            
        }

        private async Task<PagedResult<DocumentIdentifier>> GetAllAsync(PageParametersModel pageParameters = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {

            var query = new SqlQuerySpec($"SELECT root.id, root.PartitionKey FROM root WHERE root.Type = @type",
                new SqlParameterCollection(new[] { new SqlParameter { Name = "@type", Value = typeName } }));

            return await QueryAsync(query, pageParameters, cancellationToken).ConfigureAwait(false);
        }

        private async Task<PagedResult<DocumentIdentifier>> QueryAsync(SqlQuerySpec query, PageParametersModel pageParameters = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query == null)
                throw new Exception("query cannot be null");

            var doucmentDbfeedOptions =
                pageParameters == null
                    ? null
                    : new FeedOptions
                    {
                        MaxItemCount = pageParameters.MaxItemCount,
                        RequestContinuation = pageParameters.ContinuationToken
                    };

            if (isPartitionCollection)
            {
                if (doucmentDbfeedOptions == null)
                {
                    doucmentDbfeedOptions = new FeedOptions();
                }

                doucmentDbfeedOptions.EnableCrossPartitionQuery = true;
            }

            
        

            var documentQuery = documentClient.CreateDocumentQuery<dynamic>(documentCollectionUri, query,
                doucmentDbfeedOptions);





            var docQuery = documentQuery.AsDocumentQuery();

            var batch = await docQuery.ExecuteNextAsync(cancellationToken).ConfigureAwait(false);


            var results = batch.Select(doc => (DocumentIdentifier)JsonConvert.DeserializeObject<DocumentIdentifier>(doc.ToString()));

            return new PagedResult<DocumentIdentifier> { ContinuationToken = batch.ResponseContinuation, Items = results };
        }
    }
}
