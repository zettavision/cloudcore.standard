using System.Collections.ObjectModel;
using Microsoft.Azure.Documents;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public static class IncludedPathFactory
    {
        [System.Obsolete("BuildStringRange is deprecated, please use BuildRange instead.")]
        public static IncludedPath BuildStringRange(string path)
        {
            return BuildRange(path);
        }

        [System.Obsolete("BuildNumberRange is deprecated, please use BuildRange instead.")]
        public static IncludedPath BuildNumberRange(string path)
        {
            return BuildRange(path);
        }

        [System.Obsolete("BuildStringHash is deprecated, please use BuildHash instead.")]
        public static IncludedPath BuildStringHash(string path)
        {
            return BuildHash(path);
        }



        [System.Obsolete("BuildNumberHash is deprecated, please use BuildHash instead.")]
        public static IncludedPath BuildNumberHash(string path)
        {
            return BuildHash(path);
        }

        public static IncludedPath BuildRange(string path)
        {
            return new IncludedPath
            {
                Path = path,
                Indexes = new Collection<Index>
                {
                    new RangeIndex(DataType.String, -1),
                    new RangeIndex(DataType.Number, -1)
                }
            };
        }

        public static IncludedPath BuildHash(string path)
        {
            return new IncludedPath
            {
                Path = path,
                Indexes = new Collection<Index>
                {
                    new HashIndex(DataType.String, -1),
                    new HashIndex(DataType.Number, -1)
                }
            };
        }
    }
}