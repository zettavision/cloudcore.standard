namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public static class DocumentPropertyPathFactory
    {
        public static string GetExtendedPropertyPath(string extendedPropertyName)
        {
            return $"/ExtendedProperties/{extendedPropertyName}/?";
        }

        /// <param name="aggregatePropertyPath">Seperate sub-properties with / </param>
        /// <param name="partitionKeyPrefix"></param>
        public static string GetPropertyPath<T>(string aggregatePropertyPath, string typeNamePrefix = null)
        {
            var typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);
            return $"/{typeName}/{aggregatePropertyPath}/?";
        }
        
    }
}