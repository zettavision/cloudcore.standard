﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class AggregateToDocumentMapper<T> where T : IAggregate
    {
        private readonly IEnumerable<ExtendedProperty<T>> extendedProperties;
        private readonly string partitionKeyPrefix;
        private readonly string idPrefix;
        private readonly string typeName;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        public bool IsPartitionKeyFromId { get; set; } = false;

        public AggregateToDocumentMapper(string typeName, 
            IEnumerable<ExtendedProperty<T>> extendedProperties = null, 
            IEnumerable<JsonConverter> jsonConverters = null, 
            string partitionKeyPrefix = null,
            string idPrefix = null)
        {
            this.extendedProperties = extendedProperties;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;
            this.typeName = typeName;


            jsonSerializerSettings = new JsonSerializerSettings {DateFormatHandling = DateFormatHandling.IsoDateFormat};
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;
            jsonSerializerSettings.DateParseHandling = DateParseHandling.None;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }
        }

        public object Map(T aggregate, AggregateInfoValueObject aggregateInfo)
        {
            var document = new ExpandoObject() as IDictionary<string, object>;
            document.Add("id", FullyQualifiedIdentifier.GetIdentifier(aggregate, idPrefix));
            document.Add("Type", typeName);
            document.Add("AggregateInfo", aggregateInfo);
            document.Add("PartitionKey", aggregate.GetPartitionKey(partitionKeyPrefix, IsPartitionKeyFromId));

            var propertyExtensionsObject = GetPropertyExtensionsObject(aggregate, aggregateInfo);

            if (propertyExtensionsObject != null)
                document.Add("ExtendedProperties", propertyExtensionsObject);

            var aggregateJson = JsonConvert.SerializeObject(aggregate, jsonSerializerSettings);
            var aggrgateJObject = JsonConvert.DeserializeObject(aggregateJson, jsonSerializerSettings);

            document.Add(typeName, aggrgateJObject);

            return document;
        }



        private dynamic GetPropertyExtensionsObject(T aggregate, AggregateInfoValueObject aggregateInfo)
        {
            var expandoObjectDictionary = new ExpandoObject() as IDictionary<string, object>;

            expandoObjectDictionary.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.CreatedDate)}_Ticks", aggregateInfo.CreatedDate.Ticks);
            expandoObjectDictionary.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.LastModifiedDate)}_Ticks", aggregateInfo.LastModifiedDate.Ticks);

            if ((extendedProperties != null) && extendedProperties.Any())
            {
                foreach (var extendedProperty in extendedProperties)
                {
                    var value = extendedProperty.Map(aggregate, aggregateInfo);

                    expandoObjectDictionary.Add(extendedProperty.Name, value);
                }
            }

            return expandoObjectDictionary;
        }
    }
}