﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb
{
    public class AggregateRefresherDocumentDb<T, T2> : IAggregateRefresher
        where T : AggregateIdentifier
        where T2 : IAggregate
    {
        private readonly string databaseName;
        private readonly string collectionName;

        private readonly IDocumentClient documentClient;
        private readonly Uri documentCollectionUri;
        private readonly string typeName;
        private readonly bool isPartitionCollection;
        private readonly string partitionKeyPrefix;
        private readonly string idPrefix;
        private readonly DocumentToAggregateWithMetadataMapper<T2> documentToAggregateWithMetadataMapper;
        private readonly AggregateToDocumentMapper<T2> aggregateToDocumentMapper;

        public AggregateRefresherDocumentDb(
            string endpointUri, 
            string authKey, 
            string databaseName,
            string collectionName, 
            IAggregateExtendedProperties<T2> aggregateExtendedProperties = null,
            IEnumerable<JsonConverter> jsonConverters = null, 
            bool isPartitionCollection = false, 
            string partitionKeyPrefix = null,
            string typeNamePrefix = null,
            string idPrefix = null)
        {
            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;
            documentClient = new DocumentClient(new Uri(endpointUri), authKey);

            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);

            documentToAggregateWithMetadataMapper = new DocumentToAggregateWithMetadataMapper<T2>(typeName, jsonConverters);
            aggregateToDocumentMapper = new AggregateToDocumentMapper<T2>(typeName, aggregateExtendedProperties?.ExtendedProperties, null, partitionKeyPrefix, idPrefix);
        }

        public AggregateRefresherDocumentDb(
            IDocumentClient documentClient, 
            string databaseName,
            string collectionName, 
            IAggregateExtendedProperties<T2> aggregateExtendedProperties = null,
            IEnumerable<JsonConverter> jsonConverters = null, 
            bool isPartitionCollection = false, 
            string partitionKeyPrefix = null,
            string typeNamePrefix = null,
            string idPrefix = null
            )
        {
            
            this.databaseName = databaseName;
            this.collectionName = collectionName;
            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;
            this.idPrefix = idPrefix;
            this.documentClient = documentClient;


            documentCollectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            typeName = typeof(T).GetIdentifierTypeName(typeNamePrefix);

            documentToAggregateWithMetadataMapper = new DocumentToAggregateWithMetadataMapper<T2>(typeName, jsonConverters);
            aggregateToDocumentMapper = new AggregateToDocumentMapper<T2>(typeName, aggregateExtendedProperties?.ExtendedProperties, null, partitionKeyPrefix, idPrefix);
        }

        public async Task<long> RefreshAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            long refreshedCount = 0;
            string continuationToken = null;

            while (true)
            {
                var result =
                    await GetAllAsync(
                        new PageParametersModel() {ContinuationToken = continuationToken, MaxItemCount = 1000},
                        cancellationToken).ConfigureAwait(false);
                await RefreshDocumentsAysnc(result?.Items, cancellationToken).ConfigureAwait(false);
                if (result?.Items != null)
                    refreshedCount = refreshedCount + result.Items.Count();

                if(result.ContinuationToken == null)
                    break;

                continuationToken = result.ContinuationToken;
            }

            return refreshedCount;
        }

        private async Task RefreshDocumentsAysnc(IEnumerable<AggregateWithMetadata<T2>> aggregatesWithMetadata,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (aggregatesWithMetadata == null || !aggregatesWithMetadata.Any())
                return;

            foreach (var aggregateWithMetadata in aggregatesWithMetadata)
            {

                var documentId = FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate, idPrefix);
                var document = aggregateToDocumentMapper.Map(aggregateWithMetadata.Aggregate, aggregateWithMetadata.AggregateInfo);

                var documentUri = UriFactory.CreateDocumentUri(databaseName, collectionName, documentId);
                await
                    documentClient.ReplaceDocumentAsync(documentUri, document, null, cancellationToken).ConfigureAwait(false);
        
            }
            
        }



        private async Task<PagedResult<AggregateWithMetadata<T2>>> GetAllAsync(PageParametersModel pageParameters = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {

            var query = new SqlQuerySpec($"SELECT * FROM root WHERE root.Type = @type",
                new SqlParameterCollection(new[] { new SqlParameter { Name = "@type", Value = typeName } }));

            return await QueryAsync(query, pageParameters, cancellationToken).ConfigureAwait(false);
        }

        private async Task<PagedResult<AggregateWithMetadata<T2>>> QueryAsync(SqlQuerySpec query, PageParametersModel pageParameters = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query == null)
                throw new Exception("query cannot be null");

            var doucmentDbfeedOptions =
                pageParameters == null
                    ? null
                    : new FeedOptions
                    {
                        MaxItemCount = pageParameters.MaxItemCount,
                        RequestContinuation = pageParameters.ContinuationToken
                    };

            if (isPartitionCollection)
            {
                if (doucmentDbfeedOptions == null)
                {
                    doucmentDbfeedOptions = new FeedOptions();
                }

                doucmentDbfeedOptions.EnableCrossPartitionQuery = true;
            }

            
        

            var documentQuery = documentClient.CreateDocumentQuery<dynamic>(documentCollectionUri, query,
                doucmentDbfeedOptions);





            var docQuery = documentQuery.AsDocumentQuery();

            var batch = await docQuery.ExecuteNextAsync(cancellationToken).ConfigureAwait(false);


            var results = batch.Select(doc => (AggregateWithMetadata<T2>)documentToAggregateWithMetadataMapper.MapWithDocumentJson(doc.ToString()));

            return new PagedResult<AggregateWithMetadata<T2>> { ContinuationToken = batch.ResponseContinuation, Items = results };
        }
    }
}
