using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace Infrastructure.AzureRedisCache.Tests.Account.Queries
{
    [Identifier("Accounts")]
    public class AccountGetAllQuery
    {
        public string ParentId { get; set; }
        public string AdminUserId { get; set; }
        public string SearchText { get; set; }
        public PageParametersModel PageParameters { get; set; }
    }
}