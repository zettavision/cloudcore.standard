using ZettaVision.CloudCore.Domain;

namespace Infrastructure.AzureRedisCache.Tests.Account.Queries
{
    public class AccountGetByIdQuery
    {
        [Identifier("Parents")]
        public string ParentId { get; set; }

        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
    }
}