﻿using Infrastructure.AzureRedisCache.Tests.Account.Requests;
using ZettaVision.CloudCore.Domain;

namespace Infrastructure.AzureRedisCache.Tests.Account
{
    public class AccountCommand<T2> : AggregateCommand<AccountIdentifier, T2>
        where T2 : IAccountRequest
    {
        public AccountCommand(AccountIdentifier identifier, T2 request, string userId)
            : base(identifier, request, userId)
        {
        }
    }
}