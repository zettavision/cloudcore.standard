using System.Collections.Generic;
using ZettaVision.CloudCore.Infrastructure;

namespace Infrastructure.AzureRedisCache.Tests.Account
{
    public class AccountExtendedProperties : IAggregateExtendedProperties<AccountAggregate>
    {
        public IEnumerable<ExtendedProperty<AccountAggregate>> ExtendedProperties => new List<ExtendedProperty<AccountAggregate>>
        {
            new ExtendedProperty<AccountAggregate>("Name_LowerCase", typeof(string), aggregate => aggregate.Name.ToLowerInvariant() )
        };
    }
}