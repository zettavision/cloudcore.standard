﻿using System.Threading.Tasks;
using Infrastructure.AzureRedisCache.Tests.Account.Models;
using Infrastructure.AzureRedisCache.Tests.Account.Queries;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;

namespace Infrastructure.AzureRedisCache.Tests.Account
{
    public interface IAccountQueryHandler
    {
        Task<AccountModel> HandleAsync(AccountGetByIdQuery query);
        Task<PagedResult<AccountModel>> HandleAsync(AccountGetAllQuery query);

        Task<PagedResult<AggregateEventModel>> HandleAsync(AccountGetAllEventsQuery query);
    }
}