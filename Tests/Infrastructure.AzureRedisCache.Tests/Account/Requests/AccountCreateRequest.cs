﻿using System.ComponentModel.DataAnnotations;

namespace Infrastructure.AzureRedisCache.Tests.Account.Requests
{
    public class AccountCreateRequest : IAccountRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string AdminUserId { get; set; }
    }
}