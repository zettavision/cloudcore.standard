﻿using ZettaVision.CloudCore.Domain.Requests;

namespace Infrastructure.AzureRedisCache.Tests.Account.Requests
{
    public interface IAccountRequest : IAggregateRequest
    {
    }
}