﻿namespace Infrastructure.AzureRedisCache.Tests.Account.Requests
{
    public class AccountChangeNameRequest : IAccountRequest
    {
        public string Name { get; set; }
    }
}