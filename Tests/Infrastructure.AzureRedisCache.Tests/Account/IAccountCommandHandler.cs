﻿using System.Threading.Tasks;
using Infrastructure.AzureRedisCache.Tests.Account.Models;
using Infrastructure.AzureRedisCache.Tests.Account.Requests;

namespace Infrastructure.AzureRedisCache.Tests.Account
{
    public interface IAccountCommandHandler
    {
        Task<AccountModel> HandleAsync(AccountCommand<AccountCreateRequest> command);
        Task<AccountModel> HandleAsync(AccountCommand<AccountChangeAdminUserIdRequest> command);

        Task<AccountModel> HandleAsync(AccountCommand<AccountChangeNameRequest> command);
    }
}