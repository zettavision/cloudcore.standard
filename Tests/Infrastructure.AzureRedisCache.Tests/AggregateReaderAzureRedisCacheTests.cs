﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.AzureRedisCache.Tests.Account;
using Infrastructure.AzureRedisCache.Tests.Account.Models;
using Infrastructure.AzureRedisCache.Tests.Account.Requests;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.AzureRedisCache;

namespace Infrastructure.AzureRedisCache.Tests
{
    [TestClass]
    public class AggregateReaderAzureRedisCacheTests
    {
        private readonly AggregateRepositoryAzureRedisCache<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;
        private readonly AggregateReaderAzureRedisCache<AccountIdentifier, AccountModel> aggregateReader;
        private readonly Random random;
        private readonly IConfigurationRoot config;

        public AggregateReaderAzureRedisCacheTests()
        {
            random = new Random();
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregateRepository = new AggregateRepositoryAzureRedisCache<AccountIdentifier, AccountAggregate, AccountModel>(
                config["ConnectionString"], null, "test");

            aggregateReader = new AggregateReaderAzureRedisCache<AccountIdentifier, AccountModel>(config["ConnectionString"], null, "test");
        }



        [TestMethod]
        public async Task GetByIdAsync()
        {
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() {Name = "name", AdminUserId = "userid"},
                "userid"
                );


            await aggregateRepository.UpsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);


            var aggregate = await aggregateReader.GetByIdAsync(command.Identifier).ConfigureAwait(false);

            Assert.AreEqual(command.Identifier.Id, aggregate.Id);

        }

        [TestMethod]
        public async Task GetMultipleByIdAsync()
        {
            var count = 10;

            var identifiers = new List<AccountIdentifier>();
            var aggregates = new List<AccountAggregate>();
            foreach (var index in Enumerable.Range(1, count))
            {
                var identifier = new AccountIdentifier("1", index.ToString());

 
                identifiers.Add(identifier);

                var command = new AccountCommand<AccountCreateRequest>
                (
                    identifier,
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                );
                var aggregate = new AccountAggregate(command);
                aggregates.Add(aggregate);

                
            }

            var models = await aggregateRepository.UpsertMultipleAsync(aggregates, "userid").ConfigureAwait(false);

            var aggregatesFromGet = await aggregateReader.GetMultipleByIdAsync(identifiers).ConfigureAwait(false);

            Assert.AreEqual(count, aggregatesFromGet.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task GetByIdAsync_MissingId_ObjectNotFoundException()
        {
            await aggregateReader.GetByIdAsync(new AccountIdentifier("1", random.Next().ToString())).ConfigureAwait(false);
        }




        [TestMethod]
        public async Task UpsertExiprationAsync()
        {
            var command = new AccountCommand<AccountCreateRequest>
            (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
            );

            await aggregateRepository.UpsertAsync(new AccountAggregate(command), "userid", TimeSpan.FromSeconds(5)).ConfigureAwait(false);

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            while (true)
            {
                try
                {
                    await aggregateReader.GetByIdAsync(command.Identifier).ConfigureAwait(false);
                    await Task.Delay(500).ConfigureAwait(false);
                }
                catch (ObjectNotFoundException)
                {
                    stopwatch.Stop();
                    break; 
                }
            }

            if(stopwatch.Elapsed > TimeSpan.FromSeconds(6))
                throw new Exception("Took to long to expire");

            if (stopwatch.Elapsed < TimeSpan.FromSeconds(4))
                throw new Exception("expired to early");

        }
    }
}
