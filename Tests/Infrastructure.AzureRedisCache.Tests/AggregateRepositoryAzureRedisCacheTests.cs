﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.AzureRedisCache.Tests.Account;
using Infrastructure.AzureRedisCache.Tests.Account.Models;
using Infrastructure.AzureRedisCache.Tests.Account.Requests;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.AzureRedisCache;

namespace Infrastructure.AzureRedisCache.Tests
{
    [TestClass]
    public class AggregateRepositoryAzureRedisCacheTests
    {
        private readonly IConfigurationRoot config;
        public AggregateRepositoryAzureRedisCacheTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregateRepository = new AggregateRepositoryAzureRedisCache<AccountIdentifier, AccountAggregate, AccountModel>(
                config["ConnectionString"], null, "test");
        }



        private readonly AggregateRepositoryAzureRedisCache<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;



        [TestMethod]
        public async Task UpsertAsync()
        {
            var random = new Random();



            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() {Name = "name", AdminUserId = "userid"},
                "userid"
                );

 
           await aggregateRepository.UpsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
 
            
        }


        [TestMethod]
        public async Task UpsertSpeedTestAsync()
        {
            var random = new Random();


            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var count = 0;

            var testDuration = 10;

            while (true)
            {
                

                var command = new AccountCommand<AccountCreateRequest>
                (
                    new AccountIdentifier("1", random.Next().ToString()),
                    new AccountCreateRequest() {Name = "name", AdminUserId = "userid"},
                    "userid"
                );

                await aggregateRepository.UpsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

                count++;

                if (stopwatch.Elapsed > TimeSpan.FromSeconds(testDuration))
                    break;
            }


            var requestPerSecond = (double)count / (double)testDuration;


        }

        [TestMethod]
        public async Task UpsertAsync_Duplicate_ObjectAlreadyExistsException()
        {
            var random = new Random();

   
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.UpsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

        }



    }
}
