﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;
using ZettaVision.CloudCore.Infrastructure.InMemory;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests
{
    [TestClass]
    public class AggregateRepositoryWithEventStoreDocumentDbTests
    {
        private readonly
            AggregateRepositoryWithEventStore<AccountIdentifier, AccountAggregate, IAccountRequest, AccountModel>
            aggregateRepositoryWithEventStore;
        public AggregateRepositoryWithEventStoreDocumentDbTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            var aggregateEventRepositoryInMemory = new AggregateEventRepositoryInMemory();

            var aggregateRepository = new AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", new AccountExtendedProperties());

            aggregateRepositoryWithEventStore = new AggregateRepositoryWithEventStore<AccountIdentifier, AccountAggregate, IAccountRequest, AccountModel>(aggregateEventRepositoryInMemory, aggregateRepository);
        }

        [TestMethod]
        public async Task InsertAsync()
        {
            var random = new Random();

            var request = new AccountCreateRequest() {Name = "name", AdminUserId = "userid"};


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                request,
                "userid"
                );

            await aggregateRepositoryWithEventStore.InsertAsync(new AccountAggregate(command), request, "userid").ConfigureAwait(false);
        }


    }
}
