﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public interface IAccountCommandHandler
    {
        Task<AccountModel> HandleAsync(AccountCommand<AccountCreateRequest> command);
        Task<AccountModel> HandleAsync(AccountCommand<AccountChangeAdminUserIdRequest> command);

        Task<AccountModel> HandleAsync(AccountCommand<AccountChangeNameRequest> command);
    }
}