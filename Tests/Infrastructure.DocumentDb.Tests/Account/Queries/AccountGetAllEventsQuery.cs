using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Queries
{
    [Identifier("Events")]
    public class AccountGetAllEventsQuery
    {
        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
        public PageParametersModel PageParameters { get; set; }
    }
}