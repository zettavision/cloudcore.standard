using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Queries
{
    public class AccountGetByIdQuery
    {
        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
    }
}