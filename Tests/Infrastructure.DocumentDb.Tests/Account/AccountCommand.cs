﻿using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public class AccountCommand<T2> : AggregateCommand<AccountIdentifier, T2>
        where T2 : IAccountRequest
    {
        public AccountCommand(AccountIdentifier identifier, T2 request, string userId)
            : base(identifier, request, userId)
        {
        }
    }
}