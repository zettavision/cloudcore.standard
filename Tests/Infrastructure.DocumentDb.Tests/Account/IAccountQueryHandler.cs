﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Queries;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public interface IAccountQueryHandler
    {
        Task<AccountModel> HandleAsync(AccountGetByIdQuery query);
        Task<PagedResult<AccountModel>> HandleAsync(AccountGetAllQuery query);

        Task<PagedResult<AggregateEventModel>> HandleAsync(AccountGetAllEventsQuery query);
    }
}