﻿namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests
{
    public class AccountChangeAdminUserIdRequest : IAccountRequest
    {
        public string AdminUserId { get; set; }
    }
}