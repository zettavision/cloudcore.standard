﻿namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests
{
    public class AccountChangeNameRequest : IAccountRequest
    {
        public string Name { get; set; }
    }
}