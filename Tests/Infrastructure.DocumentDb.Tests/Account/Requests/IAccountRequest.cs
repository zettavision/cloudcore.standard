﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests
{
    public interface IAccountRequest : IAggregateRequest
    {
    }
}