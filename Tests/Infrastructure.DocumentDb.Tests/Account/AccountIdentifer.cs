﻿using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public class AccountIdentifier : AggregateIdentifier
    {
        public AccountIdentifier()
        {
        }

        public AccountIdentifier(string id)
        {
            Id = id;
        }

        public AccountIdentifier(string id, string eTag)
        {
            Id = id;
            ETag = eTag;
        }

        [Identifier("Accounts")]
        public sealed override string Id { get; set; }



    }
}