﻿using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public class AccountAggregate : IAggregate
    {
        private string adminUserId;
        private string name;

        public AccountAggregate()
        {
        }

        public AccountAggregate(AccountCommand<AccountCreateRequest> command)
        {
            Id = command.Identifier.Id;
            Name = command.Request.Name;
            AdminUserId = command.Request.AdminUserId;
            Type = AccountTypeEnum.Bad;
        }

        [Identifier("Accounts")]
        public string Id { get; private set; }

        public string Name
        {
            get { return name; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Name));
                name = value;
            }
        }

        public AccountTypeEnum Type { get; private set; }

        public string AdminUserId
        {
            get { return adminUserId; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(AdminUserId));
                adminUserId = value;
            }
        }

        public void Apply(AccountCommand<AccountChangeNameRequest> command)
        {
            Name = command.Request.Name;
            Type = AccountTypeEnum.Ugly;
        }

        public void Apply(AccountCommand<AccountChangeAdminUserIdRequest> command)
        {
            AdminUserId = command.Request.AdminUserId;
            Type = AccountTypeEnum.Good;
        }
    }
}