using System.Collections.Generic;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public class AccountExtendedProperties : IAggregateExtendedProperties<AccountAggregate>
    {
        public IEnumerable<ExtendedProperty<AccountAggregate>> ExtendedProperties => new List<ExtendedProperty<AccountAggregate>>
        {
            new ExtendedProperty<AccountAggregate>("NameToLower", typeof(string), aggregate => aggregate.Name.ToLowerInvariant() ),
            new ExtendedProperty<AccountAggregate>("NameToLower3", typeof(string), aggregate => aggregate.Name.ToLowerInvariant() ),
            new ExtendedProperty<AccountAggregate>("NameToLower4", typeof(string), (aggregate, o) =>  aggregate.Name.ToLowerInvariant()+o.CreatedDate.ToString())

        };
    }
}