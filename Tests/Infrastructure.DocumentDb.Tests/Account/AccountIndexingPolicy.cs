﻿using System.Collections.Generic;
using Microsoft.Azure.Documents;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account
{
    public class AccountIndexingPolicy : IAggregateIndexingPolicy
    {

        public IEnumerable<IncludedPath> IncludedPaths => new List<IncludedPath>
        {
            IncludedPathFactory.BuildRange(DocumentPropertyPathFactory.GetExtendedPropertyPath("NameToLower")),
            IncludedPathFactory.BuildHash(DocumentPropertyPathFactory.GetPropertyPath<AccountIdentifier>("Name")),
            IncludedPathFactory.BuildHash(DocumentPropertyPathFactory.GetPropertyPath<AccountIdentifier>("AdminUserId"))
        };
    }
}
