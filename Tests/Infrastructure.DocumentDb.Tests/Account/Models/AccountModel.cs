﻿using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models
{
    public class AccountModel : IAggregateModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AdminUserId { get; set; }
        public string ETag { get; set; }
        public AggregateInfoModel AggregateInfo { get; set; }
        public AccountTypeEnum Type { get; set; }
    }
}