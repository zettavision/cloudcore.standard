﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests
{
    [TestClass]
    public class Indexer
    {
        private readonly IndexingPolicyManager indexingPolicyManager;
        public Indexer()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            var aggregateIndexingPolicies = new List<IAggregateIndexingPolicy>
            {
                new AccountIndexingPolicy()
            };

            indexingPolicyManager = new IndexingPolicyManager(config["DocumentDbUri"],
                config["DocumentDbKey"], "root", "root", aggregateIndexingPolicies);


        }




        [TestMethod]
        public async Task SetIndexingPolicyAsync()
        {
            await indexingPolicyManager.SetIndexingPolicyAsync().ConfigureAwait(false);
        }

    }
}
