﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests
{
    [TestClass]
    public class AggregatePurgerDocumentDbTests
    {

        public AggregatePurgerDocumentDbTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            aggregatePurger = new AggregatePurgerDocumentDb<AccountIdentifier>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", false, null);

            aggregateRepository = new AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", new AccountExtendedProperties(), null, false);
        }

        private readonly AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;

        private readonly AggregatePurgerDocumentDb<AccountIdentifier> aggregatePurger;

        public async Task InsertAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task PurgeAsync()
        {
            await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            for (int i = 0; i < 5; i++)
            {
                await InsertAsync().ConfigureAwait(false);
            }

            var deletedCount = await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            Assert.AreEqual(5, deletedCount);
        }

        [TestMethod]
        public async Task PurgeProNetsuiteSyncPartitionAsync()
        {
            var deletedCount = await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);
        }
    }
}
