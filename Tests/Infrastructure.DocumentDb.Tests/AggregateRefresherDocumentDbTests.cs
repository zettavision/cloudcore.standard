﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests
{
    [TestClass]
    public class AggregateRefresherDocumentDbTests
    {
        public AggregateRefresherDocumentDbTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            aggregateRefresher = new AggregateRefresherDocumentDb<AccountIdentifier, AccountAggregate>(
                config["DocumentDbUri"],
                config["DocumentDbKey"], 
                "root", 
                "root", 
                new AccountExtendedProperties(), null, true);


            aggregateRepository = new AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", new AccountExtendedProperties(), null, true);

            aggregatePurger = new AggregatePurgerDocumentDb<AccountIdentifier>(
                config["DocumentDbUri"],
                config["DocumentDbKey"], "root", "root", true, null);
        }

        private readonly AggregatePurgerDocumentDb<AccountIdentifier> aggregatePurger;

        private readonly AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;

        private readonly AggregateRefresherDocumentDb<AccountIdentifier, AccountAggregate> aggregateRefresher;

        public async Task InsertAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task PrepareFiveAsync()
        {
            await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            for (int i = 0; i < 5; i++)
            {
                await InsertAsync().ConfigureAwait(false);
            }
        }

        [TestMethod]
        public async Task RefreshAllAsync()
        {
            await aggregateRefresher.RefreshAllAsync().ConfigureAwait(false);

        }


    }
}
