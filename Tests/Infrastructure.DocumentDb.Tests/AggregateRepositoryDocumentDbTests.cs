﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests
{
    [TestClass]
    public class AggregateRepositoryDocumentDbTests
    {
        public AggregateRepositoryDocumentDbTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            aggregateRepository = new AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", new AccountExtendedProperties());
        }



        private readonly AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;




        [TestMethod]
        public async Task InsertAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() {Name = "name", AdminUserId = "userid"},
                "userid"
                );
       
            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectAlreadyExistsException))]
        public async Task InsertAsync_Duplicate_ObjectAlreadyExistsException()
        {
            var random = new Random();

   
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task LoadAsync()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(OptimisticConcurrencyException))]
        public async Task LoadAsync_InvalidETag_OptimisticConcurrencyException()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.Id, "InvalidETag")).ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task LoadAsync_Missing_ObjectNotFoundException()
        {
            var random = new Random();
            await aggregateRepository.LoadAsync(new AccountIdentifier(random.Next().ToString(), null)).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task UpdateAsync()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(OptimisticConcurrencyException))]
        public async Task UpdateAsync_InvalidEtag_OptimisticConcurrencyException()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);
            aggregateWithMetadata.ETag = "W/\"datetime\'2016-11-04T19%3A41%3A25.1092927Z\'\"";

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task UpdateAsync_MissingId_ObjectNotFound()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);

            var properties = aggregateWithMetadata.Aggregate.GetType().GetProperties();

            properties.First(p => p.Name == "Id").SetValue(aggregateWithMetadata.Aggregate, random.Next().ToString());
            

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }
    }
}
