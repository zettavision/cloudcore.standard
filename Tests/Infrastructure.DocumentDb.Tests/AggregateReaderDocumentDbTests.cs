﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Azure.Documents;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.DocumentDb.Tests
{
    [TestClass]
    public class AggregateReaderDocumentDbTests
    {
        private readonly AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;
        private readonly AggregateReaderDocumentDb<AccountIdentifier, AccountModel> aggregateReader;
        private readonly Random random;

        public AggregateReaderDocumentDbTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            random = new Random();
            aggregateRepository = new AggregateRepositoryDocumentDb<AccountIdentifier, AccountAggregate, AccountModel>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", 
                new AccountExtendedProperties(), null, true);

            aggregateReader = new AggregateReaderDocumentDb<AccountIdentifier, AccountModel>(
                config["DocumentDbUri"], config["DocumentDbKey"], "root", "root", null, true);
        }



        [TestMethod]
        public async Task GetByIdAsync()
        {
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var model = await aggregateReader.GetByIdAsync(command.Identifier).ConfigureAwait(false);


        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task GetByIdAsync_MissingId_ObjectNotFoundException()
        {
            await aggregateReader.GetByIdAsync(new AccountIdentifier(random.Next().ToString())).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task GetAllAsync()
        {

            for (int i = 0; i < 10; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllAsync(null, null).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 10;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllAsync_MaxCount2()
        {
            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllAsync(null, new PageParametersModel() { MaxItemCount = 2 }).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());
        }


        [TestMethod]
        public async Task GetAllAsync_FilterOnName()
        {
            var randomName = random.Next().ToString();

            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(random.Next().ToString()),
                    new AccountCreateRequest() { Name = randomName, AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var query = new SqlQuerySpec(
    $"SELECT * FROM root WHERE root.Type = @type AND root.{aggregateReader.TypeName}.Name = @name",
    new SqlParameterCollection(new[]
    {
        new SqlParameter { Name = "@type", Value = aggregateReader.TypeName },
        new SqlParameter { Name = "@name", Value = randomName }
    }));


            var results = await aggregateReader.GetAllAsync(query).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());

        }


        [TestMethod]
        public async Task GetAllAsync_FilterOnExtendedPropertyNameToLower()
        {
            var randomName = random.Next().ToString();

            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(random.Next().ToString()),
                    new AccountCreateRequest() { Name = randomName, AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var query = new SqlQuerySpec(
                $"SELECT * FROM root WHERE root.Type = @type AND root.ExtendedProperties.NameToLower = @name",
                new SqlParameterCollection(new[]
                {
                    new SqlParameter { Name = "@type", Value = aggregateReader.TypeName },
                    new SqlParameter { Name = "@name", Value = randomName }
                }));


            var results = await aggregateReader.GetAllAsync(query).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());

        }

        [TestMethod]
        public async Task GetAllAsync_FilterOnAdminUserId()
        {
            var randomName = random.Next().ToString();

            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(random.Next().ToString()),
                    new AccountCreateRequest() { Name = randomName, AdminUserId = randomName },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var query = new SqlQuerySpec(
                $"SELECT * FROM root WHERE root.Type = @type AND root.{aggregateReader.TypeName}.AdminUserId = @adminUserId ORDER BY root.ExtendedProperties.NameToLower",
                new SqlParameterCollection(new[]
                   {
                        new SqlParameter { Name = "@type", Value = aggregateReader.TypeName },
                        new SqlParameter { Name = "@adminUserId", Value = randomName }
                    }));


            var results = await aggregateReader.GetAllAsync(query).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());
        }


    }
}
