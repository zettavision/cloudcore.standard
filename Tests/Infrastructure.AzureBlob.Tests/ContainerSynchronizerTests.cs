﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests
{
    [TestClass]
    public class ContainerSynchronizerTests
    {
        private readonly ContainerSynchronizer containerSynchronizer;
        public ContainerSynchronizerTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            containerSynchronizer = new ContainerSynchronizer(config["SourceConnectionString"], config["SourceContainerName"],
                config["DestinationConnectionString"], config["DestinationContainerName"]);
        }

        [TestMethod]
        public async Task RunAsync()
        {
            await containerSynchronizer.RunAsync().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task MirrorAsync()
        {
            await containerSynchronizer.MirrorAsync().ConfigureAwait(false);
        }
    }
}
