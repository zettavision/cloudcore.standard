﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests
{
    [TestClass]
    public class AggregatePurgerAzureBlobTests
    {
        public AggregatePurgerAzureBlobTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregatePurger = new AggregatePurgerAzureBlob<AccountIdentifier>(config["StorageConnectionString"], "testaggregates");

            aggregateRepository = new AggregateRepositoryAzureBlob<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"],
                "testaggregates");
        }

        private readonly AggregateRepositoryAzureBlob<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;

        private readonly AggregatePurgerAzureBlob<AccountIdentifier> aggregatePurger;

        public async Task InsertAsync()
        {
            var random = new Random();

            var parentId = random.Next().ToString();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(parentId, random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task PurgeAsync()
        {
            await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            for (var i = 0; i < 5; i++)
            {
                await InsertAsync().ConfigureAwait(false);
            }

            var deletedCount = await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            Assert.AreEqual(5, deletedCount);
        }
    }
}
