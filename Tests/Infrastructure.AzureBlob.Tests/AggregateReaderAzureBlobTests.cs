﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests
{
    [TestClass]
    public class AggregateReaderAzureBlobTests
    {
        private readonly AggregateRepositoryAzureBlob<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;
        private readonly AggregateReaderAzureBlob<AccountIdentifier, AccountModel> aggregateReader;
        private readonly Random random;
        private readonly IConfigurationRoot config;

        public AggregateReaderAzureBlobTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            random = new Random();
            aggregateRepository = new AggregateRepositoryAzureBlob<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"],
                "testaggregates");
            aggregateReader = new AggregateReaderAzureBlob<AccountIdentifier, AccountModel>(config["StorageConnectionString"], "testaggregates");
        }

        [TestMethod]
        public async Task CreateIfNotExistsAsync()
        {
            var aggregateAzureTableHelper = new AggregateAzureBlobHelper<AccountIdentifier>(config["StorageConnectionString"], "testaggregates");
            await aggregateAzureTableHelper.Container.CreateIfNotExistsAsync().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetByIdAsync()
        {
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateReader.GetByIdAsync(command.Identifier).ConfigureAwait(false);


        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task GetByIdAsync_MissingId_ObjectNotFoundException()
        {
            await aggregateReader.GetByIdAsync(new AccountIdentifier("1", random.Next().ToString())).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task GetAllAsync()
        {
            var parentId = random.Next().ToString();
            for (int i = 0; i < 5; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(parentId, random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllAsync(new PageParametersModel() { MaxItemCount = 11 }).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 5;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllSummariesAsync()
        {
            var parentId = random.Next().ToString();
            for (int i = 0; i < 10; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(parentId, random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllSummariesAsync(new PageParametersModel() { MaxItemCount = 11 }).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 10;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllByPartitionAsync()
        {

            var parentId = random.Next().ToString();
            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(parentId, random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }
            var partitionPrefixPath = aggregateReader.GetPartitionPrefixPathFromParentId(parentId);
            var results = await aggregateReader.GetAllByPartitionAsync(partitionPrefixPath, new PageParametersModel()).ConfigureAwait(false);


            Assert.AreEqual(2, results.Items.Count());
        }

        [TestMethod]
        public async Task GetAllSummariesByPartitionAsync()
        {

            var parentId = random.Next().ToString();
            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(parentId, random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }
            var partitionPrefixPath = aggregateReader.GetPartitionPrefixPathFromParentId(parentId);
            var results = await aggregateReader.GetAllSummariesByPartitionAsync(partitionPrefixPath, new PageParametersModel()).ConfigureAwait(false);


            Assert.AreEqual(2, results.Items.Count());
        }


        [TestMethod]
        public async Task GetAllAsync_MaxCount2()
        {

            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier("1", random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllAsync(new PageParametersModel() { MaxItemCount = 2 }).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());

        }

        [TestMethod]
        public async Task GetAllWithBlobLastModifiedAsync()
        {

            var results = await aggregateReader.GetAllWithBlobLastModifiedAsync(new PageParametersModel() { MaxItemCount = 10 }).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 5;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllByPartitionWithBlobLastModifiedAsync()
        {
            var parentId = "1994560503";// random.Next().ToString();
            var blobLastModifiedDate = DateTime.UtcNow.AddMinutes(-2);
            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier(parentId, random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }
            var partitionPrefixPath = aggregateReader.GetPartitionPrefixPathFromParentId(parentId);

            var results = await aggregateReader.GetAllByPartitionWithBlobLastModifiedAsync(
                partitionPrefixPath, 
                new PageParametersModel() { MaxItemCount = 10 },
                blobLastModifiedDate).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 1;

            Assert.AreEqual(true, isCountTenOrGreater);

        }




    }
}
