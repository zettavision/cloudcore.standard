﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests
{
    [TestClass]
    public class AggregateRepositoryAzureBlobTests
    {
        private readonly IConfigurationRoot config;
        private readonly AggregateRepositoryAzureBlob<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;

        public AggregateRepositoryAzureBlobTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregateRepository = new AggregateRepositoryAzureBlob<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"], "testaggregates", null);
        }

        [TestMethod]
        public async Task CreateIfNotExistsAsync()
        {
            var aggregateAzureBlobHelper = new AggregateAzureBlobHelper<AccountIdentifier>(config["StorageConnectionString"], "testaggregates");
            await aggregateAzureBlobHelper.Container.CreateIfNotExistsAsync(Azure.Storage.Blobs.Models.PublicAccessType.None, null, null).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task InsertSpeedTestAsync()
        {
            var random = new Random();

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var count = 0;

            var testDuration = 30;

            while (true)
            {


                var command = new AccountCommand<AccountCreateRequest>
                (
                    new AccountIdentifier("1", random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

                count++;

                if (stopwatch.Elapsed > TimeSpan.FromSeconds(testDuration))
                    break;
            }

            var requestPerSecond = (double)count / (double)testDuration;
        }

        [TestMethod]
        public async Task InsertAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectAlreadyExistsException))]
        public async Task InsertAsync_Duplicate_ObjectAlreadyExistsException()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task LoadAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.LoadAsync(new AccountIdentifier("1", command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task LoadWithETagErrorAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await Assert.ThrowsExceptionAsync<OptimisticConcurrencyException>(async () => await aggregateRepository.LoadAsync(new AccountIdentifier("1", command.Identifier.Id, "null")).ConfigureAwait(false));
 
        }

        [TestMethod]
        [ExpectedException(typeof(OptimisticConcurrencyException))]
        public async Task LoadAsync_InvalidETag_OptimisticConcurrencyException()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var response = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.LoadAsync(new AccountIdentifier("1", command.Identifier.Id, "\"InvalidETag\"")).ConfigureAwait(false);

        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task LoadAsync_Missing_ObjectNotFoundException()
        {
            var random = new Random();
            await aggregateRepository.LoadAsync(new AccountIdentifier("1", random.Next().ToString(), null)).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task UpdateAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier("1", command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(OptimisticConcurrencyException))]
        public async Task UpdateAsync_InvalidEtag_OptimisticConcurrencyException()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier("1", command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);
            aggregateWithMetadata.ETag = "W/\"datetime\'2016-11-04T19%3A41%3A25.1092927Z\'\"";

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task RoundTrip()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>(
                new AccountIdentifier(
                    "1", random.Next().ToString()), new AccountCreateRequest() { Name = "name", AdminUserId = "userid" }, "userid");
            var model = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(model.ParentId, model.Id)).ConfigureAwait(false);

            Assert.AreEqual(model.Name, aggregateWithMetadata.Aggregate.Name);

        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task UpdateAsync_MissingId_ObjectNotFound()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier("1", command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);

            var properties = aggregateWithMetadata.Aggregate.GetType().GetProperties();

            properties.First(p => p.Name == "Id").SetValue(aggregateWithMetadata.Aggregate, random.Next().ToString());


            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }
    }
}
