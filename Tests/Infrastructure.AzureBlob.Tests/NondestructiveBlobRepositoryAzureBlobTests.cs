﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests
{
    [TestClass]
    public class NondestructiveBlobRepositoryAzureBlobTests
    {
        private readonly IConfigurationRoot config;
        public NondestructiveBlobRepositoryAzureBlobTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            nondestructiveBlobRepository = new NondestructiveBlobRepositoryAzureBlob(config["StorageConnectionString"],
                "testblob", "http://www.testcdn.com/images");
            random = new Random();
        }



        private readonly NondestructiveBlobRepositoryAzureBlob nondestructiveBlobRepository;
        private readonly Random random;


        [TestMethod]
        public async Task InsertAsync()
        {
            await nondestructiveBlobRepository.InsertAsync(GetRandomName(), GetRandomBytes(10)).ConfigureAwait(false);
        }

        private byte[] GetRandomBytes(int length)
        {
            var random = new Random();
            var bytes = new byte[length];
            random.NextBytes(bytes);
            return bytes;
        }

        private string GetRandomName()
        {
            return random.Next().ToString();
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectAlreadyExistsException))]
        public async Task InsertAsync_Duplicate_ObjectAlreadyExistsException()
        {
            var blobName = GetRandomName();

            await nondestructiveBlobRepository.InsertAsync(blobName, GetRandomBytes(10)).ConfigureAwait(false);
            await nondestructiveBlobRepository.InsertAsync(blobName, GetRandomBytes(10)).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task InsertAsync_CdnUrl()
        {
            var blobName = GetRandomName();
            var expected = $"http://www.testcdn.com/images/{blobName}";
            var actual = await nondestructiveBlobRepository.InsertAsync(blobName, GetRandomBytes(10)).ConfigureAwait(false);

            Assert.AreEqual(expected, actual);
        }

    }
}
