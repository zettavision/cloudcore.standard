using System.Collections.Generic;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account
{
    public class AccountExtendedProperties : IAggregateExtendedProperties<AccountAggregate>
    {
        public IEnumerable<ExtendedProperty<AccountAggregate>> ExtendedProperties => new List<ExtendedProperty<AccountAggregate>>
        {
            new ExtendedProperty<AccountAggregate>("Name_LowerCase", typeof(string), aggregate => aggregate.Name.ToLowerInvariant() )
        };
    }
}