﻿using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account
{
    public class AccountAggregate : IAggregate
    {
        private string adminUserId;
        private string name;

        public AccountAggregate()
        {
        }

        public AccountAggregate(AccountCommand<AccountCreateRequest> command)
        {
            ParentId = command.Identifier.ParentId;
            Id = command.Identifier.Id;
            Name = command.Request.Name;
            AdminUserId = command.Request.AdminUserId;
        }

        [Identifier("Parents")]
        public string ParentId { get; private set; }

        [Identifier("Accounts")]
        public string Id { get; private set; }

        public string Name
        {
            get { return name; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Name));
                name = value;
            }
        }

        public string AdminUserId
        {
            get { return adminUserId; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(AdminUserId));
                adminUserId = value;
            }
        }

        public void Apply(AccountCommand<AccountChangeNameRequest> command)
        {
            Name = command.Request.Name;
        }

        public void Apply(AccountCommand<AccountChangeAdminUserIdRequest> command)
        {
            AdminUserId = command.Request.AdminUserId;
        }
    }
}