﻿using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account
{
    public class AccountIdentifier : AggregateIdentifier
    {
        public AccountIdentifier()
        {
        }

        public AccountIdentifier(string parentId, string id)
        {
            ParentId = parentId;
            Id = id;
        }

        public AccountIdentifier(string parentId, string id, string eTag)
        {
            ParentId = parentId;
            Id = id;
            ETag = eTag;
        }

        [Identifier("Parents")]
        public string ParentId { get; set; }

        [Identifier("Accounts")]
        public sealed override string Id { get; set; }



    }
}