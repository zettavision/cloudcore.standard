﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests
{
    public interface IAccountRequest : IAggregateRequest
    {
    }
}