﻿namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests
{
    public class AccountChangeNameRequest : IAccountRequest
    {
        public string Name { get; set; }
    }
}