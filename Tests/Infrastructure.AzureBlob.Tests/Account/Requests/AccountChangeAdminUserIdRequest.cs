﻿namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Requests
{
    public class AccountChangeAdminUserIdRequest : IAccountRequest
    {
        public string AdminUserId { get; set; }
    }
}