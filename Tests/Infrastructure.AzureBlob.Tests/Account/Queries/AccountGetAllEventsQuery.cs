using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Queries
{
    [Identifier("Events")]
    public class AccountGetAllEventsQuery
    {
        [Identifier("Parents")]
        public string ParentId { get; set; }

        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
        public PageParametersModel PageParameters { get; set; }
    }
}