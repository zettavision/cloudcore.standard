using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Queries
{
    public class AccountGetByIdQuery
    {
        [Identifier("Parents")]
        public string ParentId { get; set; }

        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
    }
}