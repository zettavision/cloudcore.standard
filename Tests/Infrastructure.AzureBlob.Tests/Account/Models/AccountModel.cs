﻿using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureBlob.Tests.Account.Models
{
    public class AccountModel : IAggregateModel
    {
        public string ParentId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string AdminUserId { get; set; }
        public string ETag { get; set; }
        public AggregateInfoModel AggregateInfo { get; set; }
    }
}