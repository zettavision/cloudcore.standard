﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ZettaVision.CloudCore.Domain.Infrastructure;

namespace ZettaVision.CloudCore.Infrastructure.Auth0Token.Tests
{
    [TestClass]
    public class Auth0TokenManagerWithMultipleClientsTests
    {
        private Auth0TokenManagerWithMultipleClients auth0TokenManagerWithMultipleClients;

        public Auth0TokenManagerWithMultipleClientsTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            auth0TokenManagerWithMultipleClients = new Auth0TokenManagerWithMultipleClients(
                            new Auth0Rs256TokenManager(config["Auth0ClientSigningCertificate"]),
                            new List<Auth0TokenManager>()
                            {
                              new Auth0TokenManager(
                                 config["Auth0RegularWeb:Name"],
                                 config["Auth0RegularWeb:Domain"],
                                 config["Auth0RegularWeb:ClientId"],
                                 config["Auth0RegularWeb:ClientSecret"], false)
                            });
        }

        [TestMethod]
        public void ValidateTokenIssuerSigningKeyForRS256SignedJwtToken()
        {
            var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik0wSTVOMEpFTWpJNVFrTXpSRFEwT0VGRU0wUkNSa00zTkVNNE5ESTVOMFUyTlVZelEwRkVNZyJ9.eyJodHRwczovL3d3dy56ZXR0YXZpc2lvbi5jb20vcm9sZXMiOlsiRXZlQWRtaW5pc3RyYXRvciIsIkV2ZVN5c3RlbUFkbWluIiwiSW5zaWdodHNBZG1pbmlzdHJhdG9yIiwiSW5zaWdodHNTeXN0ZW1BZG1pbiJdLCJlbWFpbCI6InByYXNhbnRoLm1hZGRhbGFAdmFsdWVsYWJzLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL3p5dG90ZXN0LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZTMxZjI5YzI5YjQxMzBlOTE0ZDdjZjUiLCJhdWQiOiJBWURpRjg1Y1I2Q2ZaTjZ3WmR1cDRTUzdqWnA3Q0hMMSIsImlhdCI6MTYyMTI3MjA0NCwiZXhwIjoxNjIxODc2ODQ0LCJhdF9oYXNoIjoidFlfb3h3OEdQOHNJSjB0Q0VRSWN6ZyIsIm5vbmNlIjoiZEl4RFRocGFCbDlQclJpRWFIMzZMQ2NqbFQtS1pUcE8ifQ.QeJv9Gj_jvekcpNb50v-UOZkQ0kzjfgerKLVI2DXIh85qybliqWJxOpY-6EhzoOr6ZrM7Honrm5sSsbjg0Uvfscs-jHW7sr5PJYn4K6GeWpAwkFl_wJPJeDPF4q6k_RLEQOYruyQvxsWZ-HpSs0PTDPpeBb0sFhuU38JxwT9grhCiS-wpZPz_QTcio_6aHwhRYHdqqMvDzoMmbSuuoX99QLg36JnROSqrZ2jfZG9ckpVONezUUFa5xTVnfOZPL4sEkLfWUcJ1L7zVyNtEHtHLz1WBokXyrDwzFIGxp4apI-R4b2lbbhWs8UYOoDVZQfD7XfHltv4Gn7UJwy0CsPJGQ";
            auth0TokenManagerWithMultipleClients.ValidateTokenIssuerSigningKey(token);
            var payload = auth0TokenManagerWithMultipleClients.GetPayload(token);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedAccessException))]
        public void ValidateTokenIssuerSigningKeyForRS256SignedJwtToken_ThrowExceptionWhenJwtTokenChanges()
        {
            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwczovL3d3dy56ZXR0YXZpc2lvbi5jb20vcm9sZXMiOlsiRXZlQWRtaW5pc3RyYXRvciIsIkV2ZVN5c3RlbUFkbWluIiwiSW5zaWdodHNBZG1pbmlzdHJhdG9yIiwiSW5zaWdodHNTeXN0ZW1BZG1pbiJdLCJlbWFpbCI6InByYXNhbnRoLm1hZGRhbGFAdmFsdWVsYWJzLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL3p5dG90ZXN0LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZTMxZjI5YzI5YjQxMzBlOTE0ZDdjZjUiLCJhdWQiOiJBWURpRjg1Y1I2Q2ZaTjZ3WmR1cDRTUzdqWnA3Q0hMMSIsImlhdCI6MTYyMTI3MjA0NCwiZXhwIjoxNjIxMzYxODYzLCJhdF9oYXNoIjoidFlfb3h3OEdQOHNJSjB0Q0VRSWN6ZyIsIm5vbmNlIjoiZEl4RFRocGFCbDlQclJpRWFIMzZMQ2NqbFQtS1pUcE8iLCJqdGkiOiJhMGUwOGU0Yi1iNmZhLTRmNTgtOGRlNS0xM2YxOWQ3Zjc1ODcifQ.ffWkGVH4UBhv_PLt5YrjOC42PdV4FSqwC5I-mj-tMKg";
            var payload = auth0TokenManagerWithMultipleClients.GetPayload(token);
            auth0TokenManagerWithMultipleClients.ValidateTokenIssuerSigningKey(token);
        }

        [TestMethod]
        public void ValidateTokenIssuerSigningKeyForHS256SignedJwtToken()
        {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwczovL3d3dy56ZXR0YXZpc2lvbi5jb20vcm9sZXMiOlsiRXZlQWRtaW5pc3RyYXRvciIsIkV2ZVN5c3RlbUFkbWluIiwiSW5zaWdodHNBZG1pbmlzdHJhdG9yIiwiSW5zaWdodHNTeXN0ZW1BZG1pbiJdLCJuaWNrbmFtZSI6InByYXNhbnRoLm1hZGRhbGEiLCJuYW1lIjoicHJhc2FudGgubWFkZGFsYUB2YWx1ZWxhYnMuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyL2I2NzNhZDM1MWIwN2ZjNTMwZmMzOWFhNDUxNDBjMzc3P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGcHIucG5nIiwidXBkYXRlZF9hdCI6IjIwMjEtMDUtMThUMTc6MTE6NDYuNDA4WiIsImVtYWlsIjoicHJhc2FudGgubWFkZGFsYUB2YWx1ZWxhYnMuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8venl0b3Rlc3QuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVlMzFmMjljMjliNDEzMGU5MTRkN2NmNSIsImF1ZCI6Imx3WnJ0a0NRdnpTQTlMTVNsbGQybmY4RUdBcXNmZEFZIiwiaWF0IjoxNjIxMzU3OTA2LCJleHAiOjE2MjE5NjI3MDZ9.B_tZ6CgxRarVHkZ4-4ZbLd_HrGeE9cF8laApQBrrxmg";
            auth0TokenManagerWithMultipleClients.ValidateTokenIssuerSigningKey(token);
            var payload = auth0TokenManagerWithMultipleClients.GetPayload(token);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedAccessException))]
        public void ValidateTokenIssuerSigningKeyForJS256SignedJwtToken_ThrowExceptionWhenJwtTokenChanges()
        {
            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwczovL3d3dy56ZXR0YXZpc2lvbi5jb20vcm9sZXMiOlsiRXZlQWRtaW5pc3RyYXRvciIsIkV2ZVN5c3RlbUFkbWluIiwiSW5zaWdodHNBZG1pbmlzdHJhdG9yIiwiSW5zaWdodHNTeXN0ZW1BZG1pbiJdLCJuaWNrbmFtZSI6InByYXNhbnRoLm1hZGRhbGEiLCJuYW1lIjoicHJhc2FudGgubWFkZGFsYUB2YWx1ZWxhYnMuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyL2I2NzNhZDM1MWIwN2ZjNTMwZmMzOWFhNDUxNDBjMzc3P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGcHIucG5nIiwidXBkYXRlZF9hdCI6IjIwMjEtMDUtMThUMTc6MTE6NDYuNDA4WiIsImVtYWlsIjoicHJhc2FudGgubWFkZGFsYUB2YWx1ZWxhYnMuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8venl0b3Rlc3QuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVlMzFmMjljMjliNDEzMGU5MTRkN2NmNSIsImF1ZCI6Imx3WnJ0a0NRdnpTQTlMTVNsbGQybmY4RUdBcXNmZEFZIiwiaWF0IjoxNjIxMzU3OTA2LCJleHAiOjE2MjEzNjE5NzksImp0aSI6Ijk0ZjFkOWIzLWZhNDEtNDIzYy1iZmRiLTQyZmFjZjI5ODY2ZCJ9.oMnN7WDLZXiGxkmCusqK6mNT8RENcvH_XXe4NuO2jsA";
            var payload = auth0TokenManagerWithMultipleClients.GetPayload(token);
            auth0TokenManagerWithMultipleClients.ValidateTokenIssuerSigningKey(token);
        }
    }
}
