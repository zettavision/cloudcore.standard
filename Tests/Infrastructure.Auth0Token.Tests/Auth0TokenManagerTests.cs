﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Auth0Token.Tests
{
    [TestClass]
    public class Auth0TokenManagerTests
    {
        private ZettaVision.CloudCore.Infrastructure.Auth0Token.Auth0TokenManager auth0TokenManager;

        public Auth0TokenManagerTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            auth0TokenManager = new ZettaVision.CloudCore.Infrastructure.Auth0Token.Auth0TokenManager(
                config["Auth0Name"], 
                config["Auth0Domain"], 
                config["Auth0ClientId"], 
                config["Auth0ClientSecret"], false);
        }

        [TestMethod]
        public void RoundTrip()
        {
            var token = auth0TokenManager.BuildToken(new[] {"test", "test2"}, "userid", null);
            var payload = auth0TokenManager.GetPayload(token);

            Assert.AreEqual(2, payload.Roles.Count());
        }

        [TestMethod]
        public void Decode()
        {
            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Imhhcm9vbi5hYmJhc2lAdGlua2VydmVudHVyZXMuY28iLCJwaWN0dXJlIjoiaHR0cHM6Ly9zLmdyYXZhdGFyLmNvbS9hdmF0YXIvMWZjNDI1MDgxZTEyNWVkZTlmN2MwZjc5MDhmZmY0Yjk_cz00ODAmcj1wZyZkPWh0dHBzJTNBJTJGJTJGY2RuLmF1dGgwLmNvbSUyRmF2YXRhcnMlMkZoYS5wbmciLCJuaWNrbmFtZSI6Imhhcm9vbi5hYmJhc2kiLCJuYW1lIjoiaGFyb29uLmFiYmFzaUB0aW5rZXJ2ZW50dXJlcy5jbyIsInVzZXJfbWV0YWRhdGEiOnsiZ2l2ZW5fbmFtZSI6Ikhhcm9vbiBGaXJzdE5hbWUiLCJmYW1pbHlfbmFtZSI6IkFiYmFzaSBMYXN0bmFtZSIsIm5hbWUiOiJIYXJvb24gRmlyc3ROYW1lIEFiYmFzaSBMYXN0bmFtZSIsInBpY3R1cmUiOiJodHRwczovL3Rlc3QtY2RuLnp5dG8uY29tL3phcHByb2ZpbGVpbWFnZXMvMmQ1ZTVjMWI5Y2U2NGU2Yjk3MWRhN2M1MDEwMjlmNmNfMzQyYTNmODQzMTIyNDlkNGJjOTQxYzM4NTdkMzM5YWYuanBnIn0sImFwcF9tZXRhZGF0YSI6eyJyb2xlcyI6WyJQcm9kdWN0WEFjY291bnRDdXN0b21lciIsIlByb2R1Y3RYQWNjb3VudEhlYWx0aFByb2Zlc3Npb25hbCJdfSwicm9sZXMiOlsiUHJvZHVjdFhBY2NvdW50Q3VzdG9tZXIiLCJQcm9kdWN0WEFjY291bnRIZWFsdGhQcm9mZXNzaW9uYWwiXSwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJ1c2VyX2lkIjoiYXV0aDB8NTk4NDhiNWFhOGY4MGE0MGMwYTM1YmE5IiwiY2xpZW50SUQiOiJsd1pydGtDUXZ6U0E5TE1TbGxkMm5mOEVHQXFzZmRBWSIsImlkZW50aXRpZXMiOlt7InVzZXJfaWQiOiI1OTg0OGI1YWE4ZjgwYTQwYzBhMzViYTkiLCJwcm92aWRlciI6ImF1dGgwIiwiY29ubmVjdGlvbiI6IlVzZXJuYW1lLVBhc3N3b3JkLUF1dGhlbnRpY2F0aW9uIiwiaXNTb2NpYWwiOmZhbHNlfV0sInVwZGF0ZWRfYXQiOiIyMDE3LTA4LTE2VDE4OjI0OjI5LjA3OVoiLCJjcmVhdGVkX2F0IjoiMjAxNy0wOC0wNFQxNDo1NzozMC40MjlaIiwiaXNzIjoiaHR0cHM6Ly96eXRvdGVzdC5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTk4NDhiNWFhOGY4MGE0MGMwYTM1YmE5IiwiYXVkIjoibHdacnRrQ1F2elNBOUxNU2xsZDJuZjhFR0Fxc2ZkQVkiLCJleHAiOjE1MDM1MTI2NjksImlhdCI6MTUwMjkwNzg2OX0.KQlxOKfRnGu67KtKhWAg_5ZSs7YmUfSOWkr3KbHkPCE";
            auth0TokenManager.ValidateTokenIssuerSigningKey(token);
            var payload = auth0TokenManager.GetPayload(token);

        }



        
    }
}
