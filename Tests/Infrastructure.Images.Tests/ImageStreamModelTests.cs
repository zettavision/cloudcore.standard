﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.Images;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZettaVision.Infrastructure.Images.Tests
{
    [TestClass]
    public class ImageStreamModelTests
    {


        [TestMethod]
        public void FixOrientationTest()
        {
            var fileStream = File.Open(@"C:\Users\prasanth.maddala\Downloads\faces\prasanth.jpg", FileMode.Open);
            var file = new FileStreamModel()
            {
                ContentType = "image/jpeg",
                FileName = "prasanth.jpg",
                Length = fileStream.Length,
                Stream = fileStream
            };
            var image = new ImageStreamModel(file);

            var fixedImage = image.FixOrientation();

            using (var fileStream2 = File.Create(@"C:\Users\prasanth.maddala\Downloads\faces\prasanth_fixed.jpg"))
            {
                fixedImage.Stream.Seek(0, SeekOrigin.Begin);
                fixedImage.Stream.CopyTo(fileStream2);
            }
      
        }

        [TestMethod]
        public void RotateTest()
        {
            var fileStream = File.Open(@"C:\Temp\testface\2x.jpg", FileMode.Open);
            var file = new FileStreamModel()
            {
                ContentType = "image/jpeg",
                FileName = "2x.jpg",
                Length = fileStream.Length,
                Stream = fileStream
            };
            var image = new ImageStreamModel(file);
            image = image.FixOrientation();

            var fixedImage = image.Rotate(RotateFlipType.Rotate90FlipNone);

            using (var fileStream2 = File.Create(@"C:\Temp\testface\2x_Rotate90FlipNone_2.jpg"))
            {
                fixedImage.Stream.Seek(0, SeekOrigin.Begin);
                fixedImage.Stream.CopyTo(fileStream2);
            }

        }
    }
}
