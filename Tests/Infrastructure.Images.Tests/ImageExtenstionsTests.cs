﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using ZettaVision.CloudCore.Infrastructure.Images;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZettaVision.Infrastructure.Images.Tests
{
    [TestClass]
    public class ImageExtenstionsTests
    {
        [TestMethod]
        public void ResizeTest()
        {
            var image = Image.FromFile(@"C:\Users\prasanth.maddala\Downloads\BigImage.jpg");

            using (var newBitmap = image.Resize(350, 350))
            {
                var newStream = newBitmap.SaveToMemoryStream(ImageContentTypes.Jpeg);

            }
        }

    }
}
