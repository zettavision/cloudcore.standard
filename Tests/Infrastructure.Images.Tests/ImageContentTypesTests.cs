﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeTypes;
using ZettaVision.CloudCore.Infrastructure.Images;

namespace ZettaVision.Infrastructure.Images.Tests
{
    [TestClass]
    public class ImageContentTypesTests
    {
        [TestMethod]
        public void RoundTrip()
        {
            foreach (var imageType in ImageContentTypes.All)
            {
                var fileExtension =  MimeTypeMap.GetExtension(imageType);
                var newImageType = MimeTypeMap.GetMimeType(fileExtension);


                Assert.AreEqual(imageType, newImageType);
            }


        }


    }
}
