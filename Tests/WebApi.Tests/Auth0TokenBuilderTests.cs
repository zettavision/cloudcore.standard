﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using ZettaVision.CloudCore.Infrastructure.Auth0Token;

namespace ZettaVision.CloudCore.WebApi.Tests
{
    [TestClass]
    public class Auth0TokenBuilderTests
    {
        [TestMethod]
        public void GenerateSampleToken()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            string auth0Name = config["Auth0Name"];
            string auth0Domain = config["Auth0Domain"];
            string auth0ClientId = config["Auth0ClientId"];
            string auth0ClientSecret = config["Auth0ClientSecret"];

            var tokenBuilder = new Auth0TokenManager(auth0Name, auth0Domain, auth0ClientId, auth0ClientSecret);

            var token = tokenBuilder.BuildToken(new string[] {"SystemAdmin"});
        }

        [TestMethod]
        public void ReadTest()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            string auth0Name = config["Auth0Name"];
            string auth0Domain = config["Auth0Domain"];
            string auth0ClientId = config["Auth0ClientId"];
            string auth0ClientSecret = config["Auth0ClientSecret"];
            var auth0TokenManager = new Auth0TokenManager(auth0Name, auth0Domain, auth0ClientId, auth0ClientSecret);

            var token = auth0TokenManager.BuildToken(new string[] { "SystemAdmin" }, null, null);

            string auth0Name2 = config["Auth0Name"];
            string auth0Domain2 = config["Auth0Domain"];
            string auth0ClientId2 = config["Auth0ClientId"];
            string auth0ClientSecret2 = "YdFTKNG4O1pesqWGF-aZObLQ-alrldJVyJgRiN6J6UnjZ3Yxo8lD_Ahpk3F8Igtn";
            var auth0TokenManager2 = new Auth0TokenManager(auth0Name2, auth0Domain2, auth0ClientId2, auth0ClientSecret2);

            var payload = auth0TokenManager2.GetPayload(token);

            auth0TokenManager2.ValidateTokenIssuerSigningKey(token);
        }
    }
}
