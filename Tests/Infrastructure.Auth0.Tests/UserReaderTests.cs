﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Commands;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Domain.User.Models;
using ZettaVision.CloudCore.Domain.User.Queries;
using ZettaVision.CloudCore.Infrastructure.Auth0.Tests.Factories;
using ZettaVision.CloudCore.Infrastructure.Auth0.User;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Tests
{
    [TestClass]
    public class UserReaderTests
    {
        private readonly UserReader userReader;
        private readonly string userId;
        public UserReaderTests()
        {
            userReader = UserReaderFactory.GetUserReader();

            userId = "facebook|1033418180071417";
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task UserGetByIdQueryWithInvalidId()
        {
            var model = await userReader.HandleAsync(new UserGetByIdQuery() {Id = "facebook|1010080954414078411" }).ConfigureAwait(false);
            Assert.AreEqual(userId, model.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task UserGetByIdQueryWithInvalidFormat()
        {
            var model = await userReader.HandleAsync(new UserGetByIdQuery() { Id = "1782330" }).ConfigureAwait(false);
            Assert.AreEqual(userId, model.Id);
        }

        [TestMethod]
        public async Task UserGetByIdQuery()
        {
            var model = await userReader.HandleAsync(new UserGetByIdQuery() { Id = "facebook|1033418180071417" }).ConfigureAwait(false);
            Assert.AreEqual(userId, model.Id);
        }

        [TestMethod]
        public async Task UserGetAllQuery()
        {
            var result = await userReader.HandleAsync(new UserGetAllQuery() {PageParameters = new PageParametersModel() {MaxItemCount = 2} }).ConfigureAwait(false);
     
        }

        [TestMethod]
        public async Task UserGetAllWithLastModifiedDateOnOrAfterQuery()
        {
            var lastModifiedDateOnOrAfter = DateTime.Parse("2020-05-20T00:49:28.3090000");
       

            var result = await userReader.HandleAsync(new UserGetAllQuery()
            {
                PageParameters = new PageParametersModel() {MaxItemCount = null},
                LastModifiedDateOnOrAfter = null,
                Sort = UserSortPropertyEnum.LastModifiedDate,
                SortAscending = true
            }).ConfigureAwait(false);
     
        }

        [TestMethod]
        public async Task UserGetAllQueryWithInvalidIdFormat()
        {
            var result = await userReader.HandleAsync(new UserGetAllQuery() { Ids = new []{ "1782330" } ,PageParameters = new PageParametersModel() { MaxItemCount = 2 } }).ConfigureAwait(false);

        }

        [TestMethod]
        public async Task UserGetAllQuery_GetPages()
        {
            var result = await userReader.HandleAsync(new UserGetAllQuery() ).ConfigureAwait(false);

            while (result.ContinuationToken != null)
            {
                result = await userReader.HandleAsync(new UserGetAllQuery() { PageParameters = new PageParametersModel() { ContinuationToken = result.ContinuationToken} }).ConfigureAwait(false);

            }
        }

        [TestMethod]
        public async Task UserGetAllQuery_SearchEmail()
        {
            var result = await userReader.HandleAsync(new UserGetAllQuery() {Email = "paul@zettavision.com" }).ConfigureAwait(false);


        }

        [TestMethod]
        public async Task UserGetAllQuery_SearchWithIds()
        {
            var result = await userReader.HandleAsync(new UserGetAllQuery() { Ids = new List<string> { "auth0|587f1ee1c41ff353fbd39b72", "auth0|587f20a977fa2f4cff093a8f" } }).ConfigureAwait(false);


        }


    }
}
