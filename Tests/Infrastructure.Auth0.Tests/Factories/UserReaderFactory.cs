﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Configuration;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;
using ZettaVision.CloudCore.Infrastructure.Auth0.User;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Tests.Factories
{
    public static class UserReaderFactory
    {

        public static UserReader GetUserReader()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            var apiTokenFactory = new ApiTokenFactory(new CachingService(),
                config["Auth0Domain"],
                config["Auth0ClientId"],
                config["Auth0ClientSecret"]);

            var managementApiClientFactory =
                new ManagementApiClientFactory(apiTokenFactory, config["Auth0Domain"]);

            var userReader = new UserReader(managementApiClientFactory);

            return userReader;
        }
    }
}
