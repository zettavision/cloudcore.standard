﻿using System.Configuration;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Tests
{
    [TestClass]
    public class UserTokenFactoryTests
    {
        private readonly UserTokenFactory userTokenFactory;

        public UserTokenFactoryTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            this.userTokenFactory = new UserTokenFactory(new CachingService(),
                config["Auth0Domain"],
                config["Auth0ClientId"],
                config["Auth0ClientSecret"],
                config["Auth0UserName"],
                config["Auth0UserPassword"]
                );


        }

        [TestMethod]
        public async Task GetTokenTest()
        {
            var result = await userTokenFactory.GetTokenAsync().ConfigureAwait(false);
        }
    }
}