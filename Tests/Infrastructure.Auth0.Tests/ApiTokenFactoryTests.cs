﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Tests
{
    [TestClass]
    public class ApiTokenFactoryTests
    {
        private readonly ApiTokenFactory apiTokenFactory;

        public ApiTokenFactoryTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            this.apiTokenFactory = new ApiTokenFactory(new CachingService(),
                config["Auth0Domain"],
                config["Auth0ClientId"],
                config["Auth0ClientSecret"]);
        }

        [TestMethod]
        public async Task GetTokenTest()
        {
            var result = await apiTokenFactory.GetTokenAsync().ConfigureAwait(false);
        }

    }
}
