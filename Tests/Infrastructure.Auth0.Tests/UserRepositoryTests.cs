﻿using Auth0.Core.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Commands;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.Auth0.Tests.Factories;
using ZettaVision.CloudCore.Infrastructure.Auth0.User;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Tests
{
    [TestClass]
    public class UserRepositoryTests
    {
        private readonly UserRepository userRepository;
        private readonly string userId;
        public UserRepositoryTests()
        {

            userRepository = UserRepositoryFactory.GetUserRepository();

            userId = "facebook|1033418180071417";
        }

        [TestMethod]
        public async Task LoadAsync()
        {
            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(userId)).ConfigureAwait(false);
            Assert.AreEqual(userId, aggregateWithMetadata.Aggregate.Id);
        }

        [TestMethod]
        public async Task UpdateAsync_Roles()
        {
            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(userId)).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserReplaceRoleSubsetRequest() {Roles = new List<string>() { "Test1", "Test3" }, RoleSubset = new List<string>() { "Test1", "Test2", "Test3" } }); 
            var model = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);

            //Assert.Contains("Test1", model.Roles);
            //Assert.DoesNotContain("Test2", model.Roles);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task UpdateAsync_Roles_WithInvalidIdFormat()
        {
            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier("1782330")).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserReplaceRoleSubsetRequest() { Roles = new List<string>() { "Test1", "Test3" }, RoleSubset = new List<string>() { "Test1", "Test2", "Test3" } });
            var model = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);

            //Assert.Contains("Test1", model.Roles);
            //Assert.DoesNotContain("Test2", model.Roles);
        }

        [TestMethod]
        public async Task InsertAsync()
        {
            var aggregate = new UserAggregate(new UserCreateRequest() {Email = GuidId.GetNewId()+"@test.com"});

            var model = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);

        }

        [TestMethod]
        public async Task DeleteAsync()
        {
            await userRepository.DeleteAsync(new UserIdentifier() { Id = "facebook|1033418180071417" }).ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectAlreadyExistsException))]
        public async Task InsertDuplicateAsync()
        {
            var email = GuidId.GetNewId() + "@test.com";

            var aggregate = new UserAggregate(new UserCreateRequest() { Email = email });
            var model = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);
            var model2 = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task UpdateAsync_Email()
        {
            var aggregate = new UserAggregate(new UserCreateRequest() { Email = GuidId.GetNewId() + "@test.com" });
            var model1 = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);

            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(model1.Id)).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserChangeEmailRequest() { Email = GuidId.GetNewId() + "@test.com" });
            var model2 = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task UpdateAsync_PictureFromUser()
        {
            var aggregate = new UserAggregate(new UserCreateRequest() { Email = GuidId.GetNewId() + "@mpreddy.me" });
            var model1 = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);

            var pictureUrl =
                "https://ab-prod-media-assets.s3.amazonaws.com/296/alumsite/event-pictures/alumni-meet-for-vision/vision.jpg";

            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(model1.Id)).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserChangePictureFromUserWithUrlRequest() { PictureFromUser =  pictureUrl});
            var model2 = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);

            Assert.AreEqual(pictureUrl, model2.PictureFromUser);
        }

        [TestMethod]
        public async Task UpdateAsync_Pin()
        {
            var aggregate = new UserAggregate(new UserCreateRequest() { Email = GuidId.GetNewId() + "@mpreddy.me" });
            var model1 = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);

            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(model1.Id)).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserChangePinWithSaltRequest() { Pin = "1234", Salt = "ce76240c-1234-1234-1234-37f7aeb41100" });
            var model2 = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);


        }

        [TestMethod]
        public async Task UpdateAsync_Name()
        {
            var aggregate = new UserAggregate(new UserCreateRequest() { Email = GuidId.GetNewId() + "@mpreddy.me" });
            var model1 = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);

            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(model1.Id)).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserChangeNameRequest() { GivenName = "GivenName", FamilyName = "FamilyName" });
            var model2 = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);


        }

        [TestMethod]
        public async Task UpdateAsync_Phone()
        {
            var aggregate = new UserAggregate(new UserCreateRequest() { Email = GuidId.GetNewId() + "@mpreddy.me" });
            var model1 = await userRepository.InsertAsync(aggregate).ConfigureAwait(false);

            var aggregateWithMetadata = await userRepository.LoadAsync(new UserIdentifier(model1.Id)).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(new UserChangePhoneRequest() { Phone = "+18013014117" });
            var model2 = await userRepository.UpdateAsync(aggregateWithMetadata).ConfigureAwait(false);


        }
    }
}
