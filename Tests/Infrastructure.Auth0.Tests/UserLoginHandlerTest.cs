﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;
using ZettaVision.CloudCore.Infrastructure.Auth0.Tests.Factories;
using ZettaVision.CloudCore.Infrastructure.Auth0.User;
using ZettaVision.CloudCore.Infrastructure.Auth0Token;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Tests
{
    [TestClass]
    public class UserLoginHandlerTest
    {
        private readonly UserLoginHandler userLoginHandler;
        private readonly UserRepository userRepository;

        public UserLoginHandlerTest()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            IAuth0TokenManager auth0TokenManager = new Auth0TokenManager(
                config["Auth0Name"],
                config["Auth0Domain"],
                config["Auth0ClientId"],
                config["Auth0ClientSecret"],
                false);

            userLoginHandler = new UserLoginHandler(auth0TokenManager, config["Auth0Domain"], config["Auth0ClientId"], config["Auth0ClientSecret"]);
            userRepository = UserRepositoryFactory.GetUserRepository();
        }

        [TestMethod]
        public async Task TestLoginWithCreate()
        {
            var email = GuidId.GetNewId() + "@test.com";
            var password = "Test@123";

            var aggregate = new UserAggregate(new UserCreateRequest() {Email = email});

            var model = await userRepository.InsertAsync(aggregate, password).ConfigureAwait(false);


            var result = await userLoginHandler.HandleAsync(new UserLoginRequest()
            {
                Password = password,
                UserName = email
            }).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task TestLogin()
        {
            var result = await userLoginHandler.HandleAsync(new UserLoginRequest()
            {
                Password = "Test@123",
                UserName = "developer@mpreddy.me"
            }).ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task Test2Login()
        {
            var result = await userLoginHandler.HandleAsync(new UserLoginRequest()
            {
                Password = null,
                UserName = null
            }).ConfigureAwait(false);
        }
    }
}
