﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Request;
using ZettaVision.CloudCore.Infrastructure.AzureTables.AggregateEvent;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests
{
    [TestClass]
    public class AggregateEventRepositoryAzureTablesTests
    {

        public AggregateEventRepositoryAzureTablesTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregateRepository = new AggregateEventRepositoryAzureTables(config["StorageConnectionString"]);
        }



        private readonly AggregateEventRepositoryAzureTables aggregateRepository;

        [TestMethod]
        public async Task LoadAsync()
        {
            for (int i = 1; i < 5; i++)
            {
                var request = new AccountChangeNameRequest() {Name = "name"};
                var aggregate = new AggregateEventAggregate(new AggregateEventAddRequest()
                {
                    AggregateId = $"accounts.test.1",
                    Id = i.ToString(),
                    Request = request
                });
                var model = await aggregateRepository.InsertAsync(aggregate, "userid").ConfigureAwait(false);
            }

            //var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AggregateEventIdentifier(model.AggregateId, model.Id)).ConfigureAwait(false);

            //var request2 = (AccountChangeNameRequest) aggregateWithMetadata.Aggregate.Request;
            //Assert.Equal(request.Name, request2.Name);
        }
    }
}
