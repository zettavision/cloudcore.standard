﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Queries;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.AggregatePointInTimeSnapshot;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.AggregatePointInTimeSnapshot
{
    [TestClass]
    public class AggregatePointInTimeSnapshotAzureTablesTests
    {
        public AggregatePointInTimeSnapshotAzureTablesTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            reader = new AggregatePointInTimeSnapshotReaderAzureTables<AccountIdentifier, AccountModel>(
                config["StorageConnectionString"], "pointintimesnapshot");

            repository = new AggregatePointInTimeSnapshotRepositoryAzureTables<AccountIdentifier, AccountModel>(
                config["StorageConnectionString"], "pointintimesnapshot");
        }

        private readonly AggregatePointInTimeSnapshotRepositoryAzureTables<AccountIdentifier, AccountModel> repository;

        [TestMethod]
        public async Task InsertAsync()
        {
            var random = new Random();
            var utcNow = DateTime.UtcNow;

            AccountIdentifier identifier = new AccountIdentifier("1", "1");
            AccountModel model = new AccountModel()
            { 
                ParentId = identifier.ParentId,
                Id = identifier.Id,
                ETag = "\"etag\"",
                AdminUserId = "2",
                AggregateInfo = new AggregateInfoModel()
                {
                    CreatedDate = utcNow,
                    LastModifiedDate = utcNow,
                    Version = 2
                },
                Name = "name"
            };


            await repository.InsertAsync(identifier, model).ConfigureAwait(false);
        }


        private readonly AggregatePointInTimeSnapshotReaderAzureTables<AccountIdentifier, AccountModel> reader;

        [TestMethod]
        public async Task GetAllAsync()
        {
            AccountIdentifier identifier = new AccountIdentifier("1", "1");

            var result = await reader.HandleAsync(new AggregatePointInTimeSnapshotGetAllQuery<AccountIdentifier>()
            {
                Identifier = identifier
            }).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetByPointInTimeAsync()
        {
            AccountIdentifier identifier = new AccountIdentifier("1", "1");

            var result = await reader.HandleAsync(new AggregatePointInTimeSnapshotGetByPointInTimeQuery<AccountIdentifier>()
                {
                    Identifier = identifier,
                    PointInTime = DateTime.UtcNow.AddDays(1)
                }
            ).ConfigureAwait(false);
        }
    }
}
