﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests
{
    [TestClass]
    public class AggregateRepositoryAzureTablesTests
    {
        private readonly IConfigurationRoot config;
        public AggregateRepositoryAzureTablesTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregateRepository = new AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"], KeyFormatter.InvertAndPadForInt64, null, new AccountExtendedProperties());
        }



        private readonly AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;



        [TestMethod]
        public async Task CreateIfNotExistsAsync()
        {
            var aggregateAzureTableHelper = new AggregateAzureTableHelper<AccountIdentifier>(config["StorageConnectionString"]);

            await aggregateAzureTableHelper.Table.CreateIfNotExistsAsync().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task InsertAsync()
        {
            var random = new Random();



            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() {Name = "name", AdminUserId = "userid"},
                "userid"
                );
       
            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectAlreadyExistsException))]
        public async Task InsertAsync_Duplicate_ObjectAlreadyExistsException()
        {
            var random = new Random();

   
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task LoadAsync()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.ParentId, command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task LoadAsync_RoundTrip()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var model = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.ParentId, command.Identifier.Id, model.ETag)).ConfigureAwait(false);

            Assert.AreEqual(model.Id, aggregateWithMetadata.Aggregate.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(OptimisticConcurrencyException))]
        public async Task LoadAsync_InvalidETag_OptimisticConcurrencyException()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.ParentId, command.Identifier.Id, "\"InvalidETag\"")).ConfigureAwait(false);
        }



        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task LoadAsync_Missing_ObjectNotFoundException()
        {
            var random = new Random();
            await aggregateRepository.LoadAsync(new AccountIdentifier(random.Next().ToString(), null)).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task UpdateAsync()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.ParentId, command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(OptimisticConcurrencyException))]
        public async Task UpdateAsync_InvalidEtag_OptimisticConcurrencyException()
        {
            var random = new Random();


            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.ParentId, command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);
            aggregateWithMetadata.ETag = "W/\"datetime\'2016-11-04T19%3A41%3A25.1092927Z\'\"";

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task UpdateAsync_MissingId_ObjectNotFound()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            var insertResult = await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            var aggregateWithMetadata = await aggregateRepository.LoadAsync(new AccountIdentifier(command.Identifier.Id, insertResult.ETag)).ConfigureAwait(false);

            var properties = aggregateWithMetadata.Aggregate.GetType().GetProperties();

            properties.First(p => p.Name == "Id").SetValue(aggregateWithMetadata.Aggregate, random.Next().ToString());
            

            await aggregateRepository.UpdateAsync(aggregateWithMetadata, "userid").ConfigureAwait(false);
        }
    }
}
