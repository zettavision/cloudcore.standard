using System.Collections.Generic;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account
{
    public class AccountExtendedProperties : IAggregateExtendedProperties<AccountAggregate>
    {
        public IEnumerable<ExtendedProperty<AccountAggregate>> ExtendedProperties => new List<ExtendedProperty<AccountAggregate>>
        {
            new ExtendedProperty<AccountAggregate>("Name", typeof(string), aggregate => aggregate.Name )
        };
    }
}