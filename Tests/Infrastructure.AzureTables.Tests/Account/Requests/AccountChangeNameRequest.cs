﻿namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests
{
    public class AccountChangeNameRequest : IAccountRequest
    {
        public string Name { get; set; }
    }
}