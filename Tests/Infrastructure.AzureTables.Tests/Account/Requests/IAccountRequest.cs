﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests
{
    public interface IAccountRequest : IAggregateRequest
    {
    }
}