﻿namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests
{
    public class AccountChangeAdminUserIdRequest : IAccountRequest
    {
        public string AdminUserId { get; set; }
    }
}