﻿using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account
{
    public class AccountIdentifier : AggregateIdentifier
    {
        public AccountIdentifier()
        {
        }
        
        public AccountIdentifier(string parentId, string id)
        {
            Id = id;
            ParentId = parentId;
        }

        public AccountIdentifier(string parentId, string id, string eTag)
        {
            Id = id;
            ETag = eTag;
            ParentId = parentId;
        }

        [Identifier("Parents")]
        public string ParentId { get; set; }

        [Identifier("Accounts")]
        public sealed override string Id { get; set; }

    }
}