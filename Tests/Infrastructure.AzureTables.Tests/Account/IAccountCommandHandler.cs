﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account
{
    public interface IAccountCommandHandler
    {
        Task<AccountModel> HandleAsync(AccountCommand<AccountCreateRequest> command);
        Task<AccountModel> HandleAsync(AccountCommand<AccountChangeAdminUserIdRequest> command);

        Task<AccountModel> HandleAsync(AccountCommand<AccountChangeNameRequest> command);
    }
}