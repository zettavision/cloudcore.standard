using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Queries
{
    [Identifier("Accounts")]
    public class AccountGetAllQuery
    {
        public string AdminUserId { get; set; }
        public string SearchText { get; set; }
        public PageParametersModel PageParameters { get; set; }
    }
}