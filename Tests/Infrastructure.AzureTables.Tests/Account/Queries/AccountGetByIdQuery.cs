using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Queries
{
    public class AccountGetByIdQuery
    {
        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
    }
}