﻿using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account
{
    public class AccountAggregate : IAggregate
    {
        private string adminUserId;
        private string name;
        private string parentId;
        private string id;

        public AccountAggregate()
        {
        }

        public AccountAggregate(AccountCommand<AccountCreateRequest> command)
        {
            ParentId = command.Identifier.ParentId;
            Id = command.Identifier.Id;
            Name = command.Request.Name;
            AdminUserId = command.Request.AdminUserId;
        }

        [Identifier("Parents")]
        public string ParentId
        {
            get { return parentId; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(ParentId));
                parentId = value;
            }
        }

        [Identifier("Accounts")]
        public string Id
        {
            get { return id; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Id));
                id = value;
            }
        }

        public string Name
        {
            get { return name; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Name));
                name = value;
            }
        }

        public string AdminUserId
        {
            get { return adminUserId; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(AdminUserId));
                adminUserId = value;
            }
        }

        public void Apply(AccountCommand<AccountChangeNameRequest> command)
        {
            Name = command.Request.Name;
        }

        public void Apply(AccountCommand<AccountChangeAdminUserIdRequest> command)
        {
            AdminUserId = command.Request.AdminUserId;
        }
    }
}