﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Queries;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account
{
    public interface IAccountQueryHandler
    {
        Task<AccountModel> HandleAsync(AccountGetByIdQuery query);
        Task<PagedResult<AccountModel>> HandleAsync(AccountGetAllQuery query);

        Task<PagedResult<AggregateEventModel>> HandleAsync(AccountGetAllEventsQuery query);
    }
}