﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.AggregateEvent;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Queries;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests
{
    [TestClass]
    public class AggregateEventReaderAzureTablesTests
    {
        public AggregateEventReaderAzureTablesTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregateEventReaderAzureTables =
                new AggregateEventReaderAzureTables(config["StorageConnectionString"]);
        }

        private readonly AggregateEventReaderAzureTables aggregateEventReaderAzureTables;

        [TestMethod]
        public async Task GetAllAsync()
        {
            var result = await aggregateEventReaderAzureTables
                    .HandleAsync(new AggregateEventGetAllQuery()
                    {
                        Identifier = FullyQualifiedIdentifier.GetIdentifier(
                            new AccountGetAllEventsQuery() {Id = "accounts.test.1" })
                    }).ConfigureAwait(false);

   

            var count = result.Items.Count();
        }

        [TestMethod]
        public async Task GetAllReservedAsync()
        {
            string continuationToken = null;

            while (true)
            {
                var result = await aggregateEventReaderAzureTables
                    .HandleAsync(new AggregateEventGetAllQuery
                        {
                            Identifier = FullyQualifiedIdentifier.GetIdentifier(
                                new AccountGetAllEventsQuery() {Id = "accounts.test.1" }),
                            IdLessThanOrEqual = "17",
                            IdGreaterThan = null,
                            OrderReversed = true,
                            PageParameters = new PageParametersModel() {
                                ContinuationToken = continuationToken,
                                MaxItemCount = 1}
                            
                        }
                    )
                    .ConfigureAwait(false);

                var page = result.Items;

                if (result.ContinuationToken == null)
                    break;

                continuationToken = result.ContinuationToken;
            }


        }

        [TestMethod]
        public void AggregateEventExtendedPropertyCalculatorTest()
        {
            var model = new AggregateEventModel()
            {
                RequestType =
                    "Zyto.ProductX.Cloud.Domain.Account.Commands.Requests.AccountCreateForAggregateRequest, Zyto.ProductX.Cloud.Domain"
            };
            AggregateEventExtendedPropertyCalculator.Calculate(model);

            var requestDisplayName = model.RequestTypeDisplayName;

            Assert.AreEqual("Account Create For Aggregate Request", requestDisplayName);
        }
    }
}
