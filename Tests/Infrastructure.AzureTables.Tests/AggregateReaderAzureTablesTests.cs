﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests
{
    [TestClass]
    public class AggregateReaderAzureTablesTests
    {
        private readonly AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;
        private readonly AggregateReaderAzureTables<AccountIdentifier, AccountModel> aggregateReader;
        private readonly Random random;
        private readonly IConfigurationRoot config;

        public AggregateReaderAzureTablesTests()
        {
            config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            random = new Random();
            aggregateRepository = new AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"], KeyFormatter.PadForInt32, null, new AccountExtendedProperties());
            aggregateReader = new AggregateReaderAzureTables<AccountIdentifier, AccountModel>(
                config["StorageConnectionString"], KeyFormatter.PadForInt32);
        }

        [TestMethod]
        public async Task CreateIfNotExistsAsync()
        {
            var aggregateAzureTableHelper = new AggregateAzureTableHelper<AccountIdentifier>(config["StorageConnectionString"]);
            await aggregateAzureTableHelper.Table.CreateIfNotExistsAsync().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetByIdAsync()
        {
            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() {Name = "name", AdminUserId = "userid"},
                "userid"
                );
       
            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);

            await aggregateReader.GetByIdAsync(command.Identifier).ConfigureAwait(false);


        }

        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundException))]
        public async Task GetByIdAsync_MissingId_ObjectNotFoundException()
        {
            await aggregateReader.GetByIdAsync(new AccountIdentifier("1", random.Next().ToString())).ConfigureAwait(false);
        }


        [TestMethod]
        public async Task GetAllAsync()
        {

            for (int i = 0; i < 5; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier("2", random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllAsync(null, null).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 5;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllAsyncWithPaging()
        {

            for (int i = 0; i < 5; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier("3", random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            PageParametersModel pageParametersModel = new PageParametersModel() { MaxItemCount = 2};
            int itemsCount = 0;
            do
            {
                var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "3");
                var results = await aggregateReader.GetAllAsync(filter, pageParametersModel).ConfigureAwait(false);

                if (results.Items != null)
                    itemsCount += results.Items.Count();

                pageParametersModel.ContinuationToken = results.ContinuationToken;

            } while (pageParametersModel?.ContinuationToken != null);

            var isCountTenOrGreater = itemsCount >= 5;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllAsync_MaxCount2()
        {

            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier("1", random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllAsync(null, new PageParametersModel() {MaxItemCount = 2}).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());

        }

        [TestMethod]
        public async Task GetAllByPartitionAsync()
        {

            for (int i = 0; i < 5; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier("1", random.Next().ToString()),
                    new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var results = await aggregateReader.GetAllByPartitionAsync("1", null, null).ConfigureAwait(false);

            var isCountTenOrGreater = results.Items.Count() >= 5;

            Assert.AreEqual(true, isCountTenOrGreater);

        }

        [TestMethod]
        public async Task GetAllByPartitionAsync_FilterOnName()
        {
            var randomName = random.Next().ToString();

            for (int i = 0; i < 2; i++)
            {
                var command = new AccountCommand<AccountCreateRequest>
                    (
                    new AccountIdentifier("1", random.Next().ToString()),
                    new AccountCreateRequest() { Name = randomName, AdminUserId = "userid" },
                    "userid"
                    );

                await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
            }

            var filter = TableQuery.GenerateFilterCondition("Name", QueryComparisons.Equal, randomName);
            var results = await aggregateReader.GetAllByPartitionAsync("1", filter, null).ConfigureAwait(false);

            Assert.AreEqual(2, results.Items.Count());

        }


    }
}
