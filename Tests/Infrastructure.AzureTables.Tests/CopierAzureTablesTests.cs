﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests
{
    [TestClass]
    public class CopierAzureTablesTests
    {
        private readonly CopierAzureTables azureTableCopier;
        private readonly AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;
        private readonly AggregatePurgerAzureTables<AccountIdentifier> aggregatePurger;

        public CopierAzureTablesTests()
        {
            var config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();

            aggregatePurger = new AggregatePurgerAzureTables<AccountIdentifier>(config["StorageConnectionString"], KeyFormatter.PadForInt32);

            azureTableCopier = new CopierAzureTables(config["StorageConnectionString"], "accounts",
                config["StorageConnectionString"], "accountscopy");

            aggregateRepository = new AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"], KeyFormatter.PadForInt32, null, new AccountExtendedProperties());
        }


        public async Task InsertAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("test-account", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid"},
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        public async Task InsertAsync(string partitionKey)
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier(partitionKey, random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CopyAllAzureTable()
        {
            await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            for (var i = 0; i < 2; i++)
            {
                await InsertAsync().ConfigureAwait(false);
            }

            var copiedRows = await azureTableCopier.CopyAllAsync();
            
            Assert.AreEqual(2, copiedRows);
        }

        [TestMethod]
        public async Task CopyPartitionAzureTable()
        {
            await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);
            var random = new Random();
            var partitionKey = random.Next().ToString();

            for (var i = 0; i < 2; i++)
            {
                await InsertAsync(partitionKey).ConfigureAwait(false);
            }

            var copiedRows = await azureTableCopier.CopyPartitionAsync(partitionKey);

            Assert.AreEqual(2, copiedRows);
        }
    }
}
