﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables.Tests.Account.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.Tests
{
    [TestClass]
    public class AggregatePurgerAzureTablesTests
    {
        private readonly IConfigurationRoot config;
        public AggregatePurgerAzureTablesTests()
        {
                config = new ConfigurationBuilder().AddJsonFile("configuration.json").Build();
            aggregatePurger = new AggregatePurgerAzureTables<AccountIdentifier>(config["StorageConnectionString"], KeyFormatter.PadForInt32);

            aggregateRepository = new AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel>(
                config["StorageConnectionString"], KeyFormatter.PadForInt32, null, new AccountExtendedProperties());
        }

        private readonly AggregateRepositoryAzureTables<AccountIdentifier, AccountAggregate, AccountModel> aggregateRepository;

        private readonly AggregatePurgerAzureTables<AccountIdentifier> aggregatePurger;

        public async Task InsertAsync()
        {
            var random = new Random();

            var command = new AccountCommand<AccountCreateRequest>
                (
                new AccountIdentifier("1", random.Next().ToString()),
                new AccountCreateRequest() { Name = "name", AdminUserId = "userid" },
                "userid"
                );

            await aggregateRepository.InsertAsync(new AccountAggregate(command), "userid").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task PurgeAsync()
        {
            await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            for (var i = 0; i < 5; i++)
            {
                await InsertAsync().ConfigureAwait(false);
            }

            var deletedCount = await aggregatePurger.PurgeAllAsync().ConfigureAwait(false);

            Assert.AreEqual(5, deletedCount);
        }
    }
}
