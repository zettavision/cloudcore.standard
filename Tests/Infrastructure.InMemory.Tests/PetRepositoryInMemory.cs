﻿using ZettaVision.CloudCore.Infrastructure.InMemory;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Aggregate;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;
using Zyto.LimbicArcBoost.Cloud.Domain.Pet.Infrastructure;

namespace Infrastructure.InMemory.Tests
{
    public class PetRepositoryInMemory : AggregateRepositoryInMemory<PetIdentifier, PetAggregate, PetModel>, IPetRepository
    {
        public PetRepositoryInMemory()
            : base(null, true)
        {
        }
    }
}
