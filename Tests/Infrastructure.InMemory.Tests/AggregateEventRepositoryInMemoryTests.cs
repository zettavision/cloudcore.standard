﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.InMemory;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Aggregate;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries;

namespace Infrastructure.InMemory.Tests
{
    [TestClass]
    public class AggregateEventRepositoryInMemoryTests
    {
        private readonly AggregateEventRepositoryInMemory aggregateEventRepositoryInMemory;
        private readonly AggregateRepositoryInMemory<PetIdentifier, PetAggregate, PetModel> aggregateRepositoryInMemory;
        private static PetCommandHandler commandHandler;
        private static PetQueryHandler queryHandler;


        public AggregateEventRepositoryInMemoryTests()
        {
            aggregateEventRepositoryInMemory = new AggregateEventRepositoryInMemory();
            aggregateRepositoryInMemory = new AggregateRepositoryInMemory<PetIdentifier, PetAggregate, PetModel>(null, true);
            
        }

        [ClassInitialize]
        public static async Task InitializeInMemory(TestContext context)
        {
            var repository = new PetRepositoryInMemory();
            var reader = new PetReaderInMemory(repository);
            commandHandler = new PetCommandHandler(repository);
            queryHandler = new PetQueryHandler(reader);

            var classObj = new AggregateEventRepositoryInMemoryTests();
            await classObj.CreatePetAsync("525", "1", "Dog").ConfigureAwait(false);
            await classObj.CreatePetAsync("525", "2", "Cat").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CreatePet()
        {
            //Act

            //Arrange
            var result = await CreatePetAsync("525", "10", "ABC").ConfigureAwait(false);

            //Assert
        }

        [TestMethod]
        [Ignore]
        public async Task ChangePetName()
        {
            //Arrange
            var pet = await PetGetByIdAsync("525", "1").ConfigureAwait(false);

            //Act
            var command = new PetCommand<PetChangeNameRequest>
                (
                new PetIdentifier(pet.AccountId, pet.Id, pet.ETag),
                new PetChangeNameRequest() { Name = "Puppy" },
                "userid"
                );

            //Arrange
            var result = await commandHandler.HandleAsync(command).ConfigureAwait(false);

            //Assert
            Assert.AreEqual("Puppy", result.Name);
        }

        [TestMethod]
        public async Task GetByID()
        {
            //Act
            var pet = await CreatePetAsync("525", "11", "ABC").ConfigureAwait(false);

            //Arrange
            pet = await PetGetByIdAsync(pet.AccountId, pet.Id).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(pet.AccountId, "525");
            Assert.AreEqual(pet.Id, "11");
        }

        [TestMethod]
        public async Task GetAll()
        {
            //Act

            //Arrange
            var result = await queryHandler.HandleAsync(new PetGetAllQuery()
            {
                AccountId = "525",
                PageParameters = new PageParametersModel() { MaxItemCount = 1},
                Paged = true
            }).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(result.Items.Count(), 1);
            Assert.IsNotNull(result.ContinuationToken);
        }

        [TestMethod]
        public async Task GetAll_WithPagenation()
        {
            //Act
            var result1 = await queryHandler.HandleAsync(new PetGetAllQuery()
            {
                AccountId = "525",
                PageParameters = new PageParametersModel() { MaxItemCount = 1 },
                Paged = true
            }).ConfigureAwait(false);

            //Arrange
            var result2 = await queryHandler.HandleAsync(new PetGetAllQuery()
            {
                AccountId = "525",
                PageParameters = new PageParametersModel() { MaxItemCount = 1, ContinuationToken =  result1.ContinuationToken },
                Paged = true
            }).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(result1.Items.Count(), 1);
            Assert.AreEqual(result2.Items.Count(), 1);
            Assert.IsNotNull(result1.ContinuationToken);
            Assert.IsNotNull(result2.ContinuationToken);
            Assert.AreNotEqual(result1.ContinuationToken, result2.ContinuationToken);
        }



        private async Task<PetModel> CreatePetAsync(string accountId, string id, string name)
        {
            var random = new Random();


            var command = new PetCommand<PetCreateRequest>
                (
                new PetIdentifier(accountId ?? random.Next().ToString(), id ?? random.Next().ToString()),
                new PetCreateRequest() { Name = name ?? random.Next().ToString(), Gender = GenderEnum.Male },
                "userid"
                );

            var result = await commandHandler.HandleAsync(command).ConfigureAwait(false);

            return result;

        }

        private async Task<PetModel> PetGetByIdAsync(string accountId, string id)
        {
            //return await (accountId,)
            return await queryHandler.HandleAsync(new PetGetByIdQuery()
            {
                AccountId = accountId ?? "525",
                Id = id ?? "1"
            }).ConfigureAwait(false);
        }
    }
}
