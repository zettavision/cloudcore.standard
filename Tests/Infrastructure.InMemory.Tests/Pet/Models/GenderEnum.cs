﻿namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models
{
    public enum GenderEnum
    {
        Male, Female
    }
}