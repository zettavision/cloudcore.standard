﻿using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models
{
    public class PetModel : IAggregateModel
    {
        [Required]
        public string AccountId { get; set; }

        [Required]
        public string Id { get; set; }
        public string Name { get; set; }
        public string ETag { get; set; }
        public AggregateInfoModel AggregateInfo { get; set; }
        public GenderEnum Type { get; set; }
        public bool Archived { get; set; }
    }
}