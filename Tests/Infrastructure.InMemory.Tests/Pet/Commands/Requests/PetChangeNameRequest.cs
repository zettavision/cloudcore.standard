﻿namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests
{
    public class PetChangeNameRequest : IPetRequest
    {
        public string Name { get; set; }
    }
}