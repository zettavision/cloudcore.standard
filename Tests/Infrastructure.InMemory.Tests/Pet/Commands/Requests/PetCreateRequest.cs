﻿using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests
{
    public class PetCreateRequest : IPetRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public GenderEnum Gender { get; set; }
    }
}