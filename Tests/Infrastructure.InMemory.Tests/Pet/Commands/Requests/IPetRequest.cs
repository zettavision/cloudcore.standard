﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests
{
    public interface IPetRequest : IAggregateRequest
    {
    }
}