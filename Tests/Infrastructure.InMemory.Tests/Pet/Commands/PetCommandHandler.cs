﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Aggregate;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;
using Zyto.LimbicArcBoost.Cloud.Domain.Pet.Infrastructure;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands
{
    public class PetCommandHandler : IPetCommandHandler
    {
        private readonly IPetRepository petRepository;

        public PetCommandHandler(IPetRepository petRepository)
        {
            this.petRepository = petRepository;
        }
        public async Task<PetModel> HandleAsync(PetCommand<PetCreateRequest> command)
        {
            var aggregate = new PetAggregate(command);
            var result = await petRepository.InsertAsync(aggregate);
            return result;
        }

        public Task<PetModel> HandleAsync(PetCommand<PetChangeNameRequest> command)
        {
            throw new NotImplementedException();
        }
    }
}
