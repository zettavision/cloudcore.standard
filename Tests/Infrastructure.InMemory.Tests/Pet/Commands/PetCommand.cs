﻿using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands
{
    public class PetCommand<T2> : AggregateCommand<PetIdentifier, T2>
        where T2 : IPetRequest
    {
        public PetCommand(PetIdentifier identifier, T2 request, string userId)
            : base(identifier, request, userId)
        {
        }
    }
}