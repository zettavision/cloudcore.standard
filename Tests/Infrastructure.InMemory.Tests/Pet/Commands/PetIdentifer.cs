﻿using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands
{
    public class PetIdentifier : AggregateIdentifier
    {
        public PetIdentifier()
        {
        }

        public PetIdentifier(string accountId, string id)
        {
            AccountId = accountId;
            Id = id;
        }

        public PetIdentifier(string accountId, string id, string eTag)
        {
            AccountId = accountId;
            Id = id;
            ETag = eTag;
        }

        [Identifier("Accounts")]
        public string AccountId
        {
            get;
            set;
        }

        [Identifier("Pets")]
        public sealed override string Id
        {
            get;
            set;
        }
    }
}