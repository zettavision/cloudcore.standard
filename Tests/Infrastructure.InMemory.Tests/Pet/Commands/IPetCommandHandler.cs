﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands
{
    public interface IPetCommandHandler
    {
        Task<PetModel> HandleAsync(PetCommand<PetCreateRequest> command);

        Task<PetModel> HandleAsync(PetCommand<PetChangeNameRequest> command);
    }
}