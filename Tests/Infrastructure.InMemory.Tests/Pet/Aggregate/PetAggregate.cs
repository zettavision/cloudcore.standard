﻿using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Aggregate
{
    public class PetAggregate : IAggregate
    {
        private string accountId;
        private string id;
        private string name;

        public PetAggregate()
        {
        }

        public PetAggregate(PetCommand<PetCreateRequest> command)
        {
            Id = command.Identifier.Id;
            AccountId = command.Identifier.AccountId;
            Name = command.Request.Name;
            Gender = command.Request.Gender;
        }

        [Identifier("Accounts")]
        public string AccountId
        {
            get { return accountId; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(AccountId));
                accountId = value;
            }
        }

        [Identifier("Pets")]
        public string Id
        {
            get { return id; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Id));
                id = value;
            }
        }

        public string Name
        {
            get { return name; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Name));
                name = value;
            }
        }

        [Required]
        public GenderEnum Gender { get; private set; }

        public void Apply(PetCommand<PetChangeNameRequest> command)
        {
            Name = command.Request.Name;
        }

    }
}