﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries
{
    public interface IPetQueryHandler
    {
        Task<PetModel> HandleAsync(PetGetByIdQuery query, CancellationToken cancellationToken = default);
        Task<PagedResult<PetModel>> HandleAsync(PetGetAllQuery query, CancellationToken cancellationToken = default);

        //Task<PagedResult<AggregateEventModel>> HandleAsync(PetGetAllEventsQuery query);
    }
}