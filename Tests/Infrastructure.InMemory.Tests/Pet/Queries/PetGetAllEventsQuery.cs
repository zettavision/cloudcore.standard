using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries
{
    [Identifier("Events")]
    public class PetGetAllEventsQuery
    {
        [Identifier("Accounts")]
        public string Id
        {
            get;
            set;
        }
        public PageParametersModel PageParameters { get; set; }
    }
}