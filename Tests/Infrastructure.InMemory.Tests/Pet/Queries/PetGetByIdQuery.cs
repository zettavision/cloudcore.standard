using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries
{
    public class PetGetByIdQuery
    {
        [Identifier("Accounts")]
        public string AccountId
        {
            get;
            set;
        }

        [Identifier("Pets")]
        public string Id
        {
            get;
            set;
        }
    }
}