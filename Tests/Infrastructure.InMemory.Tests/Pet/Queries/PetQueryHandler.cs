﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Infrastructure;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries
{
    public class PetQueryHandler : IPetQueryHandler
    {
        //private readonly IAggregateEventReader aggregateEventReader;
        //private readonly AggregateEventWithDetailsReader aggregateEventWithDetailsReader;
        private readonly IPetReader petReader;

        public PetQueryHandler(
            IPetReader petReader
            //IAggregateEventReader aggregateEventReader,
            //AggregateEventWithDetailsReader aggregateEventWithDetailsReader,
            )
        {
            //this.aggregateEventReader = aggregateEventReader;
            //this.aggregateEventWithDetailsReader = aggregateEventWithDetailsReader;
            this.petReader = petReader;
        }

        public async Task<PetModel> HandleAsync(PetGetByIdQuery query, CancellationToken cancellationToken = default)
        {
            RequiredStringValidator.Validate(query.AccountId, nameof(query.AccountId));
            RequiredStringValidator.Validate(query.Id, nameof(query.Id));

            var model = await petReader.HandleAsync(query, cancellationToken).ConfigureAwait(false);
            return model;
        }

        public async Task<PagedResult<PetModel>> HandleAsync(PetGetAllQuery query, CancellationToken cancellationToken = default)
        {
            PagedResult<PetModel> result;

            if(query.Paged)
            {
                result = await petReader.HandleAsync(query, cancellationToken).ConfigureAwait(false);
            }
            else
            {
                var list = new List<PetModel>();
                string continuationToken = null;


                while(true)
                {
                    query.PageParameters = new PageParametersModel()
                    {
                        ContinuationToken = continuationToken,
                        MaxItemCount = 1000
                    };

                    var pagedResult = await petReader.HandleAsync(
                        query, cancellationToken).ConfigureAwait(false);

                    if(pagedResult.Items != null && pagedResult.Items.Any())
                        list.AddRange(pagedResult.Items);

                    if(pagedResult.ContinuationToken == null)
                        break;

                    continuationToken = pagedResult.ContinuationToken;

                }

                result = new PagedResult<PetModel>()
                {
                    ContinuationToken = null,
                    Items = list
                };
            }

            return result;
        }

        private async Task<IEnumerable<PetModel>> GetAllAsync(PetGetAllQuery query, CancellationToken cancellationToken = default)
        {
            query.Paged = true;

            var result = await HandleAsync(query, cancellationToken).ConfigureAwait(false);

            return result?.Items;
        }

        //public async Task<PagedResult<AggregateEventModel>> HandleAsync(PetGetAllEventsQuery query, CancellationToken cancellationToken = default)
        //{
        //    RequiredStringValidator.Validate(query.AccountId, nameof(query.AccountId));
        //    RequiredStringValidator.Validate(query.Id, nameof(query.Id));

        //    return await aggregateEventReader.GetAllAsync(
        //        () => petReader.HandleAsync(new PetGetByIdQuery() { AccountId = query.AccountId, Id = query.Id }, cancellationToken),
        //        query,
        //        query.OrderReversed,
        //        query.PageParameters,
        //        cancellationToken).ConfigureAwait(false);
        //}

        //public async Task<PagedResult<AggregateEventWithDetailsModel>> HandleAsync(PetGetAllEventsWithDetailsQuery query, CancellationToken cancellationToken = default)
        //{
        //    var baseResults = await HandleAsync(query.Query, cancellationToken).ConfigureAwait(false);
        //    return await aggregateEventWithDetailsReader
        //        .GetAllWithDetailsAsync(baseResults, cancellationToken)
        //        .ConfigureAwait(false);
        //}

    }
}