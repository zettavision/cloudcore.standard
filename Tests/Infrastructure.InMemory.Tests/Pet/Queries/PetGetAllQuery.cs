using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries
{
    [Identifier("Accounts")]
    public class PetGetAllQuery
    {
        public string AccountId { get; set; }
        public string SearchText { get; set; }
        public PageParametersModel PageParameters { get; set; }
        public bool Paged { get; internal set; }
    }
}