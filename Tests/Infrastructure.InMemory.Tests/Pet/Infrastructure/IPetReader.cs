﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries;

namespace ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Infrastructure
{
    public interface IPetReader 
    {
        Task<PetModel> HandleAsync(PetGetByIdQuery query, CancellationToken cancellationToken = default);
        Task<PagedResult<PetModel>> HandleAsync(PetGetAllQuery query, CancellationToken cancellationToken = default);
    }
}