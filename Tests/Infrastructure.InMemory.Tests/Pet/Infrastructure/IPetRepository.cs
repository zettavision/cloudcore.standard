﻿using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Aggregate;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands.Requests;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;

namespace Zyto.LimbicArcBoost.Cloud.Domain.Pet.Infrastructure
{
    //public interface IPetRepository : IAggregateRepositoryWithEventStore<PetIdentifier, PetAggregate, IPetRequest, PetModel>
    //{
    //}

    public interface IPetRepository : IAggregateRepository<PetIdentifier, PetAggregate, PetModel>
    {
    }
}