﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Infrastructure;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Models;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Queries;
using ZettaVision.CloudCore.Infrastructure.InMemory.Tests.Pet.Commands;
using System.Linq;
using ZettaVision.CloudCore.Infrastructure.InMemory;

namespace Infrastructure.InMemory.Tests
{
    public class PetReaderInMemory : IPetReader
    {
        private readonly PetRepositoryInMemory petRepositoryInMemory;

        public PetReaderInMemory(PetRepositoryInMemory petRepositoryInMemory)
        {
            this.petRepositoryInMemory = petRepositoryInMemory;
        }

        public async Task<PetModel> HandleAsync(PetGetByIdQuery query, CancellationToken cancellationToken = default)
        {
            return await petRepositoryInMemory.GetByIdAsync(new PetIdentifier(query.AccountId, query.Id)).ConfigureAwait(false);
        }

        public async Task<PagedResult<PetModel>> HandleAsync(PetGetAllQuery query, CancellationToken cancellationToken = default)
        {
            var partitionKey = petRepositoryInMemory.GetPartitionKey(new PetIdentifier() { AccountId = query.AccountId });
            var result = await petRepositoryInMemory.GetAllByPartitionAsync(partitionKey, cancellationToken);

            if(!string.IsNullOrEmpty(query.SearchText))
            {
                result = result.Where(p => p.Name.Contains(query.SearchText)).ToList();
            }
            return PagerInMemory.GetPagedResult(result, query.PageParameters);
        }
    }
}
