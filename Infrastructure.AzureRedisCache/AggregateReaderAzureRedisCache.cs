﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using StackExchange.Redis;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureRedisCache
{
    public class AggregateReaderAzureRedisCache<T, T2> : IAggregateGetByIdReader<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        private readonly string partitionKeyPrefix;


        private readonly IDatabase cache;
        private readonly DocumentToAggregateModelMapper<T2> documentToAggregateModelMapper;

        public AggregateReaderAzureRedisCache(string connectionString,
            IEnumerable<JsonConverter> jsonConverters = null, string partitionKeyPrefix = null)
        {
            this.partitionKeyPrefix = partitionKeyPrefix;

            var connection = ConnectionMultiplexer.Connect(connectionString);
            cache = connection.GetDatabase();

            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T2>(jsonConverters);


        }

        public async Task<T2> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var key = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, partitionKeyPrefix);

            var result = await cache.StringGetAsync(key).ConfigureAwait(false);
            if (result.HasValue == false || result.IsNullOrEmpty)
            {
                throw new ObjectNotFoundException($"Object Not Found '{key}'");
            }

            var documentJson = result.ToString();

            if (string.IsNullOrEmpty(documentJson))
                throw new ObjectNotFoundException($"Object Not Found '{key}'");

            return documentToAggregateModelMapper.Map(documentJson);
        }

        public async Task<IEnumerable<T2>> GetMultipleByIdAsync(IEnumerable<T> aggregateIdentifiers, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (aggregateIdentifiers == null || !aggregateIdentifiers.Any())
                return null;

            var keysAsStrings = aggregateIdentifiers
                .Select(a => FullyQualifiedIdentifier.GetIdentifier(a, partitionKeyPrefix))
                .ToList();

            if(keysAsStrings.Any(string.IsNullOrEmpty))
                throw new InvalidOperationException("Id is cannot be empty");

            var keys = keysAsStrings
                .Select(k => (RedisKey)k)
                .ToArray();

          
            var results = await cache.StringGetAsync( keys).ConfigureAwait(false);

            var list = new List<T2>();

            foreach (var result in results)
            {
    
                if (result.HasValue == false || result.IsNullOrEmpty)
                    continue;
    

                var documentJson = result.ToString();

                if (string.IsNullOrEmpty(documentJson))
                    continue;

                var model =  documentToAggregateModelMapper.Map(documentJson);
                list.Add(model);
            }


            return list;
        }
    }
}