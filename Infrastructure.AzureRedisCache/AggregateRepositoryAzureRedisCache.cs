﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using StackExchange.Redis;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.AzureRedisCache
{
    public class AggregateRepositoryAzureRedisCache<T, T2, T3> 
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {
        private readonly string partitionKeyPrefix;

        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly AggreateToAggregateIdentifierMapper<T, T2> aggreateToAggregateIdentifierMapper;
        private readonly DocumentToAggregateModelMapper<T3> documentToAggregateModelMapper;
        private readonly IDatabase cache;

        public AggregateRepositoryAzureRedisCache(string connectionString, 
            IEnumerable < JsonConverter> jsonConverters = null, 
            string partitionKeyPrefix = null)
        {
            this.partitionKeyPrefix = partitionKeyPrefix;

            var connection = ConnectionMultiplexer.Connect(connectionString);
            cache = connection.GetDatabase();

            aggreateToAggregateIdentifierMapper = new AggreateToAggregateIdentifierMapper<T, T2>();
            documentToAggregateModelMapper = new DocumentToAggregateModelMapper<T3>(jsonConverters);

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }
        }

        public async Task<T3> UpsertAsync(T2 aggregate,  string userId = null, TimeSpan? timeToLive = null, DateTime? createDate = null)
        {
            var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregate);
            var key = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, partitionKeyPrefix);

            if (createDate == null)
                createDate = DateTime.UtcNow;

            var aggregateInfo = new AggregateInfoValueObject(userId, createDate.Value);

            var aggregateWithAggregateInfo = new AggregateWithAggregateInfoAzureRedisCache<T2>
            {
                Aggregate = aggregate,
                AggregateInfo = aggregateInfo
            };

            var documentJson = JsonConvert.SerializeObject(aggregateWithAggregateInfo, jsonSerializerSettings);

            await cache.StringSetAsync(key, documentJson, timeToLive).ConfigureAwait(false);

            return documentToAggregateModelMapper.Map(documentJson);
        }

        public async Task<IEnumerable<T3>> UpsertMultipleAsync(IEnumerable<T2> aggregates, string userId = null, DateTime? createDate = null)
        {
            if (aggregates == null || !aggregates.Any())
                return null;

            var redisValues = new List<KeyValuePair<RedisKey, RedisValue>>();
            var models = new List<T3>();

            foreach (var aggregate in aggregates)
            {
                var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregate);
                var key = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, partitionKeyPrefix);

                if (createDate == null)
                    createDate = DateTime.UtcNow;

                var aggregateInfo = new AggregateInfoValueObject(userId, createDate.Value);

                var aggregateWithAggregateInfo = new AggregateWithAggregateInfoAzureRedisCache<T2>
                {
                    Aggregate = aggregate,
                    AggregateInfo = aggregateInfo
                };

                var documentJson = JsonConvert.SerializeObject(aggregateWithAggregateInfo, jsonSerializerSettings);
                var redisKey = (RedisKey) key;
                var redisValue = (RedisValue) documentJson;
                

                redisValues.Add(new KeyValuePair<RedisKey, RedisValue>(redisKey, redisValue));
                models.Add(documentToAggregateModelMapper.Map(documentJson));
            }

            await cache.StringSetAsync(redisValues.ToArray()).ConfigureAwait(false);

            return models;
        }

        public async Task DeleteAsync(T aggregateIdentifier)
        {
            var key = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier, partitionKeyPrefix);

            await cache.KeyDeleteAsync(key).ConfigureAwait(false);
        }

        public async Task DeleteMultipleAsync(IEnumerable<T> aggregateIdentifiers)
        {
            if (aggregateIdentifiers == null || !aggregateIdentifiers.Any())
                return;

            var keysAsStrings = aggregateIdentifiers.Select(a => FullyQualifiedIdentifier.GetIdentifier(a, partitionKeyPrefix)).ToList();
            
            if (keysAsStrings.Any(string.IsNullOrEmpty))
                throw new InvalidOperationException("Id is cannot be empty");

            var keys = keysAsStrings
                .Select(k => (RedisKey)k)
                .ToArray();

            await cache.KeyDeleteAsync(keys).ConfigureAwait(false);
        }
    }
}