using System;

namespace ZettaVision.CloudCore.Infrastructure.AzureRedisCache
{
    public static class ETagGenerator
    {
        public static string GetETag(DateTime lastModifiedDate)
        {
            var isoDateFormat = lastModifiedDate.ToUniversalTime().ToString("O");
            var eTag = $"\"{isoDateFormat}\"";
            return eTag;
        }
    }
}