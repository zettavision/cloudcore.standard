﻿using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.AzureRedisCache
{
    public class AggregateWithAggregateInfoAzureRedisCache<T>
        where T : IAggregate
    {
        public AggregateInfoValueObject AggregateInfo { get; set; }
        public T Aggregate { get; set; }
    }
}