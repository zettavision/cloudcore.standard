﻿using System.Net.Http;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.AzureFunctions
{
    public static class HttpResponseMessageExtenstions
    {
        public static void AddContinuationTokenHeader(this HttpResponseMessage response, string continuationToken)
        {
            response.Headers.Add("continuation-token", continuationToken);
        }

        public static void AddETagHeader(this HttpResponseMessage response, IAggregateModel aggregateModel)
        {
             response.Headers.Add("ETag", aggregateModel.ETag);
        }
    }
}
