﻿using System.Linq;
using System.Net.Http;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.AzureFunctions
{
    public static class HttpRequestMessageExtenstions
    {

        public static string GetETag(this HttpRequestMessage request)
        {
            return request.GetSigularHeader("If-Match");
        }

        public static string GetSigularHeader(this HttpRequestMessage request, string headerName)
        {
            if (request.Headers == null)
                return null;

            var dictionary = request.Headers.ToDictionary(h => h.Key, h => h.Value);

            if (!dictionary.ContainsKey(headerName))
                return null;
            var value = dictionary[headerName];

            return value?.FirstOrDefault();
        }

        public static PageParametersModel GetPageParameters(this HttpRequestMessage request)
        {
            var pageParametersModel = new PageParametersModel();

            var continuationToken = request.GetSigularHeader("continuation-token");
            var maxItemCountString = request.GetSigularHeader("max-item-count");

            pageParametersModel.ContinuationToken = continuationToken;
            pageParametersModel.MaxItemCount = maxItemCountString == null ? (int?)null : int.Parse(maxItemCountString);

            return pageParametersModel;
        }


    }
}