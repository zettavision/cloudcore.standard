﻿using System;
using System.Dynamic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace ZettaVision.CloudCore.AzureFunctions
{
    public class HttpResponseMessageErrorGenerator
    {
        private readonly bool isDevelopmentEnvironment;

        public HttpResponseMessageErrorGenerator(bool isDevelopmentEnvironment)
        {
            this.isDevelopmentEnvironment = isDevelopmentEnvironment;
        }

        public HttpResponseMessage Generate(Exception exception)
        {
            var code = ExceptionToStatusCodeMapper.GetStatusCodeForException(exception);

            dynamic body = new ExpandoObject();

            body.Message = exception.Message;

            if (isDevelopmentEnvironment)
            {
                body.Type = exception.GetType().FullName;
                body.StackTrace = exception.StackTrace;
                if (exception.InnerException != null)
                    body.InnerException = exception.InnerException.ToString();
            }

            var json = JsonConvert.SerializeObject(body);

            return new HttpResponseMessage(code)
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };
        }
    }
}
