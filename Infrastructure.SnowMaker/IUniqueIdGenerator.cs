namespace ZettaVision.CloudCore.Infrastructure.SnowMaker
{
    public interface IUniqueIdGenerator
    {
        long NextId(string scopeName);
    }
}