﻿using System;

namespace ZettaVision.CloudCore.Infrastructure.SnowMaker
{
    public class UniqueIdGenerationException : Exception
    {
        public UniqueIdGenerationException(string message)
            : base(message)
        {
        }
    }
}