﻿namespace ZettaVision.CloudCore.Infrastructure.SnowMaker
{
    public interface IOptimisticDataStore
    {
        string GetData(string blockName);
        bool TryOptimisticWrite(string blockName, string data);
    }
}
