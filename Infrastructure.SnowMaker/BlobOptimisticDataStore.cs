﻿using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure;

namespace ZettaVision.CloudCore.Infrastructure.SnowMaker
{
    public class BlobOptimisticDataStore : IOptimisticDataStore
    {
        const string SeedValue = "1";

        readonly BlobContainerClient blobContainer;

        readonly IDictionary<string, BlobClient> blobReferences;
        readonly object blobReferencesLock = new object();

        public BlobOptimisticDataStore(BlobServiceClient blobServiceClient, string containerName)
        {
            blobContainer = blobServiceClient.GetBlobContainerClient(containerName);
            blobContainer.CreateIfNotExists();

            blobReferences = new Dictionary<string, BlobClient>();
        }

        public string GetData(string blockName)
        {
            var blobReference = GetBlobReference(blockName);

            using (var stream = new MemoryStream())
            {
                blobReference.DownloadTo(stream);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        public bool TryOptimisticWrite(string scopeName, string data)
        {
            var blobReference = GetBlobReference(scopeName);
            try
            {
                var blobUploadOptions = new BlobUploadOptions()
                {
                    Conditions = new BlobRequestConditions() { IfMatch = new ETag(blobReference.GetProperties().Value.ETag.ToString()) }
                };

                UploadText(
                    blobReference,
                    data,
                    blobUploadOptions);
            }
            catch (RequestFailedException ex)
            {
                if (ex.Status == (int)HttpStatusCode.PreconditionFailed)
                    return false;

                throw;
            }
            return true;
        }

        BlobClient GetBlobReference(string blockName)
        {
            return blobReferences.GetValue(
                blockName,
                blobReferencesLock,
                () => InitializeBlobReference(blockName));
        }

        private BlobClient InitializeBlobReference(string blockName)
        {
            var blobReference = blobContainer.GetBlobClient(blockName);

            if (blobReference.Exists())
                return blobReference;

            try
            {
                var blobUploadOptions = new BlobUploadOptions()
                {
                    Conditions = new BlobRequestConditions() { IfNoneMatch = ETag.All }
                };

                UploadText(blobReference, SeedValue, blobUploadOptions);
            }
            catch (RequestFailedException uploadException)
            {
                if (uploadException.Status != (int)HttpStatusCode.Conflict)
                    throw;
            }

            return blobReference;
        }

        void UploadText(BlobClient blobClient, string text, BlobUploadOptions blobUploadOptions)
        {
            blobUploadOptions.HttpHeaders = new BlobHttpHeaders()
            {
                ContentType = "text/plain",
                ContentEncoding = "UTF-8"
            };

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(text)))
            {
                blobClient.Upload(stream, blobUploadOptions);
            }
        }
    }
}
