﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.Rest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.Exceptions;

namespace ZettaVision.CloudCore.Infrastructure.Cloud
{
    public static class DomainExceptionMapper
    {
        public static Exception Map(Exception ex)
        {
            if (ex is HttpOperationException)
            {
                var httpOperationException = (HttpOperationException) ex;
                var message = GetMessage(httpOperationException);

                Exception newExection = null;
                if (httpOperationException.Response.StatusCode == HttpStatusCode.BadRequest)
                    newExection = new ArgumentException(message, ex);

                if (httpOperationException.Response.StatusCode == HttpStatusCode.Conflict)
                    newExection = new ObjectAlreadyExistsException(message, ex);

                if (httpOperationException.Response.StatusCode == HttpStatusCode.PreconditionFailed)
                    newExection = new OptimisticConcurrencyException(message, ex);

                if (httpOperationException.Response.StatusCode == HttpStatusCode.NotFound)
                    newExection = new ObjectNotFoundException(message, ex);

                if (httpOperationException.Response.StatusCode == HttpStatusCode.Forbidden)
                    newExection = new UnauthorizedAccessException(message, ex);

                if ((int) httpOperationException.Response.StatusCode == 422) //Unprocessable Entity
                    newExection = new InvalidOperationException(message, ex);

                if (httpOperationException.Response.StatusCode == HttpStatusCode.UnsupportedMediaType)
                    newExection = new UnsupportedMediaTypeException(message, ex);

                if (httpOperationException.Response.StatusCode == HttpStatusCode.InternalServerError)
                    newExection = new Exception(message, ex);

                if (newExection != null)
                {
                    AddData((HttpOperationException)ex, newExection);
                    return newExection;
                }
            }

            return null;
        }

        private static string GetMessage(HttpOperationException httpOperationException)
        {
            if (string.IsNullOrEmpty(httpOperationException?.Response?.Content))
                return httpOperationException.Message;

            var json = httpOperationException?.Response?.Content;

            try
            {
                var deserializeObject = JsonConvert.DeserializeObject(json);
                if (deserializeObject is JObject)
                {
                    var jObject = (JObject) deserializeObject;
                    if (jObject.HasValues)
                    {
                        var message = jObject.Value<string>("Message");
                        if (!string.IsNullOrEmpty(message))
                            return message;
                    }
                }
            }
            catch
            {
            }

            return httpOperationException.Message;
        }


        private static object Deserialize(string json)
        {
            return ToObject(JToken.Parse(json));
        }

        private static object ToObject(JToken token)
        {
            switch (token.Type)
            {
                case JTokenType.Object:
                    return token.Children<JProperty>()
                        .ToDictionary(prop => prop.Name,
                            prop => ToObject(prop.Value));

                case JTokenType.Array:
                    return token.Select(ToObject).ToList();

                default:
                    return ((JValue)token).Value;
            }
        }
        

        private static void AddData(HttpOperationException httpOperationException, Exception exception)
        {
            if (string.IsNullOrEmpty(httpOperationException?.Response?.Content))
                return;

            var json = httpOperationException?.Response?.Content;

            if (string.IsNullOrEmpty(json))
                return;

         
            var deserializeObject = Deserialize(json);
            if(deserializeObject == null)
                return;

            if(!(deserializeObject is Dictionary<string, object>))
                return;

            var bodyDictionary = (Dictionary<string, object>)deserializeObject;

            if (!bodyDictionary.ContainsKey("Data"))
                return;

            var dataObject = bodyDictionary["Data"];

            if(dataObject == null)
                return;

            if(!(dataObject is Dictionary<string, object>))
                return;

            var dataDictionary = (Dictionary<string, object>)dataObject;

            foreach (var entry in dataDictionary)
            {
                exception.Data.Add(entry.Key, entry.Value);
            }

        }

     
        public static Exception ThrowException(Exception ex)
        {
            var mappedException = Map(ex);
            if (mappedException != null)
                throw mappedException;
            throw ex;
        }
    }
}
