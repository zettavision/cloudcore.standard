﻿using ZettaVision.CloudCore.Domain.WebJobShutdown.Aggregate;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Infrastructure;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;
using ZettaVision.CloudCore.Infrastructure.AzureTables;

namespace ZettaVision.CloudCore.Infrastructure.WebJob.WebJobShutdown
{
    public class WebJobShutdownRepositoryAzureTables :
        AggregateRepositoryAzureTables
        <WebJobShutdownIdentifier, WebJobShutdownAggregate, WebJobShutdownModel>,
        IWebJobShutdownRepository
    {
        public WebJobShutdownRepositoryAzureTables(string connectionString, string tableName = null)
            : base(connectionString, null, null, null, tableName)
        {
        }
    }
}