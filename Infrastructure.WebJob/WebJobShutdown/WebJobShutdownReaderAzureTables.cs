﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Infrastructure;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Queries;
using ZettaVision.CloudCore.Infrastructure.AzureTables;

namespace ZettaVision.CloudCore.Infrastructure.WebJob.WebJobShutdown
{
    public class WebJobShutdownReaderAzureTables : IWebJobShutdownReader
    {
        private readonly AggregateReaderAzureTables<WebJobShutdownIdentifier, WebJobShutdownModel> aggregateReader;

        public WebJobShutdownReaderAzureTables(string connectionString, string tableName = null)
        {
            aggregateReader =
                new AggregateReaderAzureTables<WebJobShutdownIdentifier, WebJobShutdownModel>(connectionString, null, null, tableName);
        }

        public async Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetByIdQuery query,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return
                await
                    aggregateReader.GetByIdAsync(new WebJobShutdownIdentifier(
                        query.Id), cancellationToken).ConfigureAwait(false);
        }

        public async Task<PagedResult<WebJobShutdownModel>> HandleAsync(WebJobShutdownGetAllQuery query,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateReader
                .GetAllByPartitionAsync(string.Empty, null, query.PageParameters, cancellationToken)
                .ConfigureAwait(false);
        }
    }
}