﻿using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.DurableTask.ContextImplementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Orchestration.Infrastructure;
using ZettaVision.CloudCore.Domain.Orchestration.Models;
using ZettaVision.CloudCore.Domain.Orchestration.Queries;

namespace ZettaVision.CloudCore.Infrastructure.Orchestration
{
    public class OrchestrationSequentialReader<T> : IOrchestrationSequentialReader<T>
     where T : OrchestrationModel
    {
        private readonly IDurableClient durableClient;
        private readonly string aggregateName;
        private readonly Func<DurableOrchestrationStatus, T> orchestrationMapperFunc;
        private readonly bool showOrchestrationInput;
        private readonly string prefix;

        public OrchestrationSequentialReader(
            IDurableClientFactory durableClientFactory,
            string aggregateName,
            Func<DurableOrchestrationStatus, T> orchestrationMapperFunc,
            bool showOrchestrationInput
            )
        {
            durableClient = durableClientFactory.CreateClient();
            this.aggregateName = aggregateName;
            this.orchestrationMapperFunc = orchestrationMapperFunc;
            this.showOrchestrationInput = showOrchestrationInput;
            prefix = OrchestrationIdConverter.GetPrefix(aggregateName);
        }


        public async Task<T> HandleAsync(OrchestrationSequentialGetByIdQuery query, CancellationToken cancellationToken = default)
        {
            RequiredStringValidator.Validate(query.Id, nameof(query.Id));
            var infrastructureId = OrchestrationIdConverter.ConvertToInfrastructureId(aggregateName, query.Id);

            var result = await durableClient.GetStatusAsync(infrastructureId, query.IncludeHistory, false, showOrchestrationInput).ConfigureAwait(false);

            if (result == null)
                throw new ObjectNotFoundException($"{aggregateName} {query.Id} not found");

            return orchestrationMapperFunc(result);
        }

        public async Task<PagedResult<T>> HandleAsync(OrchestrationSequentialGetAllQuery query, CancellationToken cancellationToken = default)
        {
            var list = new List<T>();

            var pageSize = query.PageParameters?.MaxItemCount ?? 100;

            var queryCondition = new OrchestrationStatusQueryCondition();

            queryCondition.InstanceIdPrefix = prefix;
            queryCondition.PageSize = pageSize;
            queryCondition.ContinuationToken = query.PageParameters?.ContinuationToken;
            queryCondition.ShowInput = showOrchestrationInput;
            queryCondition.RuntimeStatus = query.Statuses?.Select(x => (OrchestrationRuntimeStatus)Enum.Parse(typeof(OrchestrationRuntimeStatus), x.ToString(), true)).ToList();

            if (query.CreatedAfter != null)
                queryCondition.CreatedTimeFrom = query.CreatedAfter.Value;

            if (query.CreatedBefore != null)
                queryCondition.CreatedTimeTo = query.CreatedBefore.Value;

            var result = await durableClient.ListInstancesAsync(
                queryCondition,
                CancellationToken.None).ConfigureAwait(false);

            if (result.DurableOrchestrationState != null && result.DurableOrchestrationState.Any())
            {
                list.AddRange(result.DurableOrchestrationState.Select(o => orchestrationMapperFunc(o)).ToList());
            }


            return new PagedResult<T>()
            {
                Items = list,
                ContinuationToken = result.ContinuationToken
            };
        }

        public async Task<T> HandleAsync(OrchestrationSequentialGetLastQuery query, CancellationToken cancellationToken = default)
        {
            string continuationToken = null;

            while (true)
            {
                var result = await durableClient.ListInstancesAsync(
                    new OrchestrationStatusQueryCondition() { InstanceIdPrefix = prefix, PageSize = 1, ContinuationToken = continuationToken, ShowInput = showOrchestrationInput },
                    CancellationToken.None).ConfigureAwait(false);

                if (result.DurableOrchestrationState != null && result.DurableOrchestrationState.Any())
                {
                    return orchestrationMapperFunc(result.DurableOrchestrationState.First());
                }

                if (result.ContinuationToken == null)
                    break;

                continuationToken = result.ContinuationToken;
            }

            throw new ObjectNotFoundException($"Last {aggregateName} not found");
        }
    }
}
