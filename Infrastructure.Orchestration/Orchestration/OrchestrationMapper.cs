﻿using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Orchestration.Models;
using System.Linq;

namespace ZettaVision.CloudCore.Infrastructure.Orchestration
{
    public static class OrchestrationMapper
    {
        public static T Map<T>(DurableOrchestrationStatus durableOrchestrationStatus, string aggregateName)
            where T : OrchestrationModel, new()
        {
            if (durableOrchestrationStatus == null)
                return null;

            var model = new T();

            model.Id = OrchestrationIdConverter.ConvertToDomainId(aggregateName, durableOrchestrationStatus.InstanceId);
            model.AggregateInfo = GetAggregateInfo(durableOrchestrationStatus);
            model.Duration = model.AggregateInfo.LastModifiedDate - model.AggregateInfo.CreatedDate;
            model.Status = GetStatus(durableOrchestrationStatus);
            model.Error = GetError(durableOrchestrationStatus);
            model.History = GetHistory(durableOrchestrationStatus);

            return model;
        }

        private static IEnumerable<OrchestrationHistoryItemModel> GetHistory(DurableOrchestrationStatus durableOrchestrationStatus)
        {
            if(durableOrchestrationStatus.History == null)
                return null;

            var list = new List<OrchestrationHistoryItemModel>();
            foreach (var historyItemToken in durableOrchestrationStatus.History)
            {
                var historyItem = GetHistoryItem(historyItemToken);

                if(historyItem != null)
                    list.Add(historyItem);
            }

            return !list.Any() ? null : list;
        }

        private static OrchestrationHistoryItemModel GetHistoryItem(JToken historyItemToken)
        {
            if (historyItemToken.Type != JTokenType.Object)
                return null;

            var jObject = (JObject)historyItemToken;

            var historyItemJsonModel = jObject.ToObject<OrchestrationHistoryItemJsonModel>();

            if (string.IsNullOrEmpty(historyItemJsonModel.EventType))
                return null;

            if (historyItemJsonModel.Timestamp == null)
                return null;

            var historyItemModel = new OrchestrationHistoryItemModel() 
            { 
                EventType = historyItemJsonModel.EventType,
                Timestamp = ConvertToUtc(historyItemJsonModel.Timestamp.Value),
                ScheduledTime = historyItemJsonModel.ScheduledTime == null ? (DateTime?)null : ConvertToUtc(historyItemJsonModel.ScheduledTime.Value),
                FunctionName = GetHistoryItemFunctionName(historyItemJsonModel.FunctionName, historyItemJsonModel.Name),
                OrchestrationStatus = historyItemJsonModel.OrchestrationStatus,
                Reason = historyItemJsonModel.Reason,
                Details = historyItemJsonModel.Details
            };

            historyItemModel.Duration = GetHistoryItemDuration(historyItemModel.Timestamp, historyItemModel.ScheduledTime);

            return historyItemModel;
        }

        private static DateTime ConvertToUtc(DateTime value)
        {
            if (value.Kind == DateTimeKind.Utc)
                return value;

            return value.ToUniversalTime();
        }

        private static string GetHistoryItemFunctionName(string functionName, string name)
        {
            if (!string.IsNullOrEmpty(functionName))
                return functionName;

            return name;
        }

        private static TimeSpan? GetHistoryItemDuration(DateTime timestamp, DateTime? scheduledTime)
        {
            if (scheduledTime == null)
                return null;

            if (timestamp < scheduledTime.Value)
                return null;

            return timestamp - scheduledTime.Value;
        }

        private static OrchestrationStatusEnum GetStatus(DurableOrchestrationStatus durableOrchestrationStatus)
        {
            switch (durableOrchestrationStatus.RuntimeStatus)
            {
                case OrchestrationRuntimeStatus.Unknown:
                    return OrchestrationStatusEnum.Unknown;
                case OrchestrationRuntimeStatus.Running:
                    return OrchestrationStatusEnum.Running;
                case OrchestrationRuntimeStatus.Completed:
                    return OrchestrationStatusEnum.Completed;
                case OrchestrationRuntimeStatus.ContinuedAsNew:
                    return OrchestrationStatusEnum.ContinuedAsNew;
                case OrchestrationRuntimeStatus.Failed:
                    return OrchestrationStatusEnum.Failed;
                case OrchestrationRuntimeStatus.Canceled:
                    return OrchestrationStatusEnum.Canceled;
                case OrchestrationRuntimeStatus.Terminated:
                    return OrchestrationStatusEnum.Terminated;
                case OrchestrationRuntimeStatus.Pending:
                    return OrchestrationStatusEnum.Pending;
                default:
                    throw new Exception($"Unknown RuntimeStatus {durableOrchestrationStatus.RuntimeStatus}");
            }
        }

        private static AggregateInfoModel GetAggregateInfo(DurableOrchestrationStatus durableOrchestrationStatus)
        {
            return new AggregateInfoModel() { CreatedDate = durableOrchestrationStatus.CreatedTime, LastModifiedDate = durableOrchestrationStatus.LastUpdatedTime };
        }

        private static string GetError(DurableOrchestrationStatus durableOrchestrationStatus)
        {
            if (durableOrchestrationStatus.RuntimeStatus != OrchestrationRuntimeStatus.Failed)
                return null;
            if (durableOrchestrationStatus.Output.Type != JTokenType.String)
                return null;

            var outputAsString = durableOrchestrationStatus.Output.Value<string>();

            if (string.IsNullOrEmpty(outputAsString))
                return null;

            return outputAsString;
        }

        public static T GetInput<T>(DurableOrchestrationStatus durableOrchestrationStatus)
            where T : class
        {
            if (durableOrchestrationStatus.Input == null)
                return null;

            if (durableOrchestrationStatus.Input.Type != JTokenType.Object)
                return null;

            var jObject = (JObject)durableOrchestrationStatus.Input;

            var input = jObject.ToObject<T>();

            return input;
        }

        public static T GetProgress<T>(DurableOrchestrationStatus durableOrchestrationStatus)
            where T : class
        {
            if (durableOrchestrationStatus.CustomStatus == null)
                return null;

            if (durableOrchestrationStatus.CustomStatus.Type != JTokenType.Object)
                return null;

            var jObject = (JObject)durableOrchestrationStatus.CustomStatus;

            var progress = jObject.ToObject<T>();

            return progress;
        }

    }
}
