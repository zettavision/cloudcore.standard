﻿using System;
using System.Collections.Generic;
using System.Text;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.Orchestration
{
    public static class OrchestrationIdConverter
    {


        public static string ConvertToDomainId(string aggregateName, string infrastructureId)
        {
            RequiredStringValidator.Validate(infrastructureId, nameof(infrastructureId));

            var prefix = GetPrefix(aggregateName);
            if (!infrastructureId.StartsWith(prefix))
                throw new Exception($"Infrastructure Id ({infrastructureId}) does not start with the prefix {prefix}.");

            var invertedIdAsString = infrastructureId.Substring(prefix.Length).TrimStart('0');
            var invertedIdAsLong = long.Parse(invertedIdAsString);
            var idAsLong = long.MaxValue - invertedIdAsLong;

            return idAsLong.ToString();
        }

        public static string ConvertToInfrastructureId(string aggregateName, string domainId)
        {
            var prefix = GetPrefix(aggregateName);
            RequiredStringValidator.Validate(domainId, nameof(domainId));
            var domainIdAsLong = long.Parse(domainId);
            var infrastructureId = $"{prefix}{InvertAndPadForInt64(domainIdAsLong)}";
            return infrastructureId;
        }

        public static string GetNextDomainId(string domainId)
        {
            if (domainId == null)
                return "1";

            var domainIdAsLong = long.Parse(domainId);
            var nextDomainIdAsLong = domainIdAsLong + 1;
            return nextDomainIdAsLong.ToString();
        }

        public static string GetNextInfrastructureId(string aggregateName, string infrastructureId)
        {
            RequiredStringValidator.Validate(infrastructureId, nameof(infrastructureId));

            var prefix = GetPrefix(aggregateName);
            if (!infrastructureId.StartsWith(prefix))
                throw new Exception($"Infrastructure Id ({infrastructureId}) does not start with the prefix {prefix}.");

            var invertedIdAsString = infrastructureId.Substring(prefix.Length).TrimStart('0');
            var invertedIdAsLong = long.Parse(invertedIdAsString);
            var domainIdAsLong = long.MaxValue - invertedIdAsLong;
            var nextDomainIdAsLong = domainIdAsLong + 1;

            var nextInfrastructureId = $"{prefix}{InvertAndPadForInt64(nextDomainIdAsLong)}";
            return nextInfrastructureId;
        }

        public static string GetPrefix(string aggregateName)
        {
            RequiredStringValidator.Validate(aggregateName, nameof(aggregateName));
            return $"{aggregateName.ToLowerInvariant()}.";
        }


        private static string PadForInt64(string key)
        {
            return key.PadLeft(19, '0');
        }

        private static string InvertAndPadForInt64(string id)
        {
            var idAsLong = long.Parse(id);
            var idAsInvertedLong = long.MaxValue - idAsLong;
            return PadForInt64(idAsInvertedLong.ToString());
        }

        private static string InvertAndPadForInt64(long idAsLong)
        {
            var idAsInvertedLong = long.MaxValue - idAsLong;
            return PadForInt64(idAsInvertedLong.ToString());
        }
    }
}
