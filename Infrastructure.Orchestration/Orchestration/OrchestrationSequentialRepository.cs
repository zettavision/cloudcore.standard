﻿using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.DurableTask.ContextImplementations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Orchestration.Exceptions;
using ZettaVision.CloudCore.Domain.Orchestration.Models;
using ZettaVision.CloudCore.Domain.Orchestration.Infrastructure;
using ZettaVision.CloudCore.Domain.Orchestration.Queries;
using ZettaVision.CloudCore.Domain.LastOrchestration.Infrastructure;
using System.Linq;

namespace ZettaVision.CloudCore.Infrastructure.Orchestration
{
    public class OrchestrationSequentialRepository<T, T2> : IOrchestrationSequentialRepository<T, T2>
     where T : AggregateIdentifier, new()
     where T2 : OrchestrationModel
    {
        private readonly IDurableClient durableClient;
        private readonly IOrchestrationSequentialReader<T2> orchestrationReader;
        private readonly string aggregateName;
        private readonly ILastOrchestrationSettingsReader lastOrchestrationSettingsReader;

        public OrchestrationSequentialRepository(
            IDurableClientFactory durableClientFactory,
            IOrchestrationSequentialReader<T2> orchestrationReader,
            string aggregateName,
            ILastOrchestrationSettingsReader lastOrchestrationSettingsReader)
        {
            durableClient = durableClientFactory.CreateClient();
            this.orchestrationReader = orchestrationReader;
            this.aggregateName = aggregateName;
            this.lastOrchestrationSettingsReader = lastOrchestrationSettingsReader;
        }

        public async Task<T2> CreateAsync(object input, CancellationToken cancellationToken)
        {
            T2 last;
            try
            {
                last = await orchestrationReader.HandleAsync(new OrchestrationSequentialGetLastQuery()).ConfigureAwait(false);
            }
            catch (ObjectNotFoundException)
            {
                last = null;
            }

            if (last != null
                && (last.Status == OrchestrationStatusEnum.Running || last.Status == OrchestrationStatusEnum.Pending))
            {

                var autoRestartThreshold = await GetAutoRestartThresholdAsync().ConfigureAwait(false);
                var timeSinceLastModified = CalculateOrchestrationTimeSinceLastModified(last);
                var pastAutoRestartThreshold =
                    autoRestartThreshold != null
                    && timeSinceLastModified != null
                    && timeSinceLastModified.Value > autoRestartThreshold.Value;

                if (pastAutoRestartThreshold)
                {
                    var lastAggregateIdentifier = new T
                    {
                        Id = last.Id
                    };

                    await TerminateAsync(lastAggregateIdentifier,
                        $"Past Auto Restart Threshold of {autoRestartThreshold} with Time Since Last Modified of {timeSinceLastModified}",
                        cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    if (last.Status == OrchestrationStatusEnum.Running)
                    {
                        var lastWithHistory = await orchestrationReader.HandleAsync(new OrchestrationSequentialGetByIdQuery() { Id = last.Id, IncludeHistory = true}).ConfigureAwait(false);

                        bool isRunning = true;

                        if (lastWithHistory.History != null && lastWithHistory.History.Any())
                        {
                            if (lastWithHistory.History.Any(i => i.EventType == "ExecutionCompleted" && i.OrchestrationStatus == "Completed"))
                                isRunning = false;
                        }

                        if(isRunning)
                            throw new OrchestrationAlreadyRunningException($"{aggregateName} {last.Id} is running. Cannot start a new orchestration until the previous one completes.");
                    }

                    if (last.Status == OrchestrationStatusEnum.Pending)
                    {
                        throw new OrchestrationAlreadyRunningException($"{aggregateName} {last.Id} is pending. Cannot start a new orchestration until the previous one completes.");
                    }
                }
            }



            var lastId = last?.Id;

            var nextId = OrchestrationIdConverter.GetNextDomainId(lastId);
            var nextInfrastructureId = OrchestrationIdConverter.ConvertToInfrastructureId(aggregateName, nextId);

            await durableClient.StartNewAsync($"{aggregateName}-RunOrchestratorAsync", nextInfrastructureId, input).ConfigureAwait(false);

            var result = await orchestrationReader.HandleAsync(new OrchestrationSequentialGetByIdQuery() { Id = nextId }).ConfigureAwait(false);
            return result;
        }

        private async Task<TimeSpan?> GetAutoRestartThresholdAsync()
        {
            try
            {
                var lastOrchestrationSetting = await lastOrchestrationSettingsReader.GetByTypeFromCacheAsync(aggregateName).ConfigureAwait(false);
                return lastOrchestrationSetting.AutoRestartThreshold;
            }
            catch (ObjectNotFoundException)
            {
                return null;
            }
        }

        private TimeSpan? CalculateOrchestrationTimeSinceLastModified(OrchestrationModel orchestration)
        {
            if (orchestration == null)
                return null;

            if (orchestration.Status == OrchestrationStatusEnum.Running
                 || orchestration.Status == OrchestrationStatusEnum.Pending)
            {
                return DateTime.UtcNow - orchestration.AggregateInfo.LastModifiedDate;
            }
            else
            {
                return null;
            }
        }

        public async Task<T2> RewindAsync(T aggregateIdentifier, string reason, CancellationToken cancellationToken = default)
        {
            RequiredStringValidator.Validate(aggregateIdentifier?.Id, nameof(aggregateIdentifier.Id));
            var infrastructureId = OrchestrationIdConverter.ConvertToInfrastructureId(aggregateName, aggregateIdentifier.Id);

#pragma warning disable CS0618 // Type or member is obsolete
            await durableClient.RewindAsync(infrastructureId, reason).ConfigureAwait(false);
#pragma warning restore CS0618 // Type or member is obsolete

            var result = await orchestrationReader.HandleAsync(new OrchestrationSequentialGetByIdQuery() { Id = aggregateIdentifier.Id }).ConfigureAwait(false);
            return result;
        }

        public async Task<T2> TerminateAsync(T aggregateIdentifier, string reason, CancellationToken cancellationToken = default)
        {
            RequiredStringValidator.Validate(aggregateIdentifier?.Id, nameof(aggregateIdentifier.Id));
            var infrastructureId = OrchestrationIdConverter.ConvertToInfrastructureId(aggregateName, aggregateIdentifier.Id);

            await durableClient.TerminateAsync(infrastructureId, reason).ConfigureAwait(false);

            var result = await orchestrationReader.HandleAsync(new OrchestrationSequentialGetByIdQuery() { Id = aggregateIdentifier.Id }).ConfigureAwait(false);
            return result;
        }


    }
}
