﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZettaVision.CloudCore.Domain.Orchestration.Models
{
    public class OrchestrationHistoryItemJsonModel
    {
        public string EventType { get; set; }
        public DateTime? Timestamp { get; set; }
        public DateTime? ScheduledTime { get; set; }
        public string FunctionName { get; set; }
        public string Name { get; set; }
        public string OrchestrationStatus { get; set; }
        public string Reason { get; set; }
        public string Details { get; set; }

    }
}