﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    /// <summary>
    /// Provides extension methods for Azure Table operations.
    /// </summary>
    internal static class AzureTableExtensions
    {
        internal static TableContinuationToken ToTableContinuationToken(this string continuationTokenString)
        {
            try
            {
                TableContinuationToken tableContinuationToken = null;
                if (!string.IsNullOrWhiteSpace(continuationTokenString))
                {
                    var bytes = Convert.FromBase64String(continuationTokenString);
                    string decodedString = Encoding.UTF8.GetString(bytes);

                    tableContinuationToken = JsonConvert.DeserializeObject<TableContinuationToken>(decodedString);
                }
                return tableContinuationToken;
            }
            catch (Exception exception)
            {
                throw new ArgumentException("ContinuationTokenString is not be parsed.", exception);
            }
        }

        internal static string ToContinuationTokenString(this TableContinuationToken tableContinuationToken)
        {
            if (tableContinuationToken != null)
            {
                var serialized = JsonConvert.SerializeObject(tableContinuationToken);
                var bytes = Encoding.UTF8.GetBytes(serialized);
                return Convert.ToBase64String(bytes);
            }
            return null;
        }
    }
}