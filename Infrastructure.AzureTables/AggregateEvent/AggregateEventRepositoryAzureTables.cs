﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Infrastructure.AggregateEvent;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.AggregateEvent
{
    public class AggregateEventRepositoryAzureTables : IAggregateEventRepository
    {
        private readonly AggregateRepositoryAzureTables<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel> aggregateRepository;
        private readonly AggregateReaderAzureTables<AggregateEventIdentifier, AggregateEventModel> aggregateReader;
        private readonly AggregateEventModelGenericToAggregateWithMetadataMapper aggregateEventModelGenericToAggregateWithMetadataMapper;

        public AggregateEventRepositoryAzureTables(string connectionString, string tableName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateRepository = new AggregateRepositoryAzureTables<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>(connectionString, 
                KeyFormatter.PadForInt32, null, null, tableName, jsonConverters);

            aggregateReader = new AggregateReaderAzureTables<AggregateEventIdentifier, AggregateEventModel>(connectionString, KeyFormatter.PadForInt32, null, tableName, jsonConverters);
            aggregateEventModelGenericToAggregateWithMetadataMapper = new AggregateEventModelGenericToAggregateWithMetadataMapper();
        }

        public AggregateEventRepositoryAzureTables(CloudTableClient tableClient, string tableName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateRepository = new AggregateRepositoryAzureTables<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>(tableClient,
                KeyFormatter.PadForInt32, null, null, tableName, jsonConverters);

            aggregateReader = new AggregateReaderAzureTables<AggregateEventIdentifier, AggregateEventModel>(tableClient, KeyFormatter.PadForInt32, null, tableName, jsonConverters);
            aggregateEventModelGenericToAggregateWithMetadataMapper = new AggregateEventModelGenericToAggregateWithMetadataMapper();
        }

        public async Task<AggregateEventModel> InsertAsync(AggregateEventAggregate aggregate, 
            string userId, DateTime? createDate = null, string lastModifiedByUserId = null, 
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.InsertAsync(aggregate, userId, createDate, lastModifiedByUserId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        public async Task<AggregateWithMetadata<AggregateEventAggregate>> LoadAsync(AggregateEventIdentifier aggregateIdentifier,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var model = await aggregateReader.GetByIdAsync(aggregateIdentifier).ConfigureAwait(false);

            if(aggregateIdentifier.ETag != null && aggregateIdentifier.ETag != model.ETag)
                throw new OptimisticConcurrencyException($"Unable to load '{model.AggregateId}.{model.Id}' etag does not match");

            return aggregateEventModelGenericToAggregateWithMetadataMapper.Map(model);
        }

        public async Task<AggregateEventModel> UpdateAsync(AggregateWithMetadata<AggregateEventAggregate> aggregateWithMetadata, 
            string userId, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.UpdateAsync(aggregateWithMetadata, userId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        public async Task DeleteAsync(AggregateEventIdentifier aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            await aggregateRepository.DeleteAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }
    }
}
