﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Infrastructure.AggregateEvent;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.AggregateEvent
{
    public class AggregateEventReaderAzureTables : IAggregateEventReader
    {
        private readonly AggregateReaderAzureTables<AggregateEventIdentifier, AggregateEventModel> aggregateReader;

        public AggregateEventReaderAzureTables(string connectionString, string tableName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());


            aggregateReader = new AggregateReaderAzureTables<AggregateEventIdentifier, AggregateEventModel>(connectionString, KeyFormatter.PadForInt32, null, tableName, jsonConverters);
        }

        public AggregateEventReaderAzureTables(CloudTableClient tableClient, string tableName = null)
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());


            aggregateReader = new AggregateReaderAzureTables<AggregateEventIdentifier, AggregateEventModel>(tableClient, KeyFormatter.PadForInt32, null, tableName, jsonConverters);
        }

        

        public async Task<PagedResult<AggregateEventModel>> HandleAsync(AggregateEventGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query.OrderReversed == false)
            {
                return await GetAllNormalOrderAsync(query, cancellationToken).ConfigureAwait(false);
            }
            else
            {
                return await GetAllReversedOrderAsync(query, cancellationToken).ConfigureAwait(false);
            }

        }

        private async Task<PagedResult<AggregateEventModel>> GetAllReversedOrderAsync(AggregateEventGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query.OrderReversed == false)
                throw new InvalidOperationException("OrderReversed must be true");

            if (string.IsNullOrEmpty(query.IdLessThanOrEqual))
                throw new InvalidOperationException("IdLessThanOrEqual must set if OrderReversed is true");

            var idLessThanOrEqualAsInt = int.Parse(query.IdLessThanOrEqual);

            if (idLessThanOrEqualAsInt < 1)
                throw new InvalidOperationException("IdLessThanOrEqual must 1 or greater.");

            var idGreaterThanAsInt =
                string.IsNullOrEmpty(query.IdGreaterThan) ? null : (int?)int.Parse(query.IdGreaterThan);
            if (idGreaterThanAsInt != null && idGreaterThanAsInt < 1)
                throw new InvalidOperationException("IdGreaterThanAsInt must 1 or greater if used.");

            if (query.PageParameters == null)
                query.PageParameters = new PageParametersModel();

            if (query.PageParameters?.MaxItemCount == null)
                query.PageParameters.MaxItemCount = 100;
            else if (query.PageParameters.MaxItemCount < 1)
                throw new InvalidOperationException("MaxItemCount cannot be less than 1");
            else if (query.PageParameters.MaxItemCount > 1000)
                throw new InvalidOperationException("MaxItemCount cannot be greater than 1000");

            var page = query.PageParameters.ContinuationToken == null
                ? 0
                : int.Parse(query.PageParameters.ContinuationToken);

            var pageIdLessThanOrEqualAsInt = idLessThanOrEqualAsInt - (page * query.PageParameters.MaxItemCount.Value);
            int? pageIdGreaterThanAsInt = pageIdLessThanOrEqualAsInt - query.PageParameters.MaxItemCount;
            var isLastPage = false;

            if (idGreaterThanAsInt == null)
            {
                if (pageIdGreaterThanAsInt.Value < 1)
                {
                    pageIdGreaterThanAsInt = null;
                    isLastPage = true;
                }
            }
            else if (idGreaterThanAsInt.Value >= pageIdGreaterThanAsInt)
            {
                pageIdGreaterThanAsInt = idGreaterThanAsInt.Value;
                isLastPage = true;
            }


            if (pageIdLessThanOrEqualAsInt < 1 || (pageIdGreaterThanAsInt != null && pageIdGreaterThanAsInt >= pageIdLessThanOrEqualAsInt))
            {
                return new PagedResult<AggregateEventModel>()
                {
                    ContinuationToken = null,
                    Items = new List<AggregateEventModel>()
                };
            }
            else
            {
                string continuationToken = null;
                var list = new List<AggregateEventModel>();

                while (true)
                {
                    var result = await GetAllNormalOrderAsync(new AggregateEventGetAllQuery()
                    {
                        PageParameters = new PageParametersModel()
                        {
                            ContinuationToken = continuationToken,
                            MaxItemCount = query.PageParameters.MaxItemCount
                        },
                        Identifier = query.Identifier,
                        OrderReversed = false,
                        IdGreaterThan = pageIdGreaterThanAsInt?.ToString(),
                        IdLessThanOrEqual = pageIdLessThanOrEqualAsInt.ToString()
                    }, cancellationToken).ConfigureAwait(false);

                    if (result.Items != null && result.Items.Any())
                        list.AddRange(result.Items);

                    if (result.ContinuationToken == null)
                        break;

                    continuationToken = result.ContinuationToken;
                }

                list.Reverse();

                var nextPage = page + 1;

                return new PagedResult<AggregateEventModel>()
                {
                    ContinuationToken = isLastPage ? null : nextPage.ToString(),
                    Items = list
                };
            }
        }

        private async Task<PagedResult<AggregateEventModel>> GetAllNormalOrderAsync(AggregateEventGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (query.OrderReversed)
                throw new InvalidOperationException("OrderReversed must be false");

            var filter = string.Empty;

            if (!string.IsNullOrEmpty(query.IdGreaterThan))
            {
                var idAsPaddedString = KeyFormatter.PadForInt32(query.IdGreaterThan);

                if (filter != string.Empty)
                {
                    filter += " and ";
                }

                filter += TableQuery.GenerateFilterCondition($"RowKey",
                    QueryComparisons.GreaterThan, idAsPaddedString);
            }

            if (!string.IsNullOrEmpty(query.IdLessThanOrEqual))
            {
                var idAsPaddedString = KeyFormatter.PadForInt32(query.IdLessThanOrEqual);

                if (filter != string.Empty)
                {
                    filter += " and ";
                }

                filter += TableQuery.GenerateFilterCondition($"RowKey",
                    QueryComparisons.LessThanOrEqual, idAsPaddedString);
            }

            var result = await
                aggregateReader.GetAllByPartitionAsync(aggregateReader.GetPartitionKeyFromParentId(query.Identifier), filter, query.PageParameters, cancellationToken)
                    .ConfigureAwait(false);

            if (result?.Items?.Any() == true)
            {
                foreach (var item in result.Items)
                {
                    AggregateEventExtendedPropertyCalculator.Calculate(item);
                }
            }

            return result;
        }

        public async Task<AggregateEventModel> HandleAsync(AggregateEventGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await
                aggregateReader.GetByIdAsync(new AggregateEventIdentifier(query.AggregateId, query.Id), cancellationToken)
                    .ConfigureAwait(false);

            AggregateEventExtendedPropertyCalculator.Calculate(result);

            return result;
        }
    }
}
