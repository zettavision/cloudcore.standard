﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class AggregateSingletonReaderAzureTables<T, T2> : IAggregateGetByIdReader<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        private readonly AggregateAzureTableHelper<T> azureTableHelper;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public AggregateSingletonReaderAzureTables(
            CloudTableClient tableClient, 
            Func<string, string> partitionKeyFormatterFunc = null, 
            string tableName = null, 
            IEnumerable<JsonConverter> jsonConverters = null)
        {
            azureTableHelper = new AggregateAzureTableHelper<T>(tableClient, null, partitionKeyFormatterFunc, tableName);
            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
        }

        private JsonSerializerSettings GetJsonSerializerSettings(IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            return jsonSerializerSettings;
        }

        /// <summary>
        /// Used for filtering.  The Partition Identifier must use the Identifier Attributes
        /// </summary>
        public string GetPartitionKey(object partitionIdentifier)
        {
            return azureTableHelper.GetSingletonPartitionKey(partitionIdentifier);
        }

        public async Task<T2> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            var partitionKey = azureTableHelper.GetSingletonPartitionKey(aggregateIdentifier);
            string rowKey = string.Empty;

            var retrieveOperation = TableOperation.Retrieve(partitionKey, rowKey);
            var result = await azureTableHelper.Table.ExecuteAsync(retrieveOperation, cancellationToken).ConfigureAwait(false);

            if (result.HttpStatusCode == 404)
                throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})");

            var tableEntity = (DynamicTableEntity)result.Result;

            return ToModel(tableEntity);
        }

        public async Task<PagedResult<T2>> GetAllAsync(string filter, PageParametersModel pageParameters, CancellationToken cancellationToken = default(CancellationToken))
        {
            var tableQuery = new TableQuery();

            if (!string.IsNullOrEmpty(filter))
            {
                tableQuery.Where(filter);
            }

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            TableQuerySegment<DynamicTableEntity> tableQueryResult;
            try
            {
                tableQueryResult =
                    await azureTableHelper.Table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken, cancellationToken).ConfigureAwait(false);
            }
            catch (StorageException exception)
            {
                if (exception.RequestInformation?.HttpStatusCode == 400)
                    throw new ArgumentException(exception.Message, exception);

                throw;
            }

            return new PagedResult<T2>()
            {
                ContinuationToken = tableQueryResult.ContinuationToken.ToContinuationTokenString(),
                Items = tableQueryResult.Results.Select(ToModel).ToList()
            };
        }

        private T2 ToModel(DynamicTableEntity dynamicTableEntity)
        {
            var aggregateJson = dynamicTableEntity.Properties["Aggregate"].StringValue;
            var aggregateInfoJson = dynamicTableEntity.Properties["AggregateInfo"].StringValue;

            var aggregateModel = JsonConvert.DeserializeObject<T2>(aggregateJson, jsonSerializerSettings);
            aggregateModel.AggregateInfo = JsonConvert.DeserializeObject<AggregateInfoModel>(aggregateInfoJson, jsonSerializerSettings);
            aggregateModel.ETag = dynamicTableEntity.ETag;

            return aggregateModel;
        }
    }
}