﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class AggregateRefresherAzureTables<T, T2, T3> 
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()

    {
        private readonly AggregateReaderAzureTables<T, T3> reader;
        private readonly AggregateRepositoryAzureTables<T, T2, T3> repository;
        private readonly ObjectToAggregateIdentifierMapper<T> objectToAggregateIdentifierMapper;


        public AggregateRefresherAzureTables(
            AggregateReaderAzureTables<T, T3> reader,
            AggregateRepositoryAzureTables<T, T2, T3> repository
            )
        {
            this.reader = reader;
            this.repository = repository;
            objectToAggregateIdentifierMapper = new ObjectToAggregateIdentifierMapper<T>();
        }

        public string GetPartitionKeyFromParentId(string parentId)
        {
            return reader.GetPartitionKeyFromParentId(parentId);
        }

        public async Task<long> RefreshAllAsync(string partionKey = null)
        {
            long refreshedCount = 0;
            string continuationToken = null;

            while (true)
            {
                var result = await reader.GetAllByPartitionAsync(partionKey, null, new PageParametersModel() {ContinuationToken = continuationToken, MaxItemCount = 1000 }).ConfigureAwait(false);



                if (result.Items != null)
                {
                    foreach (var item in result.Items)
                    {
                        var identifier = objectToAggregateIdentifierMapper.Map(item);
                        identifier.ETag = null;
                        var aggregateWithMetadata = await repository.LoadAsync(identifier).ConfigureAwait(false);
                        await repository.UpdateAsync(aggregateWithMetadata, null).ConfigureAwait(false);
                    }


                    refreshedCount = refreshedCount + result.Items.Count();
                }

                if(result.ContinuationToken == null)
                    break;

                continuationToken = result.ContinuationToken;
            }

            return refreshedCount;
        }



    }
}
