﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class CopierAzureTables
    {

        public CopierAzureTables(string sourceConnectionString, string sourceTableName, string destinationConnectionString, string destinationTableName)
        {

            var sourceStorageAccount = CloudStorageAccount.Parse(sourceConnectionString);
            var sourceTableClient = sourceStorageAccount.CreateCloudTableClient();
            sourceTable = sourceTableClient.GetTableReference(sourceTableName);

            var destinationStorageAccount = CloudStorageAccount.Parse(destinationConnectionString);
            var destinationTableClient = destinationStorageAccount.CreateCloudTableClient();
            destinationTable = destinationTableClient.GetTableReference(destinationTableName);
        }

        private readonly CloudTable sourceTable;
        private readonly CloudTable destinationTable;

        /// <summary>
        /// This will copy all items from the source table to the destination table.  This is not delete items in the destination.  If an item already exist it will be overwritten with the source item.
        /// </summary>
        /// <returns>The number of items copied</returns>
        public async Task<long> CopyAllAsync()
        {
            await destinationTable.CreateIfNotExistsAsync().ConfigureAwait(false);

            long insertCount = 0;
            var result = await GetAllFromSourceAsync(new PageParametersModel() { MaxItemCount = 100 }).ConfigureAwait(false);
            await InsertRowsToDestinationAysnc(result?.Items).ConfigureAwait(false);

            if (result?.Items != null)
                insertCount = insertCount + result.Items.Count();

            // ReSharper disable once PossibleNullReferenceException
            while (result.ContinuationToken != null)
            {
                result = await GetAllFromSourceAsync(new PageParametersModel() { ContinuationToken = result.ContinuationToken, MaxItemCount = 100 }).ConfigureAwait(false);
                await InsertRowsToDestinationAysnc(result?.Items).ConfigureAwait(false);
                if (result?.Items != null)
                    insertCount = insertCount + result.Items.Count();
            }

            return insertCount;
        }

        /// <summary>
        /// This will copy all items from the source partition to the destination partition.  This is not delete items in the destination.  If an item already exist it will be overwritten with the source item.
        /// </summary>
        /// <returns>The number of items copied</returns>
        public async Task<long> CopyPartitionAsync(string partitionKey)
        {
            await destinationTable.CreateIfNotExistsAsync().ConfigureAwait(false);

            long insertCount = 0;
            var result = await GetPartitionFromSourceAsync(partitionKey, new PageParametersModel() { MaxItemCount = 100 }).ConfigureAwait(false);
            await InsertRowsToDestinationAysnc(result?.Items).ConfigureAwait(false);

            if (result?.Items != null)
                insertCount = insertCount + result.Items.Count();

            // ReSharper disable once PossibleNullReferenceException
            while (result.ContinuationToken != null)
            {
                result = await GetPartitionFromSourceAsync(partitionKey, new PageParametersModel() { ContinuationToken = result.ContinuationToken, MaxItemCount = 100 }).ConfigureAwait(false);
                await InsertRowsToDestinationAysnc(result?.Items).ConfigureAwait(false);
                if (result?.Items != null)
                    insertCount = insertCount + result.Items.Count();
            }

            return insertCount;
        }

        private async Task InsertRowsToDestinationAysnc(IEnumerable<DynamicTableEntity> tableEntities)
        {

            if (tableEntities == null || !tableEntities.Any())
                return;

            var tableEntitiesGroupByPartitionKey = tableEntities.GroupBy(te => te.PartitionKey);

            foreach (var group in tableEntitiesGroupByPartitionKey)
            {
                var tableBatchOperation = new TableBatchOperation();

                foreach (var tableEntity in group)
                {
                    tableBatchOperation.InsertOrReplace(tableEntity);
                }

                await destinationTable.ExecuteBatchAsync(tableBatchOperation).ConfigureAwait(false);
            }


        }

        private async Task<PagedResult<DynamicTableEntity>> GetAllFromSourceAsync(PageParametersModel pageParameters)
        {
            var tableQuery = new TableQuery();

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            var tableQueryResult =
                await sourceTable.ExecuteQuerySegmentedAsync(tableQuery, continuationToken).ConfigureAwait(false);

            return new PagedResult<DynamicTableEntity>()
            {
                ContinuationToken = tableQueryResult?.ContinuationToken?.ToContinuationTokenString(),
                Items = tableQueryResult.Results
            };
        }

        private async Task<PagedResult<DynamicTableEntity>> GetPartitionFromSourceAsync(string partitionKey, PageParametersModel pageParameters)
        {
            if (string.IsNullOrEmpty(partitionKey))
                partitionKey = string.Empty;

            var tableQuery = new TableQuery();

            tableQuery.FilterString = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            var tableQueryResult =
                await sourceTable.ExecuteQuerySegmentedAsync(tableQuery, continuationToken).ConfigureAwait(false);

            return new PagedResult<DynamicTableEntity>()
            {
                ContinuationToken = tableQueryResult?.ContinuationToken?.ToContinuationTokenString(),
                Items = tableQueryResult.Results
            };
        }
    }
}
