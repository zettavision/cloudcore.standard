﻿using System;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.Azure.Storage;
using Microsoft.Azure.Cosmos.Table;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class AggregateAzureTableHelper<T>
        where T : AggregateIdentifier
    {
        private readonly Func<string, string> partitionKeyFormatterFunc;
        private readonly Func<string, string> rowKeyFormatterFunc;

        public bool IsPartitionKeyFromId { get; set; } = false;

        public AggregateAzureTableHelper(string connectionString, Func<string, string> rowKeyFormatterFunc = null,
            Func<string, string> partitionKeyFormatterFunc = null, string tableName = null)
        {
            this.rowKeyFormatterFunc = rowKeyFormatterFunc;
            this.partitionKeyFormatterFunc = partitionKeyFormatterFunc;

            var storageAccount = CloudStorageAccount.Parse(connectionString);
      
            var tableClient = storageAccount.CreateCloudTableClient();
            tableName = tableName ?? GetTableName();

            Table = tableClient.GetTableReference(tableName);
        }

        public AggregateAzureTableHelper(CloudTableClient tableClient, Func<string, string> rowKeyFormatterFunc = null,
    Func<string, string> partitionKeyFormatterFunc = null, string tableName = null)
        {
            this.rowKeyFormatterFunc = rowKeyFormatterFunc;
            this.partitionKeyFormatterFunc = partitionKeyFormatterFunc;
            tableName = tableName ?? GetTableName();
            Table = tableClient.GetTableReference(tableName);
        }

        public CloudTable Table { get; }


        private string GetTableName()
        {
            var idIdentifierPluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
            var tableName = idIdentifierPluralName.ToLowerInvariant();

            if (tableName.Length > 63)
                tableName = tableName.Substring(0, 63);

            if (tableName.Length < 3)
                tableName = tableName.PadRight(3, 'x');

            return tableName;
        }

        public string GetSingletonPartitionKey(object aggregate)
        {
            var identifierList = new List<string>();

            var properties = aggregate.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    if (!string.IsNullOrEmpty(identifierAttribute?.PluralName))
                        identifierList.Add(property.GetValue(aggregate) as string);
                }
            }

            string partitionKey = !identifierList.Any() ? string.Empty : string.Join(".", identifierList);

            return GetPartitionKeyFromParentId(partitionKey);
        }

        public string GetPartitionKey(object aggregate)
        {
            var identifierList = new List<string>();

            var properties = aggregate.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    if (string.IsNullOrEmpty(identifierAttribute?.PluralName))
                        continue;

                    if (IsPartitionKeyFromId)
                    {
                        if (property.Name != "Id")
                            continue;
                    }
                    else
                    {
                        if (property.Name == "Id")
                            continue;
                    }

                    identifierList.Add(property.GetValue(aggregate) as string);
                }
            }

            string partitionKey = !identifierList.Any() ? string.Empty : string.Join(".", identifierList);

            if (IsPartitionKeyFromId)
            {
                if (string.IsNullOrEmpty(partitionKey))
                    return string.Empty;

                if (!partitionKey.Contains("."))
                    return string.Empty;

                var lastPeriod = partitionKey.LastIndexOf('.');
                if (lastPeriod == 0)
                    return string.Empty;

                partitionKey = partitionKey.Substring(0, lastPeriod);
            }

            return GetPartitionKeyFromParentId(partitionKey);
        }

        public string GetPartitionKeyFromParentId(string parentId)
        {
            if (string.IsNullOrEmpty(parentId))
                parentId = string.Empty;

            return partitionKeyFormatterFunc != null ? partitionKeyFormatterFunc(parentId): parentId;
        }

        public string GetRowKey(object aggregate)
        {
            var properties = aggregate.GetType().GetProperties();

            var idProperty = properties.First(p => p.Name == "Id");

            var id = idProperty.GetValue(aggregate) as string;

            return GetRowKeyFromId(id);
        }

        public string GetRowKeyFromId(string id)
        {
            return rowKeyFormatterFunc != null ? rowKeyFormatterFunc(id) : id;
        }
    }
}