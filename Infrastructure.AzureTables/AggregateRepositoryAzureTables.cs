﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class AggregateRepositoryAzureTables<T, T2, T3> : IAggregateRepository<T, T2, T3>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {
        private readonly IAggregateExtendedProperties<T2> aggregateExtendedProperties;
        private readonly AggregateAzureTableHelper<T> azureTableHelper;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly string aggregatePluralName;
        private bool isPartitionKeyFromId = false;

        public bool IsPartitionKeyFromId
        {
            get => isPartitionKeyFromId;
            set
            {
                isPartitionKeyFromId = value;

                if (azureTableHelper != null)
                {
                    azureTableHelper.IsPartitionKeyFromId = value;
                }
            }
        }

        public AggregateRepositoryAzureTables(string connectionString, Func<string, string> rowKeyFormatterFunc = null,
            Func<string, string> partitionKeyFormatterFunc = null, IAggregateExtendedProperties<T2> aggregateExtendedProperties = null, 
            string tableName = null, IEnumerable<JsonConverter> jsonConverters = null)
        {
            this.aggregateExtendedProperties = aggregateExtendedProperties;
            azureTableHelper = new AggregateAzureTableHelper<T>(connectionString, rowKeyFormatterFunc,
                partitionKeyFormatterFunc, tableName);

            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }


        public AggregateRepositoryAzureTables(CloudTableClient tableClient, Func<string, string> rowKeyFormatterFunc = null,
    Func<string, string> partitionKeyFormatterFunc = null, IAggregateExtendedProperties<T2> aggregateExtendedProperties = null,
    string tableName = null, IEnumerable<JsonConverter> jsonConverters = null)
        {
            this.aggregateExtendedProperties = aggregateExtendedProperties;
            azureTableHelper = new AggregateAzureTableHelper<T>(tableClient, rowKeyFormatterFunc,
                partitionKeyFormatterFunc, tableName);

            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }


        private JsonSerializerSettings GetJsonSerializerSettings(IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            return jsonSerializerSettings;
        }

        private DynamicTableEntity AddExtenedProperties(DynamicTableEntity entity, T2 aggregate, AggregateInfoValueObject aggregateInfo)
        {
            entity.Properties.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.CreatedDate)}", 
                new EntityProperty(aggregateInfo.CreatedDate) );
            entity.Properties.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.LastModifiedDate)}",
                new EntityProperty(aggregateInfo.LastModifiedDate));

            entity.Properties.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.CreatedByUserId)}",
                new EntityProperty(aggregateInfo.CreatedByUserId));
            entity.Properties.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.LastModifiedByUserId)}",
                new EntityProperty(aggregateInfo.LastModifiedByUserId));

            entity.Properties.Add($"AggregateInfo_{nameof(AggregateInfoValueObject.Version)}",
                new EntityProperty(aggregateInfo.Version));


            if (aggregateExtendedProperties?.ExtendedProperties == null)
                return entity;


            foreach (var extendedProperty in aggregateExtendedProperties.ExtendedProperties)
            {
                var value = extendedProperty.Map(aggregate, aggregateInfo);
                var type = extendedProperty.ReturnType;

                if (type == typeof(DateTime) || type == typeof(DateTime?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as DateTime?));
                }
                else if (type == typeof(string))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as string));
                }
                else if (type == typeof(double) || type == typeof(double?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as double?));
                }
                else if (type == typeof(DateTimeOffset) || type == typeof(DateTimeOffset?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as DateTimeOffset?));
                }
                else if (type == typeof(Guid) || type == typeof(Guid?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as Guid?));
                }
                else if (type == typeof(bool) || type == typeof(bool?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as bool?));
                }
                else if (type == typeof(byte[]))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as byte[]));
                }
                else if (type == typeof(int) || type == typeof(int?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as int?));
                }
                else if (type == typeof(long) || type == typeof(long?))
                {
                    entity.Properties.Add(extendedProperty.Name, new EntityProperty(value as long?));
                }
                else
                {
                    throw new Exception($"Invalid Azure Table Extended Properity Type {type.Name}");
                }

            }

            return entity;
        }

        public async Task<T3> InsertAsync(T2 aggregate, string userId, DateTime? createDate = null, string lastModifiedByUserId = null, 
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                if (createDate == null)
                    createDate = DateTime.UtcNow;

                var partitionKey = azureTableHelper.GetPartitionKey(aggregate);

                string rowKey;
                try
                {
                    rowKey = azureTableHelper.GetRowKey(aggregate);
                }
                catch (Exception rowKeyException)
                {
                    throw new ObjectNotFoundException($"Object not found. {rowKeyException.Message}", rowKeyException);
                }

                var aggregateInfo = new AggregateInfoValueObject(userId, createDate.Value);

                if (lastModifiedDate != null)
                    aggregateInfo = aggregateInfo.MarkModified(lastModifiedByUserId, lastModifiedDate.Value);

                var aggregateJson = JsonConvert.SerializeObject(aggregate, jsonSerializerSettings);
                var aggregateInfoJson = JsonConvert.SerializeObject(aggregateInfo, jsonSerializerSettings);

                var entity = new DynamicTableEntity(partitionKey, rowKey, "*",
                    new Dictionary<string, EntityProperty>
                    {
                        {"Aggregate", new EntityProperty(aggregateJson)},
                        {"AggregateInfo", new EntityProperty(aggregateInfoJson)}
                    });

                entity = AddExtenedProperties(entity, aggregate, aggregateInfo);

                var result =
                    await azureTableHelper.Table.ExecuteAsync(TableOperation.Insert(entity), cancellationToken).ConfigureAwait(false);

                var model = JsonConvert.DeserializeObject<T3>(aggregateJson, jsonSerializerSettings);
                model.ETag = result.Etag;
                model.AggregateInfo = JsonConvert.DeserializeObject<AggregateInfoModel>(aggregateInfoJson,
                    jsonSerializerSettings);

                return model;
            }
            catch (StorageException ex)
            {
                var statusCode = ex.RequestInformation.HttpStatusCode;
                if (statusCode != (int)HttpStatusCode.Conflict) throw;

                throw new ObjectAlreadyExistsException($"Object Already Exists ({FullyQualifiedIdentifier.GetIdentifier(aggregate)})", ex)
                    .AddToData("Error", "RepositoryObjectAlreadyExists")
                    .AddToData("AggregatePluralName", aggregatePluralName); 
            }
        }

        public async Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            var partitionKey = azureTableHelper.GetPartitionKey(aggregateIdentifier);

            string rowKey;
            try
            {
                rowKey = azureTableHelper.GetRowKey(aggregateIdentifier);
            }
            catch (Exception rowKeyException)
            {
                throw new ObjectNotFoundException($"Object Not found. {rowKeyException.Message}", rowKeyException)
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName); 
            }
            


            var retrieveOperation = TableOperation.Retrieve(partitionKey, rowKey);
            var result = await azureTableHelper.Table.ExecuteAsync(retrieveOperation, cancellationToken).ConfigureAwait(false);

            if (result.HttpStatusCode == 404)
                throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})")
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);


            if ((aggregateIdentifier.ETag != null) && (result.Etag != aggregateIdentifier.ETag))
                throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})")
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName);

            var tableEntity = (DynamicTableEntity) result.Result;
            var aggregateJson = tableEntity.Properties["Aggregate"].StringValue;
            var aggregateInfoJson = tableEntity.Properties["AggregateInfo"].StringValue;

            var aggreateWithMetadata = new AggregateWithMetadata<T2>
            {
                Aggregate = JsonConvert.DeserializeObject<T2>(aggregateJson, jsonSerializerSettings),
                AggregateInfo =
                    JsonConvert.DeserializeObject<AggregateInfoValueObject>(aggregateInfoJson, jsonSerializerSettings),
                ETag = result.Etag
            };

            return aggreateWithMetadata;
        }

        public async Task<T3> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId,
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                if (lastModifiedDate == null)
                    lastModifiedDate = DateTime.UtcNow;

                var partitionKey = azureTableHelper.GetPartitionKey(aggregateWithMetadata.Aggregate);


                string rowKey;
                try
                {
                    rowKey = azureTableHelper.GetRowKey(aggregateWithMetadata.Aggregate);
                }
                catch (Exception rowKeyException)
                {
                    throw new ObjectNotFoundException($"Object not found. {rowKeyException.Message}", rowKeyException);
                }

    
                var aggregateInfo = aggregateWithMetadata.AggregateInfo.MarkModified(userId, lastModifiedDate.Value);

                var aggregateJson = JsonConvert.SerializeObject(aggregateWithMetadata.Aggregate, jsonSerializerSettings);
                var aggregateInfoJson = JsonConvert.SerializeObject(aggregateInfo, jsonSerializerSettings);

                var entity = new DynamicTableEntity(partitionKey, rowKey, aggregateWithMetadata.ETag,
                    new Dictionary<string, EntityProperty>
                    {
                        {"Aggregate", new EntityProperty(aggregateJson)},
                        {"AggregateInfo", new EntityProperty(aggregateInfoJson)}
                    });

                entity = AddExtenedProperties(entity, aggregateWithMetadata.Aggregate, aggregateInfo);

                var result =
                    await azureTableHelper.Table.ExecuteAsync(TableOperation.Replace(entity), cancellationToken).ConfigureAwait(false);

                var model = JsonConvert.DeserializeObject<T3>(aggregateJson, jsonSerializerSettings);
                model.ETag = result.Etag;
                model.AggregateInfo = JsonConvert.DeserializeObject<AggregateInfoModel>(aggregateInfoJson,
                    jsonSerializerSettings);

                return model;
            }
            catch (StorageException ex)
            {
                var statusCode = ex.RequestInformation.HttpStatusCode;
                if (statusCode == (int)HttpStatusCode.PreconditionFailed)
                    throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate)})", ex)
                        .AddToData("Error", "RepositoryOptimisticConcurrency")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                if (statusCode == (int)HttpStatusCode.NotFound)
                    throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate)})", ex)
                        .AddToData("Error", "RepositoryObjectNotFound")
                        .AddToData("AggregatePluralName", aggregatePluralName);

                throw;
            }
        }

        public async Task DeleteAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            var partitionKey = azureTableHelper.GetPartitionKey(aggregateIdentifier);

            string rowKey;
            try
            {
                rowKey = azureTableHelper.GetRowKey(aggregateIdentifier);
            }
            catch (Exception rowKeyException)
            {
                throw new ObjectNotFoundException($"Object not found. {rowKeyException.Message}", rowKeyException);
            }

            var retrieveOperation = TableOperation.Retrieve(partitionKey, rowKey);
            var result = await azureTableHelper.Table.ExecuteAsync(retrieveOperation, cancellationToken).ConfigureAwait(false);

            if (result.HttpStatusCode == 404)
                throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})")
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);

            if ((aggregateIdentifier.ETag != null) && (result.Etag != aggregateIdentifier.ETag))
                throw new OptimisticConcurrencyException($"Optimistic Concurrency Error ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})")
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName);

            var tableEntity = (DynamicTableEntity)result.Result;

            var deleteOperation = TableOperation.Delete(tableEntity);
            await azureTableHelper.Table.ExecuteAsync(deleteOperation, cancellationToken).ConfigureAwait(false);
        }


    }
}