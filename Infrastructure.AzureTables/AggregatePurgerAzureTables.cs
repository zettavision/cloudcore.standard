﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class AggregatePurgerAzureTables<T> : IAggregatePurger
                where T : AggregateIdentifier
    {
        private readonly AggregateAzureTableHelper<T> azureTableHelper;

        public AggregatePurgerAzureTables(string connectionString, Func<string, string> rowKeyFormatterFunc = null,
            Func<string, string> partitionKeyFormatterFunc = null, 
            string tableName = null)
        {

            azureTableHelper = new AggregateAzureTableHelper<T>(connectionString, rowKeyFormatterFunc,
                partitionKeyFormatterFunc, tableName);
        }

        public AggregatePurgerAzureTables(CloudTableClient tableClient, Func<string, string> rowKeyFormatterFunc = null,
            Func<string, string> partitionKeyFormatterFunc = null,
            string tableName = null)
        {

            azureTableHelper = new AggregateAzureTableHelper<T>(tableClient, rowKeyFormatterFunc,
                partitionKeyFormatterFunc, tableName);
        }

        public async Task<long> PurgeAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            long deletedCount = 0;
            var result = await GetAllAsync(new PageParametersModel() { MaxItemCount = 100 }, cancellationToken).ConfigureAwait(false);
            await DeleteRowsAysnc(result?.Items, cancellationToken).ConfigureAwait(false);

            if (result?.Items != null)
                deletedCount = deletedCount + result.Items.Count();

            while (result.ContinuationToken != null)
            {
                result = await GetAllAsync(
                    new PageParametersModel() {ContinuationToken = result.ContinuationToken, MaxItemCount = 100},
                    cancellationToken).ConfigureAwait(false);
                await DeleteRowsAysnc(result?.Items, cancellationToken).ConfigureAwait(false);
                if (result?.Items != null)
                    deletedCount = deletedCount + result.Items.Count();
            }

            return deletedCount;
        }

        private async Task DeleteRowsAysnc(IEnumerable<DynamicTableEntity> tableEntities,
            CancellationToken cancellationToken = default(CancellationToken))
        {

            if (tableEntities == null || !tableEntities.Any())
                return;

            var tableEntitiesGroupByPartitionKey = tableEntities.GroupBy(te => te.PartitionKey);

            foreach (var group in tableEntitiesGroupByPartitionKey)
            {
                var tableBatchOperation = new TableBatchOperation();

                foreach (var tableEntity in group)
                {
                    tableBatchOperation.Delete(tableEntity);
                }

                await azureTableHelper.Table.ExecuteBatchAsync(tableBatchOperation, cancellationToken).ConfigureAwait(false);
            }


        }

        public async Task<PagedResult<DynamicTableEntity>> GetAllAsync(PageParametersModel pageParameters,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var tableQuery = new TableQuery();

            tableQuery.SelectColumns = new List<string>() { "PartitionKey", "RowKey" };

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            var tableQueryResult =
                await azureTableHelper.Table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken, cancellationToken).ConfigureAwait(false);

            return new PagedResult<DynamicTableEntity>()
            {
                ContinuationToken = tableQueryResult?.ContinuationToken?.ToContinuationTokenString(),
                Items = tableQueryResult.Results
            };
        }
    }
}
