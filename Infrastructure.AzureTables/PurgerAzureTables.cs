﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public class PurgerAzureTables 
    {
        public PurgerAzureTables(string connectionString, string tableName)
        {

            var storageAccount = CloudStorageAccount.Parse(connectionString);
            var tableClient = storageAccount.CreateCloudTableClient();

            table = tableClient.GetTableReference(tableName);
        }

        public PurgerAzureTables(CloudTableClient tableClient, string tableName)
        {
            table = tableClient.GetTableReference(tableName);
        }

        private readonly CloudTable table;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The number of items deleted</returns>
        public async Task<long> PurgeAllAsync()
        {
            await table.CreateIfNotExistsAsync().ConfigureAwait(false);

            long deletedCount = 0;
            var result = await GetAllAsync(new PageParametersModel() { MaxItemCount = 100 }).ConfigureAwait(false);
            await DeleteRowsAysnc(result?.Items).ConfigureAwait(false);

            if (result?.Items != null)
                deletedCount = deletedCount + result.Items.Count();

            // ReSharper disable once PossibleNullReferenceException
            while (result.ContinuationToken != null)
            {
                result = await GetAllAsync(new PageParametersModel() { ContinuationToken = result.ContinuationToken, MaxItemCount = 100 }).ConfigureAwait(false);
                await DeleteRowsAysnc(result?.Items).ConfigureAwait(false);
                if (result?.Items != null)
                    deletedCount = deletedCount + result.Items.Count();
            }

            return deletedCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partitionKey"></param>
        /// <returns>The number of items deleted</returns>
        public async Task<long> PurgePartitionAsync(string partitionKey)
        {
            await table.CreateIfNotExistsAsync().ConfigureAwait(false);

            long deletedCount = 0;
            var result = await GetPartitionAsync(partitionKey, new PageParametersModel() { MaxItemCount = 100 }).ConfigureAwait(false);
            await DeleteRowsAysnc(result?.Items).ConfigureAwait(false);

            if (result?.Items != null)
                deletedCount = deletedCount + result.Items.Count();

            // ReSharper disable once PossibleNullReferenceException
            while (result.ContinuationToken != null)
            {
                result = await GetPartitionAsync(partitionKey, new PageParametersModel() { ContinuationToken = result.ContinuationToken, MaxItemCount = 100 }).ConfigureAwait(false);
                await DeleteRowsAysnc(result?.Items).ConfigureAwait(false);
                if (result?.Items != null)
                    deletedCount = deletedCount + result.Items.Count();
            }

            return deletedCount;
        }

        private async Task DeleteRowsAysnc(IEnumerable<DynamicTableEntity> tableEntities)
        {

            if (tableEntities == null || !tableEntities.Any())
                return;

            var tableEntitiesGroupByPartitionKey = tableEntities.GroupBy(te => te.PartitionKey);

            foreach (var group in tableEntitiesGroupByPartitionKey)
            {
                var tableBatchOperation = new TableBatchOperation();

                foreach (var tableEntity in group)
                {
                    tableBatchOperation.Delete(tableEntity);
                }

                await table.ExecuteBatchAsync(tableBatchOperation).ConfigureAwait(false);
            }


        }

        private async Task<PagedResult<DynamicTableEntity>> GetAllAsync(PageParametersModel pageParameters)
        {
            var tableQuery = new TableQuery();

            tableQuery.SelectColumns = new List<string>() { "PartitionKey", "RowKey" };

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            var tableQueryResult =
                await table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken).ConfigureAwait(false);

            return new PagedResult<DynamicTableEntity>()
            {
                ContinuationToken = tableQueryResult?.ContinuationToken?.ToContinuationTokenString(),
                Items = tableQueryResult.Results
            };
        }

        private async Task<PagedResult<DynamicTableEntity>> GetPartitionAsync(string partitionKey, PageParametersModel pageParameters)
        {
            if (string.IsNullOrEmpty(partitionKey))
                partitionKey = string.Empty;

            var tableQuery = new TableQuery();

            tableQuery.FilterString = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);
            tableQuery.SelectColumns = new List<string>() { "PartitionKey", "RowKey" };

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            var tableQueryResult =
                await table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken).ConfigureAwait(false);

            return new PagedResult<DynamicTableEntity>()
            {
                ContinuationToken = tableQueryResult?.ContinuationToken?.ToContinuationTokenString(),
                Items = tableQueryResult.Results
            };
        }
    }
}
