﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Infrastructure;
using ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Queries;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;


namespace ZettaVision.CloudCore.Infrastructure.AzureTables.AggregatePointInTimeSnapshot
{
    public class AggregatePointInTimeSnapshotReaderAzureTables<T,T2> :
         IAggregatePointInTimeSnapshotReader<T,T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        private readonly AggregateAzureTableHelper<T> azureTableHelper;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public AggregatePointInTimeSnapshotReaderAzureTables(string connectionString, string tableName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            RequiredStringValidator.Validate(tableName, nameof(tableName));

            azureTableHelper = new AggregateAzureTableHelper<T>(connectionString, null,
                null, tableName);

            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
        }

        private JsonSerializerSettings GetJsonSerializerSettings(IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            return jsonSerializerSettings;
        }

        public AggregatePointInTimeSnapshotReaderAzureTables(CloudTableClient tableClient, string tableName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            RequiredStringValidator.Validate(tableName, nameof(tableName));

            azureTableHelper = new AggregateAzureTableHelper<T>(tableClient, null,
                null, tableName);

            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
        }

        

        private string GetPartitionKey(T identifier)
        {
            var partitionKey = azureTableHelper.GetPartitionKey(identifier);
            var rowKey = azureTableHelper.GetRowKey(identifier);

            if (string.IsNullOrEmpty(partitionKey))
                return rowKey;

            return $"{partitionKey}.{rowKey}";
        }

        public async Task<PagedResult<T2>> HandleAsync(AggregatePointInTimeSnapshotGetAllQuery<T> query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var partitionKey = GetPartitionKey(query.Identifier);

            var partitionKeyFilter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);

            return await GetAllAsync(partitionKeyFilter, query.PageParameters, cancellationToken).ConfigureAwait(false);
        }

        private async Task<PagedResult<T2>> GetAllAsync(string filter, PageParametersModel pageParameters, CancellationToken cancellationToken = default(CancellationToken))
        {
            var tableQuery = new TableQuery();

            if (!string.IsNullOrEmpty(filter))
            {
                tableQuery.Where(filter);
            }

            if (pageParameters?.MaxItemCount != null)
                tableQuery.TakeCount = pageParameters.MaxItemCount.Value;

            TableContinuationToken continuationToken = null;

            if (pageParameters?.ContinuationToken != null)
                continuationToken = pageParameters.ContinuationToken.ToTableContinuationToken();

            var tableQueryResult =
                await azureTableHelper.Table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken, cancellationToken).ConfigureAwait(false);

            return new PagedResult<T2>()
            {
                ContinuationToken = tableQueryResult?.ContinuationToken?.ToContinuationTokenString(),
                Items = tableQueryResult.Results.Select(ToModel).ToList()
            };
        }

        private T2 ToModel(DynamicTableEntity dynamicTableEntity)
        {
            var modelJson = dynamicTableEntity.Properties["Model"].StringValue;
            var aggregateModel = JsonConvert.DeserializeObject<T2>(modelJson, jsonSerializerSettings);
            return aggregateModel;
        }

        public async Task<T2> HandleAsync(AggregatePointInTimeSnapshotGetByPointInTimeQuery<T> query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var partitionKey = GetPartitionKey(query.Identifier);
            var partitionKeyFilter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);

            var dateRowKey = KeyFormatter.InvertAndPadForInt64(query.PointInTime.Ticks);
            var filter = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, dateRowKey);

            var newFilter = TableQuery.CombineFilters(partitionKeyFilter, TableOperators.And, filter);

            string continuationToken = null;

            while (true)
            {
                var result = await GetAllAsync(newFilter, 
                    new PageParametersModel(){ContinuationToken = continuationToken, MaxItemCount = 1},
                    cancellationToken)
                    .ConfigureAwait(false);

                if (result.Items != null && result.Items.Any())
                {
                    var item = result.Items.Single();
                    return item;
                }

                if (result.ContinuationToken == null)
                    break;

                continuationToken = result.ContinuationToken;
            }

            throw new ObjectNotFoundException($"Object not found for {partitionKey} and {query.PointInTime}");
            
        }
    }
}
