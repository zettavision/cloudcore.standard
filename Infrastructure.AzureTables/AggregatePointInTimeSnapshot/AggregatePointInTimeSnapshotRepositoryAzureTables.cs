﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Infrastructure;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables.AggregatePointInTimeSnapshot
{
    public class AggregatePointInTimeSnapshotRepositoryAzureTables<T, T2> : IAggregatePointInTimeSnapshotRepository<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {

        private readonly AggregateAzureTableHelper<T> azureTableHelper;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public AggregatePointInTimeSnapshotRepositoryAzureTables(string connectionString, string tableName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            RequiredStringValidator.Validate(tableName, nameof(tableName));

            azureTableHelper = new AggregateAzureTableHelper<T>(connectionString, null,
                null, tableName);

            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
        }

        public AggregatePointInTimeSnapshotRepositoryAzureTables(CloudTableClient tableClient, string tableName, IEnumerable<JsonConverter> jsonConverters = null)
        {
            RequiredStringValidator.Validate(tableName, nameof(tableName));

            azureTableHelper = new AggregateAzureTableHelper<T>(tableClient, null,
                null, tableName);

            jsonSerializerSettings = GetJsonSerializerSettings(jsonConverters);
        }

        private JsonSerializerSettings GetJsonSerializerSettings(IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            return jsonSerializerSettings;
        }




        private string GetPartitionKey(T identifier)
        {
            var partitionKey = azureTableHelper.GetPartitionKey(identifier);
            var rowKey = azureTableHelper.GetRowKey(identifier);

            if (string.IsNullOrEmpty(partitionKey))
                return rowKey;

            return $"{partitionKey}.{rowKey}";
        }

        public async Task InsertAsync(T identifier, T2 model, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var partitionKey = GetPartitionKey(identifier);
                var rowKey = KeyFormatter.InvertAndPadForInt64(model.AggregateInfo.LastModifiedDate.Ticks);

                var modelJson = JsonConvert.SerializeObject(model, jsonSerializerSettings);

                var entity = new DynamicTableEntity(partitionKey, rowKey, "*",
                    new Dictionary<string, EntityProperty>
                    {
                        {"Model", new EntityProperty(modelJson)},
                    });

                await azureTableHelper.Table.ExecuteAsync(TableOperation.Insert(entity), cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (StorageException ex)
            {
                var statusCode = (HttpStatusCode)ex?.RequestInformation?.HttpStatusCode;
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
                if ((statusCode != null) && (statusCode == HttpStatusCode.Conflict))
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
                    throw new ObjectAlreadyExistsException(null, ex);

                throw;
            }
        }
    }
}
