﻿using System.Net;
using Microsoft.Azure.Cosmos.Table;

namespace ZettaVision.CloudCore.Infrastructure.AzureTables
{
    public static class StorageExceptionReader
    {
        public static HttpStatusCode? GetStatusCode(this StorageException ex)
        {
            if ((ex.InnerException != null) && ex.InnerException is WebException)
            {
                var webException = (WebException) ex.InnerException;
                if ((webException.Response != null) && webException.Response is HttpWebResponse)
                {
                    var httpWebResponse = (HttpWebResponse) webException.Response;

                    return httpWebResponse.StatusCode;
                }
            }
            return null;
        }
    }
}