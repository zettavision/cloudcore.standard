﻿using System;
using ZettaVision.CloudCore.Domain.Run.Commands.Requests;
using ZettaVision.CloudCore.Domain.Run.Models;

namespace ZettaVision.CloudCore.Domain.Run.Aggregate
{
    public abstract class RunAggregate : IAggregate
    {
        protected RunAggregate()
        {
            State = RunStateEnum.Waiting;
        }

        public string Error { get; private set; }

        public string CancelError { get; private set; }

        public RunStateEnum State { get; protected set; }

        public DateTime? CancelRequestedDate { get; private set; }

        public DateTime? RunDate { get; private set; }

        public TimeSpan? Duration { get; private set; }

        public string Id { get; protected set; }

        public void Apply(RunRunRequest request)
        {
            if (State != RunStateEnum.Waiting)
                throw new InvalidOperationException("Cannot run unless the state is waiting");

            State = RunStateEnum.Running;
            RunDate = DateTime.UtcNow;
            Duration = TimeSpan.Zero;
        }

        public void Apply(RunCancelRequest request)
        {
            if ((State != RunStateEnum.Running) && (State != RunStateEnum.Waiting))
                throw new InvalidOperationException("Cannot cancel unless it is in the 'Running' or 'Waiting' state");

            State = RunStateEnum.Canceling;
            CancelRequestedDate = DateTime.UtcNow;
            UpdateDuration();
        }

        protected void UpdateDuration()
        {
            if(RunDate == null)
                return;

            Duration = DateTime.UtcNow - RunDate.Value;
        }

        public void Apply(RunCanceledRequest request)
        {
            if ((State != RunStateEnum.Canceling) && (State != RunStateEnum.Running) && (State != RunStateEnum.Waiting))
                throw new InvalidOperationException(
                    "Cannot change state to canceled unless it is in the 'Running' or Canceling state");

            CancelError = request.CancelError;
            State = RunStateEnum.Canceled;
            UpdateDuration();
        }

        public void Apply(RunCompletedRequest request)
        {
            if (State != RunStateEnum.Running)
                throw new InvalidOperationException("Cannot mark completed unless the state is running");

            State = RunStateEnum.Completed;
            UpdateDuration();
        }

        public void Apply(RunErrorRequest request)
        {
            if ((State != RunStateEnum.Running) && (State != RunStateEnum.Waiting) && (State != RunStateEnum.Canceling))
                throw new InvalidOperationException(
                    "Error not be mark unless the state is running, waiting, or canceling");

            State = RunStateEnum.Error;
            Error = request.Error;
            UpdateDuration();
        }
    }
}