﻿using System;
using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.Run.Models
{
    public abstract class RunModel : IAggregateModel
    {
        public string Id { get; set; }

        public string Error { get; set; }

        public string CancelError { get; set; }

        [EnumDataType(typeof(RunStateEnum))]
        public RunStateEnum State { get; set; }

        public DateTime? CancelRequestedDate { get; set; }

        public DateTime? RunDate { get; set; }
        public TimeSpan? Duration { get; set; }

        public string ETag { get; set; }

        public AggregateInfoModel AggregateInfo { get; set; }
    }
}