﻿namespace ZettaVision.CloudCore.Domain.Run.Models
{
    public enum RunStateEnum
    {
        Waiting,
        Running,
        Completed,
        Error,
        Canceled,
        Canceling
    }
}