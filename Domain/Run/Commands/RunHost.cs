﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Run.Aggregate;
using ZettaVision.CloudCore.Domain.Run.Commands.Requests;
using ZettaVision.CloudCore.Domain.Run.Infrastructure;
using ZettaVision.CloudCore.Domain.Run.Models;

namespace ZettaVision.CloudCore.Domain.Run.Commands
{
    public class RunHost<T, T2, T3> 
        where T : AggregateIdentifier
        where T2 : RunAggregate
        where T3 : RunModel
    {
        private readonly IRunRepository<T, T2, T3> runRepository;
        private readonly IRunRunner<T> runRunner;
        private readonly IExceptionErrorReader exceptionErrorReader;

        public RunHost(
            IRunRepository<T, T2, T3> runRepository,
            IRunRunner<T> runRunner, IExceptionErrorReader exceptionErrorReader = null)
        {
            this.runRepository = runRepository;
            this.runRunner = runRunner;
            this.exceptionErrorReader = exceptionErrorReader;
        }

        public async Task RunAsync(T identifier, string userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            await MarkAsRunningAsync(identifier, userId, cancellationToken).ConfigureAwait(false);

            try
            {
                identifier.ETag = null;
                await runRunner.RunAsync(identifier, cancellationToken).ConfigureAwait(false);

                await MarkAsCompleteAsync(identifier, userId, cancellationToken).ConfigureAwait(false);
            }
            catch (OperationCanceledException operationCanceledException)
            {
                // ReSharper disable once MethodSupportsCancellation
                MarkAsCanceledAsync(identifier, userId, operationCanceledException)
                    .Wait();
            }
            catch (Exception ex)
            {
                // ReSharper disable once MethodSupportsCancellation
                MarkErrorAsync(identifier, userId, ex).Wait();
            }
        }
        
        public async Task<T3> CancelAsync(T identifier, string userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var runAggregateWithMetadata =
                await runRepository.LoadAsync(identifier, cancellationToken).ConfigureAwait(false);

            runAggregateWithMetadata.Aggregate.Apply(new RunCancelRequest());

            return
                await
                    runRepository.UpdateAsync(runAggregateWithMetadata, userId, null, cancellationToken)
                        .ConfigureAwait(false);
        }

        public async Task<T3> MarkErrorAsync(T identifier, string error, string userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var runAggregateWithMetadata =
                await runRepository.LoadAsync(identifier,  cancellationToken).ConfigureAwait(false);

            runAggregateWithMetadata.Aggregate.Apply(new RunErrorRequest
            {
                Error = error
            });

            return
                await
                    runRepository.UpdateAsync(runAggregateWithMetadata, userId, null, cancellationToken)
                        .ConfigureAwait(false);
        }

        private async Task MarkErrorAsync(T identifier, string userId, Exception ex,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var runAggregateWithMetadata =
                await runRepository.LoadAsync(identifier, cancellationToken).ConfigureAwait(false);

            if ((runAggregateWithMetadata.Aggregate.State == RunStateEnum.Running)
                || (runAggregateWithMetadata.Aggregate.State == RunStateEnum.Waiting)
                || (runAggregateWithMetadata.Aggregate.State == RunStateEnum.Canceling))
            {
                runAggregateWithMetadata.Aggregate.Apply(new RunErrorRequest
                {
                    Error = GetErrorMessage(ex)
                });
                await
                    runRepository.UpdateAsync(runAggregateWithMetadata,
                            userId, null, cancellationToken)
                        .ConfigureAwait(false);
            }
        }

        private string GetErrorMessage(Exception ex)
        {
            if (exceptionErrorReader == null)
                return ex.ToString();

            return exceptionErrorReader.GetErrorMessage(ex);
        }

        private async Task MarkAsCanceledAsync(T identifier, string userId,
            OperationCanceledException operationCanceledException,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var runAggregateWithMetadata =
                await runRepository.LoadAsync(identifier, cancellationToken).ConfigureAwait(false);
            if ((runAggregateWithMetadata.Aggregate.State == RunStateEnum.Waiting)
                || (runAggregateWithMetadata.Aggregate.State == RunStateEnum.Running)
                || (runAggregateWithMetadata.Aggregate.State == RunStateEnum.Canceling))
            {
                runAggregateWithMetadata.Aggregate.Apply(new RunCanceledRequest
                {
                    CancelError = operationCanceledException.ToString()
                });
                await
                    runRepository.UpdateAsync(runAggregateWithMetadata,
                            userId, null, cancellationToken)
                        .ConfigureAwait(false);
            }
        }

        private async Task MarkAsRunningAsync(T identifier, string userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var runAggregateWithMetadata =
                await runRepository.LoadAsync(identifier, cancellationToken).ConfigureAwait(false);
            runAggregateWithMetadata.Aggregate.Apply(new RunRunRequest());
            await runRepository.UpdateAsync(runAggregateWithMetadata, userId, null, cancellationToken)
                .ConfigureAwait(false);
        }

        private async Task MarkAsCompleteAsync(T identifier, string userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var runAggregateWithMetadata =
                await runRepository.LoadAsync(identifier, cancellationToken).ConfigureAwait(false);
            runAggregateWithMetadata.Aggregate.Apply(new RunCompletedRequest());
            await runRepository.UpdateAsync(runAggregateWithMetadata, userId, null, cancellationToken)
                .ConfigureAwait(false);
        }
    }
}