namespace ZettaVision.CloudCore.Domain.Run.Commands.Requests
{
    public class RunErrorRequest : IRunRequest
    {
        public string Error { get; set; }
    }
}