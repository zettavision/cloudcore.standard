﻿namespace ZettaVision.CloudCore.Domain.Run.Commands.Requests
{
    public class RunCanceledRequest : IRunRequest
    {
        public string CancelError { get; set; }
    }
}