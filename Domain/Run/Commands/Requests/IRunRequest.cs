﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.Run.Commands.Requests
{
    public interface IRunRequest : IAggregateRequest
    {
    }
}