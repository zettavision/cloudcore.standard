using System.Threading;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Domain.Run.Infrastructure
{
    public interface IRunRunner<in T> where T : AggregateIdentifier
    {
        Task RunAsync(T identifier, CancellationToken cancellationToken = default(CancellationToken));
    }
}