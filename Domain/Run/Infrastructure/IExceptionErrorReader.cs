﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Domain.Run.Infrastructure
{
    public interface IExceptionErrorReader
    {
        string GetErrorMessage(Exception ex);
    }
}
