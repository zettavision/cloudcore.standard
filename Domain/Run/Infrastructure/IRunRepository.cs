﻿using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Run.Aggregate;
using ZettaVision.CloudCore.Domain.Run.Models;

namespace ZettaVision.CloudCore.Domain.Run.Infrastructure
{
    public interface IRunRepository<T, T2, T3> :
        IAggregateRepository<T, T2, T3> 
        where T : AggregateIdentifier
        where T2 : RunAggregate
        where T3 : RunModel
    {
    }
}