﻿using System;

namespace ZettaVision.CloudCore.Domain
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class IdentifierAttribute : Attribute
    {
        public IdentifierAttribute(string pluralName)
        {
            PluralName = pluralName;
        }

        public string PluralName { get; private set; }
    }
}