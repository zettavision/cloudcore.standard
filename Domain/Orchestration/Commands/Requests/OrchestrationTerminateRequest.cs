﻿namespace ZettaVision.CloudCore.Domain.Orchestration.Commands.Requests
{
    public class OrchestrationTerminateRequest : IOrchestrationRequest
    {
        public string Reason { get; set; }
    }
}