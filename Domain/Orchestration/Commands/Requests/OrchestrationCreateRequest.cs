﻿namespace ZettaVision.CloudCore.Domain.Orchestration.Commands.Requests
{
    public class OrchestrationCreateRequest<T> : IOrchestrationRequest
        where T : class
    {
        public T Input { get; set; }
    }
}