﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.Orchestration.Commands.Requests
{
    public interface IOrchestrationRequest : IAggregateRequest
    {
    }
}