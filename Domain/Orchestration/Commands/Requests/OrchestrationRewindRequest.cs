﻿namespace ZettaVision.CloudCore.Domain.Orchestration.Commands.Requests
{
    public class OrchestrationRewindRequest : IOrchestrationRequest
    {
        public string Reason { get; set; }
    }
}