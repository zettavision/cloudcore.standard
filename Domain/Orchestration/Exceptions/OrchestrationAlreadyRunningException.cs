﻿using System;

namespace ZettaVision.CloudCore.Domain.Orchestration.Exceptions
{
    public class OrchestrationAlreadyRunningException : InvalidOperationException
    {
        public OrchestrationAlreadyRunningException()
        {

        }

        public OrchestrationAlreadyRunningException(string message) : base(message)
        {

        }

        public OrchestrationAlreadyRunningException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
