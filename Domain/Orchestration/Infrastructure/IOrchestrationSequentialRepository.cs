﻿

using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Orchestration.Models;

namespace ZettaVision.CloudCore.Domain.Orchestration.Infrastructure
{
    public interface IOrchestrationSequentialRepository<T, T2> : IOrchestrationRepository<T, T2>
     where T : AggregateIdentifier
     where T2 : OrchestrationModel
    {
        Task<T2> CreateAsync(object input, CancellationToken cancellationToken = default(CancellationToken));
    }
}