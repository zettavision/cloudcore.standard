﻿

using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Orchestration.Models;

namespace ZettaVision.CloudCore.Domain.Orchestration.Infrastructure
{
    public interface IOrchestrationRepository<T, T2>
        where T : AggregateIdentifier
        where T2 : OrchestrationModel
    {
        Task<T2> RewindAsync(T aggregateIdentifier, string reason, CancellationToken cancellationToken = default(CancellationToken));
        Task<T2> TerminateAsync(T aggregateIdentifier, string reason, CancellationToken cancellationToken = default(CancellationToken));
    }
}