﻿

using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Orchestration.Models;
using ZettaVision.CloudCore.Domain.Orchestration.Queries;

namespace ZettaVision.CloudCore.Domain.Orchestration.Infrastructure
{
    public interface IOrchestrationSequentialReader<T>
        where T : OrchestrationModel
    {
        Task<T> HandleAsync(OrchestrationSequentialGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken));
        Task<PagedResult<T>> HandleAsync(OrchestrationSequentialGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken));
        Task<T> HandleAsync(OrchestrationSequentialGetLastQuery query, CancellationToken cancellationToken = default(CancellationToken));
    }
}