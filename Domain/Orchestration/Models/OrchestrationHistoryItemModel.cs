﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZettaVision.CloudCore.Domain.Orchestration.Models
{
    public class OrchestrationHistoryItemModel
    {
        [Required]
        public string EventType { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }
        public DateTime? ScheduledTime { get; set; }
        public TimeSpan? Duration { get; set; }
        public string FunctionName { get; set; }
        public string OrchestrationStatus { get; set; }
        public string Reason { get; set; }
        public string Details { get; set; }

    }
}