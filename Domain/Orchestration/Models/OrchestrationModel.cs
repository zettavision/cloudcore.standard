﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.Orchestration.Models
{
    public abstract class OrchestrationModel : IAggregateModel
    {
        [Required]
        public string Id
        {
            get;
            set;
        }

        public string ETag
        {
            get;
            set;
        }

        [Required]
        public AggregateInfoModel AggregateInfo
        {
            get;
            set;
        }

        [Required]
        public TimeSpan Duration { get; set; }

        [Required]
        public OrchestrationStatusEnum Status { get; set; }

        public string Error { get; set; }

        public IEnumerable<OrchestrationHistoryItemModel> History { get; set; }

    }
}