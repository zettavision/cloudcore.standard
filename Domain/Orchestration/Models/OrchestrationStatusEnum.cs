﻿namespace ZettaVision.CloudCore.Domain.Orchestration.Models
{
    public enum OrchestrationStatusEnum
    {
        //
        // Summary:
        //     The status of the orchestration could not be determined.
        Unknown,
        //
        // Summary:
        //     The orchestration is running (it may be actively running or waiting for input).
        Running,
        //
        // Summary:
        //     The orchestration ran to completion.
        Completed,
        //
        // Summary:
        //     The orchestration completed with ContinueAsNew as is in the process of restarting.
        ContinuedAsNew,
        //
        // Summary:
        //     The orchestration failed with an error.
        Failed,
        //
        // Summary:
        //     The orchestration was canceled.
        Canceled,
        //
        // Summary:
        //     The orchestration was terminated via an API call.
        Terminated,
        //
        // Summary:
        //     The orchestration was scheduled but has not yet started.
        Pending
    }
}