﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Orchestration.Models;

namespace ZettaVision.CloudCore.Domain.Orchestration.Queries
{
    public interface IOrchestrationQueryHandler<T>
        where T : OrchestrationModel
    {
        Task<T> HandleAsync(OrchestrationSequentialGetByIdQuery query,
            CancellationToken cancellationToken = default);
        Task<PagedResult<T>> HandleAsync(OrchestrationSequentialGetAllQuery query,
            CancellationToken cancellationToken = default);

        Task<T> HandleAsync(OrchestrationSequentialGetLastQuery query,
            CancellationToken cancellationToken = default);
    }
}