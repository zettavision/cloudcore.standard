﻿using System;
using System.Collections.Generic;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Orchestration.Models;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.Orchestration.Queries
{

    public class OrchestrationSequentialGetAllQuery
    {
        public PageParametersModel PageParameters
        {
            get;
            set;
        }


        public IEnumerable<OrchestrationStatusEnum> Statuses
        {
            get;
            set;
        }


        public DateTime? CreatedAfter
        {
            get;
            set;
        }

        public DateTime? CreatedBefore
        {
            get;
            set;
        }

    }
}