﻿using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Domain.Orchestration.Queries
{
    public class OrchestrationSequentialGetByIdQuery
    {
        public OrchestrationSequentialGetByIdQuery()
        {
            IncludeHistory = false;
        }

        public string Id { get; set; }
        public bool IncludeHistory { get; set; }
    }
}