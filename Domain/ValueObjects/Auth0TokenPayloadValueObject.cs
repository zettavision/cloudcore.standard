﻿using System;
using System.Collections.Generic;

namespace ZettaVision.CloudCore.Domain.ValueObjects
{
    public class Auth0TokenPayloadValueObject
    {
        private Auth0TokenPayloadValueObject()
        {
        }

        public Auth0TokenPayloadValueObject(string userId, IEnumerable<string> roles)
        {
            UserId = userId;
            Roles = roles;
        }

        public Auth0TokenPayloadValueObject(string userId, IEnumerable<string> roles, DateTime? expirationDate)
        {
            UserId = userId;
            Roles = roles;
            ExpirationDate = expirationDate;
        }

        public Auth0TokenPayloadValueObject(string userId, IEnumerable<string> roles, DateTime? expirationDate, string json)
        {
            UserId = userId;
            Roles = roles;
            ExpirationDate = expirationDate;
            Json = json;
        }

        public string UserId { get; private set; }
        public IEnumerable<string> Roles { get; private set; }
        public DateTime? ExpirationDate { get; private set; }
        public string Json { get; private set; }
    }
}