﻿using System;

namespace ZettaVision.CloudCore.Domain.ValueObjects
{
    public class AggregateInfoValueObject
    {
        public AggregateInfoValueObject()
        {
        }

        public AggregateInfoValueObject(string createdByUserId, DateTime createdDate)
        {
            Version = 1;

            CreatedDate = createdDate;
            CreatedByUserId = createdByUserId;

            LastModifiedDate = createdDate;
            LastModifiedByUserId = createdByUserId;
        }

        public DateTime LastModifiedDate { get; private set; }
        public string LastModifiedByUserId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedByUserId { get; private set; }
        public int Version { get; private set; }

        public AggregateInfoValueObject Clone()
        {
            var newAggregateInfoValueObject = new AggregateInfoValueObject();

            newAggregateInfoValueObject.Version = Version;
            newAggregateInfoValueObject.CreatedDate = CreatedDate;
            newAggregateInfoValueObject.CreatedByUserId = CreatedByUserId;
            newAggregateInfoValueObject.LastModifiedDate = LastModifiedDate;
            newAggregateInfoValueObject.LastModifiedByUserId = LastModifiedByUserId;

            return newAggregateInfoValueObject;
        }

        public AggregateInfoValueObject MarkModified(string lastModifiedByUserId, DateTime lastModified)
        {
            var newAggregateInfoValueObject = new AggregateInfoValueObject();

            newAggregateInfoValueObject.Version = Version + 1;

            newAggregateInfoValueObject.CreatedDate = CreatedDate;
            newAggregateInfoValueObject.CreatedByUserId = CreatedByUserId;

            newAggregateInfoValueObject.LastModifiedDate = lastModified;
            newAggregateInfoValueObject.LastModifiedByUserId = lastModifiedByUserId;

            return newAggregateInfoValueObject;
        }
    }
}