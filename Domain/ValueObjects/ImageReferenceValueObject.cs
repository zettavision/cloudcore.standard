using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.ValueObjects
{
    public class ImageReferenceValueObject
    {
        private ImageReferenceValueObject()
        {
        }


        public ImageReferenceValueObject(string url, int height, int width, long length, string contentType, string md5Hash)
        {
            Url = url;
            Height = height;
            Width = width;
            Length = length;
            ContentType = contentType;
            Md5Hash = md5Hash;
        }

        public ImageReferenceValueObject(ImageReferenceModel model)
        {
            Url = model.Url;
            Height = model.Height;
            Width = model.Width;
            Length = model.Length;
            ContentType = model.ContentType;
            Md5Hash = model.Md5Hash;
        }

        public string Url { get; private set; }
        public int Height { get; private set; }
        public int Width { get; private set; }
        public long Length { get; private set; }
        public string Md5Hash { get; private set; }
        public string ContentType { get; private set; }
    }
}