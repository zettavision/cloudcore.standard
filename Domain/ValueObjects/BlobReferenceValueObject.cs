using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.ValueObjects
{
    public class BlobReferenceValueObject
    {
        private BlobReferenceValueObject()
        {
        }

        public BlobReferenceValueObject(string url, long length, string contentType, string md5Hash)
        {
            Url = url;
            Length = length;
            ContentType = contentType;
            Md5Hash = md5Hash;
        }

        public BlobReferenceValueObject(BlobReferenceModel model)
        {
            Url = model.Url;
            Length = model.Length;
            ContentType = model.ContentType;
            Md5Hash = model.Md5Hash;
        }

        public string Url { get; private set; }
        public long Length { get; private set; }
        public string Md5Hash { get; private set; }
        public string ContentType { get; private set; }
    }
}