﻿namespace ZettaVision.CloudCore.Domain
{
    public interface IAggregate
    {
        string Id { get; }
    }
}