﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZettaVision.CloudCore.Domain
{
    public static class Chunker
    {
        public static IEnumerable<IEnumerable<T>> ChunkBy<T>(IEnumerable<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}
