﻿using System.Collections.Generic;

namespace ZettaVision.CloudCore.Domain
{
    public class PagedResult<T>
    {
        public IEnumerable<T> Items { get; set; }
        public string ContinuationToken { get; set; }
        public long? Count { get; set; }
    }
}