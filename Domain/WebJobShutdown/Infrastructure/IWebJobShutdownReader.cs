﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Queries;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Infrastructure
{
    public interface IWebJobShutdownReader
    {
        Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetByIdQuery query,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<PagedResult<WebJobShutdownModel>> HandleAsync(WebJobShutdownGetAllQuery query,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}