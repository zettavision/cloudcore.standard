using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Aggregate;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Infrastructure
{
    public interface IWebJobShutdownRepository : IAggregateRepository<WebJobShutdownIdentifier, WebJobShutdownAggregate, WebJobShutdownModel>
    {
    }
}