﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands.Requests;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Commands
{
    public interface IWebJobShutdownCommandHandler
    {
        Task<WebJobShutdownModel> HandleAsync(WebJobShutdownCommand<WebJobShutdownCreateRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}