﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Aggregate;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands.Requests;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Infrastructure;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Commands
{
    public class WebJobShutdownCommandHandler : IWebJobShutdownCommandHandler
    {
        private readonly IWebJobShutdownRepository webJobShutdownRepository;


        public WebJobShutdownCommandHandler(
            IWebJobShutdownRepository webJobShutdownRepository)
        {
            this.webJobShutdownRepository = webJobShutdownRepository;
        }

        public async Task<WebJobShutdownModel> HandleAsync(WebJobShutdownCommand<WebJobShutdownCreateRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            command.Identifier.Id = GetNextId();

            var webJobShutdownAggregate = new WebJobShutdownAggregate(command.Identifier, command.Request);
            return await webJobShutdownRepository
                .InsertAsync(webJobShutdownAggregate, command.CreatedByUserId, null, null, null, cancellationToken)
                .ConfigureAwait(false);
        }

        private string GetNextId()
        {
            var invertedTicks = DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks;
            return $"{invertedTicks:D19}";
        }
        
    }
}