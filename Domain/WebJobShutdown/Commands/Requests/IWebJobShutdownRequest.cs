﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Commands.Requests
{
    public interface IWebJobShutdownRequest : IAggregateRequest
    {
    }
}