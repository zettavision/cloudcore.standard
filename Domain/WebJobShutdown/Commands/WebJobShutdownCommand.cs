﻿using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands.Requests;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Commands
{
    public class WebJobShutdownCommand<T> : AggregateCommand<WebJobShutdownIdentifier, T>
        where T : IWebJobShutdownRequest
    {
        public WebJobShutdownCommand(WebJobShutdownIdentifier identifier, T request, string userId)
            : base(identifier, request, userId)
        {
        }
    }
}