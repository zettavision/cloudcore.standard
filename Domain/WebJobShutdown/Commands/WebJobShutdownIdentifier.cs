﻿namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Commands
{
    public class WebJobShutdownIdentifier : AggregateIdentifier
    {
        public WebJobShutdownIdentifier()
        {
        }

        public WebJobShutdownIdentifier(string id)
        {
            Id = id;
        }

        public WebJobShutdownIdentifier(string id, string eTag)
        {
            Id = id;
            ETag = eTag;
        }



        [Identifier("WebJobShutdowns")]
        public sealed override string Id
        {
            get;
            set;
        }
    }
}