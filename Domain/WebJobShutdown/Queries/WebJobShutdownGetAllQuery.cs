﻿using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Queries
{
    [Identifier("WebJobShutdowns")]
    public class WebJobShutdownGetAllQuery
    {
        public PageParametersModel PageParameters
        {
            get;
            set;
        }
    }
}