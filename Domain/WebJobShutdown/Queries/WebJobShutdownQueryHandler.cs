﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Infrastructure;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Queries
{
    public class WebJobShutdownQueryHandler : IWebJobShutdownQueryHandler
    {


        private readonly IWebJobShutdownReader webJobShutdownReader;


        public WebJobShutdownQueryHandler(
            IWebJobShutdownReader webJobShutdownReader)
        {
            this.webJobShutdownReader = (webJobShutdownReader);
        }

        public async Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetLastQuery query,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            string continuationToken = null;
            while (true)
            {
                var result = await webJobShutdownReader.HandleAsync(new WebJobShutdownGetAllQuery()
                {
                    PageParameters = new PageParametersModel()
                    {
                        ContinuationToken = continuationToken,
                        MaxItemCount = 1
                    }
                }, cancellationToken).ConfigureAwait(false);

                if (result.Items != null && result.Items.Any())
                    return result.Items.Single();

                if (result.ContinuationToken == null)
                    break;

                continuationToken = result.ContinuationToken;
            }

            throw new ObjectNotFoundException("WebJobShutdowns doesn't have any items.  Last not found.");
        }

        public async Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetLastOrNullQuery query,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                return await HandleAsync(new WebJobShutdownGetLastQuery(), cancellationToken).ConfigureAwait(false);
            }
            catch (ObjectNotFoundException)
            {
                return null;
            }
        }

        public async Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetByIdQuery query,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await webJobShutdownReader.HandleAsync(query, cancellationToken).ConfigureAwait(false);
        }

        public async Task<PagedResult<WebJobShutdownModel>> HandleAsync(WebJobShutdownGetAllQuery query,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await webJobShutdownReader.HandleAsync(query, cancellationToken).ConfigureAwait(false);
        }
    }
}