﻿namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Queries
{
    public class WebJobShutdownGetByIdQuery
    {
        [Identifier("WebJobShutdowns")]
        public string Id
        {
            get;
            set;
        }
    }
}