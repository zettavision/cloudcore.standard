﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Queries
{
    public interface IWebJobShutdownQueryHandler
    {
        Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetLastQuery query,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetLastOrNullQuery query,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<WebJobShutdownModel> HandleAsync(WebJobShutdownGetByIdQuery query,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<PagedResult<WebJobShutdownModel>> HandleAsync(WebJobShutdownGetAllQuery query,
            CancellationToken cancellationToken = default(CancellationToken));
    

        
    }
}