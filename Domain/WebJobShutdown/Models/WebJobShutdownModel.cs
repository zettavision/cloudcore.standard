﻿using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Models
{
    public class WebJobShutdownModel : IAggregateModel
    {
        public string Id
        {
            get;
            set;
        }

        public string ETag
        {
            get;
            set;
        }

        public AggregateInfoModel AggregateInfo
        {
            get;
            set;
        }
    }
}