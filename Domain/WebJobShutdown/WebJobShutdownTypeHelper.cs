﻿using System.Collections.Generic;
using ZettaVision.CloudCore.Domain.Exceptions;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown
{
    public static class WebJobShutdownTypeHelper
    {
        public static string GetType(string collectionId, string id)
        {
            RequiredStringValidator.Validate(collectionId, nameof(collectionId));
            RequiredStringValidator.Validate(id, nameof(id));

  
            switch (collectionId)
            {
                case "distributors":
                    return GetDistributorType(id);
                case "virtualitems":
                    return GetVirtualItemType(id);
                case "emailtemplates":
                    return GetEmailTemplateType(id);
                case "termsandconditionsagreements":
                    return GetTermsAndConditionsAgreementType(id);
                case "questions":
                    return GetQuestionType(id);
                case "questionoptions":
                    return GetQuestionOptionType(id);
                case "questionnaires":
                    return GetQuestionnaireType(id);
                default:
                    throw new ObjectNotFoundException($"Collection {collectionId} does not exist");
            }
        }

        public static IEnumerable<string> GetTypes(string collectionId)
        {
            RequiredStringValidator.Validate(collectionId, nameof(collectionId));

            switch (collectionId)
            {
                case "distributors":
                    return DistributorTypes;
                case "virtualitems":
                    return VirtualItemTypes;
                case "emailtemplates":
                    return EmailTemplateTypes;
                case "termsandconditionsagreements":
                    return TermsAndConditionsAgreementTypes;
                case "questions":
                    return QuestionTypes;
                case "questionoptions":
                    return QuestionOptionTypes;
                case "questionnaires":
                    return QuestionnaireTypes;
                default:
                    throw new ObjectNotFoundException($"Collection Id {collectionId} does not exist");
            }
        }



        private static string GetDistributorType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 4)
            {
                if (parts[1] == "distributorproducts")
                {
                    if (parts[3] == "name")
                        return "distributorproducts.name";
                    else if (parts[3] == "description")
                        return "distributorproducts.description";
                }
            }

            throw new ObjectNotFoundException($"English Text Distributor Type not found with id {id}");
        }
        private static IEnumerable<string> DistributorTypes => new[] {"distributorproducts.name", "distributorproducts.description"};

        private static string GetVirtualItemType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 2)
            {
                if (parts[1] == "name")
                    return "name";
                else if (parts[1] == "description")
                    return "description";
            }

            throw new ObjectNotFoundException($"English Text Virtual Item Type not found with id {id}");
        }
        private static IEnumerable<string> VirtualItemTypes => new[] {"name", "description"};

        private static string GetEmailTemplateType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 2)
            {
                if (parts[1] == "subject")
                    return "subject";
                else if (parts[1] == "body")
                    return "body";
            }

            throw new ObjectNotFoundException($"English Text Email Template Type not found with id {id}");
        }
        private static IEnumerable<string> EmailTemplateTypes => new[] {"subject", "body"};

        private static string GetTermsAndConditionsAgreementType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 2)
            {
                if (parts[1] == "body")
                    return "body";
            }

            throw new ObjectNotFoundException($"English Text Terms and Conditions Agreement Type not found with id {id}");
        }
        private static IEnumerable<string> TermsAndConditionsAgreementTypes => new[] { "body"};

        private static string GetQuestionType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 2)
            {
                if (parts[1] == "name")
                    return "name";
            }

            throw new ObjectNotFoundException($"English Text Question Type not found with id {id}");
        }
        private static IEnumerable<string> QuestionTypes => new[] { "name"};

        private static string GetQuestionOptionType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 2)
            {
                if (parts[1] == "name")
                    return "name";
            }

            throw new ObjectNotFoundException($"English Text Question Type not found with id {id}");
        }
        private static IEnumerable<string> QuestionOptionTypes => new[] { "name"};

        
        private static string GetQuestionnaireType(string id)
        {
            var parts = id.Split('.');

            if (parts.Length == 2)
            {
                if (parts[1] == "name")
                    return "name";
            }
            else if (parts.Length == 4)
            {
                if (parts[1] == "sections")
                {
                    if (parts[3] == "name")
                        return "sections.name";
                }
            }

            throw new ObjectNotFoundException($"English Text Question Type not found with id {id}");
        }
        private static IEnumerable<string> QuestionnaireTypes => new[] { "name", "sections.name"};
        
    }
}
