﻿using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands;
using ZettaVision.CloudCore.Domain.WebJobShutdown.Commands.Requests;

namespace ZettaVision.CloudCore.Domain.WebJobShutdown.Aggregate
{
    public class WebJobShutdownAggregate : IAggregate
    {
 
        private string id;
  

        private WebJobShutdownAggregate()
        {

        }



        [Identifier("WebJobShutdowns")]
        public string Id
        {
            get { return id; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Id));
                id = value;
            }
        }





        public WebJobShutdownAggregate(WebJobShutdownIdentifier identifier, WebJobShutdownCreateRequest request)
        {
            Id = identifier.Id;
        }
        
    }
}