﻿namespace ZettaVision.CloudCore.Domain
{
    public class AggregateFunctionResult
    {
        public double Result { get; set; }
        public string ContinuationToken { get; set; }
    }
}