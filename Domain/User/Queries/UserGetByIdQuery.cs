namespace ZettaVision.CloudCore.Domain.User.Queries
{
    public class UserGetByIdQuery
    {
        [Identifier("Users")]
        public string Id { get; set; }
    }
}