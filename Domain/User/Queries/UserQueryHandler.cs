﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Queries
{
    public class UserQueryHandler : IUserQueryHandler
    {
        private readonly IUserReader userReader;

        public UserQueryHandler(IUserReader userReader)
        {
            this.userReader = userReader;
        }

        public async Task<UserModel> HandleAsync(UserGetByIdQuery query)
        {
            return await userReader.HandleAsync(query).ConfigureAwait(false);
        }

        public async Task<PagedResult<UserModel>> HandleAsync(UserGetAllQuery query)
        {
            return await userReader.HandleAsync(query).ConfigureAwait(false);
        }
    }
}