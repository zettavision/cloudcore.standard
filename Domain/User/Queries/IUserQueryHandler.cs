﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Queries
{
    public interface IUserQueryHandler
    {
        Task<UserModel> HandleAsync(UserGetByIdQuery query);

        Task<PagedResult<UserModel>> HandleAsync(UserGetAllQuery query);
    }
}