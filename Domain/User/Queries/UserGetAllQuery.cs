﻿using System;
using System.Collections.Generic;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Queries
{
    [Identifier("Users")]
    public class UserGetAllQuery
    {
        public string Email { get; set; }
        public IEnumerable<string> Ids { get; set; }

        public PageParametersModel PageParameters { get; set; }
        public DateTime? LastModifiedDateOnOrAfter { get; set; }
        public UserSortPropertyEnum? Sort { get; set; }
        public bool? SortAscending { get; set; }
    }
}