﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ZettaVision.CloudCore.Domain.User
{
    public static class PinHasher
    {
        public static string Hash(string pin, string salt)
        {
            RequiredStringValidator.Validate(pin, nameof(pin));
            RequiredStringValidator.Validate(salt, nameof(salt));
            
            byte[] saltByteArray = Encoding.UTF8.GetBytes(salt);
            byte[] hashByteArray = Hash(Encoding.UTF8.GetBytes(pin), saltByteArray);

            return Encoding.UTF8.GetString(hashByteArray, 0, hashByteArray.Length);
        }

        private static byte[] Hash(byte[] value, byte[] salt)
        {
            var hashAlgorithm = new HMACSHA256(salt);
            return hashAlgorithm.ComputeHash(value);
        }
    }
}
