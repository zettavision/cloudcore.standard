﻿namespace ZettaVision.CloudCore.Domain.User.Commands
{
    public class UserIdentifier : AggregateIdentifier
    {
        public UserIdentifier()
        {
        }

        public UserIdentifier(string id)
        {
            Id = id;
        }

        [Identifier("Users")]
        public sealed override string Id { get; set; }
    }
}