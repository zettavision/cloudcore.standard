﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public interface IUserRequest : IAggregateRequest
    {
    }
}