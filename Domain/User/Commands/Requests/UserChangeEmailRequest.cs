namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangeEmailRequest : IUserRequest
    {
        public string Email { get; set; }
    }
}