namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangePasswordSetupRequiredRequest : IUserRequest
    {
        public bool PasswordSetupRequired { get; set; }
    }
}