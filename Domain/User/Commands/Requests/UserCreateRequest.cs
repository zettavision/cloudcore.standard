using System.ComponentModel.DataAnnotations;

namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserCreateRequest : IUserRequest
    {
        [Required]
        public string Email { get; set; }
        public string Password { get; set; }

        public bool? PasswordSetupRequired { get; set; }
    }
}