﻿namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangePinRequest : IUserRequest
    {
        public string Pin { get; set; }
    }
}