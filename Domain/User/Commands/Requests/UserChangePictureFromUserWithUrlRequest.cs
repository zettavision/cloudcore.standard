namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangePictureFromUserWithUrlRequest : IUserRequest
    {
        public string PictureFromUser { get; set; }
    }
}