﻿using System;
using System.Collections.Generic;

namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserCreateTokenRequest : IUserRequest
    {
        public UserCreateTokenRequest()
        {
            InvertRoles = false;
            UseNullForUserId = false;
        }

        public IEnumerable<string> Roles { get; set; }

        public bool InvertRoles { get; set; }
        public bool UseNullForUserId { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}