﻿using System.ComponentModel.DataAnnotations;

namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangePinWithSaltRequest : IUserRequest
    {
        [Required]
        public string Salt { get; set; }
        public string Pin { get; set; }
    }
}