﻿namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangePhoneRequest : IUserRequest
    {
        public string Phone { get; set; }
    }
}