using System.Collections.Generic;

namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserReplaceRoleSubsetRequest : IUserRequest
    {
        public IEnumerable<string> Roles { get; set; }

        /// <summary>
        ///     these roles will be removed and replace with the roles
        /// </summary>
        public IEnumerable<string> RoleSubset { get; set; }
    }
}