namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangePasswordRequest : IUserRequest
    {
        public string Password { get; set; }
    }
}