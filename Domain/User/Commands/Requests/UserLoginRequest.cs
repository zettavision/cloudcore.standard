﻿namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserLoginRequest : IUserRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}