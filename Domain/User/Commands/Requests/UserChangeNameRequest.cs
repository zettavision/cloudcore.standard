﻿namespace ZettaVision.CloudCore.Domain.User.Commands.Requests
{
    public class UserChangeNameRequest : IUserRequest
    {
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
    }
}