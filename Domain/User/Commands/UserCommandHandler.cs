﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Domain.User.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Commands
{
    public class UserCommandHandler : IUserCommandHandler
    {
        private readonly IEnumerable<string> applicationRoles;
        private readonly IAuth0TokenManager auth0TokenManager;
        private readonly IUserRepository userRepository;

        public UserCommandHandler(IAuth0TokenManager auth0TokenManager,
            IEnumerable<string> applicationRoles, IUserRepository userRepository)
        {
            this.auth0TokenManager = auth0TokenManager;
            this.applicationRoles = applicationRoles;
            this.userRepository = userRepository;
        }

        public async Task<UserModel> HandleAsync(UserCommand<UserCreateRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregate = new UserAggregate(command.Request);

            var model = await userRepository.InsertAsync(aggregate, command.Request.Password, cancellationToken)
                .ConfigureAwait(false);
            return model;
        }

        public Task<string> HandleAsync(UserCommand<UserCreateTokenRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var tokenRoles = command.Request.InvertRoles
                ? Inverted(command.Request.Roles).Select(e => e.ToString()).ToList()
                : command.Request.Roles?.Select(e => e.ToString()).ToList();

            tokenRoles = tokenRoles ?? new List<string>();

            var userId = command.Request.UseNullForUserId ? null : command.Identifier.Id;

            var token = auth0TokenManager.BuildToken(tokenRoles,
                userId, command.Request.ExpirationDate);

            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(token);
        }

        public async Task<UserModel> HandleAsync(UserCommand<UserReplaceRoleSubsetRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregateWithMetadata =
                await userRepository.LoadAsync(command.Identifier, cancellationToken).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(command.Request);
            var model = await userRepository.UpdateAsync(aggregateWithMetadata, cancellationToken)
                .ConfigureAwait(false);
            return model;
        }

        public async Task<UserModel> HandleAsync(UserCommand<UserChangeEmailRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregateWithMetadata =
                await userRepository.LoadAsync(command.Identifier, cancellationToken).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(command.Request);
            var model = await userRepository.UpdateAsync(aggregateWithMetadata, cancellationToken)
                .ConfigureAwait(false);
            return model;
        }

        public async Task<UserModel> HandleAsync(UserCommand<UserChangePasswordRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            RequiredStringValidator.Validate(command.Request.Password, nameof(command.Request.Password));

            var aggregateWithMetadata =
                await userRepository.LoadAsync(command.Identifier, cancellationToken).ConfigureAwait(false);
            var model = await userRepository
                .UpdateAsync(aggregateWithMetadata, command.Request.Password, cancellationToken).ConfigureAwait(false);
            return model;
        }

        public async Task<UserModel> HandleAsync(UserCommand<UserChangePasswordSetupRequiredRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregateWithMetadata =
                await userRepository.LoadAsync(command.Identifier, cancellationToken).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(command.Request);
            var model = await userRepository.UpdateAsync(aggregateWithMetadata, cancellationToken)
                .ConfigureAwait(false);
            return model;
        }

        public async Task<UserModel> HandleAsync(UserCommand<UserChangePictureFromUserWithUrlRequest> command,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregateWithMetadata =
                await userRepository.LoadAsync(command.Identifier, cancellationToken).ConfigureAwait(false);
            aggregateWithMetadata.Aggregate.Apply(command.Request);
            var model = await userRepository.UpdateAsync(aggregateWithMetadata, cancellationToken).ConfigureAwait(false);
            return model;
        }

        public async Task HandleAsync(UserCommand<UserRemoveRequest> command, CancellationToken cancellationToken = default)
        {
            await userRepository.DeleteAsync(command.Identifier, cancellationToken).ConfigureAwait(false);
        }

        private IEnumerable<string> Inverted(
            IEnumerable<string> roles)
        {
            if (applicationRoles == null || !applicationRoles.Any())
            {
                return null;
            }

            if (roles == null || !roles.Any())
            {
                return applicationRoles;
            }

            return applicationRoles.Except(roles).ToList();
        }
    }
}