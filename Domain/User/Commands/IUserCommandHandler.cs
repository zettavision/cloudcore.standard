﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Commands
{
    public interface IUserCommandHandler
    {
        Task<UserModel> HandleAsync(UserCommand<UserCreateRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<string> HandleAsync(UserCommand<UserCreateTokenRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<UserModel> HandleAsync(UserCommand<UserReplaceRoleSubsetRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<UserModel> HandleAsync(UserCommand<UserChangeEmailRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<UserModel> HandleAsync(UserCommand<UserChangePasswordSetupRequiredRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<UserModel> HandleAsync(UserCommand<UserChangePictureFromUserWithUrlRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<UserModel> HandleAsync(UserCommand<UserChangePasswordRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));

        Task HandleAsync(UserCommand<UserRemoveRequest> command,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}