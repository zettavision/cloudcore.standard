﻿using ZettaVision.CloudCore.Domain.User.Commands.Requests;

namespace ZettaVision.CloudCore.Domain.User.Commands
{
    public class UserCommand<T2> : AggregateCommand<UserIdentifier, T2>
        where T2 : IUserRequest
    {
        public UserCommand()
        {
        }

        public UserCommand(UserIdentifier identifier, T2 request, string userId) : base(identifier, request, userId)
        {
        }
    }
}