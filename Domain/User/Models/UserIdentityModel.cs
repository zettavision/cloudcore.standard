﻿using System.Collections.Generic;

namespace ZettaVision.CloudCore.Domain.User.Models
{
    public class UserIdentityModel
    {
        public string AccessToken { get; set; }

        public string AccessTokenSecret { get; set; }

        public string Connection { get; set; }

        public int ExpiresIn { get; set; }

        public bool? IsSocial { get; set; }

        public IDictionary<string, object> ProfileData { get; set; }

        public string Provider { get; set; }

        public string RefreshToken { get; set; }

        public string UserId { get; set; }
    }
}