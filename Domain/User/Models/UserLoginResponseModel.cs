﻿using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.User.Models
{
    public class UserLoginResponseModel
    {
        public AuthenticationResponseModel AuthenticationResponse { get; set; }
        public Auth0TokenPayloadModel IdTokenPayload { get; set; }
    }
}