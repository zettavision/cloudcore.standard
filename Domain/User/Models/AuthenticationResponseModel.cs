﻿namespace ZettaVision.CloudCore.Domain.User.Models
{
    public class AuthenticationResponseModel
    {
        public string IdToken { get; set; }

        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public string RefreshToken { get; set; }
    }
}