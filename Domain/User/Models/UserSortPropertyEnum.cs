﻿namespace ZettaVision.CloudCore.Domain.User.Models
{
    public enum UserSortPropertyEnum
    {
        CreatedDate, LastModifiedDate
    }
}