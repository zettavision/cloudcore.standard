﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Commands;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Infrastructure
{
    public interface IUserRepository
    {
        Task<UserModel> InsertAsync(UserAggregate aggregate, CancellationToken cancellationToken = default(CancellationToken));
        Task<UserModel> InsertAsync(UserAggregate aggregate, string password, CancellationToken cancellationToken = default(CancellationToken));

        Task<AggregateWithMetadata<UserAggregate>> LoadAsync(UserIdentifier aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken));

        Task<UserModel> UpdateAsync(AggregateWithMetadata<UserAggregate> aggregateWithMetadata, CancellationToken cancellationToken = default(CancellationToken));
        Task<UserModel> UpdateAsync(AggregateWithMetadata<UserAggregate> aggregateWithMetadata, string password, CancellationToken cancellationToken = default(CancellationToken));

        Task DeleteAsync(UserIdentifier aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken));

    }
}