﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Models;
using ZettaVision.CloudCore.Domain.User.Queries;

namespace ZettaVision.CloudCore.Domain.User.Infrastructure
{
    public interface IUserReader
    {
        Task<UserModel> HandleAsync(UserGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken));

        Task<PagedResult<UserModel>> HandleAsync(UserGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken));
    }
}