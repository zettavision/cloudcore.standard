﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Infrastructure
{
    public interface IUserLoginHandler
    {
        Task<UserLoginResponseModel> HandleAsync(UserLoginRequest request,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}
