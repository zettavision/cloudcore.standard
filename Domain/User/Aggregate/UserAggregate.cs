﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Domain.User.Aggregate
{
    public class UserAggregate : IAggregate
    {
        private string email;
        private string id;

        public UserAggregate(UserCreateRequest request)
        {
            Email = request.Email;
            PasswordSetupRequired = request.PasswordSetupRequired ?? true;
        }

        private UserAggregate()
        {
        }
        public bool Blocked { get; private set; }
        public string Email
        {
            get { return email; }
            set
            {
                RequiredStringValidator.Validate(value, "Email");
                email = value;
            }
        }

        public bool? EmailVerified { get; private set; }

        public string FamilyName { get; private set; }
        public string GivenName { get; private set; }

        [Identifier("Users")]
        public string Id
        {
            get { return id; }
            private set
            {
                RequiredStringValidator.Validate(value, nameof(Id));
                id = value;
            }
        }
        public string LastIpAddress { get; private set; }
        public DateTime LastLogin { get; private set; }
        public string Locale { get; private set; }
        public int LoginsCount { get; private set; }
        public string Name { get; set; }
        public bool PasswordSetupRequired { get; private set; }
        public string Phone { get; set; }
        public string PictureFromConnection { get; private set; }
        public string PictureFromUser { get; private set; }
        public string PinHash { get; private set; }
        public IEnumerable<string> Roles { get; private set; }
        public IEnumerable<UserIdentityModel> Identities { get; private set; }

        public void Apply(UserChangeEmailRequest request)
        {
            Email = request.Email;
        }
        public void Apply(UserChangePhoneRequest request)
        {
            Phone = request.Phone;
        }
        public void Apply(UserChangeNameRequest request)
        {
            if (string.IsNullOrEmpty(request.GivenName))
                request.GivenName = null;
            else
            {
                request.GivenName = request.GivenName.Trim();

                if (string.IsNullOrEmpty(request.GivenName))
                    request.GivenName = null;
            }

            if (string.IsNullOrEmpty(request.FamilyName))
                request.FamilyName = null;
            else
            {
                request.FamilyName = request.FamilyName.Trim();

                if (string.IsNullOrEmpty(request.FamilyName))
                    request.FamilyName = null;
            }

            GivenName = request.GivenName;
            FamilyName = request.FamilyName;

            string name;

            if (GivenName == null && FamilyName == null)
                name = null;
            else if (GivenName != null && FamilyName != null)
                name = $"{GivenName} {FamilyName}";
            else if (GivenName != null)
                name = GivenName;
            else
                name = FamilyName;

            Name = name;
        }

        public void Apply(UserChangePasswordSetupRequiredRequest request)
        {
            PasswordSetupRequired = request.PasswordSetupRequired;
        }

        public void Apply(UserReplaceRoleSubsetRequest request)
        {
            if (request.RoleSubset == null || !request.RoleSubset.Any())
                return;

            var hasNewRoles = request.Roles != null && request.Roles.Any();

            var roleSubsetHashSet = new HashSet<string>(request.RoleSubset);

            var rolesHashSet = new HashSet<string>();
            if (Roles != null && Roles.Any())
                rolesHashSet.UnionWith(Roles);

            rolesHashSet.ExceptWith(roleSubsetHashSet);

            if (hasNewRoles)
            {
                var newRolesHashSet = new HashSet<string>(request.Roles);

                if (newRolesHashSet.IsSubsetOf(roleSubsetHashSet) == false)
                    throw new InvalidOperationException("request.Roles must be a subset of request.RoleSubset");

                rolesHashSet.UnionWith(newRolesHashSet);
            }

            Roles = rolesHashSet.OrderBy(r => r).ToList();
        }

        public void Apply(UserChangePictureFromUserWithUrlRequest request)
        {
            if (string.IsNullOrEmpty(request.PictureFromUser))
                request.PictureFromUser = null;

            PictureFromUser = request.PictureFromUser;
        }

        public void Apply(UserChangePinWithSaltRequest request)
        {
            if (string.IsNullOrEmpty(request.Pin))
                request.Pin = null;

            if (request.Pin != null)
            {
                if (request.Pin.Length != 4)
                    throw new ArgumentException("Pin must be a 4 digit number");

                int n;
                bool isNumeric = int.TryParse(request.Pin, out n);

                if (isNumeric == false)
                    throw new ArgumentException("Pin must be a 4 digit number");
            }

            PinHash = request.Pin == null ? null : PinHasher.Hash(request.Pin, request.Salt);
        }
    }
}