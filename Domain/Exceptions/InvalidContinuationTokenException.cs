﻿using System;

namespace ZettaVision.CloudCore.Domain.Exceptions
{
    public class InvalidContinuationTokenException : ArgumentException
    {
        public InvalidContinuationTokenException() : this("Invalid Continuation Token")
        {
        }

        public InvalidContinuationTokenException(string message)
            : base(message)
        {
        }

        public InvalidContinuationTokenException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}