﻿using System;

namespace ZettaVision.CloudCore.Domain.Exceptions
{
    public class OptimisticConcurrencyException : Exception
    {
        public OptimisticConcurrencyException() : this("Optimistic Concurrency Error")
        {
        }

        public OptimisticConcurrencyException(string message)
            : base(message)
        {
        }

        public OptimisticConcurrencyException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}