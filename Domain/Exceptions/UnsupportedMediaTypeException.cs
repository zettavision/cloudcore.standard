﻿using System;

namespace ZettaVision.CloudCore.Domain.Exceptions
{
    public class UnsupportedMediaTypeException : Exception
    {
        public UnsupportedMediaTypeException() : this("Object Not Found")
        {
        }

        public UnsupportedMediaTypeException(string message)
            : base(message)
        {
        }

        public UnsupportedMediaTypeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}