﻿using System;

namespace ZettaVision.CloudCore.Domain.Exceptions
{
    public class ObjectAlreadyExistsException : Exception
    {
        public ObjectAlreadyExistsException() : this("Object Already Exists")
        {
        }

        public ObjectAlreadyExistsException(string message)
            : base(message)
        {
        }

        public ObjectAlreadyExistsException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}