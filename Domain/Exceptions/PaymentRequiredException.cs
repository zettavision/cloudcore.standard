using System;

namespace ZettaVision.CloudCore.Domain.Exceptions
{
    public class PaymentRequiredException : Exception
    {
        public PaymentRequiredException() : this("Payment Required")
        {
        }

        public PaymentRequiredException(string message)
            : base(message)
        {
        }

        public PaymentRequiredException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}