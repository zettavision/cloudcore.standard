﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZettaVision.CloudCore.Domain.Exceptions
{
    public static class ExceptionDataHelper
    {
        public static Exception AddToData(this Exception exception, string key, object value)
        {
            exception.Data.Add(key, value);
            return exception;
        }
    }
}
