﻿namespace ZettaVision.CloudCore.Domain.AggreateEvent
{
    public class AggregateEventIdentifier : AggregateIdentifier
    {
        public AggregateEventIdentifier()
        {
        }

        public AggregateEventIdentifier(string aggregateId, string id)
        {
            AggregateId = aggregateId;
            Id = id;
        }

        public AggregateEventIdentifier(string aggregateId, string id, string eTag)
        {
            AggregateId = aggregateId;
            Id = id;
            ETag = eTag;
        }

        [Identifier("Aggregates")]
        public string AggregateId { get; set; }

        [Identifier("AggregateEvents")]
        public sealed override string Id { get; set; }
    }
}