﻿using System;
using System.Linq;
using ZettaVision.CloudCore.Domain.AggreateEvent.Request;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.AggreateEvent
{
    public class AggregateEventAggregate : IAggregate
    {
        private AggregateEventAggregate()
        {

        }

        public AggregateEventAggregate(AggregateEventAddRequest request)
        {
            int idAsInt;
            if (int.TryParse(request.Id, out idAsInt) == false)
            {
                throw new Exception("Id must be a 32 bit integer");
            }

            if (idAsInt < 1)
                throw new Exception("Id must be 1 or greater");

            AggregateId = request.AggregateId;
            Id = request.Id;
            Request = request.Request;
            RequestType = GetRequestType(request.Request);
        }

        [Identifier("Aggregates")]
        public string AggregateId { get; private set; }

        public string RequestType { get; private set; }
        public IAggregateRequest Request { get; private set; }

        [Identifier("AggregateEvents")]
        public string Id { get; private set; }

        private static string GetRequestType(object request)
        {
            return string.Join(",", request.GetType().AssemblyQualifiedName.Split(',').Take(2));
        }

        public void Apply(AggregateEventChangeRequestRequest request)
        {
            Request = request.Request;
            RequestType = GetRequestType(request.Request);
        }
    }
}