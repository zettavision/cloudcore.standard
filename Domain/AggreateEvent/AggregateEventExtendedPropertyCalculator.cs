﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;

namespace ZettaVision.CloudCore.Domain.AggreateEvent
{
    public static class AggregateEventExtendedPropertyCalculator
    {
        public static void Calculate(AggregateEventModel model)
        {
            var requestTypeQualifiedName = model.RequestType.Split(',').FirstOrDefault();

            var aggregateRequestName = requestTypeQualifiedName?.Split('.').LastOrDefault();

            if (aggregateRequestName != null)
            {
                var aggregateRequestDisplayName = Regex.Replace(aggregateRequestName, "(\\B[A-Z])", " $1");
                model.RequestTypeDisplayName = aggregateRequestDisplayName;
            }
            else
            {
                model.RequestTypeDisplayName = requestTypeQualifiedName ?? model.RequestType;
            }
        }
    }
}
