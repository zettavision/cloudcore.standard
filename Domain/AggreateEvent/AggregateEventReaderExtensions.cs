﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.AggreateEvent
{
    public static class AggregateEventReaderExtensions
    {
        public static async Task<PagedResult<AggregateEventModel>> GetAllAsync<T>(
            this IAggregateEventReader aggregateEventReader,
            Func<Task<T>> getAggregateModelAsync,
            object query,
            bool orderReversed,
            PageParametersModel pageParameters,
            CancellationToken cancellationToken = default(CancellationToken)) where T : IAggregateModel
        {
            if (orderReversed)
            {
                return await GetAllOrderReveredAsync(aggregateEventReader, getAggregateModelAsync, query, pageParameters, cancellationToken).ConfigureAwait(false);
            }
            else
            {
                return await GetAllOrderNormallyAsync(aggregateEventReader, query, pageParameters, cancellationToken)
                    .ConfigureAwait(false);
            }
        }

        private static async Task<PagedResult<AggregateEventModel>> GetAllOrderNormallyAsync(
            IAggregateEventReader aggregateEventReader,
            object query,
            PageParametersModel pageParameters,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateEventReader.HandleAsync(new AggregateEventGetAllQuery
            {
                Identifier = FullyQualifiedIdentifier.GetIdentifier(query),
                PageParameters = pageParameters,
                OrderReversed = false
            },
                cancellationToken
            ).ConfigureAwait(false);
        }

        private static async Task<PagedResult<AggregateEventModel>> GetAllOrderReveredAsync<T>(
            IAggregateEventReader aggregateEventReader,
            Func<Task<T>> getAggregateModelAsync,
            object query,
            PageParametersModel pageParameters,
            CancellationToken cancellationToken = default(CancellationToken)) where T : IAggregateModel
        {
            int lastEventIdAsInt;
            int? pageAsInt = null;

            if (!string.IsNullOrEmpty(pageParameters?.ContinuationToken))
            {
                var parts = pageParameters.ContinuationToken.Split(new[] { ':' },
                    StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length != 2)
                    throw new ArgumentException("ContinuationToken must have two parts seperated by a :");

                pageAsInt = int.Parse(parts[0]);
                lastEventIdAsInt = int.Parse(parts[1]);
            }
            else
            {
                var aggregateModel = await getAggregateModelAsync().ConfigureAwait(false);
                lastEventIdAsInt = aggregateModel.AggregateInfo.Version;
            }

            var result = await aggregateEventReader.HandleAsync(new AggregateEventGetAllQuery
            {
                Identifier = FullyQualifiedIdentifier.GetIdentifier(query),
                PageParameters = pageParameters == null
                        ? null
                        : new PageParametersModel()
                        {
                            ContinuationToken = pageAsInt?.ToString(),
                            MaxItemCount = pageParameters.MaxItemCount
                        },
                OrderReversed = true,
                IdLessThanOrEqual = lastEventIdAsInt.ToString(),
            },
                cancellationToken
            ).ConfigureAwait(false);

            if (string.IsNullOrEmpty(result.ContinuationToken))
                result.ContinuationToken = null;

            if (result.ContinuationToken != null)
            {
                result.ContinuationToken = $"{result.ContinuationToken}:{lastEventIdAsInt}";
            }

            return result;
        }
    }
}
