﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure
{
    public interface IAggregateEventReader
    {
        Task<PagedResult<AggregateEventModel>> HandleAsync(AggregateEventGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken));
        Task<AggregateEventModel> HandleAsync(AggregateEventGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken));
    }
}