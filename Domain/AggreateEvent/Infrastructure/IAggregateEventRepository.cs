﻿using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.Infrastructure;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure
{
    public interface IAggregateEventRepository :
        IAggregateRepository<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>
    {
    }
}