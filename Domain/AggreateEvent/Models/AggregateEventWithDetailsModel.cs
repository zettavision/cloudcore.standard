﻿using System.ComponentModel.DataAnnotations;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Models
{
    public class AggregateEventWithDetailsModel
    {
        [Required]
        public AggregateEventModel AggregateEvent { get; set; }
        public AggregateEventCreatedByDetailsModel CreatedByDetails { get; set; }
    }
}
