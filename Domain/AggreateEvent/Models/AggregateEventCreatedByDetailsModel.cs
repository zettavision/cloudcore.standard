﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Models
{
    public class AggregateEventCreatedByDetailsModel
    {
        public string UserId { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
