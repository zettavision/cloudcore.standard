﻿using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Models
{
    public class AggregateEventModel : IAggregateModel
    {
        public string AggregateId { get; set; }
        public string Id { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeDisplayName { get; set; }
        public IAggregateRequest Request { get; set; }
        public string ETag { get; set; }
        public AggregateInfoModel AggregateInfo { get; set; }
    }
}