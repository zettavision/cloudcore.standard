namespace ZettaVision.CloudCore.Domain.AggreateEvent.Queries
{
    public class AggregateEventGetByIdQuery
    {
        public AggregateEventGetByIdQuery()
        {
        }

        public AggregateEventGetByIdQuery(string aggregateId, string id)
        {
            AggregateId = aggregateId;
            Id = id;
        }

        public string AggregateId { get; set; }

        public string Id { get; set; }
    }
}