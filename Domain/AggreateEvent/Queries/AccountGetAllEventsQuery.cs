using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Queries
{
    public class AggregateEventGetAllQuery
    {
        public AggregateEventGetAllQuery()
        {
            OrderReversed = false;
        }

        public AggregateEventGetAllQuery(string identifier, PageParametersModel pageParameters)
        {
            Identifier = identifier;
            PageParameters = pageParameters;
        }

        public string Identifier { get; set; }

        public PageParametersModel PageParameters { get; set; }

        public string IdLessThanOrEqual { get; set; }
        public string IdGreaterThan { get; set; }

        public bool OrderReversed { get; set; }

    }
}