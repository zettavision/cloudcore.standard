﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Request
{
    public class AggregateEventChangeRequestRequest : IAggregateEventRequest
    {
        public IAggregateRequest Request { get; set; }
    }
}