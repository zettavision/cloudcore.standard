﻿using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Request
{
    public interface IAggregateEventRequest : IAggregateRequest
    {
    }
}