﻿using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.AggreateEvent.Request
{
    public class AggregateEventAddRequest : IAggregateEventRequest
    {
        [Required]
        public string AggregateId { get; set; }

        [Required]
        public string Id { get; set; }

        public IAggregateRequest Request { get; set; }
    }
}