﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZettaVision.CloudCore.Domain.LastOrchestration.Models
{
    public class LastOrchestrationSettingModel 
    {
        [Required]
        public string Type { get; set; }
        public TimeSpan? WarningThreshold { get; set; }
        public TimeSpan? AutoRestartThreshold { get; set; }
    }
}
