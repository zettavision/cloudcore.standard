﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ZettaVision.CloudCore.Domain.Orchestration.Models;
using ZettaVision.CloudCore.Domain.Run.Models;

namespace ZettaVision.CloudCore.Domain.LastOrchestration.Models
{
    public class LastOrchestrationModel : OrchestrationModel
    {
        public string Type { get; set; }
        public TimeSpan? WarningThreshold { get; set; }
        public TimeSpan? AutoRestartThreshold { get; set; }
        public TimeSpan? RuntimeSinceLastModified { get; set; }
    }
}
