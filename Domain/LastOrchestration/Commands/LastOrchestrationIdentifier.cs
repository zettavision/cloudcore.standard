﻿using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Domain.LastOrchestration.Commands
{
    public class LastOrchestrationIdentifier : AggregateIdentifier
    {
        public LastOrchestrationIdentifier()
        {
        }

        public LastOrchestrationIdentifier(string id)
        {
            Id = id;
        }

        public LastOrchestrationIdentifier(string id, string eTag)
        {
            Id = id;
            ETag = eTag;
        }

        [Identifier("LastOrchestrations")]
        public sealed override string Id { get; set; }
    }
}
