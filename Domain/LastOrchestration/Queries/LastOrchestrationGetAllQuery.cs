﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZettaVision.CloudCore.Domain.LastOrchestration.Queries
{
    public class LastOrchestrationGetAllQuery
    {
        public bool? PastWarningThreshold { get; set; }
    }
}
