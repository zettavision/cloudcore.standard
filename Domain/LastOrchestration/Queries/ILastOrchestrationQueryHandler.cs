﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.LastOrchestration.Models;

namespace ZettaVision.CloudCore.Domain.LastOrchestration.Queries
{
    public interface ILastOrchestrationQueryHandler
    {
        Task<IEnumerable<LastOrchestrationModel>> HandleAsync(LastOrchestrationGetAllQuery query,
            CancellationToken cancellationToken = default);
    }
}
