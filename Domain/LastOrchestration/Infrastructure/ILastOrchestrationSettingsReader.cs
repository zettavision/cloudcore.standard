﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.LastOrchestration.Models;

namespace ZettaVision.CloudCore.Domain.LastOrchestration.Infrastructure
{
    public interface ILastOrchestrationSettingsReader
    {
        Task<IEnumerable<LastOrchestrationSettingModel>> ReadFromCacheAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<LastOrchestrationSettingModel> GetByTypeFromCacheAsync(string type, CancellationToken cancellationToken = default(CancellationToken));
    }
}
