﻿using System;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain
{
    public abstract class AggregateCommand<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateRequest
    {
        protected AggregateCommand(T identifier, T2 request, string createdByUserId)
        {
            Identifier = identifier;
            Request = request;
            CreatedByUserId = createdByUserId;
            CreateDate = DateTime.UtcNow;
        }

        protected AggregateCommand()
        {
        }

        public DateTime CreateDate { get; }
        public string CreatedByUserId { get; set; }
        public T Identifier { get; set; }
        public T2 Request { get; set; }
    }
}