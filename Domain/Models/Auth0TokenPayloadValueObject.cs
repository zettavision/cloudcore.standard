﻿using System.Collections.Generic;

namespace ZettaVision.CloudCore.Domain.Models
{
    public class Auth0TokenPayloadModel
    {
        public string UserId { get;  set; }
        public IEnumerable<string> Roles { get; set; }
    }
}