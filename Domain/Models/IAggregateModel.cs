﻿namespace ZettaVision.CloudCore.Domain.Models
{
    public interface IAggregateModel
    {
        string ETag { get; set; }
        AggregateInfoModel AggregateInfo { get; set; }
    }
}