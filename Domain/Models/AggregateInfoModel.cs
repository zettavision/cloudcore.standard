﻿using System;

namespace ZettaVision.CloudCore.Domain.Models
{
    public class AggregateInfoModel
    {
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedByUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedByUserId { get; set; }
        public int Version { get; set; }
    }
}