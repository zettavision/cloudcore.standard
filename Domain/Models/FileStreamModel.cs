﻿using System.IO;

namespace ZettaVision.CloudCore.Domain.Models
{
    public class FileStreamModel
    {
        public Stream Stream { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public long Length { get; set; }
    }
}