﻿namespace ZettaVision.CloudCore.Domain.Models
{
    public class BlobReferenceModel
    {
        public string Url { get; set; }
        public long Length { get; set; }

        public string Md5Hash { get; set; }
        public string ContentType { get; set; }
    }
}