﻿namespace ZettaVision.CloudCore.Domain.Models
{
    public class PageParametersModel
    {
        public string ContinuationToken { get; set; }
        public int? MaxItemCount { get; set; }
    }
}