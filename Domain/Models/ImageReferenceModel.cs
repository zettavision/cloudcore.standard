﻿namespace ZettaVision.CloudCore.Domain.Models
{
    public class ImageReferenceModel
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Url { get; set; }
        public long Length { get; set; }
        public string Md5Hash { get; set; }
        public string ContentType { get; set; }
    }
}