﻿using System;

namespace ZettaVision.CloudCore.Domain
{
    public static class RequiredStringValidator
    {
        public static void Validate(string value, string paramName)
        {
            if (value == null)
                throw new ArgumentNullException(paramName);

            if (value == string.Empty)
                throw new ArgumentException($"{paramName} cannot be empty");
        }
    }
}