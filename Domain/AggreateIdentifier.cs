﻿namespace ZettaVision.CloudCore.Domain
{
    public abstract class AggregateIdentifier
    {
        public string ETag { get; set; }
        public abstract string Id { get; set; }
    }
}