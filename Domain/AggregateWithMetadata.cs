using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Domain
{
    public class AggregateWithMetadata<T>
    {
        public string ETag { get; set; }
        public AggregateInfoValueObject AggregateInfo { get; set; }
        public T Aggregate { get; set; }
    }
}