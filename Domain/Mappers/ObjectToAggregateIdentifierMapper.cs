using System;
using System.Linq;

namespace ZettaVision.CloudCore.Domain.Mappers
{
    public class ObjectToAggregateIdentifierMapper<T>
        where T : AggregateIdentifier
    {
        public T Map(object obj)
        {
            var aggregateIdentifier = (T) Activator.CreateInstance(typeof(T));

  
            var aggregateIdentifierProperties = aggregateIdentifier.GetType().GetProperties();
            var aggregateProperties = obj.GetType().GetProperties();

            var commonProperties =
                aggregateIdentifierProperties.Where(
                    p => aggregateProperties.Any(ap => ap.Name == p.Name && ap.PropertyType.Name == p.PropertyType.Name));

            foreach (var commandProperty in commonProperties)
            {
                var sourceProperty = obj.GetType().GetProperty(commandProperty.Name);
                commandProperty.SetValue(aggregateIdentifier, sourceProperty.GetValue(obj));
            }

            return aggregateIdentifier;
        }
 
    }
}