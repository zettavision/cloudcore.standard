using System;
using System.Linq;

namespace ZettaVision.CloudCore.Domain.Mappers
{
    public class AggreateToAggregateIdentifierMapper<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregate
    {
        public T Map(T2 aggregate, string eTag = null)
        {
            var aggregateIdentifier = (T) Activator.CreateInstance(typeof(T));

            aggregateIdentifier.ETag = eTag;
            aggregateIdentifier.Id = aggregate.Id;

            var aggregateIdentifierProperties = aggregateIdentifier.GetType().GetProperties();
            var aggregateProperties = aggregate.GetType().GetProperties();

            var commonProperties =
                aggregateIdentifierProperties.Where(
                    p => aggregateProperties.Any(ap => ap.Name == p.Name && ap.PropertyType.Name == p.PropertyType.Name));

            foreach (var commandProperty in commonProperties)
            {
                var sourceProperty = aggregate.GetType().GetProperty(commandProperty.Name);
                commandProperty.SetValue(aggregateIdentifier, sourceProperty.GetValue(aggregate));
            }

            return aggregateIdentifier;
        }
    }
}