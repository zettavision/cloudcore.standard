using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Domain.Mappers
{
    public class AggregateToAggregateModelMapper<T, T2>
        where T : IAggregate
        where T2 : IAggregateModel, new()
    {
        private readonly AggregateInfoValueObjectToAggregateInfoModelMapper aggregateInfoMapper =
            new AggregateInfoValueObjectToAggregateInfoModelMapper();

        private readonly JsonSerializerSettings jsonSerializerSettings;

        public AggregateToAggregateModelMapper(JsonSerializerSettings jsonSerializerSettings = null)
        {
            this.jsonSerializerSettings = jsonSerializerSettings;
        }

        public T2 Map(T aggregate, string eTag, AggregateInfoValueObject aggregateInfo)
        {
            var json = JsonConvert.SerializeObject(aggregate);
            var model = JsonConvert.DeserializeObject<T2>(json, jsonSerializerSettings);

            model.ETag = eTag;
            model.AggregateInfo = aggregateInfoMapper.Map(aggregateInfo);

            return model;
        }
    }
}