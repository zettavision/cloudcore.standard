using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Domain.Mappers
{
    public class AggregateModelToAggregateWithMetadataMapper<T, T2>
        where T : IAggregateModel, new()
        where T2 : IAggregate
    {
        private readonly AggregateInfoValueObjectToAggregateInfoModelMapper aggregateInfoMapper =
            new AggregateInfoValueObjectToAggregateInfoModelMapper();

        private readonly JsonSerializerSettings jsonSerializerSettings;

        public AggregateModelToAggregateWithMetadataMapper(JsonSerializerSettings jsonSerializerSettings = null)
        {
            this.jsonSerializerSettings = jsonSerializerSettings;
        }

        public AggregateWithMetadata<T2> Map(T aggregateModel)
        {
            var result = new AggregateWithMetadata<T2>();

            var json = JsonConvert.SerializeObject(aggregateModel);
            result.Aggregate = JsonConvert.DeserializeObject<T2>(json, jsonSerializerSettings);

            if (aggregateModel.AggregateInfo != null)
            {
                var jsonAggregateInfo = JsonConvert.SerializeObject(aggregateModel.AggregateInfo);
                result.AggregateInfo = JsonConvert.DeserializeObject<AggregateInfoValueObject>(jsonAggregateInfo, jsonSerializerSettings);
            }

            result.ETag = aggregateModel.ETag;

            return result;
        }
    }
}