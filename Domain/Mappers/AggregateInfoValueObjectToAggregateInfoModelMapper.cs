using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Domain.Mappers
{
    public class AggregateInfoValueObjectToAggregateInfoModelMapper
    {
        public AggregateInfoModel Map(AggregateInfoValueObject aggregate)
        {
            return new AggregateInfoModel
            {
                CreatedByUserId = aggregate.CreatedByUserId,
                CreatedDate = aggregate.CreatedDate,
                LastModifiedDate = aggregate.LastModifiedDate,
                LastModifiedByUserId = aggregate.LastModifiedByUserId,
                Version = aggregate.Version
            };
        }
    }
}