﻿using System;

namespace ZettaVision.CloudCore.Domain
{
    public static class GuidId
    {
        public static string GetNewId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}