﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Queries;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Infrastructure
{
    public interface IAggregatePointInTimeSnapshotReader<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        Task<PagedResult<T2>> HandleAsync(AggregatePointInTimeSnapshotGetAllQuery<T> query, CancellationToken cancellationToken = default(CancellationToken));
        Task<T2> HandleAsync(AggregatePointInTimeSnapshotGetByPointInTimeQuery<T> query, CancellationToken cancellationToken = default(CancellationToken));
    }


}