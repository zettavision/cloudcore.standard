﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Infrastructure
{
    public interface IAggregatePointInTimeSnapshotRepository<in T, in T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        Task InsertAsync(T identifier, T2 model, CancellationToken cancellationToken = default(CancellationToken));
    }
}