using System;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Queries
{
    public class AggregatePointInTimeSnapshotGetAllQuery<T>
        where T : AggregateIdentifier
    {
        public T Identifier { get; set; }

        public PageParametersModel PageParameters { get; set; }
    }


  
}