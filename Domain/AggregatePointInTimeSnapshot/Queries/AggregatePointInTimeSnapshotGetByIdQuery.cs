using System;

namespace ZettaVision.CloudCore.Domain.AggregatePointInTimeSnapshot.Queries
{
    public class AggregatePointInTimeSnapshotGetByPointInTimeQuery<T> 
        where T : AggregateIdentifier
    {
        public AggregatePointInTimeSnapshotGetByPointInTimeQuery()
        {
        }

        public AggregatePointInTimeSnapshotGetByPointInTimeQuery(T identifier, DateTime pointInTime)
        {
            Identifier = identifier;
            PointInTime = pointInTime;
        }

        public T Identifier { get; set; }

        public DateTime PointInTime { get; set; }
    }
}