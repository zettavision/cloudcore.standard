﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ZettaVision.CloudCore.Domain
{
    public static class FullyQualifiedIdentifier
    {
        public static string GetIdentifier(object entity, string idPrefix = null)
        {
            var stringBuilder = new StringBuilder();

            if(!string.IsNullOrEmpty(idPrefix))
                stringBuilder.Append($"{idPrefix}.");


            var properties = entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in properties)
            {
                var attributes = Attribute.GetCustomAttributes(property);

                var identifierAttribute = attributes.FirstOrDefault(a => a is IdentifierAttribute);

                if (identifierAttribute != null)
                {
                    var value = property.GetValue(entity, null).ToString();
                    var pluralName = ((IdentifierAttribute) identifierAttribute).PluralName;

                    stringBuilder.Append($"{pluralName.ToLowerInvariant()}.{value}.");
                }
            }

            var id = stringBuilder.ToString();

            if (id.Length > 0)
                id = id.Substring(0, id.Length - 1);

            return id;
        }
    }
}