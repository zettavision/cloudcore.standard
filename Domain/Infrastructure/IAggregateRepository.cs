﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface IAggregateRepository<T, T2, T3>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel
    {
        Task<T3> InsertAsync(T2 aggregate, string userId = null, DateTime? createDate = null, string lastModifiedByUserId = null, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken));
        Task<T3> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId = null, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken));
        Task DeleteAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken));


    }
}