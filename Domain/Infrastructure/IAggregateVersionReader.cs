﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface IAggregateVersionReader<in T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        Task<T2> GetByIdAndVersionAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken));
    }
}