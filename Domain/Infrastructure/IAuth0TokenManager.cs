﻿using System;
using System.Collections.Generic;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface IAuth0TokenManager
    {
        string BuildToken(IEnumerable<string> roles, string userId = null, DateTime? expirationDate = default(DateTime?));
        Auth0TokenPayloadValueObject GetPayload(string token);
        void ValidateTokenIssuerSigningKey(string token);
    }
}