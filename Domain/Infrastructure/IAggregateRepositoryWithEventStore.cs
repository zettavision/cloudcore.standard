﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface IAggregateRepositoryWithEventStore<T, T2, T3, T4>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateRequest
        where T4 : IAggregateModel
    {
        Task<T4> InsertAsync(T2 aggregate, T3 aggregateRequest, string userId = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken));
        Task<T4> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, T3 aggregateRequest, string userId = null, CancellationToken cancellationToken = default(CancellationToken));
    }
}