﻿using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface IAuth0TokenDecoder
    {
        string Decode(string token);
        Auth0TokenPayloadValueObject GetPayload(string token);
    }
}