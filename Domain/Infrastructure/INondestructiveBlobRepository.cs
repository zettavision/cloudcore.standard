﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface INondestructiveBlobRepository
    {
        Task<string> InsertAsync(string blobName, Stream stream, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<string> InsertAsync(string blobName, byte[] bytes, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}