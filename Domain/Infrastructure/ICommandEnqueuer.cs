﻿using System.Threading;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Domain.Infrastructure
{
    public interface ICommandEnqueuer
    {
        Task EnqueueAsync(string queueName, object command, CancellationToken cancellationToken = default(CancellationToken));
        Task<int> GetApproximateMessageCountAsync(string queueName, CancellationToken cancellationToken = default(CancellationToken));
    }
}