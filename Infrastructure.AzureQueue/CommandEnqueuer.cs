﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Queue;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Infrastructure;

namespace ZettaVision.CloudCore.Infrastructure.AzureQueue
{
    public class CommandEnqueuer : ICommandEnqueuer
    {
        private readonly HashSet<string> existingQueues;
        private readonly CloudQueueClient queueClient;

        public CommandEnqueuer(string storageConnectionString)
        {
            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            queueClient = storageAccount.CreateCloudQueueClient();

            existingQueues = new HashSet<string>();
        }

        public async Task EnqueueAsync(string queueName, object command, CancellationToken cancellationToken = default(CancellationToken))
        {
            RequiredStringValidator.Validate(queueName, nameof(queueName));
            var queue = await SetupQueue(queueName, cancellationToken).ConfigureAwait(false);

            var json = JsonConvert.SerializeObject(command);

            var message = new CloudQueueMessage(json);
            await queue.AddMessageAsync(message, cancellationToken).ConfigureAwait(false);
        }

        private async Task<CloudQueue> SetupQueue(string queueName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var queue = queueClient.GetQueueReference(queueName);

            if (existingQueues.Contains(queueName))
                return queue;

            await queue.CreateIfNotExistsAsync(cancellationToken).ConfigureAwait(false);
            existingQueues.Add(queueName);

            return queue;
        }

        public async Task<int> GetApproximateMessageCountAsync(string queueName, CancellationToken cancellationToken = default(CancellationToken))
        {
            RequiredStringValidator.Validate(queueName, nameof(queueName));
            var queue = await SetupQueue(queueName, cancellationToken).ConfigureAwait(false);

            await queue.FetchAttributesAsync(cancellationToken).ConfigureAwait(false);

            return queue.ApproximateMessageCount ?? 0;
        }
    }
}
