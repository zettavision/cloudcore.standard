﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;

namespace ZettaVision.CloudCore.Infrastructure.InMemory
{
    public class NondestructiveBlobRepositoryInMemory : INondestructiveBlobRepository
    {
        private readonly string cdnPrefix;
        private readonly Dictionary<string,byte[]> blobDictionary;

        public NondestructiveBlobRepositoryInMemory(string cdnPrefix)
        {
            if (cdnPrefix != null && cdnPrefix.EndsWith("/"))
                this.cdnPrefix = cdnPrefix.Substring(0, cdnPrefix.Length - 1);
            else
                this.cdnPrefix = cdnPrefix;

            blobDictionary = new Dictionary<string, byte[]>();
        }

        private string GetUrl(string blobName)
        {
             return $"{cdnPrefix}/{blobName}";
        }

        public async Task<string> InsertAsync(string blobName, Stream stream, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            stream.Position = 0;
            var bytes = ReadFully(stream);

            return await InsertAsync(blobName, bytes, contentType, cancellationToken).ConfigureAwait(false);

        }

        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public  Task<string> InsertAsync(string blobName, byte[] bytes, string contentType = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (blobDictionary.ContainsKey(blobName))
                throw new ObjectAlreadyExistsException();

            blobDictionary.Add(blobName, bytes);

            var url = GetUrl(blobName);

            cancellationToken.ThrowIfCancellationRequested();
            return Task.FromResult(url);

        }

        public Task<byte[]> GetBlob(string blobName, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if(!blobDictionary.ContainsKey(blobName))
                throw new ObjectNotFoundException();

            var bytes = blobDictionary[blobName];

            cancellationToken.ThrowIfCancellationRequested();
            return Task.FromResult(bytes);
        }
    }
}