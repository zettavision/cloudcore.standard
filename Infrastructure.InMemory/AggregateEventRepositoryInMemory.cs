﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Infrastructure.AggregateEvent;

namespace ZettaVision.CloudCore.Infrastructure.InMemory
{


    public class AggregateEventRepositoryInMemory : IAggregateEventReader, IAggregateEventRepository
    {
        private readonly
            AggregateRepositoryInMemory<AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>
            aggregateRepository;

        public AggregateEventRepositoryInMemory()
        {
            var jsonConverters = new List<JsonConverter>();
            jsonConverters.Add(new AggregateEventJsonCreationConverter());

            aggregateRepository = new AggregateRepositoryInMemory
                <AggregateEventIdentifier, AggregateEventAggregate, AggregateEventModel>
                (jsonConverters);
        }


        public async Task<AggregateEventModel> UpdateAsync(AggregateWithMetadata<AggregateEventAggregate> aggregateWithMetadata, 
            string userId, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.UpdateAsync(aggregateWithMetadata, userId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        public async Task DeleteAsync(AggregateEventIdentifier aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            await aggregateRepository.DeleteAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }

        public async Task<AggregateEventModel> InsertAsync(AggregateEventAggregate aggregate, 
            string userId, DateTime? createDate = null, string lastModifiedByUserId = null, DateTime? lastModifiedDate = null, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.InsertAsync(aggregate, userId, createDate, lastModifiedByUserId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        public async Task<AggregateWithMetadata<AggregateEventAggregate>> LoadAsync(AggregateEventIdentifier aggregateIdentifier, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.LoadAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }


        public async Task<PagedResult<AggregateEventModel>> HandleAsync(AggregateEventGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var allEvents = await aggregateRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);

            var filteredEvents = allEvents.Where(e => e.AggregateId == query.Identifier);
            

            var result = PagerInMemory.GetPagedResult(filteredEvents, query.PageParameters);


            if (result?.Items?.Any() == true)
            {
                foreach (var item in result.Items)
                {
                    AggregateEventExtendedPropertyCalculator.Calculate(item);
                }
            }

            return result;
        }

        public async Task<AggregateEventModel> HandleAsync(AggregateEventGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result= await
                aggregateRepository.GetByIdAsync(new AggregateEventIdentifier(query.AggregateId, query.Id), cancellationToken)
                    .ConfigureAwait(false);

            AggregateEventExtendedPropertyCalculator.Calculate(result);

            return result;
        }
    }

}
