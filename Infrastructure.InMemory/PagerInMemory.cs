﻿using System.Collections.Generic;
using System.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory
{
    public static class PagerInMemory
    {


        public static PagedResult<T> GetPagedResult<T>(IEnumerable<T> items, PageParametersModel pageParameters)
        {
            int skip = 0;
            if (pageParameters?.ContinuationToken != null)
            {
                skip = int.Parse(pageParameters.ContinuationToken);
                items = items.Skip(skip);
            }

            var maxItemCount = pageParameters?.MaxItemCount ?? 1000;

            if (maxItemCount > 1000)
                maxItemCount = 1000;

            var count = items.Count();
            items = items.Take(maxItemCount);

            string newContinuationToken = null;

            if (count > maxItemCount)
            {
                var newSkip = skip + maxItemCount;
                newContinuationToken = newSkip.ToString();
            }

            return new PagedResult<T>() { Items = items, ContinuationToken = newContinuationToken };
        }
    }
}
