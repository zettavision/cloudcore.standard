﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure.InMemory
{


    public class AggregateRepositoryWithEventStoreInMemory<T, T2, T3, T4> : AggregateRepositoryWithEventStore<T, T2, T3, T4>, IAggregateSimpleReader<T4, T>
        where T : AggregateIdentifier 
        where T2 : IAggregate 
        where T3 : IAggregateRequest
        where T4 : IAggregateModel, new()
    {
        private readonly AggregateRepositoryInMemory<T, T2, T4> aggregateRepositoryInMemory;

        public AggregateRepositoryWithEventStoreInMemory(AggregateEventRepositoryInMemory aggregateEventRepositoryInMemory, 
             AggregateRepositoryInMemory<T, T2, T4> aggregateRepositoryInMemory) 
            : base(aggregateEventRepositoryInMemory, aggregateRepositoryInMemory)
        {
            this.aggregateRepositoryInMemory = aggregateRepositoryInMemory;
        }




        public async Task<T4> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepositoryInMemory.GetByIdAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }

        public async Task<IEnumerable<T4>> GetAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepositoryInMemory.GetAllAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<IEnumerable<T4>> GetAllByPartitionAsync(string partitionKey, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepositoryInMemory.GetAllByPartitionAsync(partitionKey, cancellationToken).ConfigureAwait(false);
        }
    }
}
