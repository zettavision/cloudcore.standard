﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.InMemory
{
    public class AggregateRepositoryInMemory<T, T2, T3> : IAggregateSimpleReader<T3, T>, IAggregateRepository<T, T2, T3>, IAggregatePurger
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {

        private readonly bool isPartitionCollection;
        private readonly string partitionKeyPrefix;
        private bool isPartitionKeyFromId = false;

        public AggregateRepositoryInMemory(IEnumerable<JsonConverter> jsonConverters = null, bool isPartitionCollection = false, string partitionKeyPrefix = null)
        {

            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if(jsonConverters != null && jsonConverters.Any())
            {
                foreach(var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }

            this.isPartitionCollection = isPartitionCollection;
            this.partitionKeyPrefix = partitionKeyPrefix;


            aggregateMapper = new AggregateToAggregateModelMapper<T2, T3>(jsonSerializerSettings);
            aggregatePluralName = IdentifierAttributeReader.GetIdIdentifierPluralName(typeof(T));
        }

        private readonly Dictionary<string, AggregateWithMetadata<T2>> aggregateDictionary = new Dictionary<string, AggregateWithMetadata<T2>>();
        private readonly Dictionary<string, HashSet<string>> partitionKeyIdentifierMappingDictionary = new Dictionary<string, HashSet<string>>();

        private readonly AggregateToAggregateModelMapper<T2, T3>
            aggregateMapper;
        private readonly string aggregatePluralName;

        public Task<T3> InsertAsync(T2 aggregate, string userId, DateTime? createDate = null,
            string lastModifiedByUserId = null, DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if(createDate == null)
                createDate = DateTime.UtcNow;


            var fullyQualifiedIdentifier = FullyQualifiedIdentifier.GetIdentifier(aggregate);

            if(aggregateDictionary.ContainsKey(fullyQualifiedIdentifier))
            {
                throw new ObjectAlreadyExistsException($"{fullyQualifiedIdentifier} already exist")
                    .AddToData("Error", "RepositoryObjectAlreadyExists")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }




            var aggregateWithAggregateInfo = AddETagToAggregate(aggregate);
            aggregateWithAggregateInfo.AggregateInfo = new AggregateInfoValueObject(userId, createDate.Value);
            if(lastModifiedDate != null)
                aggregateWithAggregateInfo.AggregateInfo = aggregateWithAggregateInfo.AggregateInfo.MarkModified(lastModifiedByUserId, lastModifiedDate.Value);


            aggregateDictionary.Add(fullyQualifiedIdentifier, aggregateWithAggregateInfo);

            if(isPartitionCollection)
            {
                AddPartitionKeyMapping(aggregate, fullyQualifiedIdentifier);
            }

            var model = ConvertToModel(aggregateWithAggregateInfo);
            return Task.FromResult(model);
        }

        private void AddPartitionKeyMapping(T2 aggregate, string fullyQualifiedIdentifier)
        {
            var partitionKey = GetPartitionKey(aggregate, partitionKeyPrefix, isPartitionKeyFromId);
            if(partitionKeyIdentifierMappingDictionary.ContainsKey(partitionKey))
            {
                var identifiers = partitionKeyIdentifierMappingDictionary[partitionKey];
                identifiers.Add(fullyQualifiedIdentifier);
            }
            else
            {
                partitionKeyIdentifierMappingDictionary.Add(partitionKey, new HashSet<string>() { fullyQualifiedIdentifier });
            }
        }

        public Task<T3> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId,
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if(lastModifiedDate == null)
                lastModifiedDate = DateTime.UtcNow;

            var fullyQualifiedIdentifier = FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate);

            if(!aggregateDictionary.ContainsKey(fullyQualifiedIdentifier))
            {
                throw new ObjectNotFoundException($"{fullyQualifiedIdentifier} is not found")
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            var oldAggregateWithAggregateInfo = aggregateDictionary[fullyQualifiedIdentifier];
            if(aggregateWithMetadata.ETag != oldAggregateWithAggregateInfo.ETag)
            {
                throw new OptimisticConcurrencyException($"{fullyQualifiedIdentifier} ETag does not match")
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            var aggregateWithAggregateInfo = AddETagToAggregate(aggregateWithMetadata.Aggregate);
            aggregateWithAggregateInfo.AggregateInfo = oldAggregateWithAggregateInfo.AggregateInfo.MarkModified(userId,
                lastModifiedDate.Value);
            aggregateDictionary[fullyQualifiedIdentifier] = aggregateWithAggregateInfo;

            var model = ConvertToModel(aggregateWithAggregateInfo);

            return Task.FromResult(model);
        }

        public Task DeleteAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var fullyQualifiedIdentifier = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier);

            if(!aggregateDictionary.ContainsKey(fullyQualifiedIdentifier))
            {
                throw new ObjectNotFoundException($"{fullyQualifiedIdentifier} is not found")
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            var aggregateWithMetadata = aggregateDictionary[fullyQualifiedIdentifier];

            if(aggregateIdentifier != null && aggregateIdentifier.ETag != aggregateWithMetadata.ETag)
            {
                throw new OptimisticConcurrencyException($"{fullyQualifiedIdentifier} ETag does not match")
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            aggregateDictionary.Remove(fullyQualifiedIdentifier);

            return Task.FromResult<object>(null);
        }


        public Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var fullyQualifiedIdentifier = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier);

            if(!aggregateDictionary.ContainsKey(fullyQualifiedIdentifier))
            {
                throw new ObjectNotFoundException($"{fullyQualifiedIdentifier} is not found")
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            var aggregateWithMetadata = aggregateDictionary[fullyQualifiedIdentifier];

            if((aggregateIdentifier.ETag != null) && aggregateIdentifier.ETag != aggregateWithMetadata.ETag)
            {
                throw new OptimisticConcurrencyException($"{fullyQualifiedIdentifier} ETag does not match")
                    .AddToData("Error", "RepositoryOptimisticConcurrency")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            return Task.FromResult(aggregateWithMetadata);
        }



        private AggregateWithMetadata<T2> AddETagToAggregate(T2 aggregate)
        {
            return new AggregateWithMetadata<T2>()
            {
                ETag = Guid.NewGuid().ToString("n"),
                Aggregate = aggregate
            };
        }

        private T3 ConvertToModel(AggregateWithMetadata<T2> aggregateWithMetadata)
        {
            return aggregateMapper.Map(aggregateWithMetadata.Aggregate, aggregateWithMetadata.ETag,
                aggregateWithMetadata.AggregateInfo);
        }

        public Task<T3> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var fullyQualifiedIdentifier = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier);

            if(!aggregateDictionary.ContainsKey(fullyQualifiedIdentifier))
            {
                throw new ObjectNotFoundException($"Object Not Found ({FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier)})")
                    .AddToData("Error", "RepositoryObjectNotFound")
                    .AddToData("AggregatePluralName", aggregatePluralName);
            }

            var aggregateWithAggregateInfo = aggregateDictionary[fullyQualifiedIdentifier];
            var model = ConvertToModel(aggregateWithAggregateInfo);
            return Task.FromResult(model);
        }

        public Task<IEnumerable<T3>> GetAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult((IEnumerable<T3>)
                aggregateDictionary.Values.Select(ConvertToModel).ToList());
        }

        public Task<IEnumerable<T3>> GetAllByPartitionAsync(string partitionKey, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if(!partitionKeyIdentifierMappingDictionary.ContainsKey(partitionKey))
            {
                return Task.FromResult((IEnumerable<T3>)(new List<T3>()));
            }

            var identifiers = partitionKeyIdentifierMappingDictionary[partitionKey];
            var selectedValues = identifiers.Where(aggregateDictionary.ContainsKey)
                     .Select(x => aggregateDictionary[x])
                     .ToList();

            return Task.FromResult((IEnumerable<T3>)selectedValues.Select(ConvertToModel).ToList());
        }

        public Task<long> PurgeAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var aggregateCount = aggregateDictionary.Count;
            aggregateDictionary.Clear();

            cancellationToken.ThrowIfCancellationRequested();
            return Task.FromResult(Convert.ToInt64(aggregateCount));
        }

        public static string GetPartitionKey(object aggregate, string partitionKeyPrefix, bool fromId)
        {
            var identifierList = new List<string>();

            if(!string.IsNullOrEmpty(partitionKeyPrefix))
                identifierList.Add(partitionKeyPrefix);

            var properties = aggregate.GetType().GetProperties();

            foreach(var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if((attributes == null) || !attributes.Any())
                    continue;

                var identifierAttribute =
                    attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                if(string.IsNullOrEmpty(identifierAttribute?.PluralName))
                    continue;

                if(fromId)
                {
                    if(property.Name != "Id")
                        continue;
                }
                else
                {
                    if(property.Name == "Id")
                        continue;
                }

                identifierList.Add(property.GetValue(aggregate) as string);
            }

            var partitionKey = !identifierList.Any() ? string.Empty : string.Join(".", identifierList);

            if(fromId)
            {
                if(string.IsNullOrEmpty(partitionKey))
                    return string.Empty;

                if(!partitionKey.Contains("."))
                    return string.Empty;

                var lastPeriod = partitionKey.LastIndexOf('.');
                if(lastPeriod == 0)
                    return string.Empty;

                partitionKey = partitionKey.Substring(0, lastPeriod);
            }

            return partitionKey;
        }

        public string GetPartitionKey(object partitionIdentifier)
        {
            return GetPartitionKey(partitionIdentifier, partitionKeyPrefix, isPartitionKeyFromId);
        }
    }
}
