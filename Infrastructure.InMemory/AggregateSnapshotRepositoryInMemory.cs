﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.InMemory
{
    public class AggregateSnapshotRepositoryInMemory<T,T2> : IAggregateSnapshotRepository<T,T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {


        private readonly Dictionary<string, T2> dictionary;
        public AggregateSnapshotRepositoryInMemory()
        {
            dictionary = new Dictionary<string, T2>();
        }



        private static string GetPath(T aggregateIdentifier, int version)
        {
            var identifierList = new List<string>();

            var properties = aggregateIdentifier.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);

                if ((attributes != null) && attributes.Any())
                {
                    var identifierAttribute =
                        attributes.FirstOrDefault(a => a is IdentifierAttribute) as IdentifierAttribute;

                    var value = property.GetValue(aggregateIdentifier) as string;

                    identifierList.Add($"{value}");
                }
            }

            identifierList.Add(version.ToString());

            return string.Join("/", identifierList);

        }

        public Task InsertAsync(T aggregateIdentifier, T2 aggregateModel, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var path = GetPath(aggregateIdentifier, aggregateModel.AggregateInfo.Version);

            if (dictionary.ContainsKey(path))
            {
                var fullyQualifiedIdentifier = FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier);
                throw new ObjectAlreadyExistsException($"{fullyQualifiedIdentifier} already exist");
            }
            dictionary.Add(path, aggregateModel);

            return Task.FromResult<object>(null);
        }

        public Task UpdateAsync(T aggregateIdentifier, T2 aggregateModel, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var path = GetPath(aggregateIdentifier, aggregateModel.AggregateInfo.Version);

            if (!dictionary.ContainsKey(path))
            {
                throw new ObjectNotFoundException();
            }

            dictionary[path] = aggregateModel;

            return Task.FromResult<object>(null);
        }

        public Task<T2> LoadAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var path = GetPath(aggregateIdentifier, version);
            if (!dictionary.ContainsKey(path))
            {
                throw new ObjectNotFoundException();
            }
            return Task.FromResult(dictionary[path]);
        }
    }
}
