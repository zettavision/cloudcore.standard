﻿using System;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure
{
    public class ExtendedProperty<T>
        where T : IAggregate
    {
        public ExtendedProperty(string name, Type returnType, Func<T, object> mapFunc)
        {
            var funcReturnType = mapFunc.Method.ReturnType;
            if (funcReturnType != returnType &&  !returnType.IsSubclassOf(funcReturnType))
                throw new Exception($"returnType {returnType.Name} and funcReturnType {funcReturnType.Name} are not compatible");


            Name = name;
            this.mapFunc = mapFunc;
            ReturnType = returnType;


        }



        public ExtendedProperty(string name, Type returnType, Func<T, AggregateInfoValueObject, object> mapFunc)
        {
            var funcReturnType = mapFunc.Method.ReturnType;
            if (funcReturnType != returnType && !returnType.IsSubclassOf(funcReturnType))
                throw new Exception($"returnType {returnType.Name} and funcReturnType {funcReturnType.Name} are not compatible");


            Name = name;
            this.mapFunc2 = mapFunc;
            ReturnType = returnType;
        }

        public string Name { get; }

        public Type ReturnType { get; }

        private readonly Func<T, object> mapFunc;
        private readonly Func<T, AggregateInfoValueObject, object> mapFunc2;

        public object Map(T aggregate, AggregateInfoValueObject aggregateInfo)
        {
            if(mapFunc != null)
                return mapFunc(aggregate);
            else
                return mapFunc2(aggregate, aggregateInfo);
        }
    }
}
