﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure
{
    public static class IdentifierAttributeReader
    {
        public static string GetIdIdentifierPluralName(Type type)
        {

            var properties = type.GetProperties();
            var idProperty = properties.Single(p => p.Name == "Id");
            var attributes = idProperty.GetCustomAttributes(false);

            if ((attributes == null) || !attributes.Any())
                throw new Exception("No attributes found on the Id property");


            var identifierAttribute =
                attributes.Single(a => a is IdentifierAttribute) as IdentifierAttribute;


            return identifierAttribute.PluralName;



        }
    }
}
