﻿using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure
{
    public interface IAggregateGetByIdReader<in T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        Task<T2> GetByIdAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken));
    }
}