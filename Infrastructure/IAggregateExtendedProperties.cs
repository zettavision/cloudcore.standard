﻿using System.Collections.Generic;
using ZettaVision.CloudCore.Domain;

namespace ZettaVision.CloudCore.Infrastructure
{
    public interface IAggregateExtendedProperties<T>
        where T : IAggregate
    {
        IEnumerable<ExtendedProperty<T>> ExtendedProperties { get; }
    }
}