﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.AggreateEvent.Queries;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Requests;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure
{
    public class AggregateVersionReaderFromPlayBack<T, T2, T3> : IAggregateVersionReader<T, T3>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {
        private readonly IAggregateEventReader aggregateEventReader;
        private readonly IAggregateGetByIdReader<T, T3> aggregateReader;
        private readonly AggregateToAggregateModelMapper<T2, T3> modelMapper;

        public AggregateVersionReaderFromPlayBack(IAggregateEventReader aggregateEventReader, IAggregateGetByIdReader<T, T3> aggregateReader)
        {
            this.aggregateEventReader = aggregateEventReader;
            this.aggregateReader = aggregateReader;
            modelMapper = new AggregateToAggregateModelMapper<T2, T3>();
        }

        public async Task<T3> GetByIdAndVersionAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken))
        {
            if(version < 1)
                throw new InvalidOperationException("Version must be 1 or greater");

            var aggregateModelCurrentVersion = await aggregateReader.GetByIdAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);

            if(version > aggregateModelCurrentVersion.AggregateInfo.Version)
                throw new InvalidOperationException($"Version cannot be {version}.  It must be less than or equal to the current version which is {aggregateModelCurrentVersion.AggregateInfo.Version}.");

            if (version == aggregateModelCurrentVersion.AggregateInfo.Version)
            {
                return aggregateModelCurrentVersion;
            }

            var events = await GetEventsUpToVersionAsync(aggregateIdentifier, version, cancellationToken).ConfigureAwait(false);

            var firstEvent = events.First();
            var aggregate = (T2)Activator.CreateInstance(typeof(T2), aggregateIdentifier, firstEvent.Request);
            var aggregateInfo = new AggregateInfoValueObject(firstEvent.AggregateInfo.CreatedByUserId, firstEvent.AggregateInfo.CreatedDate);

            var allEventsAfterFirst = events.Skip(1).ToList();
            foreach (var eventModel in allEventsAfterFirst)
            {
                var methodInfo = typeof(T2).GetMethod("Apply");
                object[] parametersArray = { eventModel.Request };
                methodInfo.Invoke(aggregate, parametersArray);

                aggregateInfo.MarkModified(eventModel.AggregateInfo.LastModifiedByUserId,
                eventModel.AggregateInfo.CreatedDate);
            }

            if(aggregateInfo.Version != version)
                throw new InvalidOperationException($"aggregateInfo.Version {aggregateInfo.Version} must match the requested version of {version}");

            var model = modelMapper.Map(aggregate, null, aggregateInfo);
            return model;
        }

        private async Task<IEnumerable<AggregateEventModel>> GetEventsUpToVersionAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken))
        {
            var list = new List<AggregateEventModel>();

            var result = await aggregateEventReader.HandleAsync(
                new AggregateEventGetAllQuery(FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier), null), cancellationToken).ConfigureAwait(false);

            var versionFound = false;

            if (result.Items != null)
            {
                list.AddRange(result.Items.Where(e => int.Parse(e.Id) <= version));
                versionFound = list.Any(e => int.Parse(e.Id) == version);
            }

            var continuationToken = result.ContinuationToken;

            while (continuationToken != null && versionFound == false)
            {
                result = await aggregateEventReader.HandleAsync(
                    new AggregateEventGetAllQuery(FullyQualifiedIdentifier.GetIdentifier(aggregateIdentifier), 
                    new PageParametersModel() {ContinuationToken = continuationToken}),
                    cancellationToken).ConfigureAwait(false);

                if (result.Items != null)
                {
                    list.AddRange(result.Items.Where(e => int.Parse(e.Id) <= version));
                    versionFound = list.Any(e => int.Parse(e.Id) == version);
                }

                continuationToken = result.ContinuationToken;
            }

            if (list.Count != version)
                throw new InvalidOperationException($"Unable to find all events for version {version} only found {list.Count}");

            return list;
        }
    }
}
