﻿namespace ZettaVision.CloudCore.Infrastructure
{
    public static class KeyFormatter
    {
        public static string PadForInt32(string key)
        {
            return key.PadLeft(10, '0');
        }

        public static string PadForInt64(string key)
        {
            return key.PadLeft(19, '0');
        }

        public static string InvertAndPadForInt64(string id)
        {
            var idAsLong = long.Parse(id);
            var idAsInvertedLong = long.MaxValue - idAsLong;
            return PadForInt64(idAsInvertedLong.ToString());
        }

        public static string InvertAndPadForInt64(long idAsLong)
        {
            var idAsInvertedLong = long.MaxValue - idAsLong;
            return PadForInt64(idAsInvertedLong.ToString());
        }
    }
}