﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure
{
    public class AggregateRepositoryWithSnapshotStore<T, T2, T3> : IAggregateRepository<T, T2, T3>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateModel, new()
    {
        private readonly IAggregateRepository<T, T2, T3> aggregateRepository;
        private readonly IAggregateSnapshotRepository<T, T3> aggregateSnapshotRepository;
        private readonly AggreateToAggregateIdentifierMapper<T, T2> aggreateToAggregateIdentifierMapper;
        private readonly AggregateToAggregateModelMapper<T2, T3> aggregateToAggregateModelMapper;

        public AggregateRepositoryWithSnapshotStore(IAggregateRepository<T, T2, T3> aggregateRepository, 
            IAggregateSnapshotRepository<T, T3> aggregateSnapshotRepository, IEnumerable<JsonConverter> jsonConverters = null)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;

            if (jsonConverters != null && jsonConverters.Any())
            {
                foreach (var jsonConverter in jsonConverters)
                {
                    jsonSerializerSettings.Converters.Add(jsonConverter);
                }
            }


            this.aggregateRepository = aggregateRepository;
            this.aggregateSnapshotRepository = aggregateSnapshotRepository;
            aggreateToAggregateIdentifierMapper = new AggreateToAggregateIdentifierMapper<T, T2>();
            aggregateToAggregateModelMapper = new AggregateToAggregateModelMapper<T2, T3>(jsonSerializerSettings);
        }

        public async Task<T3> InsertAsync(T2 aggregate, string userId = null, 
            DateTime? createDate = null, string lastModifiedByUserId = null, 
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (createDate == null)
                createDate = DateTime.UtcNow;

            await AddCreateSnapshotAsync(aggregate, userId, createDate.Value, lastModifiedByUserId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
            return await aggregateRepository.InsertAsync(aggregate, userId, createDate, lastModifiedByUserId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        private async Task AddCreateSnapshotAsync(T2 aggregate, string userId, 
            DateTime createDate, string lastModifiedByUserId = null, 
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var aggregateInfo = new AggregateInfoValueObject(userId, createDate);

            if(lastModifiedDate != null)
                aggregateInfo = aggregateInfo.MarkModified(lastModifiedByUserId, lastModifiedDate.Value);

            var model = aggregateToAggregateModelMapper.Map(aggregate, null, aggregateInfo);
            var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregate, null);

            try
            {
                await aggregateSnapshotRepository.InsertAsync(aggregateIdentifier, model, cancellationToken).ConfigureAwait(false);
            }
            catch (ObjectAlreadyExistsException)
            {

                var oldSnapshot =
                    await aggregateSnapshotRepository.LoadAsync(aggregateIdentifier, 1, cancellationToken).ConfigureAwait(false);

                bool alreadyExist = true;
                try
                {
                    await
                        aggregateRepository.LoadAsync(aggregateIdentifier, cancellationToken)
                            .ConfigureAwait(false);
                }
                catch (ObjectNotFoundException)
                {
                    alreadyExist = false;
                }


                if (alreadyExist)
                {
                    throw new ObjectAlreadyExistsException();
                }

                var expiration = oldSnapshot.AggregateInfo.CreatedDate.AddMinutes(15);
                if (expiration < DateTime.UtcNow)
                {
                    await aggregateSnapshotRepository.UpdateAsync(aggregateIdentifier, model, cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    var expirationTimeSpan = expiration - DateTime.UtcNow;
                    throw new OptimisticConcurrencyException(
                        $"Snapshot already exist.  Either the aggregate is currently being processed or it was not saved due to an error.  This event can be overridden in {expirationTimeSpan.TotalMinutes} minutes");
                }
            }
        }


        public async Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await aggregateRepository.LoadAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }

        public async Task<T3> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId = null, 
            DateTime? lastModifiedDate = null, CancellationToken cancellationToken = default(CancellationToken))
        {

            if (lastModifiedDate == null)
                lastModifiedDate = DateTime.UtcNow;

            await AddUpdateSnapshotAsync(aggregateWithMetadata, userId, lastModifiedDate.Value, cancellationToken).ConfigureAwait(false);

            return await aggregateRepository.UpdateAsync(aggregateWithMetadata, userId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        private async Task AddUpdateSnapshotAsync(AggregateWithMetadata<T2> aggregateWithMetadata, string userId, 
            DateTime lastModifiedDate, CancellationToken cancellationToken = default(CancellationToken))
        {

            var clonedAggregateInfo = aggregateWithMetadata.AggregateInfo.Clone();

            clonedAggregateInfo = clonedAggregateInfo.MarkModified(userId, lastModifiedDate);

            var model = aggregateToAggregateModelMapper.Map(aggregateWithMetadata.Aggregate, null,
                clonedAggregateInfo);

            var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregateWithMetadata.Aggregate, null);

            if (clonedAggregateInfo.Version < 2)
                throw new Exception("Version must be greater than 1 for an update event");

            if (aggregateWithMetadata.ETag == null)
                throw new Exception("ETag cannot be null");

            try
            {
                await aggregateSnapshotRepository.InsertAsync(aggregateIdentifier, model, cancellationToken).ConfigureAwait(false);
            }
            catch (ObjectAlreadyExistsException)
            {

                var oldSnapshot =
                    await aggregateSnapshotRepository.LoadAsync(aggregateIdentifier, clonedAggregateInfo.Version, cancellationToken).ConfigureAwait(false);


                var aggregateIdentifierWithETag = aggreateToAggregateIdentifierMapper.Map(aggregateWithMetadata.Aggregate, aggregateWithMetadata.ETag);
                //try to load if the etag does not match this will fail.  In that case the another process completed the transcation with the same event id or version and an optimisic concurrency exception is thrown
                await aggregateRepository.LoadAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);


                var expiration = oldSnapshot.AggregateInfo.CreatedDate.AddMinutes(15);
                if (expiration < DateTime.UtcNow)
                {
                    await aggregateSnapshotRepository.UpdateAsync(aggregateIdentifier, model, cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    var expirationTimeSpan = expiration - DateTime.UtcNow;
                    throw new OptimisticConcurrencyException(
                        $"Snapshot already exist.  Either the aggregate is currently being processed or it was not saved due to an error.  This snapshot can be overridden in {expirationTimeSpan.TotalMinutes} minutes");
                }
            }
        }


        public async Task DeleteAsync(T aggregateIdentifier, CancellationToken cancellationToken = default(CancellationToken))
        {
            await aggregateRepository.DeleteAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);
        }
    }
}
