﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure
{
    public interface IAggregateSnapshotRepository<in T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
         Task InsertAsync(T aggregateIdentifier, T2 aggregateModel, CancellationToken cancellationToken = default(CancellationToken));
         Task UpdateAsync(T aggregateIdentifier, T2 aggregateModel, CancellationToken cancellationToken = default(CancellationToken));
         Task<T2> LoadAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken));
    }
}
