﻿using System.Threading;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Infrastructure
{
    public interface IAggregateRefresher
    {
        Task<long> RefreshAllAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}