﻿using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure.AggregateEvent
{
    public class AggregateEventModelGeneric<T> : AggregateEventModel where T : IAggregateRequest
    {
        public new T Request
        {
            get { return (T) base.Request; }
            set { base.Request = value; }
        }
    }
}