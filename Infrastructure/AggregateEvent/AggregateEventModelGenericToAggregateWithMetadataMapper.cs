﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Domain.ValueObjects;

namespace ZettaVision.CloudCore.Infrastructure.AggregateEvent
{
    public class AggregateEventModelGenericToAggregateWithMetadataMapper
    {
        public AggregateWithMetadata<AggregateEventAggregate> Map(AggregateEventModel model)
        {
            var aggregate = (AggregateEventAggregate)Activator.CreateInstance(typeof(AggregateEventAggregate), true);
            typeof(AggregateEventAggregate).GetProperty("AggregateId").SetValue(aggregate, model.AggregateId);
            typeof(AggregateEventAggregate).GetProperty("Id").SetValue(aggregate, model.Id);
            typeof(AggregateEventAggregate).GetProperty("RequestType").SetValue(aggregate, model.RequestType);
            typeof(AggregateEventAggregate).GetProperty("Request").SetValue(aggregate, model.Request);

            var aggregateWithMetadata = new AggregateWithMetadata<AggregateEventAggregate>();

            aggregateWithMetadata.Aggregate = aggregate;
            aggregateWithMetadata.ETag = model.ETag;
            aggregateWithMetadata.AggregateInfo =
                JsonConvert.DeserializeObject<AggregateInfoValueObject>(JsonConvert.SerializeObject(model.AggregateInfo));

            return aggregateWithMetadata;
            
        }
    }
}
