﻿using System;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain.AggreateEvent.Models;
using ZettaVision.CloudCore.Infrastructure.Json;

namespace ZettaVision.CloudCore.Infrastructure.AggregateEvent
{
 
    public class AggregateEventJsonCreationConverter : JsonCreationConverter<AggregateEventModel>
    {

        protected override AggregateEventModel Create(Type objectType, JObject jsonObject)
        {
            var requestTypeName = jsonObject["RequestType"].ToString();
            var requestType = Type.GetType(requestTypeName);
            var eventType = typeof(AggregateEventModelGeneric<>).MakeGenericType(requestType);
            return (AggregateEventModel)Activator.CreateInstance(eventType);
        }
    }
}
