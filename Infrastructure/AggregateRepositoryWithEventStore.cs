﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.AggreateEvent;
using ZettaVision.CloudCore.Domain.AggreateEvent.Infrastructure;
using ZettaVision.CloudCore.Domain.AggreateEvent.Request;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Mappers;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.Requests;

namespace ZettaVision.CloudCore.Infrastructure
{
    public class AggregateRepositoryWithEventStore<T, T2, T3, T4> : IAggregateRepositoryWithEventStore<T, T2, T3, T4>
        where T : AggregateIdentifier
        where T2 : IAggregate
        where T3 : IAggregateRequest
        where T4 : IAggregateModel, new()
    {

        private readonly IAggregateEventRepository aggregateEventRepository;
        private readonly IAggregateRepository<T, T2, T4> aggregateRepository;
        private readonly AggreateToAggregateIdentifierMapper<T,T2> aggreateToAggregateIdentifierMapper;
        public AggregateRepositoryWithEventStore(IAggregateEventRepository aggregateEventRepository, 
            IAggregateRepository<T,T2,T4> aggregateRepository )
        {
            this.aggregateEventRepository = aggregateEventRepository;
            this.aggregateRepository = aggregateRepository;
            aggreateToAggregateIdentifierMapper = new AggreateToAggregateIdentifierMapper<T, T2>();
        }

        public async Task<T4> InsertAsync(T2 aggregate, T3 aggregateRequest, string userId = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var createDate = DateTime.UtcNow;
 
            if(aggregateRequest != null)
                await AddCreateEventAsync(aggregate, aggregateRequest, userId, createDate, cancellationToken).ConfigureAwait(false);

            return await aggregateRepository.InsertAsync(aggregate, userId, createDate, null, null, cancellationToken).ConfigureAwait(false);
        }

        public async Task<T4> UpdateAsync(AggregateWithMetadata<T2> aggregateWithMetadata, T3 aggregateRequest, string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var lastModifiedDate = DateTime.UtcNow;

            if (aggregateRequest != null)
                await AddUpdateEventAsync(aggregateWithMetadata.AggregateInfo.Version + 1, aggregateWithMetadata,
                    aggregateRequest, userId, lastModifiedDate, cancellationToken).ConfigureAwait(false);

            return await aggregateRepository.UpdateAsync(aggregateWithMetadata, userId, lastModifiedDate, cancellationToken).ConfigureAwait(false);
        }

        private async Task AddCreateEventAsync(T2 aggregate, T3 aggregateRequest, string userId, DateTime createDate,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            

            var aggregateEvent = GetAggregateEvent(aggregate, aggregateRequest, 1);
            try
            {
                await aggregateEventRepository.InsertAsync(aggregateEvent, userId, createDate, null, null, cancellationToken).ConfigureAwait(false);
            }
            catch (ObjectAlreadyExistsException)
            {
                var aggregateId = FullyQualifiedIdentifier.GetIdentifier(aggregate);

                var oldAggregateEventWithMetadata =
                    await aggregateEventRepository.LoadAsync(new AggregateEventIdentifier(aggregateId, "1"), cancellationToken).ConfigureAwait(false);

                bool alreadyExist = true;
         
                try
                {
                    await
                        aggregateRepository.LoadAsync(aggreateToAggregateIdentifierMapper.Map(aggregate), cancellationToken)
                            .ConfigureAwait(false);
                }
                catch (ObjectNotFoundException)
                {
                    alreadyExist = false;
                }


                if (alreadyExist)
                {
                    throw new ObjectAlreadyExistsException();
                }

                var expiration = oldAggregateEventWithMetadata.AggregateInfo.CreatedDate.AddMinutes(15);
                if (expiration < DateTime.UtcNow)
                {
                    oldAggregateEventWithMetadata.Aggregate = aggregateEvent;

                   
                    await aggregateEventRepository.DeleteAsync(new AggregateEventIdentifier(aggregateId, "1", oldAggregateEventWithMetadata.ETag), cancellationToken).ConfigureAwait(false);
                    await aggregateEventRepository.InsertAsync(aggregateEvent, userId, createDate, null, null, cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    var expirationTimeSpan = expiration - DateTime.UtcNow;
                    throw new OptimisticConcurrencyException(
                        $"Event already exist.  Either the aggregate is currently being processed or it was not saved due to an error.  This event can be overridden in {expirationTimeSpan.TotalMinutes} minutes");
                }
            }
        }

        private async Task AddUpdateEventAsync(int version, AggregateWithMetadata<T2> aggregateWithMetadata, T3 aggregateRequest,  string userId, DateTime createDate, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (version < 2)
                throw new Exception("Version must be greater than 1 for an update event");

            if (aggregateWithMetadata.ETag == null)
                throw new Exception("ETag cannot be null");



            var aggregateEvent = GetAggregateEvent(aggregateWithMetadata.Aggregate, aggregateRequest, version);
            try
            {
                await aggregateEventRepository.InsertAsync(aggregateEvent, userId, createDate, null, null, cancellationToken).ConfigureAwait(false);
            }
            catch (ObjectAlreadyExistsException)
            {
                var aggregateId = FullyQualifiedIdentifier.GetIdentifier(aggregateWithMetadata.Aggregate);

                var oldAggregateEventWithMetadata =
                    await aggregateEventRepository.LoadAsync(new AggregateEventIdentifier(aggregateId, version.ToString()), cancellationToken).ConfigureAwait(false);


                var aggregateIdentifier = aggreateToAggregateIdentifierMapper.Map(aggregateWithMetadata.Aggregate, aggregateWithMetadata.ETag);
                //try to load if the etag does not match this will fail.  In that case the another process completed the transcation with the same event id or version and an optimisic concurrency exception is thrown
                await aggregateRepository.LoadAsync(aggregateIdentifier, cancellationToken).ConfigureAwait(false);


                var expiration = oldAggregateEventWithMetadata.AggregateInfo.CreatedDate.AddMinutes(15);
                if (expiration < DateTime.UtcNow)
                {
                    oldAggregateEventWithMetadata.Aggregate = aggregateEvent;

                    await aggregateEventRepository.DeleteAsync(new AggregateEventIdentifier(aggregateId, version.ToString(), oldAggregateEventWithMetadata.ETag), cancellationToken).ConfigureAwait(false);
                    await aggregateEventRepository.InsertAsync(aggregateEvent, userId, createDate, null, null, cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    var expirationTimeSpan = expiration - DateTime.UtcNow;
                    throw new OptimisticConcurrencyException(
                        $"Event already exist.  Either the aggregate is currently being processed or it was not saved due to an error.  This event can be overridden in {expirationTimeSpan.TotalMinutes} minutes");
                }
            }
        }

        public Task<AggregateWithMetadata<T2>> LoadAsync(T aggregateIdentifier,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return aggregateRepository.LoadAsync(aggregateIdentifier, cancellationToken);
        }



        private AggregateEventAggregate GetAggregateEvent(T2 aggregate, T3 aggregateRequest, int version)
        {
            return new AggregateEventAggregate(new AggregateEventAddRequest()
            {
                AggregateId = FullyQualifiedIdentifier.GetIdentifier(aggregate),
                Id = version.ToString(),
                Request = aggregateRequest
            });
        }



        
    }
}
