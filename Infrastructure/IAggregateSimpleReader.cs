﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure
{
    public interface IAggregateSimpleReader<T, in T2> : IAggregateGetByIdReader<T2, T>
        where T : IAggregateModel
        where T2 : AggregateIdentifier
    {
        Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}