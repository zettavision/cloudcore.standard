﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Infrastructure
{
    public interface IAggregatePurger
    {
        Task<long> PurgeAllAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
