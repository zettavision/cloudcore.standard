using System.Threading;
using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure
{
    public class AggregateSnapshotReader<T, T2> : IAggregateVersionReader<T, T2>
        where T : AggregateIdentifier
        where T2 : IAggregateModel
    {
        private readonly IAggregateSnapshotRepository<T, T2> repository;

        public AggregateSnapshotReader(IAggregateSnapshotRepository<T, T2> repository)
        {
            this.repository = repository;
        }

        public async Task<T2> GetByIdAndVersionAsync(T aggregateIdentifier, int version, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await repository.LoadAsync(aggregateIdentifier, version, cancellationToken).ConfigureAwait(false);
        }
    }
}