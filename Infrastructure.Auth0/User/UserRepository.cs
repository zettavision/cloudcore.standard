﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Auth0.Core.Exceptions;
using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Commands;
using ZettaVision.CloudCore.Domain.User.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Models;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.User
{
    public class UserRepository : IUserRepository
    {
        private readonly ManagementApiClientFactory managementApiClientFactory;

        private readonly UserMapper userMapper;

        public UserRepository(ManagementApiClientFactory managementApiClientFactory)
        {
            this.managementApiClientFactory = managementApiClientFactory;
            userMapper = new UserMapper();
        }

        public async Task<UserModel> InsertAsync(UserAggregate aggregate, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await InsertAsync(aggregate, null, cancellationToken).ConfigureAwait(false);
        }

        public async Task<UserModel> InsertAsync(UserAggregate aggregate, string password, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var managementApiClient =
                await managementApiClientFactory.GetManagementApiClientAsync().ConfigureAwait(false);

            dynamic newUserMetadata = new ExpandoObject();
            newUserMetadata.password_setup_required = aggregate.PasswordSetupRequired;

            newUserMetadata.name = aggregate.Name;
            newUserMetadata.family_name = aggregate.FamilyName;
            newUserMetadata.given_name = aggregate.GivenName;
            newUserMetadata.phone = aggregate.Phone;

            password = string.IsNullOrEmpty(password) ? "A" + GuidId.GetNewId() + "!!!" : password;

            var userCreateRequest = new UserCreateRequest
            {
                Connection = "Username-Password-Authentication",
                Email = aggregate.Email,
                Password = password,
                UserMetadata = newUserMetadata
            };

            global::Auth0.ManagementApi.Models.User auth0User;
            try
            {
                auth0User = await managementApiClient.Users.CreateAsync(userCreateRequest).ConfigureAwait(false);

            }
            catch (ErrorApiException errorApiException)
            {
                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                throw;
            }

            cancellationToken.ThrowIfCancellationRequested();

            return userMapper.MapToModel(auth0User);
        }

        public async Task<AggregateWithMetadata<UserAggregate>> LoadAsync(UserIdentifier aggregateIdentifier,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var managementApiClient =
                await managementApiClientFactory.GetManagementApiClientAsync().ConfigureAwait(false);

            global::Auth0.ManagementApi.Models.User auth0User;
            try
            {
                auth0User = await managementApiClient.Users.GetAsync(aggregateIdentifier.Id).ConfigureAwait(false);
            }
            catch (ErrorApiException errorApiException)
            {

                errorApiException.AddToData("ZettaVision.User.Id", aggregateIdentifier.Id);

                if (errorApiException.StatusCode == HttpStatusCode.BadRequest
                    && errorApiException.ApiError.ErrorCode == "invalid_uri")
                {
                    throw new ObjectNotFoundException(errorApiException.Message, errorApiException);
                }


                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                throw;
            }

            if (!string.IsNullOrEmpty(aggregateIdentifier.ETag) && Auth0ETagGenerator.GetETag(auth0User.UpdatedAt ?? DateTime.MinValue) != aggregateIdentifier.ETag)
                throw new OptimisticConcurrencyException();

            cancellationToken.ThrowIfCancellationRequested();

            return userMapper.MapToAggregate(auth0User);
        }

        public async Task DeleteAsync(UserIdentifier aggregateIdentifier,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var managementApiClient =
                await managementApiClientFactory.GetManagementApiClientAsync().ConfigureAwait(false);

            try
            {
                await managementApiClient.Users.DeleteAsync(aggregateIdentifier.Id).ConfigureAwait(false);
            }
            catch (ErrorApiException errorApiException)
            {
                if (errorApiException.StatusCode == HttpStatusCode.BadRequest
                && errorApiException.ApiError.ErrorCode == "invalid_uri")
                {
                    throw new ObjectNotFoundException(errorApiException.Message, errorApiException);
                }

                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                throw;
            }
        }

        public async Task<UserModel> UpdateAsync(AggregateWithMetadata<UserAggregate> aggregateWithMetadata,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await UpdateAsync(aggregateWithMetadata, null, cancellationToken).ConfigureAwait(false);
        }

        public async Task<UserModel> UpdateAsync(AggregateWithMetadata<UserAggregate> aggregateWithMetadata, string password,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var managementApiClient =
                await managementApiClientFactory.GetManagementApiClientAsync().ConfigureAwait(false);


            global::Auth0.ManagementApi.Models.User auth0User;
            try
            {
                auth0User =
                    await managementApiClient.Users.GetAsync(aggregateWithMetadata.Aggregate.Id).ConfigureAwait(false);

            }
            catch (ErrorApiException errorApiException)
            {
                if (errorApiException.StatusCode == HttpStatusCode.BadRequest
                && errorApiException.ApiError.ErrorCode == "invalid_uri")
                {
                    throw new ObjectNotFoundException(errorApiException.Message, errorApiException);
                }

                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                throw;
            }

            var model = userMapper.MapToModel(auth0User);


            if (model.ETag != aggregateWithMetadata.ETag)
                throw new OptimisticConcurrencyException();

            var newRolesAsString = ConvertRolesToString(aggregateWithMetadata.Aggregate.Roles);
            var oldRolesAsString = ConvertRolesToString(model.Roles);

            var rolesChanged = !string.Equals(oldRolesAsString, newRolesAsString);

            var pictureFromUserChanged =
                !string.Equals(model.PictureFromUser, aggregateWithMetadata.Aggregate.PictureFromUser);

            var emailChanged =
                !string.Equals(model.Email, aggregateWithMetadata.Aggregate.Email,
                    StringComparison.InvariantCultureIgnoreCase);

            var pinHashChanged = !string.Equals(model.PinHash, aggregateWithMetadata.Aggregate.PinHash);

            var nameChanged = !string.Equals(model.Name, aggregateWithMetadata.Aggregate.Name);
            var givenNameChanged = !string.Equals(model.GivenName, aggregateWithMetadata.Aggregate.GivenName);
            var familyNameChanged = !string.Equals(model.FamilyName, aggregateWithMetadata.Aggregate.FamilyName);
            var phoneChanged = !string.Equals(model.Phone, aggregateWithMetadata.Aggregate.Phone);

            var passwordSetupRequiredChanged =
                model.PasswordSetupRequired != aggregateWithMetadata.Aggregate.PasswordSetupRequired;

            bool passwordChanged = !string.IsNullOrEmpty(password);

            var hasAnyChanges = rolesChanged || emailChanged 
                || pictureFromUserChanged || passwordSetupRequiredChanged 
                || passwordChanged || pinHashChanged
                || nameChanged || familyNameChanged
                || givenNameChanged || phoneChanged;

            if (hasAnyChanges == false)
            {
                return userMapper.MapToModel(auth0User);
            }

            var userUpdateRequest = new UserUpdateRequest
            {
                Connection = "Username-Password-Authentication"
            };

            if (rolesChanged)
            {
                dynamic newAppMetadata = new ExpandoObject();

                if (rolesChanged)
                    newAppMetadata.roles = aggregateWithMetadata.Aggregate.Roles;

                userUpdateRequest.AppMetadata = newAppMetadata;
            }

            var userMetadataChanges = 
                pictureFromUserChanged || passwordSetupRequiredChanged 
                || pinHashChanged || nameChanged 
                || familyNameChanged || givenNameChanged 
                || phoneChanged;

            if (userMetadataChanges)
            {
                dynamic newUserMetadata = new ExpandoObject();

                if (pictureFromUserChanged)
                    newUserMetadata.picture = aggregateWithMetadata.Aggregate.PictureFromUser;

                if (pinHashChanged)
                    newUserMetadata.pin_hash = aggregateWithMetadata.Aggregate.PinHash;

                if (passwordSetupRequiredChanged)
                    newUserMetadata.password_setup_required = aggregateWithMetadata.Aggregate.PasswordSetupRequired;

                if (nameChanged)
                    newUserMetadata.name = aggregateWithMetadata.Aggregate.Name;

                if (familyNameChanged)
                    newUserMetadata.family_name = aggregateWithMetadata.Aggregate.FamilyName;

                if (givenNameChanged)
                    newUserMetadata.given_name = aggregateWithMetadata.Aggregate.GivenName;

                if (phoneChanged)
                    newUserMetadata.phone = aggregateWithMetadata.Aggregate.Phone;

                userUpdateRequest.UserMetadata = newUserMetadata;
            }

            if (emailChanged)
            {
                userUpdateRequest.Email = aggregateWithMetadata.Aggregate.Email;
            }

            if (passwordChanged)
            {
                userUpdateRequest.Password = password;
            }

            try
            {
                auth0User = await
                        managementApiClient.Users.UpdateAsync(aggregateWithMetadata.Aggregate.Id, userUpdateRequest)
                            .ConfigureAwait(false);
            }
            catch (ErrorApiException errorApiException)
            {
                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                throw;
            }

            cancellationToken.ThrowIfCancellationRequested();

            return userMapper.MapToModel(auth0User);
        }

        private string ConvertRolesToString(IEnumerable<string> roles)
        {
            return (roles == null || !roles.Any())
                ? null
                : string.Join(",", roles.OrderBy(r => r).ToList());
        }
    }
}