﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Auth0.Core.Exceptions;
using Auth0.Core.Http;
using ZettaVision.CloudCore.Domain.Infrastructure;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.User.Commands.Requests;
using ZettaVision.CloudCore.Domain.User.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Models;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.User
{
    public class UserLoginHandler : IUserLoginHandler
    {

        private readonly IAuth0TokenManager auth0TokenManager;
        private readonly string clientId;
        private readonly string clientSecret;
        private readonly AuthenticationApiClient authenticationApiClient;
        private readonly JwtSignatureAlgorithm signingAlgorithm;

        public UserLoginHandler(IAuth0TokenManager auth0TokenManager, 
            string domain, 
            string clientId, 
            string clientSecret,
            JwtSignatureAlgorithm signingAlgorithm)
        {
            this.auth0TokenManager = auth0TokenManager;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.signingAlgorithm = signingAlgorithm;
            authenticationApiClient = new AuthenticationApiClient(domain);

        }

        public UserLoginHandler(IAuth0TokenManager auth0TokenManager,
            string domain,
            string clientId,
            string clientSecret)
        {
            this.auth0TokenManager = auth0TokenManager;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.signingAlgorithm = JwtSignatureAlgorithm.HS256;
            authenticationApiClient = new AuthenticationApiClient(domain);

        }

        public async Task<UserLoginResponseModel> HandleAsync(UserLoginRequest request,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                cancellationToken.ThrowIfCancellationRequested();

                var authenticationResponse = await authenticationApiClient.GetTokenAsync(new ResourceOwnerTokenRequest
                {
                    ClientId = clientId,
                    ClientSecret = clientSecret,
                    Scope = "openid profile email",
                    Username = request.UserName,
                    Password = request.Password,
                    Realm = "Username-Password-Authentication",
                    SigningAlgorithm = signingAlgorithm

                }).ConfigureAwait(false);

                cancellationToken.ThrowIfCancellationRequested();

                var user = await authenticationApiClient.GetUserInfoAsync(authenticationResponse.AccessToken).ConfigureAwait(false);

                if (authenticationResponse == null)
                    throw new InvalidOperationException("authenticationResponse cannot be null");

                if (authenticationResponse.TokenType == null)
                    throw new InvalidOperationException("authenticationResponse.TokenType cannot be null");

                if (authenticationResponse.AccessToken == null)
                    throw new InvalidOperationException("authenticationResponse.AccessToken cannot be null");

                if (authenticationResponse.IdToken == null)
                    throw new InvalidOperationException("authenticationResponse.IdToken cannot be null");

                var payloadValueObject = auth0TokenManager.GetPayload(authenticationResponse.IdToken);
                var payloadModel = new Auth0TokenPayloadModel() { UserId = payloadValueObject.UserId, Roles = payloadValueObject.Roles };


                var model = new UserLoginResponseModel
                {
                    AuthenticationResponse = new AuthenticationResponseModel()
                    {
                        AccessToken = authenticationResponse.AccessToken,
                        IdToken = authenticationResponse.IdToken,
                        RefreshToken = authenticationResponse.RefreshToken,
                        TokenType = authenticationResponse.TokenType
                    },
                    IdTokenPayload = payloadModel
                };

                cancellationToken.ThrowIfCancellationRequested();
     
                return model;
            }
            catch (ErrorApiException errorApiException)
            {
                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                if (errorApiException.Message == "Wrong email or password.")
                {
                    throw new UnauthorizedAccessException(errorApiException.Message, errorApiException);
                }

                throw;
            }
        }
    }
}
