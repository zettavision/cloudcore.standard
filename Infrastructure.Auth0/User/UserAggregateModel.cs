﻿using System;
using System.Collections.Generic;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.User
{
    internal class UserAggregateModel
    {
        public bool Blocked { get; set; }
        public string Email { get; set; }
        public bool? EmailVerified { get; set; }
        public string FamilyName { get; set; }
        public string GivenName { get; set; }
        public string Id { get; set; }
        public string LastIpAddress { get; set; }
        public DateTime LastLogin { get; set; }
        public string Locale { get; set; }
        public int LoginsCount { get; set; }
        public string Name { get; set; }
        public bool PasswordSetupRequired { get; set; }
        public string Phone { get; set; }
        public string PictureFromConnection { get; set; }
        public string PictureFromUser { get; set; }
        public string PinHash { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public IEnumerable<UserIdentityModel> Identities { get; set; }
    }
}