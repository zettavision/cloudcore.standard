﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auth0.ManagementApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Models;
using ZettaVision.CloudCore.Domain.User.Aggregate;
using ZettaVision.CloudCore.Domain.User.Models;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.User
{
    internal class UserMapper
    {
        private readonly JsonSerializerSettings jsonSerializerSettings;

        internal UserMapper()
        {
            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.ContractResolver = new JsonPrivateSetterContractResolver();
            jsonSerializerSettings.ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;
        }

        internal AggregateWithMetadata<UserAggregate> MapToAggregate(global::Auth0.ManagementApi.Models.User auth0User)
        {
            var userAggregateModel = new UserAggregateModel
            {
                Blocked = auth0User.Blocked ?? false,
                Email = auth0User.Email,
                EmailVerified = auth0User.EmailVerified,
                FamilyName = GetFamilyNameFromUserMetadata(auth0User.UserMetadata),
                GivenName = GetGivenNameFromUserMetadata(auth0User.UserMetadata),
                Id = auth0User.UserId,
                LastIpAddress = auth0User.LastIpAddress,
                LastLogin = auth0User.LastLogin ?? DateTime.MinValue,
                Locale = auth0User.Locale,
                LoginsCount = auth0User.LoginsCount == null ? 0 : int.Parse(auth0User.LoginsCount),
                Name = GetNameFromUserMetadata(auth0User.UserMetadata),
                PasswordSetupRequired = GetPasswordSetupRequiredFromUserMetadata(auth0User.UserMetadata),
                Phone = GetPhoneFromUserMetadata(auth0User.UserMetadata),
                PictureFromConnection = auth0User.Picture,
                PictureFromUser = GetPictureFromUserMetadata(auth0User.UserMetadata),
                PinHash = GetPinHashFromUserMetadata(auth0User.UserMetadata),
                Roles = GetRolesFromAppMetaData(auth0User.AppMetadata),
                Identities = GetIdentities(auth0User.Identities)
    };

            if (string.IsNullOrEmpty(userAggregateModel.Email))
                userAggregateModel.Email = null;

            if (string.IsNullOrEmpty(userAggregateModel.Name))
                userAggregateModel.Name = null;

            if (string.IsNullOrEmpty(userAggregateModel.GivenName))
                userAggregateModel.GivenName = null;

            if (string.IsNullOrEmpty(userAggregateModel.FamilyName))
                userAggregateModel.FamilyName = null;

            if (string.IsNullOrEmpty(userAggregateModel.Phone))
                userAggregateModel.Phone = null;

            if (string.IsNullOrEmpty(userAggregateModel.Locale))
                userAggregateModel.Locale = null;

            if (string.IsNullOrEmpty(userAggregateModel.PictureFromConnection))
                userAggregateModel.PictureFromConnection = null;

            if (string.IsNullOrEmpty(userAggregateModel.PictureFromUser))
                userAggregateModel.PictureFromUser = null;

            if (string.IsNullOrEmpty(userAggregateModel.PinHash))
                userAggregateModel.PinHash = null;

            var userAggregateWithMetadataModel = new UserAggregateWithMetadataModel
            {
                Aggregate = userAggregateModel,
                ETag = Auth0ETagGenerator.GetETag(auth0User.UpdatedAt ?? DateTime.MinValue),
                AggregateInfo =
                    new AggregateInfoModel { CreatedDate = auth0User.CreatedAt ?? DateTime.MinValue, LastModifiedDate = auth0User.UpdatedAt ?? DateTime.MinValue }
            };

            var json = JsonConvert.SerializeObject(userAggregateWithMetadataModel);
            var userAggregateWithMetadata = JsonConvert.DeserializeObject<AggregateWithMetadata<UserAggregate>>(json,
                jsonSerializerSettings);
            return userAggregateWithMetadata;
        }

        private IEnumerable<UserIdentityModel> GetIdentities(Identity[] identities)
        {
            if (identities == null || identities.Length == 0)
                return null;

            var newIdentities = new List<UserIdentityModel>();

            foreach (var identity in identities)
            {
                newIdentities.Add(Map(identity));
            }

            return newIdentities;
        }

        private UserIdentityModel Map(Identity identity)
        {
            if (identity == null)
                return null;

            return new UserIdentityModel()
            {
                AccessToken = identity.AccessToken,
                AccessTokenSecret = identity.AccessTokenSecret,
                Connection = identity.Connection,
                ExpiresIn = identity.ExpiresIn,
                IsSocial = identity.IsSocial,
                ProfileData = identity.ProfileData,
                Provider = identity.Provider,
                RefreshToken = identity.RefreshToken,
                UserId = identity.UserId
            };
        }

        internal UserModel MapToModel(global::Auth0.ManagementApi.Models.User auth0User)
        {
            var model = new UserModel();
            model.ETag = Auth0ETagGenerator.GetETag(auth0User.UpdatedAt ?? DateTime.MinValue);
            model.AggregateInfo = new AggregateInfoModel
            {
                CreatedDate = auth0User.CreatedAt ?? DateTime.MinValue,
                LastModifiedDate = auth0User.UpdatedAt ?? DateTime.MinValue
            };

            model.Blocked = auth0User.Blocked ?? false;
            model.Email = auth0User.Email;
            model.Name = GetNameFromUserMetadata(auth0User.UserMetadata);
            model.GivenName = GetGivenNameFromUserMetadata(auth0User.UserMetadata);
            model.FamilyName = GetFamilyNameFromUserMetadata(auth0User.UserMetadata);
            model.Phone = GetPhoneFromUserMetadata(auth0User.UserMetadata);
            model.EmailVerified = auth0User.EmailVerified;
            model.Id = auth0User.UserId;
            model.LastIpAddress = auth0User.LastIpAddress;
            model.LastLogin = auth0User.LastLogin??DateTime.MinValue;
            model.Locale = auth0User.Locale;
            model.LoginsCount = auth0User.LoginsCount == null ? 0 : int.Parse(auth0User.LoginsCount);
            model.PictureFromConnection = auth0User.Picture;
            model.PictureFromUser = GetPictureFromUserMetadata(auth0User.UserMetadata);
            model.PinHash = GetPinHashFromUserMetadata(auth0User.UserMetadata);
            model.Roles = GetRolesFromAppMetaData(auth0User.AppMetadata);
            model.PasswordSetupRequired = GetPasswordSetupRequiredFromUserMetadata(auth0User.UserMetadata);
            model.Identities = GetIdentities(auth0User.Identities);


            if (string.IsNullOrEmpty(model.Email))
                model.Email = null;

            if (string.IsNullOrEmpty(model.Name))
                model.Name = null;

            if (string.IsNullOrEmpty(model.GivenName))
                model.GivenName = null;

            if (string.IsNullOrEmpty(model.FamilyName))
                model.FamilyName = null;

            if (string.IsNullOrEmpty(model.Phone))
                model.Phone = null;

            if (string.IsNullOrEmpty(model.Locale))
                model.Locale = null;

            if (string.IsNullOrEmpty(model.PictureFromConnection))
                model.PictureFromConnection = null;

            if (string.IsNullOrEmpty(model.PictureFromUser))
                model.PictureFromUser = null;

            if (string.IsNullOrEmpty(model.PinHash))
                model.PinHash = null;

            return model;
        }

        internal IEnumerable<string> GetRolesFromAppMetaData(dynamic appMetadata)
        {
            return GetStringArrayPropertyFromMetadata(appMetadata, "roles");
        }

        internal string GetPictureFromUserMetadata(dynamic userMetadata)
        {
            return GetStringPropertyFromMetadata(userMetadata, "picture");
        }

        internal string GetPinHashFromUserMetadata(dynamic userMetadata)
        {
            return GetStringPropertyFromMetadata(userMetadata, "pin_hash");
        }

        

        internal bool GetPasswordSetupRequiredFromUserMetadata(dynamic userMetadata)
        {
            var value = GetStringPropertyFromMetadata(userMetadata, "password_setup_required");

            if (string.IsNullOrEmpty(value))
                return false;

            return bool.Parse(value);
        }



        internal string GetNameFromUserMetadata(dynamic userMetadata)
        {
            return GetStringPropertyFromMetadata(userMetadata, "name");
        }

        internal string GetGivenNameFromUserMetadata(dynamic userMetadata)
        {
            return GetStringPropertyFromMetadata(userMetadata, "given_name");
        }

        internal string GetPhoneFromUserMetadata(dynamic userMetadata)
        {
            return GetStringPropertyFromMetadata(userMetadata, "phone");
        }

        internal string GetFamilyNameFromUserMetadata(dynamic userMetadata)
        {
            return GetStringPropertyFromMetadata(userMetadata, "family_name");
        }

        internal string GetStringPropertyFromMetadata(dynamic metadata, string propertyName)
        {
            if (metadata == null)
                return null;

            var metadataJObject = (JObject)metadata;

            var hasProperty = metadataJObject.Properties().Any(p => p.Name == propertyName);

            if (hasProperty == false)
                return null;

            var value = metadataJObject.GetValue(propertyName);

            if (value == null)
                return null;

            if (value is JArray)
            {

                var valueJArray = (JArray)value;

                var stringArray = valueJArray.Select(e => e.Value<string>()).ToList();

                if (stringArray == null || !stringArray.Any())
                    return null;

                return string.Join(",", stringArray);
            }
            else if (value is JValue)
            {
                var valueJValue = (JValue)value;
                return valueJValue.Value<string>();
            }

            return null;
        }

        internal IEnumerable<string> GetStringArrayPropertyFromMetadata(dynamic metadata, string propertyName)
        {
            if (metadata == null)
                return null;

            var metadataJObject = (JObject)metadata;

            var hasProperty = metadataJObject.Properties().Any(p => p.Name == propertyName);

            if (hasProperty == false)
                return null;

            var value = metadataJObject.GetValue(propertyName);

            if (value == null)
                return null;

            if (value is JArray)
            {

                var valueJArray = (JArray)value;

                var stringArray = valueJArray.Select(e => e.Value<string>()).ToList();

                if (stringArray == null || !stringArray.Any())
                    return null;

                return stringArray;
            }

            return null;
        }
    }
}