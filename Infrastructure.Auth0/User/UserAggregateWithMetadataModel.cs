﻿using ZettaVision.CloudCore.Domain.Models;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.User
{
    internal class UserAggregateWithMetadataModel
    {
        public string ETag { get; set; }
        public AggregateInfoModel AggregateInfo { get; set; }
        public UserAggregateModel Aggregate { get; set; }
    }
}