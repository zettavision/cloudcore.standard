﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Auth0.ManagementApi.Paging;
using Auth0.Core.Exceptions;
using Auth0.ManagementApi.Models;
using ZettaVision.CloudCore.Domain;
using ZettaVision.CloudCore.Domain.Exceptions;
using ZettaVision.CloudCore.Domain.User.Infrastructure;
using ZettaVision.CloudCore.Domain.User.Models;
using ZettaVision.CloudCore.Domain.User.Queries;
using ZettaVision.CloudCore.Infrastructure.Auth0.Common;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.User
{
    public class UserReader : IUserReader
    {
        private readonly ManagementApiClientFactory managementApiClientFactory;

        private readonly UserMapper userMapper;

        public UserReader(ManagementApiClientFactory managementApiClientFactory)
        {
            this.managementApiClientFactory = managementApiClientFactory;
            userMapper = new UserMapper();
        }



        public async Task<UserModel> HandleAsync(UserGetByIdQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var managementApiClient =
                await managementApiClientFactory.GetManagementApiClientAsync().ConfigureAwait(false);

            global::Auth0.ManagementApi.Models.User auth0User;
            try
            {
                auth0User = await managementApiClient.Users.GetAsync(query.Id).ConfigureAwait(false);
            }
            catch (ErrorApiException errorApiException)
            {
                errorApiException.AddToData("ZettaVision.User.Id", query.Id);

                if (errorApiException.StatusCode == HttpStatusCode.BadRequest
                    && errorApiException.ApiError.ErrorCode == "invalid_uri")
                {
                    throw new ObjectNotFoundException(errorApiException.Message, errorApiException);
                }



                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    


                    throw exception;
                }

                throw;
            }

            cancellationToken.ThrowIfCancellationRequested();

            return userMapper.MapToModel(auth0User);


        }

        public async Task<PagedResult<UserModel>> HandleAsync(UserGetAllQuery query, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();

            var managementApiClient =
                await managementApiClientFactory.GetManagementApiClientAsync().ConfigureAwait(false);

            var perPage = 100;
            if (query.PageParameters?.MaxItemCount != null)
            {
                if (query.PageParameters.MaxItemCount.Value < 100)
                    perPage = query.PageParameters.MaxItemCount.Value;
            }

            int? page = null;
            if (query.PageParameters?.ContinuationToken != null)
            {
                page = int.Parse(query.PageParameters?.ContinuationToken);
            }

            string luceneQueryString = null;

            if (!string.IsNullOrEmpty(query.Email))
                luceneQueryString = $"email:\"{query.Email}\"";

            if (query.Ids != null && query.Ids.Any())
            {
                if(query.Ids.Count() > 50)
                   throw new InvalidOperationException("Cannot query more than 50 Ids");
                   
                if (luceneQueryString != null)
                    luceneQueryString = luceneQueryString + " AND ";
                else
                    luceneQueryString = string.Empty;

                var quotedStrings = query.Ids.Select(i => $"\"{i}\"").ToList();
                var quotedStringsWithOrs = string.Join(" OR ", quotedStrings);


                luceneQueryString = luceneQueryString + $" user_id:({quotedStringsWithOrs})";

            }

            if (query.LastModifiedDateOnOrAfter != null)
            {
                if (luceneQueryString != null)
                    luceneQueryString = luceneQueryString + " AND ";
                else
                    luceneQueryString = string.Empty;

                var dateString = query.LastModifiedDateOnOrAfter.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                luceneQueryString = luceneQueryString + $" updated_at:[{dateString} TO *]";
                
            }

            string luceneSortString = null;
            if (query.SortAscending != null && query.Sort == null)
                query.Sort = UserSortPropertyEnum.CreatedDate;

 

            if (query.Sort != null)
            {
                if (query.SortAscending == null)
                    query.SortAscending = true;

                var fieldName = GetFieldName(query.Sort.Value);
                var ascendString = query.SortAscending.Value ? "1" : "-1";
                luceneSortString = $"{fieldName}:{ascendString}";
            }


            IPagedList<global::Auth0.ManagementApi.Models.User> result;

            try
            {

                result = await
                    managementApiClient.Users.GetAllAsync(new GetUsersRequest() {Query = luceneQueryString, Sort = luceneSortString, SearchEngine = "v3"}, new PaginationInfo(page ?? 0, perPage, true))
                        .ConfigureAwait(false);


            }
            catch (ErrorApiException errorApiException)
            {
                var exception = ApiExceptionMapper.Map(errorApiException);
                if (exception != null)
                {
                    throw exception;
                }

                throw;
            }



            var p = result.Paging;

            var items = result?.Select(u => userMapper.MapToModel(u)).ToList();

            //figure out continuationToken
            string continuationToken = null;
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (items != null && items.Any() && (result.Paging.Total > result.Paging.Length + result.Paging.Start))
            {
                // ReSharper disable once MergeConditionalExpression
                continuationToken = page == null ? "1" : (page.Value + 1).ToString();
            }

            var pagedResult = new PagedResult<UserModel> {Items = items, ContinuationToken = continuationToken};

            cancellationToken.ThrowIfCancellationRequested();

            return pagedResult;
        }

        private string GetFieldName(UserSortPropertyEnum sort)
        {
            switch (sort)
            {
                case UserSortPropertyEnum.CreatedDate:
                    return "created_at";
                case UserSortPropertyEnum.LastModifiedDate:
                    return "updated_at";
                default:
                    throw new ArgumentOutOfRangeException(nameof(sort), sort, null);
            }
        }
    }
}