﻿using System;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public static class Auth0ETagGenerator
    {
        public static string GetETag(DateTime lastModifiedDate)
        {
            var isoDateFormat = lastModifiedDate.ToUniversalTime().ToString("O");
            var eTag = $"\"{isoDateFormat}\"";
            return eTag;
        }
    }
}