﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using LazyCache;
using Newtonsoft.Json;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public class UserTokenFactory : IUserTokenFactory
    {
        private readonly AuthenticationResponseModel staticToken;
        private readonly CachingService cachingService;
        private readonly string domain;
        private readonly string clientId;
        private readonly string clientSecret;
        private readonly string userName;
        private readonly string password;
        private readonly AuthenticationApiClient authenticationApiClient;

        public UserTokenFactory(CachingService cachingService, string domain, string clientId, string clientSecret,
            string userName, string password)
        {
            
            this.cachingService = cachingService;
            this.domain = domain;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.userName = userName;
            this.password = password;
            this.staticToken = null;
            authenticationApiClient = new AuthenticationApiClient(domain);
        }

        public UserTokenFactory(AuthenticationResponseModel staticToken)
        {
            this.staticToken = staticToken ?? throw new ArgumentNullException(nameof(staticToken));
        }

        public async Task<AuthenticationResponseModel> GetTokenAsync()
        {
            if (staticToken != null)
                return staticToken;

            return await cachingService.GetOrAddAsync(
                $"ZettaVision.CloudCore.Infrastructure.Auth0.Common.UserTokenFactory.GetTokenAsync?domain={domain}&clientid={clientId}&username={userName}&password={password}",
                GetTokenAsyncWithoutCache, DateTime.UtcNow.AddDays(1)).ConfigureAwait(false);
        }

        private async Task<AuthenticationResponseModel> GetTokenAsyncWithoutCache()
        {

            var authenticationResponse = await authenticationApiClient.GetTokenAsync(new ResourceOwnerTokenRequest
            {
                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = "openid profile email",
                Username = userName,
                Password = password,
                Realm = "Username-Password-Authentication"

            }).ConfigureAwait(false);



            if(authenticationResponse == null)
                throw new InvalidOperationException("authenticationResponse cannot be null");

            if (authenticationResponse.TokenType == null)
                throw new InvalidOperationException("authenticationResponse.TokenType cannot be null");

            if (authenticationResponse.AccessToken == null)
                throw new InvalidOperationException("authenticationResponse.AccessToken cannot be null");

            if (authenticationResponse.IdToken == null)
                throw new InvalidOperationException("authenticationResponse.IdToken cannot be null");


            return new AuthenticationResponseModel()
            {
                AccessToken = authenticationResponse.AccessToken,
                IdToken = authenticationResponse.IdToken,
                RefreshToken =  authenticationResponse.RefreshToken,
                TokenType = authenticationResponse.TokenType
            };
        }
    }



}
