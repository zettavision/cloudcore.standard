﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Auth0.ManagementApi;
using LazyCache;
using Newtonsoft.Json;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public class ManagementApiClientFactory
    {
        private readonly ApiTokenFactory apiTokenFactory;
        private readonly string domain;


        public ManagementApiClientFactory(ApiTokenFactory apiTokenFactory, string domain)
        {
            this.apiTokenFactory = apiTokenFactory;
            this.domain = domain;
        }

        public async Task<ManagementApiClient> GetManagementApiClientAsync()
        {
            var apiToken = await apiTokenFactory.GetTokenAsync().ConfigureAwait(false);
            var managementApiClient = new ManagementApiClient(apiToken.AccessToken, domain);
            return managementApiClient;
        }

  
    }
}
