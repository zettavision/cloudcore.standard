﻿namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public class ApiTokenModel
    {
        public string AccessToken { get; set; }
        public long ExpiresIn { get; set; }
        public string Scope { get; set; }
        public string TokenType { get; set; }
    }
}