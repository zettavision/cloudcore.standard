﻿using System.Threading.Tasks;
using ZettaVision.CloudCore.Domain.User.Models;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public interface IUserTokenFactory
    {
        Task<AuthenticationResponseModel> GetTokenAsync();
    }
}