﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LazyCache;
using Newtonsoft.Json;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public class ApiTokenFactory
    {
        private readonly CachingService cachingService;
        private readonly string domain;
        private readonly string clientId;
        private readonly string clientSecret;

        public ApiTokenFactory(CachingService cachingService, string domain, string clientId, string clientSecret)
        {
            this.cachingService = cachingService;
            this.domain = domain;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
        }

        public async Task<ApiTokenModel> GetTokenAsync()
        {
            return await cachingService.GetOrAddAsync(
                $"ZettaVision.CloudCore.Infrastructure.Auth0.Common.GetTokenAsync?domain={domain}&clientid={clientId}&clientsecret={clientSecret}",
                GetTokenAsyncWithoutCache, DateTime.UtcNow.AddDays(1)).ConfigureAwait(false);
        }

        private async Task<ApiTokenModel> GetTokenAsyncWithoutCache()
        {

            var jsonBody = "{\"client_id\":\"" + clientId + "\",\"client_secret\":\"" + clientSecret +
   "\",\"audience\":\"https://" + domain + "/api/v2/\",\"grant_type\":\"client_credentials\"}";

            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri($"https://{domain}/");
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "oauth/token");
            request.Content = new StringContent(jsonBody,
                                                Encoding.UTF8,
                                                "application/json");

            var response = await httpClient.SendAsync(request).ConfigureAwait(false);

            response.EnsureSuccessStatusCode();
            var responseJson = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<ApiTokenJsoModel>(responseJson);

            var apiToken = new ApiTokenModel
            {
                AccessToken = model.access_token,
                ExpiresIn = model.expires_in,
                Scope = model.scope,
                TokenType = model.token_type
            };

            return apiToken;
        }
    }
}
