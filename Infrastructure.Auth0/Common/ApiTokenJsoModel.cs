﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public class ApiTokenJsoModel
    {
        public string access_token { get; set; }
        public long expires_in { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
    }
}



