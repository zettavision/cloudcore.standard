﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Auth0.Core.Exceptions;
using ZettaVision.CloudCore.Domain.Exceptions;

namespace ZettaVision.CloudCore.Infrastructure.Auth0.Common
{
    public static class ApiExceptionMapper
    {
        public static Exception Map(ErrorApiException errorApiException)
        {
            if (errorApiException.StatusCode == HttpStatusCode.NotFound)
            {
                return new ObjectNotFoundException(errorApiException.Message, errorApiException);
            }
            else if (errorApiException.StatusCode == HttpStatusCode.Conflict)
            {
                return new ObjectAlreadyExistsException(errorApiException.Message, errorApiException);
            }
            else if (errorApiException.StatusCode == (HttpStatusCode)422)
            {
                return new InvalidOperationException(errorApiException.Message, errorApiException);
            }
            else if (errorApiException.StatusCode == HttpStatusCode.BadRequest)
            {
                if(errorApiException.ApiError.ErrorCode == "auth0_idp_error" && errorApiException.ApiError.Message == "The specified new email already exists")
                    return new ObjectAlreadyExistsException(errorApiException.Message, errorApiException);
                else
                {
                    return new ArgumentException(errorApiException.Message, errorApiException);
                }
            }
            else if (errorApiException.StatusCode == HttpStatusCode.Forbidden)
            {
                return new UnauthorizedAccessException(errorApiException.Message, errorApiException);
            }

            return null;
        }
    }
}
